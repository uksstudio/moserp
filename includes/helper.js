
/*
Author	: Imam Mahmudi
Email	: imam.mhm21@gmail.com
date	: 02 April 2018
this files for all function helper, likes number_format, ettc
*/


/*
number_format is as like number_format in php
*/
function number_format(number, decimals, dec_point, thousands_point) {

    if (number == null || !isFinite(number)) {
        throw new TypeError("number is not valid");
    }

    if (isNaN(number) || number=='') number = 0;

    if (!decimals) {
        var len = number.toString().split('.').length;
        decimals = len > 1 ? len : 0;
    }

    if (!dec_point) {
        dec_point = '.';
    }

    if (!thousands_point) {
        thousands_point = ',';
    }

    var negative = number < 0;

    if(negative) number *= -1;

    number = parseFloat(number).toFixed(decimals);

    number = number.replace(".", dec_point);

    var splitNum = number.split(dec_point);
    splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
    number = splitNum.join(dec_point);

    return (negative?'-':'') + number;
}


/*
parse2float is parse to float and remove comma's
*/

function parse2float(number)
{
    string = number.replace(/[^\d\.\-]/g, ""); 
    var number = parseFloat(string);
    return number;
}

/*
roundUp
this for round up to thousand ex
1,252,400 return 1,300,000
*/
function roundUp(num, precision) {
  precision = Math.pow(10, precision)
  return Math.ceil(num * precision) / precision
}


/*
load Coa Code
default load coa by ID
*/
function getCmbCoa(id)
{

    var getCoa = $(id).combogrid({
        panelWidth:300,
        url: "Finance/getCoa",
        idField:'subcode',
        textField:'subcode',
        mode:'remote',
        fitColumns:true,
        groupField:'code',
        method: 'get',
        columns:[[
            {field:'subcode',title:'Account',width:100},
            {field:'name',title:'Name',width:250}
        ]],
        onSelect: function(index,record)
        {
            
        }
    });
    return getCoa;

}