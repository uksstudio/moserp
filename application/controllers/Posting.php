<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posting extends CI_Controller {

	var $_prefix_cost = 'CO';

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Public_model");
	}

	public function postingbyco()
	{
		$this->load->view("posting/postingbyco");
	}

	public function getLocation()
	{
		echo $this->Public_model->getOutletList();
	}


	
	public function getTerminal()
	{
		$locationid = $this->uri->segment(3);
		echo $this->Public_model->getTerminal($locationid);	
	}

	public function getPostingJobs()
	{
		$this->db->where('posting_status !=', 'COMPLETE');
		$rs = $this->db->get('post_invcost_header');
		$data = array();
		foreach ($rs->result() as $row) {
			array_push($data, $row);
		}
		$json['rows'] = $data;
		$json['total'] = count($data);
		echo json_encode($json);
	}

	public function getJobsComplete()
	{
		$this->db->where('posting_status', 'COMPLETE');
		$this->db->order_by('postingid', 'DESC');
		$rs = $this->db->get('post_invcost_header');
		$data = array();
		foreach ($rs->result() as $row) {
			//array_push($data, $row);
			$data[] = array(
				'postingid'=>$row->postingid,
				'issuedate'=>$row->issuedate,
				'posting_number'=>$row->posting_number,
				'posting_status'=>$row->posting_status,
				'remark'=>$row->remark,
				'costing_number'=>$row->costing_number,
				'timeprocess'=>number_format($row->timeprocess, 3).' Sec'
			);
		}
		$json['rows'] = $data;
		$json['total'] = count($data);
		echo json_encode($json);
	}

	public function printPosting()
	{
		$data['postingid'] = $this->uri->segment(3);//$this->input->get('postingid');
		$this->load->view("posting/postingprinting", $data);
	}

	/*
	costingstatus
	1. COSTING  -> Pertama kali masuk siap untuk di tarus di JOBS costing porses
	2. JOBS     -> Sudah dalam antrian proses jobs
	*/
	public function getPostingnumber()
	{
		$data = array();
		$this->db->where("costingstatus", "COSTING"); // 1. COSTING, 2. JOBS
		$costingRow = $this->db->get('costing_header');
		foreach($costingRow->result() as $field)
		{
			array_push($data, $field);
		}
		$json['rows'] = $data;
		echo json_encode($json);
	}

	public function doPostingByCo()
	{
		$costingnumber = $this->input->post('costingnumber');
		$locationcode = $this->input->post('locationcode');
		$receiveReport  = $this->input->post('receiveReport');
		$costingRow = $this->db->get_where('costing_header', array('costingnumber'=>$costingnumber))->row();

		if (!isset($costingRow)){
			die(json_encode(array('status'=>0, 'errorMsg'=>'COSTING NUMBER NOT FOUND')));
		}

		if($costingnumber != '' && $locationcode != '')
		{
			$locationid = $this->db->get_where('locationinfo', array('locationcode'=>$locationcode))->row()->locationid;
			if($locationid != null && $locationid > 0)
			{
				$data = array(
					'issuedate'=>date('Y-m-d H:i:s'),
					'issued_by'=>$this->session->userdata('username'),
					'posting_status'=>'QUEUE',
					'costing_number'=>$costingRow->costingnumber,
					'locationcode'=>$locationcode,
					'locationid'=>$locationid,
					'receivereportitem'=>$receiveReport
				);

				$check_costing = $this->db->get_where('post_invcost_header', array('costing_number'=>$costingnumber))->num_rows();
				if($check_costing > 0)
				{
					echo json_encode(array('status'=>0, 'errorMsg'=>'CO NUMBER already POST.'));		
				}
				else
				{
					$this->db->query("update costing_header set costingstatus='JOBS' where costingnumber='".$costingnumber."'");
					$this->db->insert('post_invcost_header', $data);
					echo json_encode(array('status'=>1, 'errorMsg'=>'POSTING JOBS Successfully'));		
				}
				
			}
			else
			{
				echo json_encode(array('status'=>0, 'errorMsg'=>'Location Code not found'));
			}
		}
		else
		{
			echo json_encode(array('status'=>0, 'errorMsg'=>'Costing Number and Location Code can not be null'));
		}
	}

}