<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

	}
	
	public function index()
	{
		$this->load->view('login/login_view');
	}

	public function oAuth()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$result = $this->db->get_where('mos_user_access', array('username'=>$username));
		$num_rows = $result->num_rows();
		if($num_rows == 1)
		{
			$row = $result->row();
			$this->updateUserBlock($username, 99); // just update timestamp
			if($row->userblock)
			{
				die(json_encode(array("status"=>0, "msg"=>"Your Account has been block, cause wrong password more than 3x.","csrf"=>$this->security->get_csrf_hash())));
			}

			if($password == $row->password)
			{
				$newdata = array(
					"compid"=>$row->companyid,
					"divid"=>$row->comp_divisionid,
					"userid"=>$row->id,
					"username"=>$row->username,
					"name"=>$row->name,
					"is_logedin"=>TRUE);
				$this->session->set_userdata($newdata);
				echo json_encode(array("status"=>1, "msg"=>"Login success","csrf"=>$this->security->get_csrf_hash()));
			}
			else
			{
				$this->updateUserBlock($username, $row->wronglogin_count);
				echo json_encode(array("status"=>0, "msg"=>"WRONG PASSWORD!!","csrf"=>$this->security->get_csrf_hash()));
				
			}
		}
		else
		{
			echo json_encode(array("status"=>0, "msg"=>"USERNAME or PASSWORD not found","csrf"=>$this->security->get_csrf_hash()));
		}
	}

	private function updateUserBlock($username, $currcount, $reset=false)
	{
		$this->db->set('last_login', 'NOW()', FALSE);

		if($currcount != 99)
		{	
			if($reset)
				$this->db->set("wronglogin_count",0);
			else
				$this->db->set("wronglogin_count",'wronglogin_count+1', FALSE);

			if($currcount > 3)
				$this->db->set("userblock",1);
		}

		$this->db->where('username', $username);
		$this->db->update('mos_user_access');
	}

	public function logout()
	{
		$user_data = $this->session->all_userdata();
		foreach ($user_data as $key => $value) {
			if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this->session->unset_userdata($key);
			}
		}
		redirect("login");
	}
}