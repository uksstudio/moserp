<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Public_model");
	}

	public function createNewUser()
	{
		$this->load->view("admin/createNewUser");
	}


	public function getDivisionByComp()
	{
		$compid = $this->session->userdata("compid");
		echo $this->Public_model->getCompanyDivision($compid);
	}

	public function addMenuKas()
	{
		$this->load->view("admin/addMenuKas");	
	}

	public function setEdcMachine()
	{
		$this->load->view("admin/edcMachine");
	}

	public function pmLogicalVoucher()
	{
		$type = $this->uri->segment(3);
		if($type == "show")
		{
			$rs = $this->db->get("acc_logical_prv");
			$data = array();
			foreach($rs->result() as $row)
			{
				array_push($data, $row);
			}

			$json['rows'] = $data;
			print json_encode($json);
		}
		else if($type == "add")
		{
			echo json_encode(array("status"=>1, "errorMsg"=>"", 'csrf_name'=>$this->security->get_csrf_hash()));
		}
		else if($type == "update")
		{
			echo json_encode(array("status"=>1, "errorMsg"=>"", 'csrf_name'=>$this->security->get_csrf_hash()));
		}
		else if($type == "delete")
		{
			echo json_encode(array("status"=>1, "errorMsg"=>"", 'csrf_name'=>$this->security->get_csrf_hash()));
		}
		else
			$this->load->view("admin/pmLogicalVoucher");
	}

	public function getCompany()
	{
		echo $this->Public_model->getCompany();	
	}

	public function getUserAkses()
	{
		$rs = $this->db->get('mos_user_access');
		$data = array();
		foreach ($rs->result() as $row) {
			array_push($data, $row);
		}
		$json['rows'] = $data;
		echo json_encode($json);
	}

	public function getKasMenu()
	{
		$this->db->order_by("parent", "ASC");
		$rs = $this->db->get_where('menu_modul', array("type"=>"ACC"));
		$data = array();
		foreach ($rs->result() as $row) {
			$data[] = array("id"=>$row->id, "nama"=>$row->nama, "parent"=>$row->parent, "urutan"=>$row->urutan, "type"=>$row->type, "akun"=>$row->coa);
		}
		$json['rows'] = $data;
		echo json_encode($json);	
	}


	public function getEdcMachine()
	{
		$this->db->order_by("id", "ASC");
		$this->db->select("swp.coacode, swp.commision, swp.edcname, swp.id, aco.name as namaakun");
		$this->db->join("acc_coa aco","aco.subcode=swp.coacode","left");
		$rs = $this->db->get('swapbank swp');
		$data = array();
		foreach ($rs->result() as $row) {
			array_push($data, $row);
		}
		$json['rows'] = $data;
		echo json_encode($json);
	}

	public function addEdc()
	{
		$type = $this->uri->segment(3);
		$edcname = $this->input->post("edcname");
		$coacode = $this->input->post("coacode");
		$commision = $this->input->post("commision");
		if($type == "update")
		{
			$id = $this->uri->segment(4);
			$this->db->set("edcname", $edcname);
			$this->db->set("coacode", $coacode);
			$this->db->set("commision", $commision);
			$this->db->where("id", $id);
			$this->db->update("swapbank");
		}
		elseif ($type == "add") {
			$this->db->set("edcname", $edcname);
			$this->db->set("coacode", $coacode);
			$this->db->set("commision", $commision);
			$this->db->insert("swapbank");	
		}
		elseif ($type == "delete") {
			$this->db->where("id", $id);
			$this->db->delete("swapbank");	
		}

		echo json_encode(array("status"=>1, "errorMsg"=>"", 'csrf_name'=>$this->security->get_csrf_hash()));

	}

	public function getParentId()
	{
		$code = $this->input->get("code");

		$this->db->order_by("parent", "ASC");
		$rs = $this->db->get_where('menu_modul', array("type"=>$code));
		$data = array();
		foreach ($rs->result() as $row) {
			array_push($data, $row);
		}
		
		echo json_encode($data);
	}

	public function addMenuKasModul()
	{
		$group = $this->input->post('group');
		$parent = $this->input->post('parent');
		$coa = $this->input->post('coa');
		$menu = $this->input->post('menu');
		
		$this->db->set("nama", $menu);
		$this->db->set("parent", $parent);
		$this->db->set("urutan", 0);
		$this->db->set("type", "ACC");
		$this->db->set("coa", $coa);
		$this->db->set("url", "Finance/".$group."/".$coa);
		$this->db->insert("menu_modul");

		echo json_encode(array('status'=>1, 'msg'=>'Successfully','csrf'=>$this->security->get_csrf_hash()));
	}

	public function editMenumodul()
	{
		$this->load->view("admin/editMenumodul");	
	}

	public function userRollAccess()
	{
		$this->load->view("admin/userRollAccess");	
	}

	public function saveNewUser()
	{
		$name = $this->input->post('name');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$division = $this->input->post('division');
		$company = $this->input->post('company');

		$data = array('name'=>$name, 'username'=>$username, 'password'=>$password, 'createdate'=>date('Y-m-d'), 'comp_divisionid'=>$division, 'companyid'=>$company, 'userlevel'=>9);
		$ok = $this->db->insert('mos_user_access', $data);
		echo json_encode(array('status'=>$ok, 'msg'=>'Successfully','csrf'=>$this->security->get_csrf_hash()));
	}

	public function updateOwnModul()
	{
		$userid = $this->input->post('userid');
		$sgridmodul = $this->input->post('sgridmodul');

		$emodul = explode(",", $sgridmodul);
		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$ok = $this->db->query('delete from mos_userroll_access where userid='.$userid);
		if($ok)
		{
			$batch = array();
			for ($i=0; $i < count($emodul); $i++) { 
				$batch[] = array('userid'=>$userid, 'menu_modul_id'=>$emodul[$i], 'is_edit'=>'1', 'is_add'=>'1', 'is_del'=>'1', 'is_read'=>'1');
			}
			$this->db->insert_batch('mos_userroll_access',$batch);
		}

		$this->db->trans_complete();

		$status = ($this->db->trans_status() === FALSE) ? 0 : 1;
		echo json_encode(array('status'=>$status, 'msgErr'=>($status) ? 'Successfully' : 'Something Error', 'csrf_name'=>$this->security->get_csrf_hash()));
	}

	public function updateModul()
	{
		$type = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$nama = $this->input->post('nama');
		$url = $this->input->post('url');
		$urutan = $this->input->post('urutan');

		if($type == "update")
		{
			$this->db->set("nama", $nama);
			$this->db->set("urutan", $urutan);
			$this->db->set("url", $url);
			$this->db->where("id", $id);
			$status = $this->db->update("menu_modul");	
		}
		else
		{
			$this->db->set("nama", $nama);
			$this->db->set("urutan", $urutan);
			$this->db->set("url", $url);
			$this->db->set("parent", $id);
			$status = $this->db->insert("menu_modul");
		}

		echo json_encode(array('status'=>($status) ? 1 : 0, 'msgErr'=>($status) ? 'Successfully' : 'Something Error', 'csrf_name'=>$this->security->get_csrf_hash()));
	}


	public function getModul()
	{
		header('Content-Type: application/json');
		$parent = $this->uri->segment(3);
		$userid = $this->uri->segment(4);

		$rs = $this->db->from("menu_modul")->where("parent", $parent)->order_by('id', 'ASC')->get();
		$data = array();
		foreach($rs->result_array() as $row)
		{
			$item = array();
			if($this->has_child($row['id']))
			{
				$item['id'] = $row['id'];
				$item['text'] = $row['nama'];
				$item['urutan'] = $row['urutan'];
				$item['url'] = $row['url'];
				$item['nama'] = $row['nama'];
				if($row['url'] != "") $item['checked'] = $this->checkUserModul($row['id'], $userid);
				$item['children'] = $this->recursiverFormodul($row['id'], $userid);
			}
			else
			{
				$item['id'] = $row['id'];
				$item['text'] = $row['nama'];
				$item['urutan'] = $row['urutan'];
				$item['url'] = $row['url'];
				$item['nama'] = $row['nama'];
				$item['checked'] = $this->checkUserModul($row['id'], $userid);
			}
			$data[] = $item;
		}

		echo json_encode($data);
	}

	public function checkUserModul($modulid, $userid)
	{
		$this->db->where("menu_modul_id", $modulid);
		$this->db->where("userid", $userid);
		$this->db->from('mos_userroll_access');
		$count = $this->db->count_all_results();
		return (($count > 0) || ($count != null)) ? true : false;
	}


	private function recursiverFormodul($id, $userid)
	{
		$rs = $this->db->from("menu_modul")->where("parent", $id)->order_by('urutan', 'ASC')->get();
		$data = array();
		foreach($rs->result_array() as $row)
		{
			$item = array();
			if($this->has_child($row['id']))
			{
				$item['id'] = $row['id'];
				$item['text'] = "[".$row['nama']."][".$row['url']."][".$row['urutan']."]";
				$item['urutan'] = $row['urutan'];
				$item['url'] = $row['url'];
				$item['nama'] = $row['nama'];
				if($row['url'] != "")$item['checked'] = $this->checkUserModul($row['id'], $userid);
				$item['children'] = $this->recursiverFormodul($row['id'], $userid);
			}
			else
			{
				$item['id'] = $row['id'];
				$item['text'] = "[".$row['nama']."][".$row['url']."][".$row['urutan']."]";
				$item['urutan'] = $row['urutan'];
				$item['url'] = $row['url'];
				$item['nama'] = $row['nama'];
				$item['checked'] = $this->checkUserModul($row['id'], $userid);
			}
			$data[] = $item;
		}
		return $data;
	}

	private function recursiver($id, $userid)
	{
		$rs = $this->db->from("menu_modul")->where("parent", $id)->order_by('urutan', 'ASC')->get();
		$data = array();
		foreach($rs->result_array() as $row)
		{
			$item = array();
			if($this->has_child($row['id']))
			{
				$item['id'] = $row['id'];
				$item['text'] = $row['nama'];
				if($row['url'] != "")$item['checked'] = $this->checkUserModul($row['id'], $userid);
				$item['children'] = $this->recursiver($row['id'], $userid);
			}
			else
			{
				$item['id'] = $row['id'];
				$item['text'] = $row['nama'];
				$item['checked'] = $this->checkUserModul($row['id'], $userid);
			}
			$data[] = $item;
		}
		return $data;
	}

	private function has_child($id){
		$this->db->where("parent", $id);
		$this->db->from('menu_modul');
		$count = $this->db->count_all_results();
		return $count > 0 ? true : false;
	}
}