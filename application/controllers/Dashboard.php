<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Finance_model");
	}
	
	public function index()
	{
		$this->load->view('dashboard/dashboard_view');
	}

	public function treeMenu()
	{
		$this->load->view("dashboard/tree_menu");
	}

	public function testreport()
	{
		$this->load->library('PHPJasperXML');
		echo "PHPJasperXML loaded";
	}

	public function cashAdvance()
	{
		$data['canumber'] = $this->Finance_model->getCashAdvanceNumber();
		$this->load->view("dashboard/empcashadvance", $data);
	}

	public function getCashAdvance()
	{
		$novoc = $this->input->get("q");
		
		$kas = $this->db->get_where("acc_cash_advance", array("canumber"=>$novoc, "realisestatus"=>0))->row();
		
		$detail = array();
		if(isset($kas))
		{
			$this->db->select("acc_cash_advance_detail.*");
			$this->db->where("acc_cash_advance_detail.cashadvanceid", $kas->id);
			$rs = $this->db->get("acc_cash_advance_detail");
			$i = 0;
			foreach($rs->result() as $key)
			{
				$detail[] = array("id"=>$key->id, "cashadvanceid"=>$key->cashadvanceid, "amount"=>$key->amount, "description"=>$key->description);
				$i++;
			}

			for($j=0; $j < (10-$i); $j++)
			{
				$detail[] = array("id"=>0, "cashadvanceid"=>0, "amount"=>0, "description"=>"");
			}
		}

		$data['kas'] = $kas;
		$data['detail'] = $detail;
		echo json_encode($data);
	}

	public function saveCashAdvance()
	{

		$crud = $this->uri->segment(3);

		$supplierid = $this->input->post("supplierid");
		$remark = $this->input->post("remark");
		$payto = $this->input->post("payto");
		$noaccount  = $this->input->post("noaccount");
		$novoucher = $this->input->post("novoucher");
		$tanggal = $this->input->post("tanggal");
		$type = $this->input->post("type");
		$srcpvno = $this->input->post("srcpvno");
		$rows = $this->input->post("rows");

		$items = json_decode($rows);

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$in_item = array();
		$balance = 0;

		if($crud == "update")
		{
			$srcpvid = $this->db->query("select id from acc_cash_advance where canumber='".$srcpvno."'")->row()->id;
			$delete = $this->db->query("delete from acc_cash_advance where id=".$srcpvid);
			$deldetail = $this->db->query("delete from acc_cash_advance_detail where cashadvanceid=".$srcpvid);

			$novoucher = $srcpvno;
		}

		for($i=0; $i < count($items); $i++)
		{
			$balance += $items[$i]->amount;
		}

		$this->db->set("issuedate", $tanggal);
		$this->db->set("canumber", $novoucher);
		$this->db->set("remark", $remark);
		$this->db->set("issuename", $payto);
		$this->db->set("totalamount", $balance);
		$this->db->set("realisestatus", 0);
		$this->db->set("employeeid", $this->session->userdata('userid'));
		$this->db->set("companycode", $this->session->userdata('compid'));
		$this->db->insert("acc_cash_advance");
		$id = $this->db->insert_id();


		for($i=0; $i < count($items); $i++)
		{
			
			$item = array();
			$item['cashadvanceid'] = $id;
			$item['description'] = $items[$i]->description;
			$item['amount'] = $items[$i]->amount;
			$in_item[] = $item;
		}
		$this->db->insert_batch('acc_cash_advance_detail',$in_item);
		$this->db->trans_complete();

		

		if ($this->db->trans_status() === FALSE)
		{
				echo json_encode(array('status'=>0,'msg'=>'Failed', 'po_number'=>$novoucher, 'lastid'=>$id));
		}
		else
			echo json_encode(array('status'=>1,'msg'=>'Successfully', 'po_number'=>$novoucher, 'lastid'=>$id));
	}

	public function printCashAdvance()
	{
		$pvnumber = $this->uri->segment(3);
		$this->load->view("printing/print_cashadvance_employee_A4_landscape");
	}
}