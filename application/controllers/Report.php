<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Public_model");
	}

	public function reprintDonum()
	{
		$donum = $this->input->post('donumber');
		$this->load->view("report/reprintdocn");

	}

	public function adjustmentReport()
	{
		$this->load->view("report/adjustmentReport");
	}

	public function readadjustmentreport()
	{
		$this->load->model('Inventory_model');

		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$rows = isset($_GET['rows']) ? intval($_GET['rows']) : 50;
		
		$offset = ($page-1)*$rows;
		$result = array();
		
		$trfdono = $this->input->get('trfdono');

		if(!empty($trfdono))
		{
			$this->db->where('stockadjustmentnumber', $trfdono); // and etc...
		}
	    $this->db->from('stockadjustment');
	    $total_rows = $this->db->count_all_results();

	    $this->db->select("sta.adjustmenttype, sta.remark, sta.stockadjustmentnumber, sta.raisedate, sta.locationcode");
	    //$this->db->join("stockadjustment sta", "sta.stockadjustmentid=staitem.stockadjustment", "LEFT");
	    //$this->db->join("productitem pitem", "pitem.productitemid=staitem.productitem", "LEFT");
	    $this->db->from("stockadjustment sta");
	    if(!empty($trfdono))
		{
			$this->db->where('sta.stockadjustmentnumber', $trfdono); // and etc...
		}
		$this->db->order_by("sta.stockadjustmentid", "DESC");
	    $this->db->limit($rows, $offset);
		$rs = $this->db->get();

		$data = array();
		foreach ($rs->result() as $key) {
			array_push($data, $key);
		}

		$result["rows"] = $data;
		$result["total"] = $total_rows;
		echo json_encode($result);
	}

	public function reprintdncn_A4()
	{
		$this->load->model('Inventory_model');
		$trfno = $this->uri->segment(3);
		$data['header'] = $this->Inventory_model->_readreprint($trfno);
		
		$this->load->view("printing/print_stock_issue_A4", $data);
	}

	public function reprintadjustment_A4()
	{
		$this->load->model('Inventory_model');
		$trfno = $this->uri->segment(3);

		$this->db->select("std.*, mua.username");
		$this->db->join("mos_user_access mua","mua.id=std.raiseby", "LEFT");
		$this->db->from("stockadjustment std");
		$this->db->where("std.stockadjustmentnumber", $trfno);
		$rs = $this->db->get();
		$header = array();
		foreach ($rs->result() as $key) {
			array_push($header, $key);
		}

		$data['header']['rows'] = $header;
		
		$this->load->view("printing/print_stock_adjustment_A4", $data);
	}

	public function readreprintdncn()
	{
		$this->load->model('Inventory_model');

		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$rows = isset($_GET['rows']) ? intval($_GET['rows']) : 50;
		
		$offset = ($page-1)*$rows;
		$result = array();
		
		$trfdono = $this->input->get('trfdono');

		if(!empty($trfdono))
		{
			$this->db->where('transfernotenumber', $trfdono); // and etc...
			$this->db->or_where('stockreceiptnumber', $trfdono); // and etc...
		}
	    $this->db->from('transfernote');
	    $total_rows = $this->db->count_all_results();

	    $this->db->select("trnote.*, (select locationcode from locationinfo where locationid=trnote.fromlocation) as flocation, (select locationcode from locationinfo where locationid=trnote.tolocation) as tlocation");
	    $this->db->from("transfernote trnote");
	    if(!empty($trfdono))
		{
			$this->db->where('trnote.transfernotenumber', $trfdono); // and etc...
			$this->db->or_where('trnote.stockreceiptnumber', $trfdono); // and etc...
		}
		$this->db->order_by("trnote.transfernoteid", "DESC");
	    $this->db->limit($rows, $offset);
		$rs = $this->db->get();

		$data = array();
		foreach ($rs->result() as $key) {
			array_push($data, $key);
		}

		$result["rows"] = $data;
		$result["total"] = $total_rows;
		echo json_encode($result);
	}

	public function fprintKhr()
	{
		$donum = $this->input->post('donumber');
		$this->load->view("report/printKhr");
	}

	public function getPostingList()
	{
		$costingnumber = ($this->uri->segment(3) === FALSE) ? '' : $this->uri->segment(3);
		if($costingnumber != "")$this->db->where('costingnumber', $costingnumber);
		
		$this->db->where('isposting', true);
		$this->db->limit(100);
		$rs = $this->db->order_by('id', 'DESC')->get('costing_header');
		
		$data = array();
		foreach($rs->result() as $field)
		{
			$det = $this->db->get_where('costing_detail', array('costing_header_id'=>$field->id));
			$item = array();
			$biaya = array();
			$item['costingnumber'] = $field->costingnumber;
			$item['costingdate'] = $field->costingdate;
			$item['costingstatus'] = $field->costingstatus;
			$item['currency'] = $field->currency;
			$item['exchangerate'] = $field->exchangerate;
			$item['isposting'] = $field->isposting;

			$item['pibnumber'] = $field->pibnumber;
			$item['awbnumber'] = $field->awbnumber;
			$item['invoicenumber'] = $field->invoicenumber;
			$item['spcpdate'] = $field->spcpdate;
			$item['prepareduser'] = $field->prepareduser;
		

			$total = 0;
			foreach($det->result() as $row)
			{
				$group = str_replace(' ','',strtolower($row->groupname));
				@$item[$group] += $row->amount;
				$total += $row->amount;
			}
			$item['total'] = number_format($total,2,'.','.');
			$data[] = $item;
		}

		$json['rows'] = $data;
		$json['total'] = count($data);
		print json_encode($json);
	}

	
	public function creditCardKnockOff()
	{
		$this->load->view("report/ccknockoff");
	}

	public function showCcKnockOff()
	{
		$storecode = $this->input->post("storecode");
		$locationid = $this->input->post("locationid");
		$this->db->select("a.transactiontype, DATE(a.invoicedate) AS invoicedate, a.invoiceid, b.amount,  a.invoicenumber, b.creditcardtype, b.paymentid, a.salesmancode, b.edcmachine, b.accountnumber");
		$this->db->where("a.location", $locationid);
		$this->db->where("a.invoiceid = b.invoice", '', FALSE);
		$this->db->where("b.paymenttype", 'CC');
		$this->db->where("b.knockoffstatus", 0);
		$this->db->where("a.status", 1);
		$this->db->from("invoiceinfo a, paymentinfo b");
		$this->db->order_by("DATE(a.invoicedate)", "DESC");
		$rs = $this->db->get();
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$json['rows'] = $data;
		$json['total'] = count($data);
		$json['csrf_name'] = $this->security->get_csrf_hash();
		print json_encode($json);
	}


}