<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Public_model");
	}

	public function exchangerate()
	{
		$this->load->view('setup/exchangerate_view');
	}

	public function getExchangeRate()
	{
		echo $this->Public_model->getExchangeRate();
	}

	public function supplierData()
	{
		$this->load->view('setup/supplierdata');	
	}

	public function depLifetime()
	{
		$type = $this->uri->segment(3);
		if($type == "add")
		{

		}
		else if($type == "update")
		{
			$id = $this->uri->segment(4);
			if($id)
			{
				$this->db->set("yr_commercial", $this->input->post('yr_commercial'));
				$this->db->set("yr_fiscal", $this->input->post('yr_fiscal'));
				$this->db->set("mth_commercial", $this->input->post('mth_commercial'));
				$this->db->set("mth_fiscal", $this->input->post('mth_fiscal'));
				$this->db->set("titlename", $this->input->post('titlename'));
				$this->db->set("groupcode", $this->input->post('groupcode'));
				$this->db->set("account", $this->input->post('account'));

				$this->db->where("id", $id);
				$update = $this->db->update("acc_depreciation_group");
				echo json_encode(array("status"=>($update) ? 1 : 0, "csrf_name"=>$this->security->get_csrf_hash(), "errorMsg"=>"Something Error"));
			}
			else{
				echo json_encode(array("status"=>0, "csrf_name"=>$this->security->get_csrf_hash(), "errorMsg"=>"ID not found!!"));
			}
		}
		else if($type == "read")
		{
			$rs = $this->db->get("acc_depreciation_group");
			$data = array();
			foreach($rs->result() as $row)
			{
				array_push($data, $row);
			}
			$json['rows'] = $data;
			echo json_encode($json);
		}
		else{
			$this->load->view('setup/depLifetime');
		}
	}

	public function addeditexchangerate()
	{
		$currcode = $this->input->post('currcode'); 
		$countrycode = $this->input->post('countrycode');
		$kurs = $this->input->post('kurs');
		$id = $this->input->post('id');
		$csrf_name  = $this->input->post('csrf_name');

		$this->db->set('currencycode', $currcode);
		$this->db->set('countrycode', $countrycode);
		$this->db->set('exchangerate', $kurs);
		$this->db->where('currencyid', $id);
		$update = $this->db->update('currency');

		echo json_encode(array("status"=>$update, "csrf_name"=>$this->security->get_csrf_hash()));
	}
	

	public function productcategory()
	{
		$this->load->view('setup/productcategory_view');
	}

	public function getProductCategory()
	{
		echo $this->Public_model->getProductCategory();
	}

	public function addeditproductcategory()
	{
		$markup = $this->input->post('markup');
		$categorycode = $this->input->post('categorycode');
		$id = $this->input->post('id');

		
		$this->db->set('markup', $markup, false);
		$this->db->where('productcategoryid', $id);
		$this->db->where('productcategorycode', $categorycode);
		$update = $this->db->update('productcategory');

		echo json_encode(array("status"=>$update, "csrf_name"=>$this->security->get_csrf_hash()));
	}

	public function getSupplierInfo()
	{
		$rs = $this->db->get("supplierinfo");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$json['rows'] = $data;
		echo json_encode($json);
	}

}