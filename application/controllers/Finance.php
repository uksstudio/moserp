<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Finance extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Public_model");
		$this->load->model("Finance_model");
	}

	public function paymentvoucher()
	{
		$akun = $this->uri->segment(3);
		$data['title'] = "Payment Voucher";
		$data['noakun'] = $akun;
		$data['type'] = "PV";
		$data['url'] = "Finance/paymentvoucher/".$akun;
		$data['coaname'] = $this->db->get_where("acc_coa", array("subcode"=>$akun))->row()->name;
		$data['invoice'] = "PV".date('ym').$this->Finance_model->getCountKas("",$data['type']);
		$this->load->view("finance/paymentvouchermulti", $data);
	}

	public function voucherReceive()
	{
		$akun = $this->uri->segment(3);
		$data['title'] = "Receive Voucher";
		$data['noakun'] = $akun;
		$data['type'] = "RV";
		$data['url'] = "Finance/voucherReceive/".$akun;
		$data['coaname'] = $this->db->get_where("acc_coa", array("subcode"=>$akun))->row()->name;
		$data['invoice'] = "RV".date('ym').$this->Finance_model->getCountKas("",$data['type']);
		$this->load->view("finance/paymentvoucher", $data);
	}

	public function getLocationCode()
	{
		$data = $this->Finance_model->getLocationCode();
		echo json_encode($data);
	}

	public function releaseCashadvance()
	{
		$akun = $this->uri->segment(3);
		$data['title'] = "Release Cash Advance";
		$data['type'] = "CA";
		$data['url'] = "Finance/releaseCashadvance";
		$data['invoice'] = "CA".date('ym').$this->Finance_model->getCountKas("",$data['type']);
		$this->load->view("finance/releaseCashadvance", $data);
	}

	public function getCashadvanceVoucher()
	{
		$this->db->where("paymentstatus", 0);
		$rs = $this->db->get("acc_cash_advance");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		echo json_encode($data);
	}

	public function getCashadvanceVoucherDetail()
	{
		$novoc = $this->input->get('canumber');
		$kas = $this->db->get_where("acc_cash_advance", array("canumber"=>$novoc, "realisestatus"=>0))->row();
		$detail = array();
		if(isset($kas))
		{
			$this->db->select("acc_cash_advance_detail.*");
			$this->db->where("acc_cash_advance_detail.cashadvanceid", $kas->id, false);
			$rs = $this->db->get("acc_cash_advance_detail");
			foreach($rs->result() as $key)
			{
				$detail[] = array("description"=>$key->description, "amount"=>$key->amount, "akun"=>"", "namaakun"=>"", "id"=>$key->id, "type"=>"D");
			}
		}

		$data['kas'] = $kas;
		$data['detail'] = $detail;
		echo json_encode($data);
	}	

	public function getJournalDetail()
	{
		$src_account = $this->input->get("q");
		//if(!$q) return;
		//$src_account = $this->uri->segment(3);
		if(!$src_account) return;

		
		$this->db->where("ajd.typedk","D");
		$this->db->where("ajd.accountno",$src_account);
		$this->db->where("ajd.depreciation",0);
		$this->db->select("ajd.id, ajd.kasid, ajd.description, ajd.typedk, ajd.accountno, ajd.amount, ajd.depreciation, aj.voucherno, aj.createdate");
		$this->db->join("acc_gl_journal_detail ajd","ajd.kasid=aj.id", "left");
		$this->db->from("acc_gl_journal aj");
		$rs = $this->db->get();
		$data = array();
		foreach ($rs->result() as $row) {
			array_push($data, $row);
		}
		$json["rows"] = $data;
		$json["sql"] = $this->db->last_query();
		echo json_encode($json);

	}

	public function getDepreciationGroup()
	{
		$q = $this->input->get("q");
		if($q)  {
			//$this->db->like("adg.groupno",$q);
			$this->db->like("adg.titlename",$q);
		}
		$rs = $this->db->get("acc_depreciation_group adg");
		$data = array();
		foreach ($rs->result() as $row) {
			array_push($data, $row);
		}
		$json["rows"] = $data;
		echo json_encode($json);
	}

	public function payCashAdvance()
	{
		$supplierid = 0;
		$remark = $this->input->post("remark");
		$canumber = $this->input->post("canumber");
		$payto = $this->input->post("payto");
		$bilyet = $this->input->post("bilyet");
		$noaccount  = $this->input->post("noaccount");
		$novoucher = $this->input->post("novoucher");
		$tanggal = $this->input->post("tanggal");
		$type = $this->input->post("type");
		$locationcode = "HO";
		$rows = $this->input->post("rows");

		$items = json_decode($rows);

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$this->db->set("createdate", $tanggal);
		$this->db->set("voucherno", $novoucher);
		$this->db->set("remark", $remark);
		$this->db->set("supplierid", $supplierid);
		$this->db->set("ceknumber", $bilyet);
		$this->db->set("coaccount", $noaccount);
		$this->db->set("status", 0);
		$this->db->set("paymentto", $payto);
		$this->db->set("type", $type);
		$this->db->set("locationcode", $locationcode);
		$this->db->set("balance", "(select balance from acc_coa where subcode='".$noaccount."')", false);
		$this->db->set("companyid", $this->session->userdata("compid"));
		$this->db->set("createby", $this->session->userdata("name"));
		$this->db->insert("acc_gl_journal");
		$id = $this->db->insert_id();

		$this->db->where('canumber', $canumber);
		$this->db->update("acc_cash_advance", array("paymentstatus"=>1) );
		$in_item = array();
		$balance = 0;
		
		for($i=0; $i < count($items); $i++)
		{
			$item = array();
			$item['kasid'] = $id;
			$item['description'] = $items[$i]->description;
			$item['typedk'] = $items[$i]->type;
			$item['amount'] = $items[$i]->amount;
			$item['accountno'] = $items[$i]->akun;
			$in_item[] = $item;
			//$balance += $items[$i]->amount;

			$in_item[] = array("kasid"=>$id, "description"=>$items[$i]->description, "typedk"=>"K", "amount"=>$items[$i]->amount, "accountno"=>$noaccount);
		}
		
		
		$this->db->insert_batch('acc_gl_journal_detail',$in_item);
		$this->db->trans_complete();

		

		if ($this->db->trans_status() === FALSE)
		{
				echo json_encode(array('status'=>0,'msg'=>'Failed', 'po_number'=>$novoucher));
		}
		else
			echo json_encode(array('status'=>1,'msg'=>'Successfully', 'po_number'=>$novoucher));
	}

	public function savePaymentVoucher()
	{
		$typesegment = $this->uri->segment(3);

		$supplierid = $this->input->post("supplierid");
		$remark = $this->input->post("remark");
		$payto = $this->input->post("payto");
		$bilyet = $this->input->post("bilyet");
		$noaccount  = $this->input->post("noaccount");
		$novoucher = $this->input->post("novoucher");
		$tanggal = $this->input->post("tanggal");
		$type = $this->input->post("type");
		$locationcode = $this->input->post("locationcode");
		$srcpv = $this->input->post("srcpv");
		$rows = $this->input->post("rows");

		$items = json_decode($rows);

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		if($typesegment == "update")
		{
			$srcpvid = $this->db->query("select id from acc_gl_journal where voucherno='".$srcpv."'")->row()->id;
			$delete = $this->db->query("delete from acc_gl_journal where id=".$srcpvid);
			$deldetail = $this->db->query("delete from acc_gl_journal_detail where kasid=".$srcpvid);

			$novoucher = $srcpv;
		}

		$this->db->set("createdate", $tanggal);
		$this->db->set("voucherno", $novoucher);
		$this->db->set("remark", $remark);
		$this->db->set("supplierid", $supplierid);
		$this->db->set("ceknumber", $bilyet);
		$this->db->set("coaccount", $noaccount);
		$this->db->set("status", 0);
		$this->db->set("paymentto", $payto);
		$this->db->set("type", $type);
		$this->db->set("locationcode", $locationcode);
		$this->db->set("balance", "(select balance from acc_coa where subcode='".$noaccount."')", false);
		$this->db->set("companyid", $this->session->userdata("compid"));
		$this->db->set("createby", $this->session->userdata("name"));
		$this->db->insert("acc_gl_journal");
		$id = $this->db->insert_id();


		$in_item = array();
		$balance = 0;
		
		$totalAmt = 0;
		$coaname = "";
		for($i=0; $i < count($items); $i++)
		{
			//$amt = ($items[$i]->amount > 0) ? $items[$i]->amount : ($items[$i]->amount * -1);
			if($coaname == "") $coaname = $items[$i]->description;
			$totalAmt += $items[$i]->amount;
		}

		//$coaname = $this->db->query("select name from acc_coa where subcode='".$noaccount."'")->row()->name;

		if($type == "RV")
			$in_item[] = array("kasid"=>$id, "wcon"=>"", "description"=>($remark != "") ? $remark : $coaname, "typedk"=>"D","amount"=>$totalAmt, "accountno"=>$noaccount);

		for($i=0; $i < count($items); $i++)
		{
			
			$item = array();
			$item['kasid'] = $id;
			$item['description'] = $items[$i]->description;

			if($type == "PV")
			{
				if($items[$i]->amount < 0){
					$item['typedk'] = "K";
					$item['wcon'] = "-";
					$item['amount'] = ($items[$i]->amount * -1);
				}
				else{
					$item['typedk'] = "D";
					$item['wcon'] = "";
					$item['amount'] = $items[$i]->amount;
				}
			}
			else if($type == "RV")
			{
				if($items[$i]->amount < 0){
					$item['typedk'] = "D";
					$item['wcon'] = "-";
					$item['amount'] = ($items[$i]->amount * -1);
				}
				else{
					$item['typedk'] = "K";
					$item['wcon'] = "";
					$item['amount'] = $items[$i]->amount;
				}
			}

			$item['accountno'] = $items[$i]->akun;
			$in_item[] = $item;
		}

		if($type == "PV")
			$in_item[] = array("kasid"=>$id, "wcon"=>"","description"=>$coaname, "typedk"=>"K", "amount"=>$totalAmt, "accountno"=>$noaccount);

		
		$this->db->insert_batch('acc_gl_journal_detail',$in_item);

		$this->db->trans_complete();

		

		if ($this->db->trans_status() === FALSE)
		{
				echo json_encode(array('status'=>0,'msg'=>'Failed', 'po_number'=>$novoucher));
		}
		else
			echo json_encode(array('status'=>1,'msg'=>'Successfully', 'po_number'=>$novoucher, "TYPE"=>$typesegment));

	}


	public function savePaymentVoucherMulti()
	{
		$typesegment = $this->uri->segment(3);

		$supplierid = $this->input->post("supplierid");
		$remark = $this->input->post("remark");
		$payto = $this->input->post("payto");
		$bilyet = $this->input->post("bilyet");
		$noaccount  = $this->input->post("noaccount");
		$novoucher = $this->input->post("novoucher");
		$tanggal = $this->input->post("tanggal");
		$type = $this->input->post("type");
		$locationcode = $this->input->post("locationcode");
		$srcpv = $this->input->post("srcpv");
		$rows = $this->input->post("rows");

		$items = json_decode($rows);

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		if($typesegment == "update")
		{
			$srcpvid = $this->db->query("select id from acc_gl_journal where voucherno='".$srcpv."'")->row()->id;
			$delete = $this->db->query("delete from acc_gl_journal where id=".$srcpvid);
			$deldetail = $this->db->query("delete from acc_gl_journal_detail where kasid=".$srcpvid);

			$novoucher = $srcpv;
		}

		$this->db->set("createdate", $tanggal);
		$this->db->set("voucherno", $novoucher);
		$this->db->set("remark", $remark);
		$this->db->set("supplierid", $supplierid);
		$this->db->set("ceknumber", $bilyet);
		$this->db->set("coaccount", $noaccount);
		$this->db->set("status", 0);
		$this->db->set("paymentto", $payto);
		$this->db->set("type", $type);
		$this->db->set("locationcode", $locationcode);
		$this->db->set("balance", "(select balance from acc_coa where subcode='".$noaccount."')", false);
		$this->db->set("companyid", $this->session->userdata("compid"));
		$this->db->set("createby", $this->session->userdata("name"));
		$this->db->insert("acc_gl_journal");
		$id = $this->db->insert_id();


		$in_item = array();
		$balance = 0;
		
		$totalAmt = 0;
		$coaname = "";
		for($i=0; $i < count($items); $i++)
		{
			//$amt = ($items[$i]->amount > 0) ? $items[$i]->amount : ($items[$i]->amount * -1);
			if($coaname == "") $coaname = $items[$i]->description;
			$totalAmt += $items[$i]->amount;
		}

		if(!empty($remark)) $coaname = $remark;

		if($type == "RV")
			$in_item[] = array("kasid"=>$id, "wcon"=>"", "description"=>($remark != "") ? $remark : $coaname, "typedk"=>"D","amount"=>$totalAmt, "accountno"=>$noaccount, "storelocation"=>$locationcode);

		for($i=0; $i < count($items); $i++)
		{
			
			$item = array();
			$item['kasid'] = $id;
			$item['description'] = $items[$i]->description;

			if($type == "PV")
			{
				if($items[$i]->amount < 0){
					$item['typedk'] = "K";
					$item['wcon'] = "-";
					$item['amount'] = ($items[$i]->amount * -1);
				}
				else{
					$item['typedk'] = "D";
					$item['wcon'] = "";
					$item['amount'] = $items[$i]->amount;
				}
			}
			else if($type == "RV")
			{
				if($items[$i]->amount < 0){
					$item['typedk'] = "D";
					$item['wcon'] = "-";
					$item['amount'] = ($items[$i]->amount * -1);
				}
				else{
					$item['typedk'] = "K";
					$item['wcon'] = "";
					$item['amount'] = $items[$i]->amount;
				}
			}



			$item['accountno'] = $items[$i]->akun;
			$item['storelocation'] = $items[$i]->storelocation;
			$in_item[] = $item;

			if(!empty($items[$i]->storelocation))
			{
				$res = $this->db->get_where("acc_logical_prv", array("financetype"=>$type, "kasfromlocation"=>$locationcode));
				foreach($res->result() as $rlog)
				{
					$ritem = array();
					$ritem['kasid'] = $id;
					$ritem['description'] = $items[$i]->description;
					$ritem['typedk'] = $rlog->typedk;
					$ritem['wcon'] = "";
					$ritem['amount'] = $items[$i]->amount;
					$ritem['accountno'] = $rlog->account;
					$ritem['storelocation'] = $items[$i]->storelocation;
					$in_item[] = $ritem;
				}
			}

			
		}

		if($type == "PV")
			$in_item[] = array("kasid"=>$id, "wcon"=>"","description"=>$coaname, "typedk"=>"K", "amount"=>$totalAmt, "accountno"=>$noaccount, "storelocation"=>$locationcode);

		//print_r($in_item);
		$this->db->insert_batch('acc_gl_journal_detail',$in_item);

		$this->db->trans_complete();

		

		if ($this->db->trans_status() === FALSE)
		{
				echo json_encode(array('status'=>0,'msg'=>'Failed', 'po_number'=>$novoucher));
		}
		else
			echo json_encode(array('status'=>1,'msg'=>'Successfully', 'po_number'=>$novoucher, "TYPE"=>$typesegment));
	}

	public function getCoa()
	{
		$q = isset($_GET['q']) ? strval($_GET['q']) : '';
		echo $this->Finance_model->getCoa($q, 5);
	}

	public function printPaymentVoucher()
	{
		$pvnumber = $this->uri->segment(3);
		$type = $this->uri->segment(4);
		$akun = $this->uri->segment(5);
		$data['header'] = $this->Finance_model->getKasHeader($pvnumber);
		$data['type'] = $type;
		$data['akun'] = $akun;
		$this->load->view("printing/print_kasmasuk_A4", $data);
	}


	public function getCoaChar()
	{
		$countchar = $this->uri->segment(3);
		$code = $this->input->get("code");
		//$this->db->where("length(subcode)=".$countchar,'', false);
		if(!empty($code)) $this->db->where('code', $code);
		$rs = $this->db->get("acc_coa");
		//echo $this->db->last_query();
		$data = array();
		foreach ($rs->result() as $row) {
			array_push($data, $row);
		}
		echo json_encode($data);
	}
}