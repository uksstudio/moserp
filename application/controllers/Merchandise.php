<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchandise extends CI_Controller {

	protected $_items_cp = array();
	protected $_items_adj = array();
	protected $_items_cp_string = 'change_price';
	protected $_items_stk_adj = 'stock_adjustment';

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Public_model");
		$this->load->model("Inventory_model");
		$this->_items_cp = $this->session->userdata($this->_items_cp_string);
		$this->_items_adj = $this->session->userdata($this->_items_stk_adj);
	}

	public function changePrice()
	{
		if($this->uri->segment(3) == 'refreshpo')
		{
			$this->session->unset_userdata('change_price');
		}

		$this->load->view("merchandise/changeprice");
	}

	public function getAdjustmentType()
	{
		echo $this->Public_model->getAdjustmentType();
	}

	public function saveLineStockAdjustment()
	{
		$productcode = $this->input->post('productcode');
		$rowid = $this->input->post('rowid');
		$qty = $this->input->post('qty');
		$reason = $this->input->post('reason');
		$type = $this->input->post('type');
		$typecode = $this->input->post('typecode');

		$this->db->select('productitem.productitemdesc, stockbalance.totalquantity');
		$this->db->where('stockbalance.locationcode', 'WH');
		$this->db->where('stockbalance.productcode', strtoupper($productcode));
		$this->db->join("productitem","productitem.productitemid=stockbalance.productitem", "LEFT");
		$this->db->from("stockbalance");
		$row = $this->db->get()->row();
		$status = 0;
		$msg = "";
		if(isset($row))
		{
			if($row->totalquantity < $qty)
			{
				$status = 0;
				$msg = "Insufficient available qty";
			}
			elseif(($qty < 0) && (($qty + $row->totalquantity) < 0))
			{
				$status = 0;
				$msg = "Insufficient qty less then available qty";
			}
			else
			{
				$status = 1;
				$iscart['rowid'] = $rowid;
				$iscart['itemcode'] = $productcode;
				$iscart['productdesc'] = $row->productitemdesc;
				$iscart['qty'] = $row->totalquantity;
				$iscart['adjqty'] = $qty;
				$iscart['reason'] = $reason;
				$iscart['action'] = "";
				$iscart['type'] = $type;
				$iscart['typecode'] = $typecode;
				$item[] = $iscart;
				$this->_items_adj[$rowid] = $iscart;

				$this->session->set_userdata(array('stock_adjustment'=>$this->_items_adj));	
				$msg = "Successfully";
			}
			
		}
		else
		{
			$status = 0;
			$msg = "Not found barcode";
		}
		
		$result['csrf_name'] = $this->security->get_csrf_hash();
		$result['status'] = $status;
		$result['msg'] = $msg;
		$result['sql'] = $this->db->last_query();;
		echo json_encode($result);
	}


	public function removeLineStockAdjustment()
	{
		$rowid = $this->uri->segment(3);

	}

	public function getStockAdjustment()
	{
		$item = array();
		if(count($this->_items_adj) > 0)
		{
			foreach ($this->_items_adj as $key => $val)
			{
				$iscart = array();
				$iscart['rowid'] = $val['rowid'];
				$iscart['itemcode'] = $val['itemcode'];
				$iscart['productdesc'] = $val['productdesc'];
				$iscart['qty'] = $val['qty'];
				$iscart['adjqty'] = $val['adjqty'];
				$iscart['reason'] = $val['reason'];
				$iscart['action'] = "";
				$iscart['type'] = $val['type'];
				$iscart['typecode'] = $val['typecode'];
				$item[] = $iscart;
			}
		}

		$data['rows'] = $item;
		echo json_encode($data);
	}

	public function addLineStockAdjustment()
	{
		$item = array();
		$start = (count($this->_items_adj) > 0) ? count($this->_items_adj) : 0;
		$rowid = md5($start+1);
		$iscart['rowid'] = $rowid;
		$iscart['itemcode'] = "";
		$iscart['productdesc'] = "";
		$iscart['qty'] = "";
		$iscart['adjqty'] = "";
		$iscart['reason'] = "";
		$iscart['action'] = "";
		$iscart['type'] = "";
		$iscart['typecode'] = "";
		$item[] = $iscart;
		$this->_items_adj[$rowid] = $iscart;

		$this->session->set_userdata(array('stock_adjustment'=>$this->_items_adj));

		$data['rows'] = $item;
		echo json_encode($data);
	}

	public function showAd()
	{
		print_r($this->_items_adj);
	}

	public function resetItems()
	{
		$this->session->unset_userdata($this->_items_cp_string);
		$this->load->view('merchandise/changeprice');
	}


	public function adjustStock()
	{
		if($this->uri->segment(3) == 'refreshpo')
		{
			$this->session->unset_userdata($this->_items_stk_adj);
		}
		$this->load->view('merchandise/adjustStock');	
	}

	public function getItems()
	{
		$item = array();
		if(count($this->_items_cp) > 0)
		{
			foreach ($this->_items_cp as $key => $val)
			{
				$iscart = array();
				$iscart['rowid'] = $val['rowid'];
				$iscart['itemcode'] = $val['itemcode'];
				$iscart['productdesc'] = $val['productdesc'];
				$iscart['qty'] = $val['qty'];
				$iscart['retailprice'] = $val['retailprice'];
				$iscart['newprice'] = $val['newprice'];
				$iscart['varian'] = $val['varian'];
				$item[] = $iscart;
			}
		}
		$data['rows'] = $item;
		echo json_encode($data);
	}

	public function commitAdjustment()
	{
		$locationcode = $this->input->post('locationcode');
		$type = $this->input->post('type');
		$narrative = $this->input->post('narrative');
		$reffno = "";
		if(count($this->_items_adj) > 0)
		{
			$time_start = microtime(true);
			$this->db->trans_strict(TRUE);
			$this->db->trans_start();

			$reffno = $this->Public_model->getAdjustmentReff();
			$this->db->set("location", "(select locationid from locationinfo where locationcode='".$locationcode."')", false);
			$this->db->set("locationcode", $locationcode);
			$this->db->set("raiseby", $this->session->userdata('userid'));
			$this->db->set("raisedate", date('Y-m-d H:i:s'));
			$this->db->set("remark", $narrative);
			$this->db->set("adjustmenttype", 5); // 5 : ADJUSTMENT NEGATIVE / POSITIVE, 6 : CHANGE PRICE
			$this->db->set("stockadjustmentnumber", $reffno);
			$this->db->set("stockadjustmentid", "nextval('stockadjustment_seq'::text)", FALSE);
			
			$this->db->insert("stockadjustment");
			$adjustmentid = $this->db->insert_id();
			foreach ($this->_items_adj as $key => $val)
			{
				// update stockbalance
				$this->db->set("totalquantity", "(totalquantity+".$val['adjqty'].")", false);
				$this->db->where("stockbalance.locationcode", $locationcode);
				$this->db->where("stockbalance.productcode", strtoupper($val['itemcode']));
				$this->db->update("stockbalance");

				// insert to stockadjustmentitem
				$this->db->set("adjustmentqty", $val['adjqty']);
				$this->db->set("stockadjustment", $adjustmentid);
				//$this->db->where("adjustmenttype", $val['type']);
				$this->db->set("stockadjustmentitemid", "nextval('stockadjustmentitem_seq'::text)", FALSE);
				$this->db->set("adjustmenttypecode", $val['typecode']);
				$this->db->set("lpprice", "(select retailsalesprice from stockbalance where productcode='".$val['itemcode']."' and locationcode='".$locationcode."')", false);
				$this->db->set("productitemcode", $val['itemcode']);
				$this->db->set("productitem", "(select productitemid from productitem where productcode='".$val['itemcode']."')", false);
				$this->db->set("serialnumber", $val['reason']);

				$this->db->insert("stockadjustmentitem");

				// insert into inventory_movement
				$this->db->set('movementdate',date('Y-m-d H:i:s'));
				$this->db->set('movement_value',0);
				$this->db->set('movement_oldvalue', 0);
				
				$this->db->set('movement_qty',(int)$val['adjqty']);
				$this->db->set('locationcode', $locationcode);
				$this->db->set('movement_type', $val['typecode']);
				$this->db->set('reffno', $reffno);
				$this->db->set('locationid', "(select locationid from locationinfo where locationcode='".$locationcode."')", false);
				$this->db->set('productitemcode', $val['itemcode']);
				$this->db->set('productitem', "(select productitemid from productitem where productcode='".$val['itemcode']."')", false);
				$this->db->set('createduser', $this->session->userdata('username'));

				$this->db->set('movement_qtybalance',str_replace("-", "", $val['adjqty']));
				$this->db->set('reason','');
				$this->db->set('narrative',$val['reason']);
				$this->db->set('tofromlocation',"(select locationid from locationinfo where locationcode='".$locationcode."')", false);
				$this->db->set('tofromlocationname','');

				$this->db->insert('inventory_movement');

				$this->Inventory_model->movementPeriode($val['itemcode'], $locationcode, ($val['adjqty'] < 0) ? "adjout": "adjin", str_replace("-", "", $val['adjqty']));
			}

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				$foundzero = 0;
				$msg = "Something wrong";

			}
			else
			{
				$foundzero = 1;
				$msg = "Successfully";
			}
		}

		$result['status'] = $foundzero;
		$result['msg'] = $msg;
		$result['reff'] = $reffno;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);

	}


	public function adjustNewpricesession()
	{
		$rowid = $this->input->post('rowid');
		$newprice = $this->input->post('newprice');

		$tmp = $this->_items_cp[$rowid];

		$this->_items_cp[$rowid]['rowid'] = $rowid;
		$this->_items_cp[$rowid]['productitemid'] = $tmp['productitemid'];
		$this->_items_cp[$rowid]['itemcode'] = $tmp['itemcode'];
		$this->_items_cp[$rowid]['productdesc'] = $tmp['productdesc'];
		$this->_items_cp[$rowid]['qty'] = $tmp['qty'];
		$this->_items_cp[$rowid]['retailprice'] = $tmp['retailprice'];
		$this->_items_cp[$rowid]['newprice'] = $newprice;
		$this->_items_cp[$rowid]['varian'] = (($tmp['retailprice'] - $newprice) / $newprice) * 100;
		
		// masukkan data baru ke session
		$this->session->set_userdata(array('change_price'=>$this->_items_cp));
		$result['csrf_name'] = $this->security->get_csrf_hash();
		$result['status'] = 1;
		echo json_encode($result);
	}

	public function addDraftItem()
	{
		$productcode = $this->input->post('productcode');
		$location = $this->input->post('location');
		$msgEr = "Productitem can not found on WH location";
		$this->db->select("prod.productitemdesc, prod.productitemid, prod.productcode, stk.totalquantity, stk.retailsalesprice");
		$this->db->from('stockbalance stk');
		$this->db->join('productitem prod','prod.productitemid=stk.productitem', 'left');
		$this->db->where('stk.productcode', $productcode);
		$this->db->where('stk.locationcode', $location);
		$rs = $this->db->get();
		$status = 0;
		foreach($rs->result() as $row)
		{

			if($row->totalquantity > 0)
			{
				$rowid = md5($productcode);
				$iscart['rowid'] = $rowid;
				$iscart['productitemid'] = $row->productitemid;
				$iscart['itemcode'] = $row->productcode;
				$iscart['productdesc'] = $row->productitemdesc;
				$iscart['qty'] = $row->totalquantity;
				$iscart['retailprice'] = $row->retailsalesprice;
				$iscart['newprice'] = 0.00;
				$iscart['varian'] = "";
				
				$this->_items_cp[$rowid] = $iscart;
				$status = 1;
			}
			else
			{
				$status = 0;
				$msgEr = "QTY is zero";
			}
			
		}
		
		$this->session->set_userdata(array('change_price'=>$this->_items_cp));
		
		$result['status'] = $status;
		$result['msgEr'] = $msgEr;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}


	public function commitChangePrice()
	{
		$location = $this->input->post('location');
		$narrative = $this->input->post('narrative');
		$locationid = $this->input->post('locationid');
		$foundzero = false;
		$reffno = "";
		$msg = "";
		if(count($this->_items_cp) > 0)
		{
			foreach ($this->_items_cp as $key => $val)
			{
				if($val['newprice'] == 0)
				{
					$foundzero = true;
					break;
				}
			}
		}

		if($foundzero)
		{
			$msg = "Found zero new price, please update";
		}
		else
		{
			if(count($this->_items_cp) > 0)
			{
				$time_start = microtime(true);
				$this->db->trans_strict(TRUE);
				$this->db->trans_start();
				
				$reffno = $this->Public_model->getAdjustmentReff();

				$this->db->set("locationcode", "(select locationcode from locationinfo where locationid='".$locationid."')", false);
				$this->db->set("location", $locationid);
				$this->db->set("raiseby", $this->session->userdata('userid'));
				$this->db->set("raisedate", date('Y-m-d H:i:s'));
				$this->db->set("remark", $narrative);
				$this->db->set("adjustmenttype", 6); // 5 : ADJUSTMENT NEGATIVE / POSITIVE, 6 : CHANGE PRICE
				$this->db->set("stockadjustmentnumber", $reffno);
				$this->db->set("stockadjustmentid", "nextval('stockadjustment_seq'::text)", FALSE);
				
				$this->db->insert("stockadjustment");

				$stockadjustmentid = $this->db->insert_id();
				foreach ($this->_items_cp as $key => $val)
				{
					// minus
					$this->db->set('movementdate',date('Y-m-d H:i:s'));
					$this->db->set('movement_value',$val['newprice']);
					$this->db->set('movement_oldvalue', $val['retailprice']);
					
					$this->db->set('movement_qty',-$val['qty']);
					$this->db->set('locationcode', $location);
					$this->db->set('movement_type', 'X');
					$this->db->set('reffno', $reffno);
					$this->db->set('locationid', "(select locationid from locationinfo where locationcode='".$location."')", false);
					$this->db->set('productitemcode', $val['itemcode']);
					$this->db->set('productitem', "(select productitemid from productitem where productcode='".$val['itemcode']."')", false);
					$this->db->set('createduser', $this->session->userdata('username'));

					$this->db->set('movement_qtybalance',-$val['qty']);
					$this->db->set('reason','');
					$this->db->set('narrative',$narrative);
					$this->db->set('tofromlocation',"(select locationid from locationinfo where locationcode='".$location."')", false);
					$this->db->set('tofromlocationname','');

					$this->db->insert('inventory_movement');

					// plus
					$this->db->set('movementdate',date('Y-m-d H:i:s'));
					$this->db->set('movement_value',$val['newprice']);
					$this->db->set('movement_oldvalue', $val['retailprice']);
					$this->db->set('movement_qty',$val['qty']);
					$this->db->set('locationcode', $location);
					$this->db->set('movement_type', 'Z');
					$this->db->set('reffno', $reffno);
					$this->db->set('locationid', "(select locationid from locationinfo where locationcode='".$location."')", false);
					$this->db->set('productitemcode', $val['itemcode']);
					$this->db->set('productitem', "(select productitemid from productitem where productcode='".$val['itemcode']."')", false);
					$this->db->set('createduser', $this->session->userdata('username'));

					$this->db->set('movement_qtybalance',$val['qty']);
					$this->db->set('reason','');
					$this->db->set('narrative',$narrative);
					$this->db->set('tofromlocation',"(select locationid from locationinfo where locationcode='".$location."')", false);
					$this->db->set('tofromlocationname','');

					$this->db->insert('inventory_movement');


					// add to adjustmenitem
					$this->db->set('expirydate',date('Y-m-d H:i:s'));
					
					$this->db->set('lpprice',$val['newprice']);
					$this->db->set('nfbrsp', $val['retailprice']);
					$this->db->set('adjustmentqty',$val['qty']);
					$this->db->set('location', $locationid);
					$this->db->set('adjustmenttypecode', "X");
					$this->db->set('productitemcode', $val['itemcode']);
					$this->db->set('productitem', "(select productitemid from productitem where productcode='".$val['itemcode']."')", false);
					$this->db->set("stockadjustmentitemid", "nextval('stockadjustmentitem_seq'::text)", FALSE);
					$this->db->set('stockadjustment',$stockadjustmentid);
					$this->db->insert('stockadjustmentitem');

					$this->db->query("update stockbalance set retailsalesprice=".$val['newprice']." WHERE productcode='".$val['itemcode']."' AND locationcode='".$location."'");

					$this->Inventory_model->movementPeriode(strtoupper($val['itemcode']), $location, "cpin", $val['qty']);
					$this->Inventory_model->movementPeriode(strtoupper($val['itemcode']), $location, "cpout", $val['qty']);
				}

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{
						$this->session->unset_userdata('change_price');
						$foundzero = false;
						$msg = "Successfully";

				}
			}
		}


		$result['status'] = (!$foundzero) ? 1 : 0;
		$result['msg'] = $msg;
		$result['reff'] = $reffno;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}

	public function showDraftPoitems()
	{
		$this->_items_cp = $this->session->userdata('change_price');
		// do we want the newest first?
		$cart = (FALSE) ? array_reverse($this->_items_cp) : $this->_items_cp;
		print_r($cart);
	}

}