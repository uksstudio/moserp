<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchasing extends CI_Controller {
	protected $_items = array();
	protected $_items_request = array();

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Public_model");
		$this->_items = $this->session->userdata('items');
		$this->_items_request = $this->session->userdata('items_request');
	}

	public function createpo()
	{
		if($this->uri->segment(3) == 'refreshpo')
		{
			$this->session->unset_userdata('items');
		}

		$data["json_currency"] = $this->Public_model->getCurrency();
		$data['new_ponum'] = $this->Public_model->getCountPonum();
		$data['division'] = $this->getDiv();
		//$this->load->view('purchasing/createpo_view', $data);
		$this->load->view('purchasing/createpo_v1', $data);
	}

	public function purchaseRequest()
	{
		if($this->uri->segment(3) == 'refreshpo')
		{
			$this->session->unset_userdata('items_request');
		}

		$data["json_currency"] = $this->Public_model->getCurrency();
		$data['new_ponum'] = $this->Public_model->getCountPonum();
		$data['division'] = $this->getDiv();
		//$this->load->view('purchasing/createpo_view', $data);
		$this->load->view('purchasing/purchaseRequest', $data);
	}

	private function getDiv()
	{
		$codid = $this->session->userdata('divid');
		$div = $this->db->query('select * from company_division where codid='.$codid)->row()->divisioncode;
		return $div;
	}

	public function updatepo()
	{
		$data["json_currency"] = $this->Public_model->getCurrency();
		$this->load->view('purchasing/updatepo_view', $data);
	}


	public function voidPo()
	{
		$po_number = $this->input->post('po_number');
		$data = array('status'=>'VOID');
		$this->db->where('prepared_by',$this->session->userdata('username'));
		$this->db->where('po_number',$po_number);
		$updatepo = $this->db->update("po_header", $data);
		echo json_encode(array("status"=> $updatepo, "csrf_name"=>$this->security->get_csrf_hash(), "errorMsg"=>$this->db->last_query()));
	}

	public function Po_list()
	{
		$data["json_currency"] = $this->Public_model->getCurrency();
		$this->load->view('purchasing/polist_view', $data);	
	}


	public function copyData()
	{
		$rs = $this->db->get_where('po_detail', array('po_header_id'=>19));
		
		foreach ($rs->result() as $row) {
			$itemcode[$row->barcode] = $row;
			$sizing[$row->barcode][] = array('size'=>$row->size_code, 'qty'=>$row->qty);
		}
		$i = 0;
		foreach ($itemcode as $value) {
			$rowid = MD5($i);
			
			$iscart['rowid'] = $rowid;
			$iscart['itemcode'] = $value->barcode;
			$iscart['label'] = $value->label_code;
			$iscart['cate'] = $value->cate_code;
			$iscart['catedesc'] = "";
			$iscart['gender'] = $value->gender;
			$iscart['color'] = $value->color_code;
			$iscart['fabric'] = $value->fabric_code;
			$iscart['supplier_reff'] = $value->supplier_item_reff_code;
			$iscart['style'] = $value->style_code;
			$iscart['supplier_color'] = $value->supplier_color;
			$iscart['fabric_dec'] = $value->fabric_desc;
			$iscart['style_desc'] = $value->style_desc;
			$iscart['origin'] = $value->country_origin;
			$iscart['cost'] = $value->unitcost;

			$iscart['exchangerate'] = 0;
			$iscart['currency'] = "";
			$iscart['season'] = "";
			$iscart['sugestedrp'] = $value->originalsuggested;
			$iscart['sizegrid'] = "";
			$iscart['editing'] = false;
			$iscart['sizing'] = array();
			
			


			$iscart['sizing'] = array();
			$total_qty=0;
			for($j=0; $j < count($sizing[$value->barcode]); $j++)
			{
				$iscart['sizing'][] = array('size'=>$sizing[$value->barcode][$j]['size'],'qty'=>$sizing[$value->barcode][$j]['qty']);
				$total_qty += $sizing[$value->barcode][$j]['qty'];
			}
			$iscart['totalqty'] = $total_qty;
			$this->_items[$rowid] = $iscart;
			$i++;
			
		}
		$this->session->set_userdata(array('items'=>$this->_items));
		print_r($sizing);
	}

	public function saveDraftPoitems()
	{
		$itemcode = $this->input->post('itemcode');
		$label = $this->input->post('label');
		$cate = $this->input->post('cate');
		$gender = $this->input->post('gender');
		$color = $this->input->post('color');
		$fabric = $this->input->post('fabric');
		$supplier_reff = $this->input->post('supplier_reff');
		$style = $this->input->post('style');
		$supplier_color = $this->input->post('supplier_color');
		$fabric_dec = $this->input->post('fabric_dec');
		$style_desc = $this->input->post('style_desc');
		$origin = $this->input->post('origin');
		$cost = $this->input->post('cost');
		$sugestedrp = $this->input->post('sugestedrp');

		$season = $this->input->post('season');
		$exchangerate = $this->input->post('exchangerate');
		$currency = $this->input->post('currency');

		$adata = $this->input->post('adata');
		
		$itemsize = json_decode($adata);

		$rowid = md5($itemcode);
		$iscart = array();
		
		
		$iscart['rowid'] = $rowid;
		$iscart['itemcode'] = $itemcode;
		$iscart['label'] = $label;
		$iscart['cate'] = $cate;
		$iscart['gender'] = $gender;
		$iscart['color'] = $color;
		$iscart['fabric'] = $fabric;
		$iscart['supplier_reff'] = $supplier_reff;
		$iscart['style'] = $style;
		$iscart['supplier_color'] = $supplier_color;
		$iscart['fabric_dec'] = $fabric_dec;
		$iscart['style_desc'] = $style_desc;
		$iscart['origin'] = $origin;
		$iscart['cost'] = $cost;

		$iscart['exchangerate'] = $exchangerate;
		$iscart['currency'] = $currency;
		$iscart['season'] = $season;
		$iscart['sugestedrp'] = ($sugestedrp == '') ? 0 : $sugestedrp;

		$iscart['sizing'] = array();

		for($i=0; $i < count($itemsize); $i++)
		{
			$iscart['sizing'][] = array('size'=>$itemsize[$i]->size,'qty'=>$itemsize[$i]->qty);
		}
		$this->_items[$rowid] = $iscart;

		$this->session->set_userdata(array('items'=>$this->_items));
		$result['rowid'] = $rowid;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}


	public function saveRowsItems()
	{
		$rowid = $this->input->post('rowid');
		$rows = $this->input->post('rows');
		$rowitems = json_decode($rows);

		
		$this->_items[$rowid]['itemcode'] = $rowitems->itemcode;
		$this->_items[$rowid]['label'] = $rowitems->label;
		$this->_items[$rowid]['cate'] = $rowitems->cate;
		$this->_items[$rowid]['catedesc'] = $rowitems->catedesc;
		$this->_items[$rowid]['gender'] = $rowitems->gender;
		$this->_items[$rowid]['color'] = $rowitems->color;
		$this->_items[$rowid]['fabric'] = $rowitems->fabric;
		$this->_items[$rowid]['supplier_reff'] = $rowitems->supplier_reff;
		$this->_items[$rowid]['style'] = $rowitems->style;
		$this->_items[$rowid]['supplier_color'] = $rowitems->supplier_color;
		$this->_items[$rowid]['fabric_dec'] = $rowitems->fabric_dec;
		$this->_items[$rowid]['style_desc'] = $rowitems->style_desc;
		$this->_items[$rowid]['origin'] = $rowitems->origin;
		$this->_items[$rowid]['cost'] = $rowitems->cost;
		$this->_items[$rowid]['sugestedrp'] = $rowitems->sugestedrp;
		


		// masukkan data baru ke session
		$this->session->set_userdata(array('items'=>$this->_items));
		$result['rowid'] = $rowid;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);

	}


	public function updatePurchaseRequestRows()
	{
		$rows = $this->input->post('rows');
		$items = json_decode($rows);
		foreach ($items as $key => $value) {
			$rowid = $value->rowid;

			$this->_items_request[$rowid]['itemcode'] = $value->itemcode;
			$this->_items_request[$rowid]['qty'] = $value->qty;
			$this->_items_request[$rowid]['uom'] = $value->uom;
			$this->_items_request[$rowid]['unitcost'] = $value->unitcost;
			$this->_items_request[$rowid]['totalprice'] = $value->totalprice;
		}
		
	
		// masukkan data baru ke session
		$this->session->set_userdata(array('items_request'=>$this->_items_request));
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}

	public function updateRows()
	{
		$rows = $this->input->post('rows');
		$items = json_decode($rows);
		foreach ($items as $key => $value) {
			$rowid = $value->rowid;

			$this->_items[$rowid]['itemcode'] = $value->itemcode;
			$this->_items[$rowid]['label'] = $value->label;
			$this->_items[$rowid]['cate'] = $value->cate;
			$this->_items[$rowid]['catedesc'] = $value->catedesc;
			$this->_items[$rowid]['gender'] = $value->gender;
			$this->_items[$rowid]['color'] = $value->color;
			$this->_items[$rowid]['fabric'] = $value->fabric;
			$this->_items[$rowid]['supplier_reff'] = $value->supplier_reff;
			$this->_items[$rowid]['style'] = $value->style;
			$this->_items[$rowid]['supplier_color'] = $value->supplier_color;
			$this->_items[$rowid]['fabric_dec'] = $value->fabric_dec;
			$this->_items[$rowid]['style_desc'] = $value->style_desc;
			$this->_items[$rowid]['origin'] = $value->origin;
			$this->_items[$rowid]['cost'] = $value->cost;
			$this->_items[$rowid]['sugestedrp'] = $value->sugestedrp;
		}
		
	
		// masukkan data baru ke session
		$this->session->set_userdata(array('items'=>$this->_items));
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}

	public function addSizetoItems()
	{
		$rowid = $this->input->post('rowid');
		$adata = $this->input->post('adata');
		$sizecode = $this->input->post('sizecode');
		$itemsize = json_decode($adata);
		
		// reset new array
		$this->_items[$rowid]['sizing'] = array();

		$newsize = array();
		$totalqty = 0;
		for($i=0; $i < count($itemsize); $i++)
		{
			$newsize[] = array('size'=>$itemsize[$i]->size,'qty'=>$itemsize[$i]->qty);
			$totalqty += $itemsize[$i]->qty;
		}


		// di isi new size
		$this->_items[$rowid]['sizegrid'] = $sizecode;
		$this->_items[$rowid]['sizing'] = $newsize;
		$this->_items[$rowid]['totalqty'] = $totalqty;
		//

		// masukkan data baru ke session
		$this->session->set_userdata(array('items'=>$this->_items));
		$result['rowid'] = $rowid;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);



	}

	public function addPurchaseRequestDraftItem()
	{
		$countRows = $this->input->post('rows');

		if($countRows > 0)
		{

			$start = (count($this->_items_request) > 0) ? count($this->_items_request) : 0;

			for($i=$start; $i < ($countRows + $start); $i++)
			{
				$iscart = array();
				$rowid = md5($i);
				$iscart['rowid'] = $rowid;
				$iscart['itemcode'] = "";
				$iscart['qty'] = "";
				$iscart['unitcost'] = "";
				$iscart['totalprice'] = "";
				$iscart['uom'] = "";
				

				$this->_items_request[$rowid] = $iscart;
			}
		}
		$this->session->set_userdata(array('items_request'=>$this->_items_request));
		$result['rowid'] = $rowid;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}

	public function addDraftItem()
	{
		$countRows = $this->input->post('rows');
		$items = array();
		if($countRows > 0)
		{

			$start = (count($this->_items) > 0) ? count($this->_items) : 0;

			for($i=$start; $i < ($countRows + $start); $i++)
			{
				$iscart = array();
				$rowid = md5($i);
				$iscart['rowid'] = $rowid;
				$iscart['itemcode'] = "";
				$iscart['label'] = "";
				$iscart['cate'] = "";
				$iscart['catedesc'] = "";
				$iscart['gender'] = "";
				$iscart['color'] = "";
				$iscart['fabric'] = "";
				$iscart['supplier_reff'] = "";
				$iscart['style'] = "";
				$iscart['supplier_color'] = "";
				$iscart['fabric_dec'] = "";
				$iscart['style_desc'] = "";
				$iscart['origin'] = "";
				$iscart['cost'] = 0.00;

				$iscart['exchangerate'] = "";
				$iscart['currency'] = "";
				$iscart['season'] = "";
				$iscart['sugestedrp'] = 0.00;
				$iscart['sizegrid'] = "";
				$iscart['editing'] = false;
				$iscart['sizing'] = array();
				
				$iscart['totalqty'] = 0;
				$items[] = $iscart;
				$this->_items[$rowid] = $iscart;
			}
		}
		$this->session->set_userdata(array('items'=>$this->_items));
		$result['rowid'] = $rowid;
		$result['rows'] = $items;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}

	public function showDraftPoitems()
	{
		$this->_items = $this->session->userdata('items');
		// do we want the newest first?
		$cart = (FALSE) ? array_reverse($this->_items) : $this->_items;
		print_r($cart);
	}

	public function clearDraftPoitems()
	{
		$this->session->unset_userdata('items');
	}

	public function getRequestPurchaseItems()
	{
		$item = array();
		if(count($this->_items_request) > 0)
		{
			foreach ($this->_items_request as $key => $val)
			{
				$total_qty = 0;

				$iscart = array();
				$iscart['rowid'] = $val['rowid'];
				$iscart['itemcode'] = $val['itemcode'];
				$iscart['qty'] = $val['qty'];
				$iscart['uom'] = $val['uom'];
				$iscart['unitcost'] = $val['unitcost'];
				$iscart['totalprice'] = $val['totalprice'];
				
				$item[] = $iscart;
				
			}
		}
		
		$data['rows'] = $item;
		echo json_encode($data);
	}

	public function getItems()
	{
		$item = array();
		if(count($this->_items) > 0)
		{
			foreach ($this->_items as $key => $val)
			{
				$total_qty = 0;
				foreach($val['sizing'] as $keys=>$vals)
				{
					$total_qty += $vals['qty'];
				}

				$iscart = array();
				$iscart['rowid'] = $val['rowid'];
				$iscart['itemcode'] = $val['itemcode'];
				$iscart['label'] = $val['label'];
				$iscart['cate'] = $val['cate'];
				$iscart['catedesc'] = $val['catedesc'];
				$iscart['gender'] = $val['gender'];
				$iscart['color'] = $val['color'];
				$iscart['fabric'] = $val['fabric'];
				$iscart['supplier_reff'] = $val['supplier_reff'];
				$iscart['style'] = $val['style'];
				$iscart['supplier_color'] = $val['supplier_color'];
				$iscart['fabric_dec'] = $val['fabric_dec'];
				$iscart['style_desc'] = $val['style_desc'];
				$iscart['origin'] = $val['origin'];
				$iscart['cost'] = $val['cost'];

				$iscart['exchangerate'] = $val['exchangerate'];
				$iscart['currency'] = $val['currency'];
				$iscart['season'] = $val['season'];
				$iscart['sugestedrp'] = $val['sugestedrp'];
				$iscart['sizegrid'] = $val['sizegrid'];
				$iscart['editing'] = $val['editing'];
				$iscart['sizing'] = array();
				
				$iscart['totalqty'] = $total_qty;
				$item[] = $iscart;
				
			}
		}
		else
		{
			//set default
			$start = (count($this->_items) > 0) ? count($this->_items) : 0;
			$countRows = 3;
			for($i=$start; $i < ($countRows + $start); $i++)
			{
				$iscart = array();
				$rowid = md5($i);
				$iscart['rowid'] = $rowid;
				$iscart['itemcode'] = "";
				$iscart['label'] = "";
				$iscart['cate'] = "";
				$iscart['catedesc'] = "";
				$iscart['gender'] = "";
				$iscart['color'] = "";
				$iscart['fabric'] = "";
				$iscart['supplier_reff'] = "";
				$iscart['style'] = "";
				$iscart['supplier_color'] = "";
				$iscart['fabric_dec'] = "";
				$iscart['style_desc'] = "";
				$iscart['origin'] = "";
				$iscart['cost'] = 0.00;

				$iscart['exchangerate'] = "";
				$iscart['currency'] = "";
				$iscart['season'] = "";
				$iscart['sugestedrp'] = 0.00;
				$iscart['sizegrid'] = "";
				$iscart['editing'] = false;
				$iscart['sizing'] = array();
				
				$iscart['totalqty'] = 0;

				$this->_items[$rowid] = $iscart;
				$item[] = $iscart;
			}
			$this->session->set_userdata(array('items'=>$this->_items));
			$result['rowid'] = $rowid;
		}
		
		$data['rows'] = $item;
		echo json_encode($data);
	}


	public function getSizing()
	{
		//$rowid = MD5($this->input->get('rowid'));
		$rowid = $this->uri->segment(3);
		//$rowid = MD5($id);
		$item = array();
		for($i=0; $i < count($this->_items[$rowid]['sizing']); $i++)
		{
			$it = $this->_items[$rowid]['sizing'][$i];
			$item[] = array('nsize'=>$it['size'],'nqty'=>$it['qty']);
		}
		$data['rows'] = $item;
		echo json_encode($data);
	}
	

	public function saveCreatingPo()
	{
		$ponum = strtoupper(substr($this->session->userdata('username'),0,2).date('ym').$this->Public_model->getCountPonum());
		$supplierId = $this->input->post('supplierId');
		$supplierName = $this->input->post('supplierName');
		$suppReffnum = $this->input->post('suppReffnum');
		$currency = $this->input->post('currency');
		$season = $this->input->post('season');
		$supplierCode = $this->input->post('supplierCode');
		$description = $this->input->post('description');
		$exchangerate = $this->input->post('exchangerate');
		$type = $this->input->post('type');
		$division = $this->input->post('division');
		$shipmentReff = $this->input->post('shipmentReff');
		$brandcode = $this->input->post('brandcodeid');
		$buyer = $this->input->post('buyer');
		
		$this->db->trans_strict(TRUE);
		$this->db->trans_start();
		
		$this->db->query("INSERT INTO po_header (
						po_number, 
						createdate, 
						description, 
						reff, 
						supplier_id, 
						supplier_name, 
						po_type, 
						exchangerate, 
						shipment_reff,
						doc_currency, season,status,prepared_by, brandcode) values 
						('".$ponum."',current_timestamp, '".$description."','".$suppReffnum."',".$supplierId.",'".$supplierName."','".$type."','".$exchangerate."','".$shipmentReff."','".$currency."','".$season."','NEW','".$this->session->userdata('username')."','".$brandcode."')");
		$po_header_id = $this->db->insert_id();
		
		$this->_items = $this->session->userdata('items');
		$in_item = array();
		foreach ($this->_items as $key => $val)
		{
	 
			$total_qty = 0;
			foreach($val['sizing'] as $keys=>$vals)
			{
				$total_qty += $vals['qty'];
				$in_item[] = array(
					'po_header_id'=>$po_header_id,
					'item_type'=>'GOODS',
					'style_code'=>$val['style'],
					'season_code'=>$season,
					'barcode'=>$val['itemcode'],
					'description'=>$val['style_desc'],
					'qty'=>$vals['qty'],
					'unitprice'=>($val['cost'] * $exchangerate),
					'color_code'=>$val['color'],
					'style_desc'=>$val['style'],
					'size_code'=>$vals['size'],
					'gender'=>$val['gender'],
					'label_code'=>$val['label'],
					'supplier_item_reff_code'=>$val['supplier_reff'],
					'supplier_color'=>$val['supplier_color'],
					'country_origin'=>$val['origin'],
					'unitcost'=>$val['cost'],
					'fabric_code'=>$val['fabric'],
					'cate_code'=>$val['cate'],
					'fabric_desc'=>$val['fabric_dec'],
					'originalsuggested'=>$val['sugestedrp'],
					'sugestedprice'=>($val['sugestedrp'] * $exchangerate)
					);
				
			}
		}

		$this->db->insert_batch('po_detail',$in_item);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
				echo json_encode(array('status'=>0,'msg'=>'Failed', 'po_number'=>$ponum));
		}
		else
			echo json_encode(array('status'=>1,'msg'=>'Successfully', 'po_number'=>$ponum));
	}

	public function getSupplierInfo()
	{
		echo $this->Public_model->getSupplierInfo();
	}

	public function getProductCategory()
	{
		echo $this->Public_model->getProductCategory();
	}

	public function getProductSex()
	{
		echo $this->Public_model->getProductSex();
	}

	public function getProductColor()
	{
		echo $this->Public_model->getProductColor();
	}

	public function getProductLabel()
	{
		echo $this->Public_model->getProductLabel();
	}
	
	public function getListNewPO()
	{
		echo $this->Public_model->getListNewPO();
	}

	public function getProductSeason()
	{
		echo $this->Public_model->getProductSeason();
	}

	public function getPaymentterm()
	{
		echo $this->Public_model->getPaymentterm();
	}

	public function getProductBrand()
	{
		echo $this->Public_model->getProductBrand();	
	}

	public function getSizeGrid()
	{
		$size_code = $this->uri->segment(3);
		$rowid = $this->uri->segment(4);
		$item = array();
		for($i=0; $i < count($this->_items[$rowid]['sizing']); $i++)
		{
			$it = $this->_items[$rowid]['sizing'][$i];
			$item[] = array('nsize'=>$it['size'],'nqty'=>$it['qty']);
		}

		if($size_code !== FALSE)
			echo $this->Public_model->distincSizeGrid($size_code, false, $item);
		else
			echo $this->Public_model->distincSizeGrid();	
	}

	public function getSizeGridGroup()
	{
		$size_code = $this->uri->segment(3);
		$selected = $this->uri->segment(4);
		if($size_code !== FALSE)
			echo $this->Public_model->distincSizeGrid($size_code, $selected);
		else
			echo $this->Public_model->distincSizeGrid();	
	}

	public function getPurchaseOrderList()
	{
		$this->db->order_by('id', 'DESC');
		$rs = $this->db->get('po_header');
		$data = array();
		foreach ($rs->result() as $row) {
			array_push($data, $row);
		}

		$json['rows'] = $data;
		$json['total'] = count($data);
		echo json_encode($json);
	}

	public function getDetailPurchasing()
	{
		//$po_header_id = $this->input->get('po_header_id');
		$po_header_id = $this->uri->segment(3);
		$rs = $this->db->get_where('po_detail', array('po_header_id'=>$po_header_id));
		$data = array();
		foreach ($rs->result() as $row) {
			array_push($data, $row);
		}
		$json['rows'] = $data;
		$json['total'] = count($data);
		echo json_encode($json);

	}

}