<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Costing extends CI_Controller {

	var $_prefix_cost = 'CO';

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Public_model");
	}

	public function costbypo()
	{
		$data["json_currency"] = $this->Public_model->getCurrency();
		$this->load->view("costing/createcostingbypo", $data);
	}

	public function getPoNewest()
	{

		$sql = "id, po_number, (select SUM(qty) from po_detail where po_header_id=po_header.id) as total_qty, (select SUM(unitcost * qty) from po_detail where po_header_id=po_header.id) as total_cost, (select SUM(unitprice * qty) from po_detail where po_header_id=po_header.id) as total_price";

		$this->db->where(array('status'=>'NEW', 'po_type'=>'Goods'));
		$this->db->select($sql);
		$rs = $this->db->get("po_header");
		$data = array();
		$total_cost = 0;
		$total_price = 0;
		foreach($rs->result() as $row)
		{
			$total_cost += $row->total_cost;
			$total_price += $row->total_price ;
			$data[] = array(
				'chk'=>'N',
				'id'=>$row->id, 
				'po_number'=>$row->po_number, 
				'qty'=>$row->total_qty, 
				'cost'=>$row->total_cost, 
				'price'=>$row->total_price,
				'invoice'=>'',
				'donumber'=>'',
				'pbtax'=>0.00,
				'luxtax'=>0.00,
				'freight'=>0.00,
				'handling'=>0.00,
				'insurance'=>0.00,
				'total'=>0.00,
				'avgcost'=>0.00
				);
		}
		$json['rows'] = $data;
		$json['footer'] = array(
			array(
				'cost'=>0.00,
				'price'=>0.00,
				'pbtax'=>0.00,
				'luxtax'=>0.00,
				'freight'=>0.00,
				'handling'=>0.00,
				'total'=>0.00,
				'avgcost'=>0.00
				)
			);
		$json['total'] = count($data);
		echo json_encode($json);
		
	}



	public function getPoByCostingNumber()
	{

		//$costingnumber = $this->input->get('costingnumber');
		$costingnumber = $this->uri->segment(3);


		$rs = $this->db->select('costing_detail.*')->from('costing_detail')->join('costing_header','costing_detail.costing_header_id=costing_header.id','LEFT')->where('costing_header.costingnumber', $costingnumber)->get();
		//echo $this->db->last_query();
		$group = array();
		foreach($rs->result() as $row)
		{
			$groupname = str_replace(' ','',strtolower($row->groupname));
			@$group[$groupname] += $row->amount;
		}


		$sql = "id, po_number, (select SUM(qty) from po_detail where po_header_id=po_header.id) as total_qty, (select SUM(unitcost * qty) from po_detail where po_header_id=po_header.id) as total_cost, (select SUM(unitprice * qty) from po_detail where po_header_id=po_header.id) as total_price";

		$this->db->where(array('costing_number'=>$costingnumber));
		$this->db->select($sql);
		$rs = $this->db->get("po_header");
		$data = array();
		$total_cost = 0;
		$total_price = 0;
		$total_qty = 0;
		$sum_pb_tax = 0;
		$sum_lux_tax = 0;
		$sum_fret = 0;
		$sum_handling = 0;
		$sum_qty = 0;
		$sum_other = 0;
		$total_aja = 0;
		$groupPonumer = array();
		$field = array();
		foreach($rs->result() as $row)
		{
			$total_cost += $row->total_cost;
			$total_price += $row->total_price ;

			array_push($field, $row);
			$groupPonumer[$row->po_number] = $row; 
		}
		//print_r($groupPonumer);
		foreach ($groupPonumer as $key => $row) {
			$pbtax = ($row->total_price / $total_price) * $group['importduty'];
			$luxtax = ($row->total_price / $total_price) * $group['luxurytax'];
			$fret = ($row->total_price / $total_price) * $group['freight'];
			$handling = ($row->total_price / $total_price) * $group['handling'];
			$other = ($row->total_price / $total_price) * $group['other'];
			
			$sum_pb_tax += $pbtax;
			$sum_lux_tax += $luxtax;
			$sum_fret += $fret;
			$sum_handling += $handling;
			$sum_qty += $row->total_qty;
			$sum_other += $other;
			$data[] = array(
				'chk'=>'N',
				'id'=>0, 
				'po_number'=>$key, 
				'qty'=>$row->total_qty , 
				'cost'=>number_format($row->total_cost , 2, '.',','), 
				'price'=>number_format($row->total_price , 2, '.',','),
				'invoice'=>'',
				'donumber'=>'',
				'pbtax'=>number_format($pbtax, 2, '.',','),
				'luxtax'=>number_format($luxtax, 2, '.',','),
				'freight'=>number_format($fret, 2, '.',','),
				'handling'=>number_format($handling, 2, '.',','),
				'insurance'=>number_format($other, 2, '.',','),
				'total'=>number_format(($pbtax + $luxtax + $fret + $handling + $other), 2, '.',',')
				);
		}

		$total_aja = ($sum_pb_tax + $sum_lux_tax + $sum_handling + $sum_fret + $sum_other);
		$json['rows'] = $data;
		$json['footer'] = array(
			array(
				'qty'=>number_format($sum_qty, 0, '.',','),
				'cost'=>number_format($total_cost, 2, '.',','),
				'price'=>number_format($total_price, 2, '.',','),
				'pbtax'=>number_format($sum_pb_tax, 2, '.',','),
				'luxtax'=>number_format($sum_lux_tax, 2, '.',','),
				'freight'=>number_format($sum_fret, 2, '.',','),
				'handling'=>number_format($sum_handling, 2, '.',','),
				'insurance'=>number_format($sum_other, 2, '.',','),
				'total'=>number_format($total_aja, 2, '.',','),
				)
			);
		$json['total'] = count($data);
		echo json_encode($json);
		
	}
	

	public function costingfield()
	{
		$costingid = $this->uri->segment(3);
		if($costingid)
		{
			$this->db->select("costing_field.subcategory,costing_detail.amount, costing_field.category");
			$this->db->where("costing_detail.costing_header_id", $costingid);
			$this->db->join("costing_field","costing_field.id=costing_detail.cosfieldid");
			$rs = $this->db->get("costing_detail");
		}
		else
		{
			$rs = $this->db->get("costing_field");		
		}
		
		$group = array();
		foreach($rs->result() as $row)
		{
			$group[] = array('name'=>$row->subcategory, 'value'=>$row->amount,'group'=>$row->category, 'editor'=>'numberbox');
		}
		$data['rows'] = $group;
		$data['total'] = count($group);
		echo json_encode($data);
	}

	public function costingview()
	{
		$data["json_currency"] = $this->Public_model->getCurrency();
		$this->load->view("costing/costingreview_view", $data);
	}

	public function getCostingReview()
	{
		//$costingnumber = ($this->input->get('costingnumber')) ? $this->input->get('costingnumber') : '';
		$costingnumber = ($this->uri->segment(3) === FALSE) ? '' : $this->uri->segment(3);
		echo json_encode($this->_getCostingReview($costingnumber));
	}

	private function _getCostingReview($cosnumber="")
	{
		$data = array();
		if($cosnumber != "")$this->db->where('costingnumber', $cosnumber);
		
		$rs = $this->db->order_by('id', 'DESC')->get('costing_header');

		foreach($rs->result() as $field)
		{
			$det = $this->db->get_where('costing_detail', array('costing_header_id'=>$field->id));
			$item = array();
			$biaya = array();
			$item['costingnumber'] = $field->costingnumber;
			$item['costingdate'] = $field->costingdate;
			$item['costingstatus'] = $field->costingstatus;
			$item['currency'] = $field->currency;
			$item['exchangerate'] = $field->exchangerate;
			$item['isposting'] = $field->isposting;

			$item['pibnumber'] = $field->pibnumber;
			$item['awbnumber'] = $field->awbnumber;
			$item['invoicenumber'] = $field->invoicenumber;
			$item['spcpdate'] = $field->spcpdate;
			$item['prepareduser'] = $field->prepareduser;
		

			$total = 0;
			foreach($det->result() as $row)
			{
				$group = str_replace(' ','',strtolower($row->groupname));
				@$item[$group] += $row->amount;
				$total += $row->amount;
			}
			$item['total'] = number_format($total,2,'.','.');
			$data[] = $item;
		}

		$json['rows'] = $data;
		$json['total'] = count($data);
		return $json;
	}

	public function printngkhr()
	{
		//$cosnumber = $this->input->get('cosnumber');
		$cosnumber = $this->uri->segment(3);
		$data['header'] = $this->_getCostingReview($cosnumber);
		$data['data'] = $this->getDetailCostingItems($cosnumber);
		$this->load->view("printing/printkhr_A4_landscape", $data);
	}

	public function printngkhranother()
	{
		$cosnumber = $this->uri->segment(3);

		$data['header'] = $this->_getCostingReview($cosnumber);
		$data['data'] = $this->getDetailCostingItems($cosnumber);
		$this->load->view("printing/printkhr_A4", $data);
	}

	/*
	field costingstatus
		:COSTING -> bisa di revisi
		:POSTING -> sudah masuk ke inventory
		:VOID -> Cancel Costing dan link tidak muncul
	*/
	
	public function saveCostingItems()
	{
		$costingid = $this->input->post('costingid');
		$costing_number = $this->input->post('costingnumber');

		$costingField = $this->input->post('costingField');
		$poItems = $this->input->post('poItems');
		

		$itcost = json_decode($costingField);
		$ipo = json_decode($poItems);
		$arM = $this->Public_model->getProductCategory(false);
		
		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		foreach ($ipo as $items)
		{
			$this->db->query("update po_header set status='COSTING', costing_number='".$costing_number."' where po_number='".$items->po_num."'");
			$rs = $this->db->get_where('po_detail', array('po_header_id'=>$items->po_id));
			foreach($rs->result() as $row)
			{
				//$biaya = (($row->unitprice / parse_double($items->total_price)) * parse_double($items->total_biaya)) / $row->qty;
				// calculation unitprice
				$biaya = (($row->unitprice / parse_double($items->total_price)) * parse_double($items->total_biaya));
				//calculation by QTY
				//$biaya = (($row->qty / parse_double($items->total_qty)) * parse_double($items->total_biaya));
				$cogs = ($row->unitprice + $biaya);
				$rprice = ($cogs * $arM[trim($row->cate_code)]);
				$this->db->query("update po_detail set biaya=".$biaya.", markup=".$arM[$row->cate_code].", cogs=".$cogs.", retail_price=".$rprice.", roundupamount=".roundup_ribuan($rprice, false)." where detailid=".$row->detailid);
			}
		}
		$this->db->set('costingstatus', 'COSTING');
		$this->db->where('id', $costingid);
		$this->db->update('costing_header');
		$this->db->trans_complete();
		
		echo json_encode(array('status'=>1, 'co_number'=>$costing_number, 'csrf_name'=>$this->security->get_csrf_hash()));
	}


	public function voidCosting()
	{
		$costingnumber = $this->input->post('costingnumber');
		$this->db->trans_strict(TRUE);
		$this->db->trans_start();
		$this->db->query("update costing_header set costingstatus='VOID' where costingnumber='".$costingnumber."'");
		$this->db->query("update po_header set status='NEW' where costing_number='".$costingnumber."'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			echo json_encode(array('status'=>0, 'csrf_name'=>$this->security->get_csrf_hash(),'msg'=>'Error'));
		}
		else
		{
			echo json_encode(array('status'=>1, 'csrf_name'=>$this->security->get_csrf_hash(),'msg'=>'Error'));
		}
	}

	public function detailCostingItem()
	{
		//$cosnumber = $this->input->get('cosnumber');
		$cosnumber = $this->uri->segment(3);
		
		echo json_encode($this->getDetailCostingItems($cosnumber));
	}


	private function getDetailCostingItems($cosnumber)
	{
		$rs = $this->db->order_by('id', 'ASC')->get_where('po_header', array('costing_number'=>$cosnumber));

		$data = array();
		$sum_qty = 0;
		$sum_biaya = 0;
		$sum_unitprice = 0;
		$sum_unitcost = 0;
		$sum_unitcogs = 0;
		$sum_retailprice = 0;
		$sum_retailpriceround = 0;
		$sum_totalretailpriceround = 0;
		$status = 'NEW';
		foreach($rs->result() as $row)
		{
			
			$status = $row->status;
			$item = $this->db->get_where('po_detail', array('po_header_id'=>$row->id));
			foreach($item->result() as $f)
			{
				$sitem = array();
				$sitem['status'] = $status;
				$sitem['detail_id'] = $f->detailid;
				$sitem['barcode'] = $f->barcode;
				$sitem['unitcost'] = number_format($f->unitcost,2,'.',',');
				$sitem['unitprice'] = number_format($f->unitprice,2,'.',',');
				$sitem['cogs'] = number_format($f->cogs,2,'.',',');
				$sitem['retail_price'] = number_format($f->retail_price,2,'.',',');
				$sitem['biaya'] = number_format($f->biaya,2,'.',',');
				$sitem['qty'] = $f->qty;
				$sitem['markup'] = $f->markup;
				$sitem['size'] = $f->size_code;
				
				$sitem['sugested_retail_price'] = number_format($f->sugestedprice,2,'.',',');
				

				$sitem['margin'] = number_format((($f->retail_price - $f->cogs) / $f->retail_price) * 100,2,'.',',');
				$sitem['var1'] = number_format((($f->retail_price - $f->sugestedprice) / $f->retail_price) * 100,2,'.',',');
				$sitem['retailprice_adjustment'] = $f->retailprice_adjustment;

				$sitem['marginafteradjust'] = ($f->retailprice_adjustment == 0) ? 0 : number_format((($f->retailprice_adjustment - $f->cogs) / $f->retailprice_adjustment) * 100,2,'.',',');
				$sitem['varafteradjust'] = ($f->retailprice_adjustment == 0) ? 0 : number_format((($f->retailprice_adjustment - $f->retail_price) / $f->retailprice_adjustment) * 100,2,'.',',');
				
				$sitem['retail_price_roundup'] = roundup_ribuan(($f->retailprice_adjustment > 0) ? $f->retailprice_adjustment : $f->retail_price, true); 
				$sitem['total_retail_price'] = roundup_ribuan(($f->qty * (($f->retailprice_adjustment > 0) ? $f->retailprice_adjustment : $f->retail_price)), true);

				$sum_qty += $f->qty;
				$sum_biaya += ($f->biaya * $f->qty);
				$sum_unitprice += ($f->unitprice * $f->qty);
				$sum_unitcost += ($f->unitcost * $f->qty);
				$sum_retailprice += $f->retail_price;
				$sum_unitcogs += ($f->cogs + $f->qty);
				$sum_retailpriceround += roundup_ribuan(($f->retailprice_adjustment > 0) ? $f->retailprice_adjustment : $f->retail_price, false); 
				$sum_totalretailpriceround += ($f->qty * (($f->retailprice_adjustment > 0) ? $f->retailprice_adjustment : $f->retail_price));
				$data[] = $sitem;
			}
			
		}

		$json['rows'] = $data;
		$json['footer'] = array(
			array(
				'qty'=>$sum_qty,
				'unitcost'=>number_format($sum_unitcost,2,'.',','),
				'biaya'=>number_format($sum_biaya,2,'.',','), 
				'unitprice'=>number_format($sum_unitprice,2,'.',','),
				'cogs'=>number_format($sum_unitcogs,2,'.',','),
				'retail_price'=>number_format($sum_retailprice,2,'.',','),
				'margin'=>0,
				'retail_price_roundup'=>number_format($sum_retailpriceround,2,'.',','),
				'total_retail_price'=>number_format($sum_totalretailpriceround,2,'.',',')
			)
		);
		$json['total'] = count($data);
		$json['status'] = $status;
		return $json;
	}

	/*
	posting_status:
		: QUEUE
		: PROCESS
		: WAITING => 
		: COMPLETE
	*/
	public function doPosting()
	{
		$costingnumber = $this->input->post('costingnumber');

		$check_costing_count = $this->db->where('costing_number',$costingnumber)->from("post_invcost_header")->count_all_results();

		if($check_costing_count <= 0)
		{
			$header = array(
				'issuedate'=>date('Y-m-d h:i:s'),
				'issued_by'=>$this->session->userdata('username'),
				'posting_number'=>'',
				'posting_number'=>'',
				'posting_status'=>'QUEUE', 
				'remark'=>''
				);

			$this->db->insert('post_invcost_header', $header);
			$header_id = $this->db->insert_id();
			$update = array("isposting"=>TRUE);
			$this->db->query("update costing_header set isposting=TRUE where costing_number='".$costing_number."'");

			echo json_encode(array('status'=>1, 'errorMsg'=>'Please see on the JOBS'));

		}
		else
		{
			echo json_encode(array('status'=>0, 'errorMsg'=>'Duplicate COSTING_NUMBER'));
		}
	}


	public function adjustRetailPrice()
	{
		$newprice = $this->input->post('newprice');
		$detail_id = $this->input->post('detail_id');
		$fromprice = $this->input->post('fromprice');

		$insertLog = array(
			'datettime'=>date('Y-m-d H:i:s'),
			'fromprice'=>$fromprice,
			'toprice'=>$newprice,
			'prepared_by'=>$this->session->userdata('username'),
			'table_name'=>'po_detail',
			'table_name_id'=>$detail_id,
			'remark'=>'adjust price'
			);
		$this->db->insert('log_adjustment_po', $insertLog);

		$update_po = array('retailprice_adjustment'=>$newprice);
		$this->db->where('detailid', $detail_id);
		$ok = $this->db->update('po_detail', $update_po);

		echo json_encode(array('status'=>$ok, 'csrf_name'=>$this->security->get_csrf_hash(),'errorMsg'=>'Successfully'));

	}

}