<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobtask extends CI_Controller {
	var $_prefix_postbyco = 'PS';
	var $_format_sales_desc = "%s, %s %s"; //SALES [outlet][tgl->2018-07-12]
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model("Public_model");
		$this->load->model("Inventory_model");
		$this->load->model("Finance_model");
	}


	/*
	1. update posting_number in table post_invcost_header and return posting_number
	2. update costing_header set iscosting=true, costingdate=currentdate
	2. insert transfernote with posting_number return transfernoteid
	3. insert transfernoteitem with transfernoteid
	4. check productitem and insert ot update return productitemid
	5. insert or update stockbalance where productitemid as detail 
	6. insert movement

	*/
	public function postingByCosting()
	{
		$time_start = microtime(true);
		$this->db->trans_strict(TRUE);
		$this->db->trans_start();
		$rs = $this->db->get_where('post_invcost_header', array('posting_status'=>'QUEUE'), 1, 0);

		foreach ($rs->result() as $row) {
			$posting_number = $this->_prefix_postbyco.date('ym').$this->Public_model->getCountPosting();

			$duplicate = 0;
			if($duplicate > 0)
			{
				echo "Duplicate Costing Number";
				$this->db->query("update post_invcost_header set remark='DUPLICATE COSTING NUMBER', posting_status='COMPLETE' where postingid=".$row->postingid);
			}
			else
			{
				$this->db->query("update post_invcost_header set posting_status='PROCESS', posting_number='".$posting_number."' where postingid=".$row->postingid);
				$this->db->query("update costing_header set isposting='true', postingdate='NOW()' where costingnumber='".$row->costing_number."'");
				

				//$this->db->select('po_detail.*');
				//$this->db->from('po_detail');
				//$this->db->join('po_header','po_detail.po_header_id=po_header.id', 'LEFT');
				///$this->db->where('po_header.costing_number',$row->costing_number);
				//$pobycosting = $this->db->get();
				$pobycosting = $this->db->query("select po_detail.*, po_header.brandcode from po_detail left join po_header ON po_header.id=po_detail.po_header_id WHERE po_header.costing_number='".$row->costing_number."'");
				
				$po_detail_iems = array();
				$productitemidbybarcode = array();
				$productitemid = 0;
				foreach ($pobycosting->result() as $por) {
					array_push($po_detail_iems, $por);

					$barcode = strtoupper($por->barcode.$por->size_code);
					$productitem = $this->db->get_where('productitem', array('productcode'=>$barcode))->row();
					
					// masuk ke master data
					if(isset($productitem)) // update
					{

						$this->db->set('supplieritemcode', $por->supplier_item_reff_code);
						$this->db->set('size', $por->size_code);
						$this->db->set('sex', $por->gender);
						$this->db->set('productseason',"(select productseasonid from productseason where productseasoncode='".$por->season_code."')", false);
						$this->db->set('productitemdesc', $por->description);
						$this->db->set('productgroup',"(select productgroupid from productgroup where productgroupcode='".$por->label_code."')", false);
						$this->db->set('productcategory',"(select productcategoryid from productcategory where productcategorycode='".$por->cate_code."')", false);
						$this->db->set('descline1',$por->fabric_desc); //fabric_desc
						$this->db->set('descline2',$por->supplier_color); //color
						$this->db->set('suppliercode', "(select supplier_id from po_header where id='".$por->po_header_id."')", false);
						$this->db->set('style_number', $por->style_code);
						$this->db->set('productbrandcode', $por->brandcode);
						$this->db->set('merchinhand', 'merchinhand + '.$por->qty, false);
						$this->db->where('productitemid',$productitem->productitemid);
						$this->db->update('productitem');
						$productitemid = $productitem->productitemid;
						//echo "<li>update</li>";

					}
					else // insert new item
					{
						$this->db->set('supplieritemcode', $por->supplier_item_reff_code);
						$this->db->set('size', $por->size_code);
						$this->db->set('sex', $por->gender);
						$this->db->set('productseason',"(select productseasonid from productseason where productseasoncode='".$por->season_code."')", FALSE);
						$this->db->set('productitemdesc', $por->description);
						$this->db->set('productgroup',"(select productgroupid from productgroup where productgroupcode='".$por->label_code."')", FALSE);
						$this->db->set('productcategory',"(select productcategoryid from productcategory where productcategorycode='".$por->cate_code."')", false);
						$this->db->set('descline1',$por->fabric_desc); //fabric_desc
						$this->db->set('descline2',$por->supplier_color); //color
						$this->db->set('productcode', $barcode);
						$this->db->set('status', 1);
						$this->db->set('deletestatus', 1);
						$this->db->set('createdatetime', date('Y-m-d H:i:s'));
						$this->db->set('style_number', $por->style_code);
						$this->db->set('suppliercode', "(select supplier_id from po_header where id='".$por->po_header_id."')", false);
						$this->db->set('productitemid', "nextval('productitem_seq'::text)", FALSE);
						$this->db->set('productbrandcode', $por->brandcode);
						$this->db->set('merchinhand', $por->qty, false);
						$this->db->insert('productitem');
						$productitemid = $this->db->insert_id();
						//echo "<li>insert</li>";
					}

					$productitemidbybarcode[$barcode] = $productitemid;

				} // end foreach pobycosting

				
				// input ke LBM = Laporang Barang Masuk
				//print_r($po_detail_iems);
				if($row->receivereportitem)
				{
					$reportnumber = $this->Public_model->getCountReportPosting();
					//echo $reportnumber;
					$this->db->set('datetime',date('Y-m-d H:i:s'));
					$this->db->set('report_number',$reportnumber);
					$this->db->set('costing_number',$row->costing_number);
					//$this->db->set('costing_id',$row->costing_number);
					$this->db->insert('post_report_items_header');
					//echo $this->db->last_query();
					$report_id = $this->db->insert_id();


					$insert_detail_item = array();
					
					foreach ($po_detail_iems as $por) 
					{

						$barcode = strtoupper($por->barcode.$por->size_code);
						$insert_detail_item[] = array(
							'post_report_header_id'=>$report_id,
							'productitemid'=>$productitemidbybarcode[$barcode],
							'productcode'=>$barcode,
							'po_number'=>$row->costing_number, 
							'po_id'=>$por->po_header_id,
							'qty'=>$por->qty, 
							'currentcost'=>$por->unitprice, 
							'retailprice'=>($por->retailprice_adjustment > 0)? $por->retailprice_adjustment : $por->roundupamount, 
							'unitcogs'=>$por->cogs, 
							'unitlandedcost'=>$por->biaya);

						
					}
					$this->db->insert_batch('post_report_items_detail',$insert_detail_item);
					//echo $this->db->_error_message();
				}


				// insert ke stockbalance WH
				
				foreach ($po_detail_iems as $por) 
				{
					$barcode = strtoupper($por->barcode.$por->size_code);
					$stkbalid = $this->db->get_where('stockbalance', array('productcode'=>$barcode, 'locationcode'=>$row->locationcode))->row();
					if(isset($stkbalid))
					{
						$this->db->set('transactiondate',date('Y-m-d H:i:s'));
						$this->db->set('retailsalesprice',($por->retailprice_adjustment > 0)? $por->retailprice_adjustment : $por->roundupamount);
						$this->db->set('lastdonumber',$row->costing_number);
						$this->db->set('currentcost',$por->cogs);
						$this->db->set('totalquantity',$por->qty);

						$this->db->set('productwithqty',1);
						$this->db->set('bfvalue',0);
						$this->db->set('bfquantity',$stkbalid->totalquantity);


						$this->db->where('locationcode', $row->locationcode);
						$this->db->where('productcode', $barcode);
						$this->db->update('stockbalance');
					}
					else
					{
						$this->db->set('transactiondate',date('Y-m-d H:i:s'));
						$this->db->set('retailsalesprice',($por->retailprice_adjustment > 0)? $por->retailprice_adjustment : $por->roundupamount);
						$this->db->set('lastdonumber',$row->costing_number);
						$this->db->set('currentcost',$por->cogs);
						$this->db->set('totalquantity',$por->qty);
						$this->db->set('locationcode', $row->locationcode);
						$this->db->set('location', "(select locationid from locationinfo where locationcode='".$row->locationcode."')", false);
						$this->db->set('productcode', $barcode);
						$this->db->set('productitem', "(select productitemid from productitem where productcode='".$barcode."')", false);
						$this->db->set('stockbalanceid', "nextval('stockbalance_seq'::text)", FALSE);
						$this->db->set('productwithqty',1);
						$this->db->set('bfvalue',0);
						$this->db->set('bfquantity',0);

						$this->db->insert('stockbalance');
					}
				}


				// insert ke movement
				/*
				movement type:
				I = IN 									(PENERIMAAN BARANG DARI TOKO)
				O = OUT 								(KELUAR BARANG KE TOKO/TOKO KONSINYASI)
				C = CUSTOMER ORDER 						(DIGUNAKAN KELUAR EXPORT)
				D = CUSTOMER ORDER RECEIPT
				A = STOCK ADJUSTMENT
				B = TRANSFER OUT of BOND
				R = Misc RECEIPT
				S = Misc ISSUE
				L = ALLOCATED ADJUSTMENT
				M = MANUFACTURE ISSUE
				N = MANUF RECEIPT
				P = PURCHACE RECEIPT
				Q = BACK TO BACK
				F = FROZEN ADJUSTMENT
				W = NEW INPUT FROM KHR 					(BARANG BARU / KHR)

				X  = CHANGE PRICE OUT 					(PERUBAHAN HARGA DI KELUARIN DULU ALL QTY)
				Z  = CHANGE PRICE IN 					(SETELAH PERUBAHAN MASUK LAGI DENGAN ALL QTY DAN HARGA BARU)
				*/
				foreach ($po_detail_iems as $por) 
				{
					$barcode = strtoupper($por->barcode.$por->size_code);
					
					$this->db->set('movementdate',date('Y-m-d H:i:s'));
					$this->db->set('movement_value',($por->retailprice_adjustment > 0)? $por->retailprice_adjustment : $por->roundupamount);
					$this->db->set('movement_qty',$por->qty);
					$this->db->set('locationcode', $row->locationcode);
					$this->db->set('movement_type', 'W');
					$this->db->set('reffno', $row->costing_number);
					$this->db->set('locationid', "(select locationid from locationinfo where locationcode='".$row->locationcode."')", false);
					$this->db->set('productitemcode', $barcode);
					$this->db->set('productitem', "(select productitemid from productitem where productcode='".$barcode."')", false);
					$this->db->set('createduser', $row->issued_by);

					$this->db->set('movement_qtybalance',"(select sum(totalquantity) from stockbalance where productcode='".$barcode."' and locationcode='".$row->locationcode."')", false);
					$this->db->set('reason','');
					$this->db->set('narrative','');
					$this->db->set('tofromlocation',"(select locationid from locationinfo where locationcode='".$row->locationcode."')", false);
					$this->db->set('tofromlocationname','');

					$this->db->insert('inventory_movement');

					$this->Inventory_model->movementPeriode($barcode, $row->locationcode, "khr", $por->qty);

				}


				// masuk ke productionumber untuk di set carton transfernya
				$this->db->set('transfernotenumber',$row->costing_number);
				$this->db->set('lastupdate',date('Y-m-d H:i:s'));
				$this->db->set('fromlocation',"(select locationid from locationinfo where locationcode='".$row->locationcode."')", false);
				$this->db->set('fromlocationcode',$row->locationcode);
				$this->db->set('uom','BOX');
				$this->db->insert("productionumber");
				$p_ioid = $this->db->insert_id();

				foreach ($po_detail_iems as $por) 
				{
					$barcode = strtoupper($por->barcode.$por->size_code);
					
					$this->db->set('prodioid',$p_ioid);
					$this->db->set('productitem', "(select productitemid from productitem where productcode='".$barcode."')", false);
					$this->db->set('productcode', $barcode);
					$this->db->set('qty',$por->qty);
					$this->db->insert('productionumberdetail');
				}

				
			}
			$time_end = microtime(true);
    		$time = $time_end - $time_start;
			$this->db->query("update post_invcost_header set posting_status='COMPLETE', timeprocess=".$time." where postingid=".$row->postingid);
		}
		$this->db->trans_complete();
		$time_end = microtime(true);
    	$time = $time_end - $time_start;
		echo json_encode(array('status'=>1, 'co_number'=>'', 'csrf_name'=>$this->security->get_csrf_hash(), 'time'=>$time));
	}

	private function getCountCostingNumber($costingnumber)
	{
		$this->db->where('costing_number', $costingnumber);
		$check_duplicate_costing = $this->db->count_all_results('post_invcost_header');
		return $check_duplicate_costing;
	}


	/*
	salesTransfer
	related table
		1. inventory_move_open, update increament sal for store
		2. insert inventory_movement 
		3. ----- accounting next
	*/
	public function transferSales()
	{
		$time_start = microtime(true);
		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$date=date("Y-m-d");

		$this->db->select("productitem.productcode, invoiceitem.quantity, invoiceinfo.outletcode, invoiceitem.retailsalesprice, invoiceinfo.invoicenumber, invoiceinfo.location, invoiceitem.productitem, invoiceinfo.transactiontype");
		$this->db->where('DATE(invoiceinfo.invoicedate)', $date);
		//$this->db->where('invoiceinfo.transactiontype', "SI");
		$this->db->where('invoiceinfo.status', 1);
		$this->db->join("invoiceitem", "invoiceitem.invoice=invoiceinfo.invoiceid", "LEFT");
		$this->db->join("productitem", "productitem.productitemid=invoiceitem.productitem", "LEFT");
		$this->db->from("invoiceinfo");
		$rs = $this->db->get();
		//echo $this->db->last_query();
		foreach ($rs->result() as $row) {
			// update inventory_move_open
			$this->Inventory_model->movementPeriode($row->productcode, $row->outletcode, "sal", $row->quantity);

			// inventory movement
			$this->db->set('movementdate',date('Y-m-d H:i:s'));
			$this->db->set('movement_value',$row->retailsalesprice);
			$this->db->set('movement_qty',-$row->quantity);
			$this->db->set('locationcode', $row->outletcode);
			$this->db->set('movement_type', 'C');
			$this->db->set('reffno', $row->invoicenumber);
			$this->db->set('locationid', $row->location);
			$this->db->set('productitemcode', $row->productcode);
			$this->db->set('productitem', $row->productitem);
			$this->db->set('createduser', "SYS");

			$this->db->set('movement_qtybalance',$row->quantity);
			$this->db->set('reason','');
			$this->db->set('narrative','CUSTOMER ORDER');
			$this->db->set('tofromlocation',$row->location);
			$this->db->set('tofromlocationname',$row->outletcode);

			$this->db->insert('inventory_movement');

			
			
			
		}


		$this->db->trans_complete();
		$time_end = microtime(true);
    	$time = $time_end - $time_start;
		echo json_encode(array('status'=>1, 'co_number'=>'', 'csrf_name'=>$this->security->get_csrf_hash(), 'time'=>$time));
	}


	public function setSalesJournal()
	{
		$time_start = microtime(true);
		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$date=date("Y-m-d");
		//$date="2018-07-12";
		$this->db->select("invoiceinfo.transactiontype, invoiceinfo.invoicedate, to_char(invoiceinfo.invoicedate, 'YYYY-MM-DD') as tgl, invoiceinfo.invoicenumber, invoiceinfo.invoiceid, invoiceinfo.netsalesamount, invoiceinfo.outletcode");
		$this->db->where('DATE(invoiceinfo.invoicedate)', $date);
		$this->db->where('invoiceinfo.status', 1);
		//$this->db->join("invoiceitem", "invoiceitem.invoice=invoiceinfo.invoiceid", "LEFT");
		//$this->db->join("paymentinfo", "paymentinfo.invoice=invoiceinfo.invoiceid", "LEFT");
		$this->db->from("invoiceinfo");
		$rs = $this->db->get();
		//echo $this->db->last_query();
		foreach ($rs->result() as $row) {
			$this->db->set("createdate", $row->invoicedate);
			$this->db->set("voucherno", "SL".$row->invoicenumber);
			$this->db->set("remark", "");
			$this->db->set("supplierid", 0);
			$this->db->set("locationcode", $row->outletcode);
			$this->db->set("ceknumber", "");
			$this->db->set("coaccount", "");
			$this->db->set("status", 0);
			$this->db->set("paymentto", "");
			$this->db->set("type", "SL");
			$this->db->set("isposting", "true", false);
			$this->db->set("balance", 0, false);
			$this->db->set("companyid", 1);
			$this->db->set("createby", "SYS");
			$this->db->insert("acc_gl_journal");
			$id = $this->db->insert_id();

			if($row->transactiontype == "SI")
			{
				// format keterangan journal : SALES [outlet][tgl->2018-07-12]
				$desc = sprintf($this->_format_sales_desc, "SALES", $row->outletcode, $row->tgl);
				$payment = $this->db->query("select * from paymentinfo where invoice=".$row->invoiceid);
				$in_item = array();
				foreach($payment->result() as $field)
				{
					
					if($field->paymenttype == "CH") //CASH
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11101");
					else if($field->paymenttype == "CC") //CREDIT CARD
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11103");
					else if($field->paymenttype == "CE") //DEBIT
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11104");
					else
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11101");

				}

				$afterppn = ($row->netsalesamount / 1.1);
				$ppn = ($afterppn * 0.1);
				$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "K", $afterppn, "41001"); //AFTER PPN
				$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "K", $ppn, "21407"); //PPN
				$hpp = $this->db->query("select SUM(stockbalance.currentcost) as hpp from stockbalance where productitem IN (select productitem from invoiceitem where invoice=".$row->invoiceid.") AND locationcode='".$row->outletcode."'")->row()->hpp;
				$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $hpp, "51001"); //HPP
				$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "K", $hpp, "11601"); // lawan HPP
				$this->db->insert_batch('acc_gl_journal_detail',$in_item);
			}
			else if($row->transactiontype == "CN")
			{
				$in_item = array();
				$afterppn = ($row->netsalesamount / 1.1);
				$ppn = ($afterppn * 0.1);
				$hpp = $this->db->query("select SUM(stockbalance.currentcost) as hpp from stockbalance where productitem IN (select productitem from invoiceitem where invoice=".$row->invoiceid.") AND locationcode='".$row->outletcode."'")->row()->hpp;
				$desc = sprintf($this->_format_sales_desc, "SALES CN", $row->outletcode, $row->tgl);
				$in_item[] = $this->Finance_model->batch_journal_detail($id,$desc , "D", $afterppn, "41001"); // PENJUALAN LOCAL
				$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $ppn, "21407"); // PPN PENJUALAN LOCAL
				$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "K", $row->netsalesamount, "11101"); // CASH

				$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $hpp, "11601"); // lawan HPP -> persediaan
				$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "K", $hpp, "51001"); //HPP
				
				$this->db->insert_batch('acc_gl_journal_detail',$in_item);
			}
			else if($row->transactiontype == "MC")
			{
				$desc = sprintf($this->_format_sales_desc, "SALES MC", $row->outletcode, $row->tgl);
				$payment = $this->db->query("select * from paymentinfo where invoice=".$row->invoiceid);
				$in_item = array();
				foreach($payment->result() as $field)
				{
					
					if($field->paymenttype == "CH") //CASH
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11101");
					else if($field->paymenttype == "CC") //CREDIT CARD
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11103");
					else if($field->paymenttype == "CE") //DEBIT
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11104");
					else
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11101");

				}

				$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "K", $row->netsalesamount, "71101"); // PENDAPATAN LAIN LAIN
				$this->db->insert_batch('acc_gl_journal_detail',$in_item);
			}
			else if($row->transactiontype == "RT")
			{
				$desc = sprintf($this->_format_sales_desc, "SALES RT", $row->outletcode, $row->tgl);
				$payment = $this->db->query("select * from paymentinfo where invoice=".$row->invoiceid);
				$in_item = array();
				foreach($payment->result() as $field)
				{
					
					if($field->paymenttype == "CH") //CASH
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11101");
					else if($field->paymenttype == "CC") //CREDIT CARD
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11103");
					else if($field->paymenttype == "CE") //DEBIT
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11104");
					else
						$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "D", $field->amount, "11101");

				}

				$in_item[] = $this->Finance_model->batch_journal_detail($id, $desc, "K", $row->netsalesamount, "21202"); // UANG MUKA PENJUALAN
				$this->db->insert_batch('acc_gl_journal_detail',$in_item);
			}
			
		}


		$this->db->trans_complete();
		$time_end = microtime(true);
    	$time = $time_end - $time_start;
		echo json_encode(array('status'=>1, 'co_number'=>'', 'csrf_name'=>$this->security->get_csrf_hash(), 'time'=>$time));
	}


	public function setKnockOff()
	{
		$time_start = microtime(true);
		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$date=date("Y-m-d");
		//$date="2018-07-11";
		$this->db->select("(select edcmachine from paymentinfo where invoice=ccni.invoice and knockoffstatus=1 and amount=ccni.totalccamount) as edcmachine, ccno.locationcode, ccni.netccamount, ccni.invoice, to_char(ccno.createdatetime, 'YYYY-MM-DD') as tgl, ccni.invoicenumber, ccni.totalccamount, ccni.totalcommission, ccni.commissionrate");
		$this->db->where('DATE(ccno.createdatetime)', $date);
		$this->db->join("creditcardknockoffitem ccni", "ccni.creditcardknockoff=ccno.creditcardknockoffid", "LEFT");
		$this->db->from("creditcardknockoff ccno");
		$rs = $this->db->get();
		//echo $this->db->last_query();
		$group_edc = array();
		
		foreach($rs->result() as $row)
		{
			if(!empty($row->edcmachine))
			{
				$group_edc[$row->edcmachine."-".$row->locationcode."-".$row->invoicenumber][] = array("outlet"=>$row->locationcode, "netccamount"=>$row->netccamount, "invoice"=>$row->invoice, "tgl"=>$row->tgl, "invoicenumber"=>$row->invoicenumber, "totalccamount"=>$row->totalccamount, "totalcommission"=>$row->totalcommission, "commissionrate"=>$row->commissionrate);
			}
		}
		//print_r($group_edc);
		$in_item = array();
		foreach ($group_edc as $eedc => $value) {
			$edc = explode("-", $eedc)[0];
			$outletcode = explode("-", $eedc)[1];
			$invoicenumber = explode("-", $eedc)[2];

			$this->db->set("createdate", "CURRENT_DATE", false);
			$this->db->set("voucherno", "S".$outletcode.$invoicenumber);
			$this->db->set("remark", "");
			$this->db->set("supplierid", 0);
			$this->db->set("locationcode", $outletcode);
			$this->db->set("ceknumber", "");
			$this->db->set("coaccount", "");
			$this->db->set("status", 0);
			$this->db->set("paymentto", "");
			$this->db->set("type", "RR");
			$this->db->set("isposting", "true", false);
			$this->db->set("balance", 0, false);
			$this->db->set("companyid", 1);
			$this->db->set("createby", "SYS");
			$this->db->insert("acc_gl_journal");
			$id = $this->db->insert_id();

			$totalccamount = 0;
			$totalnetccamount = 0;
			$totalcommission = 0;
			$tgl = "";
			foreach ($value as $index => $arval) {
				$totalccamount += $arval['totalccamount'];
				$totalnetccamount += $arval['netccamount'];
				$totalcommission += $arval['totalcommission'];
				$tgl = $arval['tgl'];
			}

			$akun = $this->db->query("select coacode from swapbank where upper(edcname) ='".strtoupper($edc)."'")->row()->coacode;
			$in_item[] = $this->Finance_model->batch_journal_detail($id, "KNOCK, ".$edc." ".$tgl, "D", $totalnetccamount, $akun);
			$in_item[] = $this->Finance_model->batch_journal_detail($id, "KNOCK, ".$edc." ".$tgl, "D", $totalcommission, "61020");// 61304beban komisi CC
			$in_item[] = $this->Finance_model->batch_journal_detail($id, "KNOCK, ".$edc." ".$tgl, "K", $totalccamount, "11103");//CC
		}
		$this->db->insert_batch('acc_gl_journal_detail',$in_item);
		$this->db->trans_complete();
		$time_end = microtime(true);
    	$time = $time_end - $time_start;
		echo json_encode(array('status'=>1, 'co_number'=>'', 'csrf_name'=>$this->security->get_csrf_hash(), 'time'=>$time));
		
	}

	/*
	step
		1. where yang sudah di receipt and fromlocation <> WH
	*/
	public function transferReceipt()
	{

	}


	

}