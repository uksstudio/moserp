<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Public_model");
		$this->load->model("Finance_model");
	}

	public function paymentvoucher()
	{
		$akun = $this->uri->segment(3);
		$data['title'] = "Posting Journal, Payment and Voucher Receive";
		$data['noakun'] = $akun;
		$data['invoice'] = "PV".$akun.date('ym').$this->Finance_model->getCountKas($akun);
		$data['url'] = "accounting/paymentvoucher/".$akun;
		$this->load->view("accounting/postingJournal", $data);
	}

	public function generalledger()
	{
		$this->load->view("accounting/generalledger");	
	}

	public function chartofaccount()
	{
		$this->load->view("accounting/acc_coa");
	}

	public function getTrialBalance()
	{
		$this->load->view("accounting/trialBalance");	
	}

	public function balancesheet()
	{
		$this->load->view("accounting/balancesheet");	
	}

	public function profitandlost()
	{
		$this->load->view("accounting/profitandlost");		
	}


	public function printView()
	{
		$this->load->view("accounting/printviewjournal");
	}

	public function getPreviewJournal()
	{
		$rs = $this->db->get("acc_gl_journal");
		$data = array();
		foreach ($rs->result() as $key) {
			//array_push($data, $key);
			$data[] = array(
				"id"=>$key->id,
				"createdate"=>$key->createdate,
				"voucherno"=>$key->voucherno,
				"amount"=>$key->amount,
				"remark"=>$key->remark,
				"paymentto"=>$key->paymentto,
				"companyid"=>$key->companyid,
				"supplierid"=>$key->supplierid,
				"ceknumber"=>$key->ceknumber,
				"createby"=>$key->createby,
				"status"=>$key->status,
				"isposting"=>$key->isposting,
				"coaccount"=>$key->coaccount,
				"postingdate"=>$key->postingdate,
				"postingby"=>$key->postingby,
				"type"=>$key->type,
				"balance"=>$key->balance,
				"locationcode"=>$key->locationcode,
				"action"=>"<a href='#' id='showprint' class='easyui-linkbutton' data-options=\"iconCls:'icon-print'\" row-id='".$key->id."' po-number='".$key->voucherno."'>Print</a>"
			);
		}
		$json['rows'] = $data;
		echo json_encode($json);
	}

	public function journalMemorial()
	{
		
		$data['title'] = "Journal Memorial";
		$data['type'] = "JM";
		$data['invoice'] = $data['type'].date('ym').$this->Finance_model->getCountKas("",$data['type']);
		$data['url'] = "accounting/journalMemorial";
		$this->load->view("accounting/journalMemorial", $data);
	}

	public function journalSales()
	{
		$data['title'] = "Journal Sales";
		$data['type'] = "SL";
		$data['invoice'] = $data['type'].date('ym').$this->Finance_model->getCountKas("",$data['type']);
		$data['url'] = "accounting/journalSales";
		$this->load->view("accounting/journalMemorial", $data);
	}

	public function adjustmentMemorial()
	{
		$data['title'] = "Adjustment Memorial";
		$data['type'] = "AM";
		$data['invoice'] = $data['type'].date('ym').$this->Finance_model->getCountKas("",$data['type']);
		$data['url'] = "accounting/adjustmentMemorial";
		$this->load->view("accounting/journalMemorial", $data);	
	}

	public function journalStockAccru()
	{
		$data['title'] = "Journal Transfer Stock Acru";
		$data['type'] = "SS";
		$data['invoice'] = $data['type'].date('ym').$this->Finance_model->getCountKas("",$data['type']);
		$data['url'] = "accounting/journalStockAccru";
		$this->load->view("accounting/journalMemorial", $data);	
	}

	public function journalStockAwal()
	{
		$data['title'] = "Journal Stock Awal";
		$data['type'] = "SA";
		$data['invoice'] = $data['type'].date('ym').$this->Finance_model->getCountKas("",$data['type']);
		$data['url'] = "accounting/journalStockAwal";
		$this->load->view("accounting/journalMemorial", $data);	
	}

	public function memorialFinancial()
	{
		$data['title'] = "FInancial Memorial";
		$data['type'] = "FM";
		$data['invoice'] = $data['type'].date('ym').$this->Finance_model->getCountKas("",$data['type']);
		$data['url'] = "accounting/memorialFinancial";
		$this->load->view("accounting/journalMemorial", $data);	
	}

	public function memorialJournalYearly()
	{
		$data['title'] = "Memorial Journal Yearly";
		$data['type'] = "MT";
		$data['invoice'] = $data['type'].date('ym').$this->Finance_model->getCountKas("",$data['type']);
		$data['url'] = "accounting/memorialJournalYearly";
		$this->load->view("accounting/journalMemorial", $data);	
	}

	public function depreciationlist()
	{
		$this->load->model("Depreciation_model");
		$type = $this->uri->segment(3);
		if($type == "add")
		{
			$lastid = $this->Depreciation_model->insert();
			echo json_encode(array("status"=>($lastid > 0) ? 1: 0, "msg"=>"Something Error", "csrf_name"=>$this->security->get_csrf_hash()));
		}
		else if($type == "update")
		{

		}
		else if($type == "setjournal")
		{
			//$id = $this->uri->segment(4);
			$id = $this->input->post('idjournal');
			$ok = $this->Depreciation_model->setJournal($id);
			echo json_encode(array('status' => $ok, "msg"=>"Something Error", "csrf_name"=>$this->security->get_csrf_hash()));
		}
		else if($type == "changeLocation")
		{
			$id = $this->uri->segment(4);
			$ok = $this->Depreciation_model->changeLocation($id);
			echo json_encode(array('status' => $ok, "msg"=>"Something Error", "csrf_name"=>$this->security->get_csrf_hash()));
		}
		else if($type == "setadjustment")
		{
			$id = $this->uri->segment(4);
			$update = $this->Depreciation_model->setAdjustment($id);
			echo json_encode(array("status"=>($update > 0) ? 1: 0, "msg"=>"Something Error", "csrf_name"=>$this->security->get_csrf_hash()));
		}
		else if($type == "show")
		{
			$data['rows'] = $this->Depreciation_model->read();

			echo json_encode($data);
		}
		else
		{
			$this->load->view("accounting/depreciationlist");		
		}
		
	}



	public function getDepreciationNumber()
	{
		$akun = $this->input->get("akun");
		echo $this->Finance_model->getCountDepreciation($akun);
	}

	public function showCoa()
	{
		$q = isset($_GET['q']) ? strval($_GET['q']) : '';
		echo $this->Finance_model->getCoa($q);
	}

	public function showCoaBalance()
	{
		$q = isset($_GET['q']) ? strval($_GET['q']) : '';
		echo $this->Finance_model->getCoaBalance($q);
	}

	private function has_child($id){
		$this->db->where("parent", $id);
		$this->db->from('menu_modul');
		$count = $this->db->count_all_results();
		return $count > 0 ? true : false;
	}
	
	public function voucherReceive()
	{
		$akun = $this->uri->segment(3);
		$data['title'] = "Receive Voucher";
		$data['noakun'] = $akun;
		$data['invoice'] = "RV".$akun.date('ym').$this->Finance_model->getCountKas($akun);
		$this->load->view("accounting/paymentvoucher", $data);
	}

	public function getPaymentVoucher()
	{
		$novoc = $this->uri->segment(3);
		$type = $this->uri->segment(4); // 1. PV, 2. RV, 3. empty(kosong)
		$coaaccount = $this->uri->segment(5); // 1. 
		$totalsegment = $this->uri->total_segments();

		if($totalsegment == 5)
		{
			$kas = $this->db->get_where("acc_gl_journal", array("voucherno"=>$novoc, "isposting"=>false, "coaccount"=>$coaaccount))->row();
		}
		else
		{
			$kas = $this->db->get_where("acc_gl_journal", array("voucherno"=>$novoc, "isposting"=>false))->row();	
		}
		
		$detail = array();
		if(isset($kas))
		{
			$this->db->select("acc_gl_journal_detail.*, acc_coa.name as namaakun");
			$this->db->where("acc_gl_journal_detail.kasid", $kas->id);
			$this->db->join("acc_coa", "acc_coa.subcode=acc_gl_journal_detail.accountno", "left");
			$rs = $this->db->get("acc_gl_journal_detail");
			$i = 0;
			foreach($rs->result() as $key)
			{
				if(($type == "PV") && ($key->typedk == "D"))
				{
					$detail[] = array("description"=>$key->description, "amount"=>$key->amount, "akun"=>$key->accountno, "namaakun"=>$key->namaakun, "id"=>$key->id, "type"=>$key->typedk);
					$i++;	
				}
				else if(($type == "RV") && ($key->typedk == "K"))
				{
					$detail[] = array("description"=>$key->description, "amount"=>$key->amount, "akun"=>$key->accountno, "namaakun"=>$key->namaakun, "id"=>$key->id, "type"=>$key->typedk);
					$i++;	
				}
				else if($type == "")
				{
					$detail[] = array("description"=>$key->description, "amount"=>$key->amount, "akun"=>$key->accountno, "namaakun"=>$key->namaakun, "id"=>$key->id, "type"=>$key->typedk);
					$i++;	
				}
				

			}

			for($j=0; $j < (10-$i); $j++)
			{
				$detail[] = array("description"=>"", "amount"=>0, "akun"=>"", "namaakun"=>"", "id"=>"", "type"=>"");
			}
		}

		$data['kas'] = $kas;
		$data['detail'] = $detail;
		$data['tseg'] = $totalsegment;
		echo json_encode($data);
	}

	public function savePaymentVoucher()
	{
		$novoucher = $this->input->post("novoucher");
		$tanggal = $this->input->post("tanggal");
		$noaccount = $this->input->post("noaccount");
		$rows = $this->input->post("rows");
		$items = json_decode($rows);

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$this->db->set("postingdate", $tanggal);
		$this->db->set("isposting", true);
		$this->db->set("postingby", $this->session->userdata("username"));
		$this->db->where("voucherno", $novoucher);
		$this->db->update("acc_gl_journal");

		for($i=0; $i < count($items); $i++)
		{
			$this->Finance_model->updateBalanceCoaV1($items[$i]->type, $items[$i]->amount, $items[$i]->akun, $noaccount);
			$this->db->query("update acc_gl_journal_detail set accountno='".$items[$i]->akun."' where id=".$items[$i]->id);
		}
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
				echo json_encode(array('status'=>0,'msg'=>'Failed', 'po_number'=>$novoucher, 'csrf_name'=>$this->security->get_csrf_hash()));
		}
		else
			echo json_encode(array('status'=>1,'msg'=>'Successfully', 'po_number'=>$novoucher, 'csrf_name'=>$this->security->get_csrf_hash()));
	}


	public function saveMemorialJournal()
	{
		$typesegment = $this->uri->segment(3);

		$supplierid = $this->input->post("supplierid");
		$remark = $this->input->post("remark");
		$payto = $this->input->post("payto");
		$bilyet = $this->input->post("bilyet");
		$noaccount  = $this->input->post("noaccount");
		$novoucher = $this->input->post("novoucher");
		$tanggal = $this->input->post("tanggal");
		$locationcode = $this->input->post("locationcode");
		$type = $this->input->post("type");
		$srcpv = $this->input->post("srcpv");
		$rows = $this->input->post("rows");

		$items = json_decode($rows);

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		if($typesegment == "update")
		{
			$srcpvid = $this->db->query("select id from acc_gl_journal where voucherno='".$srcpv."'")->row()->id;
			$delete = $this->db->query("delete from acc_gl_journal where id=".$srcpvid);
			$deldetail = $this->db->query("delete from acc_gl_journal_detail where kasid=".$srcpvid);

			$novoucher = $srcpv;
		}


		$this->db->set("createdate", $tanggal);
		$this->db->set("voucherno", $novoucher);
		$this->db->set("remark", $remark);
		$this->db->set("supplierid", $supplierid);
		$this->db->set("ceknumber", $bilyet);
		$this->db->set("coaccount", $noaccount);
		$this->db->set("status", 0);
		$this->db->set("paymentto", $payto);
		$this->db->set("type", $type);
		$this->db->set("locationcode", $locationcode);
		$this->db->set("balance", "(select balance from acc_coa where subcode='".$noaccount."')", false);
		$this->db->set("companyid", $this->session->userdata("compid"));
		$this->db->set("createby", $this->session->userdata("name"));
		$this->db->insert("acc_gl_journal");
		$id = $this->db->insert_id();


		$in_item = array();
		$balance = 0;
		
		for($i=0; $i < count($items); $i++)
		{
			//$this->db->query("update acc_coa set balance = balance + ".$items[$i]->amount ." where subcode='".$items[$i]->akun."'");
			$item = array();
			$item['kasid'] = $id;
			$item['description'] = $items[$i]->description;
			$item['typedk'] = $items[$i]->type;
			$item['amount'] = $items[$i]->amount;
			$item['accountno'] = $items[$i]->akun;
			$in_item[] = $item;
		}
		
		$this->db->insert_batch('acc_gl_journal_detail',$in_item);

		$this->db->trans_complete();

		

		if ($this->db->trans_status() === FALSE)
		{
				echo json_encode(array('status'=>0,'msg'=>'Failed',"csrf_name"=>$this->security->get_csrf_hash(), 'po_number'=>$novoucher));
		}
		else
			echo json_encode(array('status'=>1,'msg'=>'Successfully', "csrf_name"=>$this->security->get_csrf_hash(), 'po_number'=>$novoucher));
	}

	public function getGeneralLedger()
	{
		
		$yrmin = $this->input->post("yrmin");
		$mthmin = $this->input->post("mthmin");
		$yrmax = $this->input->post("yrmax");
		$mthmax = $this->input->post("mthmax");
		$startdate = $this->input->post("startdate");
		$enddate = $this->input->post("enddate");
		/*
		$yrmin = "2018";
		$mthmin = "08";
		$yrmax = "2018";
		$mthmax = "08";
		$startdate = "2018-08";
		$enddate = "2018-08";
		*/
		$cdate = strtotime($startdate."-01")-1; // di convert tanggal 01 => 2018-07-01
		$newformat = date('Y-m-d',$cdate);
		
		//if(!isset($yrmin) && !isset($mthmin)) show_error("User Not Allowed","Please Ask to administrator");

		//$prevym = date('Y-m',$cdate);
		$prevym = date('Y',$cdate)-1;

		$tmpakun = array();
		$tmpsaldo = 0;
		//agc.period = '".$prevym."'
		$rsprev = $this->db->query("select agc.coacode, agc.begin_balance_db, aco.name as namaakun from acc_gl_balance agc left join acc_coa aco on agc.coacode=aco.subcode where extract(YEAR from createdate)='".$prevym."' order by agc.coacode ASC");
		//echo $this->db->last_query();
		foreach ($rsprev->result() as $key) {
			if(!empty($key->coacode))
			{
				$tmpakun[$key->coacode][] = array(
					"description"=>"", 
					"typedk"=>"D",
					"locationcode"=>"",
					"amount"=>$key->begin_balance_db, 
					"novoucher"=>"PREV", 
					"tanggal"=>$startdate,
					"namaakun"=>$key->namaakun,
					"akun"=>$key->coacode,
					"saldo"=>0
				);
			}
			
		}
		$this->db->select("to_char(aks.createdate, 'YYYY-MM-DD') as createdate, to_char(aks.createdate, 'YYYY-MM') as period, aks.voucherno, aks.coaccount, akd.description, akd.amount, akd.typedk, akd.accountno, aco.name as namaakun, aks.locationcode");
		$this->db->join("acc_gl_journal_detail akd", "akd.kasid=aks.id", "left");
		$this->db->join("acc_coa aco", "akd.accountno=aco.subcode", "left");
		$this->db->from("acc_gl_journal aks");
		
		$this->db->where("date_part('year', createdate) >= '".$yrmin."'", '', false);
		$this->db->where("date_part('month', createdate) >= '".$mthmin."'", '', false);
		$this->db->where("date_part('year', createdate) <= '".$yrmax."'", '', false);
		$this->db->where("date_part('month', createdate) <= '".$mthmax."'", '', false);
		/*
		$this->db->where("date_part('year', createdate) >= '2018'", '', false);
		$this->db->where("date_part('month', createdate) >= '06'", '', false);
		$this->db->where("date_part('year', createdate) <= '2018'", '', false);
		$this->db->where("date_part('month', createdate) <= '06'", '', false);
		*/
		$this->db->where("isposting", true);
		$this->db->order_by("aco.subcode", "ASC");
		$rs = $this->db->get();
		//echo $this->db->last_query();
		
		
		foreach($rs->result() as $row)
		{
			
			$tmpakun[$row->accountno][] = array(
				"description"=>$row->description, 
				"typedk"=>$row->typedk, 
				"amount"=>$row->amount, 
				"novoucher"=>$row->voucherno, 
				"tanggal"=>$row->createdate,
				"namaakun"=>$row->namaakun,
				"akun"=>$row->accountno,
				"locationcode"=>$row->locationcode,
				"saldo"=>0
			);
			
		}
		//print_r($tmpakun);
		//exit();
		$akun = array();
		foreach($tmpakun as $coa=>$arperiod)
		{
			//$tmpsaldo = $this->Finance_model->getPrevBalance((string)$coa, $period);
			$saldo = 0;
			$tmpsaldo = 0;
			$prevsaldo = 0;
			$locationcode = "";
			$totaldb = 0;
			$totalkr = 0;
			for($i=0;$i < count($arperiod); $i++)
			{
				if($arperiod[$i]['novoucher'] == 'PREV'){
					$prevsaldo = $arperiod[$i]['amount'];
					$tmpsaldo = $prevsaldo;
				}
				
				$amt_debet = 0;
				$amt_kredit = 0;
				if($arperiod[$i]['typedk'] == "D") 
					$amt_debet = ($arperiod[$i]['novoucher'] == 'PREV') ? 0 : $arperiod[$i]['amount'];
				else if($arperiod[$i]['typedk'] == "K")
					$amt_kredit = ($arperiod[$i]['novoucher'] == 'PREV') ? 0 : $arperiod[$i]['amount'];

				$saldo = ($tmpsaldo + $amt_debet ) - $amt_kredit;

				$akun[] = array(
					"description"=>$arperiod[$i]['description'], 
					"debet"=>number_format($amt_debet), 
					"kredit"=>number_format($amt_kredit), 
					"novoucher"=>$arperiod[$i]['novoucher'], 
					"tanggal"=>$arperiod[$i]['tanggal'],
					"namaakun"=>$arperiod[$i]['namaakun'],
					"akun"=>$arperiod[$i]['akun'],
					"saldo"=>number_format($saldo),
					"beginbalance"=>number_format($prevsaldo)
				);
				$locationcode = $arperiod[$i]['locationcode'];
				$tmpsaldo = $saldo;
				$totaldb += $amt_debet;
				$totalkr += $amt_kredit;
				
			}
			
			if($startdate == $enddate) $this->Finance_model->updatePrevBalance((string)$coa, $startdate, $tmpsaldo, $totaldb, $totalkr, $locationcode);

		}
		//print_r($akun);
		//exit();
		
		$json['status'] = 1;
		$json['csrf_name'] = $this->security->get_csrf_hash();
		$json['rows'] = $akun;
		$json['footer'] = array(
				array(
					"description"=>"", 
					"debet"=>"", 
					"kredit"=>"", 
					"novoucher"=>"", 
					"tanggal"=>"",
					"namaakun"=>"",
					"akun"=>"",
					"saldo"=>0)
			);
		echo json_encode($json);
		
		//echo $this->db->last_query();
		
	}


	


	public function addNewCoa()
	{
		$code = $this->input->post("code");
		$subcode = $this->input->post("subcode");
		$name = $this->input->post("name");
		$typecode = $this->input->post("typecode");

		$type = $this->uri->segment(3); //update/new/updateBalance

		//if(empty($name)) show_error("User Not Allowed","Please Ask to administrator");

		if($type == "update")
		{
			$id = $this->uri->segment(4);
			$this->db->set("name", $name);
			$this->db->set("code", $code);
			$this->db->set("subcode", $subcode);
			$this->db->set("typecode", $typecode);
			$this->db->where("id", $id);
			$this->db->update("acc_coa");
		}
		else if($type == "updateBalance")
		{
			
			
			$balance = $this->input->post("balance");
			$tanggal = $this->input->post("tanggal");
			$id = $this->uri->segment(4);
			
			$this->db->set("saldoawal", $balance);
			$this->db->where("id", $id);
			$this->db->update("acc_coa");
			
			$row = $this->db->get_where("acc_gl_balance", array("period"=>substr($tanggal, 0, 7), "coacode"=>$subcode));

			$this->db->set("locationcode", 'HO');
			$this->db->set("period", substr($tanggal, 0, 7));
			$this->db->set("createdate", $tanggal);
			$this->db->set("coacode", $subcode);
			$this->db->set("begin_balance_db", $balance);
			if(isset($row))
			{
				$this->db->where("coacode", $subcode);
				$this->db->where("period", substr($tanggal, 0, 7));
				$this->db->update("acc_gl_balance");
			}
			else
			{
				$this->db->insert("acc_gl_balance");	
			}
			
				
		}
		else
		{
			$this->db->set("name", $name);
			$this->db->set("code", $code);
			$this->db->set("subcode", $subcode);
			$this->db->set("typecode", $typecode);

			$this->db->insert("acc_coa");
		}
		

		$json['status'] = 1;
		$json['msg'] = "Successfully";
		$json['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($json);
	}

	public function getVoucherPending()
	{
		$this->db->where("isposting", false);
		$rs = $this->db->get("acc_gl_journal");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		echo json_encode($data);
	}


	public  function tbalance()
	{
		echo $this->Finance_model->getLastBalanceOfCoa('2018', '06', '2018', '06', '112', '');
	}

	

	public function showTrialBalance()
	{
		$yrmin = $this->input->post("yrmin");
		$mthmin = $this->input->post("mthmin");
		$yrmax = $this->input->post("yrmax");
		$mthmax = $this->input->post("mthmax");
		$period = $yrmin."-".$mthmin;
		//if(!isset($yrmin) && !isset($mthmin)) show_error("User Not Allowed","Please Ask to administrator");
		$this->db->select("agc.coacode, sum(agc.begin_balance_db) as begin_balance_db, sum(agc.totaldb) as totaldb, sum(agc.totalkr) as totalkr, aco.name as namaakun");
		$this->db->join("acc_coa aco", "agc.coacode=aco.subcode", "left");
		$this->db->from("acc_gl_balance agc");

		$this->db->where("substring(agc.period from 1 for 4) >= '".$yrmin."'", '', false);
		$this->db->where("substring(agc.period from 6 for 7) >= '".$mthmin."'", '', false);
		$this->db->where("substring(agc.period from 1 for 4) <= '".$yrmax."'", '', false);
		$this->db->where("substring(agc.period from 6 for 7) <= '".$mthmax."'", '', false);
		/*
		$this->db->where("date_part('year', createdate) >= '2018'", '', false);
		$this->db->where("date_part('month', createdate) >= '06'", '', false);
		$this->db->where("date_part('year', createdate) <= '2018'", '', false);
		$this->db->where("date_part('month', createdate) <= '06'", '', false);
		*/
		//$this->db->where("isposting", true);
		$this->db->group_by("agc.coacode, namaakun");
		$this->db->order_by("agc.coacode", "ASC");
		$rs = $this->db->get();
		
		$akun = array();
		$tmpsaldo = 0;
		$sumprevbalance = 0;
		$sumdebet = 0;
		$sumkredit = 0;
		$sumbalance = 0;
		$prevbalance = 0;
		foreach($rs->result() as $row)
		{
			$prevbalance = $this->Finance_model->getPrevBalance($row->coacode, $yrmin);
			$saldo = ($prevbalance + $row->totaldb) - $row->totalkr;
			if($row->coacode != "31201") //saldo laba berjalan
			{
				$akun[] = array(
					"description"=>$row->namaakun, 
					"debet"=>number_format($row->totaldb), 
					"kredit"=>number_format($row->totalkr), 
					"akun"=>$row->coacode,
					"saldo"=>number_format($saldo),
					"prevbalance"=>number_format($prevbalance)
				);

				$this->Finance_model->updateLastBalYearofCoa($row->coacode, $yrmin, $saldo);

				$sumprevbalance += $prevbalance;
				$sumdebet += $row->totaldb;
				$sumkredit += $row->totalkr;
				$sumbalance += $saldo;	
			}
			
		}
		

		
		//print_r($akun);
		$json['status'] = 1;
		$json['csrf_name'] = $this->security->get_csrf_hash();
		$json['rows'] = $akun;
		$json['footer'] = array(
			array(
				"debet"=>number_format($sumdebet), 
				"kredit"=>number_format($sumkredit), 
				"saldo"=>number_format($sumbalance),
				"prevbalance"=>number_format($sumprevbalance)
			)
		);
		echo json_encode($json);
	}
	
	public function showBalanceSheet()
	{
		// aktiva kepala 1
		// kewajiba kepala 2
		// modal kepala 3

		$yrmin = $this->input->post("yrmin");
		$mthmin = $this->input->post("mthmin");
		$yrmax = $this->input->post("yrmax");
		$mthmax = $this->input->post("mthmax");
		
		//if(!isset($yrmin) && !isset($mthmin)) show_error("User Not Allowed","Please Ask to administrator");

		$this->db->select("aco.name as namaakun, aco.subcode, aco.code");
		$this->db->from("acc_coa aco");
		$this->db->where("typecode","B/S");
		$this->db->order_by("length(aco.subcode), aco.subcode", "ASC");
		$rs = $this->db->get();
		
		$tmpakun = array();
		$maxperiod = $yrmax."-".$mthmax;
		$minperiod = $yrmin."-".$mthmin;
		$aktivabalance = 0;
		$modalkewajiban = 0;
		foreach($rs->result() as $row)
		{
			$digit1 = substr($row->subcode, 0,1);
			$digit2 = substr($row->subcode, 0,2);
			if(strlen($row->subcode) == 1){
				$tmpakun[] = array("id"=>$row->subcode, "namaakun"=>$row->namaakun);
			}
			else if(strlen($row->subcode) == 2){
				$tmpakun[] = array("id"=>$row->subcode, "namaakun"=>$row->namaakun, "_parentId"=>$digit1);
			}
			else if(strlen($row->subcode) == 3){
				
				$balanceprevyear = $this->Finance_model->getPrevYearBalance($row->subcode, ($yrmin - 1));
				$balance = $this->Finance_model->getLastBalanceOfCoa($yrmin, $mthmin, $yrmax, $mthmax, $row->subcode, '');
				$balance += $balanceprevyear;
				
				if($row->subcode == "311") // laba tahun lalu
				{
					$balance = $this->Finance_model->getPrevYearBalance($row->subcode, ($yrmin - 1));
				}
				elseif($row->subcode == "312") // laba tahun berjalan
				{
					//$balance = $this->Finance_model->getCurrMthBalance($row->subcode, $minperiod, $maxperiod);
					$balance = $this->Finance_model->getPrevYearBalance($row->subcode, $yrmin);
					//echo "Balance " . $balance;
				}
				elseif($row->subcode == "124") // Akum Depr. Penys Gedung & Gudang Kantor
				{
					$balance = $this->Finance_model->getCurrMthBalance($row->subcode, $minperiod, $maxperiod);
				}
				elseif($row->subcode == "125") // Akumulasi penyusutan lainnya
				{
					$balance = $this->Finance_model->getCurrMthBalance($row->subcode, $minperiod, $maxperiod);
					
				}
				$tmpakun[] = array("id"=>$row->subcode, "namaakun"=>$row->namaakun, "balance"=>number_format($balance), "_parentId"=>$digit2);

				if($digit1 == "1")
				{
					$aktivabalance += $balance;
					//echo " --> " .$row->subcode."--".$balance."\n";
				}
				else
				{
					$modalkewajiban += $balance;
					//echo " --> " .$row->subcode."--".$balance."\n";	
				}
			}
			else if(strlen($row->subcode) == 5){
				$digit1 = substr($row->subcode, 0,1);
				$digit2 = substr($row->subcode, 0,2);

			}
			
			
		}
		//echo " --> " .$aktivabalance."\n";
		$tmpakun[] = array("id"=>0, "namaakun"=>"Total AKTIVA", "balance"=>number_format($aktivabalance), "_parentId"=>1);
		$tmpakun[] = array("id"=>0, "namaakun"=>"Total KEWAJIBAN & MODAL", "balance"=>number_format($modalkewajiban), "_parentId"=>3);
		//print_r($tmpakun);
		$json['status'] = 1;
		$json['csrf_name'] = $this->security->get_csrf_hash();
		$json['rows'] = $tmpakun;
		
		
		echo json_encode($json);
	}


	public function showProfitandLost()
	{
		// aktiva kepala 1
		// kewajiba kepala 2
		// modal kepala 3
		$yrmin = $this->input->post("yrmin");
		$mthmin = $this->input->post("mthmin");
		$yrmax = $this->input->post("yrmax");
		$mthmax = $this->input->post("mthmax");
		$startdate = $this->input->post("startdate");
		$enddate = $this->input->post("enddate");
		/*
		$yrmin = "2018";///$this->input->post("yrmin");
		$mthmin = "07";//$this->input->post("mthmin");
		$yrmax = "2018";//$this->input->post("yrmax");
		$mthmax = "07";//$this->input->post("mthmax");
		*/
		if(!isset($yrmin) && !isset($mthmin)) show_error("User Not Allowed","Please Ask to administrator");

		$this->db->select("aco.name as namaakun, aco.subcode, aco.code, aco.hitung");
		$this->db->from("acc_coa aco");
		$this->db->where("typecode","P/L");
		$this->db->order_by("length(aco.subcode), aco.subcode", "ASC");
		$rs = $this->db->get();
		
		$tmpakun = array();
		
		$aktivabalance = 0;
		$modalkewajiban = 0;

		$A51 = $this->Finance_model->getPrevBalance("11601", $yrmin."-".$mthmin);
		$B51 = $this->Finance_model->getPrevBalance("11602", $yrmin."-".$mthmin);
		$C51 = $this->Finance_model->getLastBalanceOfCoa($yrmin, $mthmin, $yrmax, $mthmax, "11601", "", "SS");

		$hpp = $this->Finance_model->getLastBalanceOfCoa($yrmin, $mthmin, $yrmax, $mthmax, "51001", "", "");
		$D51 = ($A51 + $B51 + $C51);
		//echo " A51: ".$A51." D51 ".$D51;
		//A51: 1300000 D51 5300000
		$E51 = $D51 - $hpp;
		$F51 = 0; // Persediaan Akhir Konsinyasi
		
		$cust_field_persediaan = array(
			array("subcode"=>"51A", "name"=>"Persediaan Awal WH", "hitung"=>""),
			array("subcode"=>"51B", "name"=>"Persediaan Awal Konsinyasi", "hitung"=>""),
			array("subcode"=>"51C", "name"=>"Pembelian", "hitung"=>""),
			array("subcode"=>"51D", "name"=>"Barang Tersedia Dijual", "hitung"=>"+"),
			array("subcode"=>"51E", "name"=>"Persediaan Akhir WH", "hitung"=>"-"),
			array("subcode"=>"51F", "name"=>"Persediaan Akhir Konsinyasi", "hitung"=>"-")
		);

		$fdata = array();
		$tdata = array();
		$thdata = array();

		foreach ($cust_field_persediaan as $key => $value) {
			$subcode = $value['subcode'];
			$thdata[51][$subcode] = array("nama"=>$value['name'], "hitung"=>$value['hitung']);	
		}

		foreach($rs->result() as $row)
		{
			$digit1 = substr($row->subcode, 0,1);
			$digit2 = substr($row->subcode, 0,2);
			if(strlen($row->subcode) == 2)
			{
				$tdata[$digit1][$digit2] = $row->namaakun;
			}
			else if(strlen($row->subcode) == 3)
			{
				$thdata[$digit2][$row->subcode] = array("nama"=>$row->namaakun, "hitung"=>$row->hitung);
			}

			if(strlen($row->subcode) == 1) 
			{
				$fdata[$row->subcode] = $row->namaakun;
			}
		}
		
		$totalProfit = 0;
		$labasebelumpajak = 0;
		$labasetelahpajak = 0;
		//print_r($fdata);
		//print_r($tdata);
		foreach($fdata as $p1=>$p1name)
		{
			$tmpakun[] = array("id"=>$p1, "namaakun"=>$p1name, "type"=>"");
			
			foreach($tdata[$p1] as $p2=>$p2name)
			{
				$tmpakun[] = array("id"=>$p2, "namaakun"=>$p2name, "_parentId"=>$p1, "type"=>"");
				$totalAmount = 0;
				foreach($thdata[$p2] as $p3=>$p3name)
				{
					if(!is_numeric(substr($p3, -1))){ // artinya bukan COA $A51
						$var = substr($p3, -1) . substr($p3, 0,2);
						$balance = $$var;
					}
					else
					{
						$balance = $this->Finance_model->getLastBalanceOfCoa($yrmin, $mthmin, $yrmax, $mthmax, $p3, '');
					}
					$tmpakun[] = array("id"=>$p3, "namaakun"=>$p3name['nama'], "balance"=>number_format($balance), "_parentId"=>$p2, "type"=>"");
					if($p3name['hitung'] == "+")
						$totalAmount += $balance;
					else if($p3name['hitung'] == "-")
						$totalAmount -= $balance;
				}

				switch ($p2) {
					case 41: //penjualan
						$tmpakun[] = array("id"=>$p2, "namaakun"=>"PENJUALAN NET", "balance"=>number_format($totalAmount), "_parentId"=>$p1, "type"=>"TOTAL");
						$totalProfit += $totalAmount;

						break;
					case 51: //Harga Pokok Penjualan
						$totalProfit -= $totalAmount;
						//$tmpakun[] = array("id"=>$p2, "namaakun"=>"#TOTAL NET", "balance"=>number_format($totalAmount), "_parentId"=>$p1, "type"=>"TOTAL");
						$tmpakun[] = array("id"=>$p2, "namaakun"=>"LABA SEBELUM BIAYA", "balance"=>number_format($totalProfit), "_parentId"=>$p1, "type"=>"TOTAL");
						$labasebelumpajak  += $totalProfit;
						break;
					case 61: //Beban Operasional
						$tmpakun[] = array("id"=>$p2, "namaakun"=>"TOTAL BEBAN OPERASIONAL", "balance"=>number_format($totalAmount), "_parentId"=>$p1, "type"=>"TOTAL");
						$labasebelumpajak  -= $totalAmount;
						# code...
						break;
					case 71: //Beban & Pendapatan Lain-lain
						$labasebelumpajak  -= $totalAmount;
						$tmpakun[] = array("id"=>$p2, "namaakun"=>"TOTAL BEBAN LAIN - LAIN", "balance"=>number_format($totalAmount), "_parentId"=>$p1, "type"=>"TOTAL");
						$tmpakun[] = array("id"=>$p2, "namaakun"=>"LABA SEBELUM PAJAK", "balance"=>number_format($labasebelumpajak), "_parentId"=>$p1, "type"=>"TOTAL");
						$labasetelahpajak += $labasebelumpajak;
						# code...
						break;
					case 81: //Beban Pajak
						$labasetelahpajak -= $totalAmount;
						$tmpakun[] = array("id"=>$p2, "namaakun"=>"TOTAL BEBAN PAJAK", "balance"=>number_format($totalAmount), "_parentId"=>$p1, "type"=>"TOTAL");
						$tmpakun[] = array("id"=>$p2, "namaakun"=>"LABA SETELAH PAJAK", "balance"=>number_format($labasetelahpajak), "_parentId"=>$p1, "type"=>"TOTAL");
						if($startdate == $enddate){
							$this->Finance_model->updatePrevBalance("31201", $enddate, $labasetelahpajak, 0, 0);
						}
						# code...
						break;
					default:
						# code...
						break;
				}
			}
		}

		
		$json['status'] = 1;
		$json['csrf_name'] = $this->security->get_csrf_hash();
		$json['rows'] = $tmpakun;
		
		
		echo json_encode($json);
	}
}