<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transferout extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Public_model");
	}


	// set to cronjob
	// inquiry yesterday receive
	// step 
	/*
	1. check transfernote where when datereceived yesterday
	2. insert into movement io
	3. update stock qty in stockbalance
	4. insert ke productionumber
	*/
	public function updateTransferReceived()
	{
		$time_start = microtime(true);
		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$this->db->where("transfernotestatus", 2);
		$this->db->where("receivedate", "CURRENT_DATE-1", false); // artinya kemarin
		$rs = $this->db->get("transfernote");
		foreach($rs->result() as $rw)
		{
			$this->db->query("insert into productionumber (transfernotenumber, lastupdate, fromlocation, uom) values ('".$rw->transfernotenumber."', CURRENT_DATE, ".$rw->fromlocation.", 'BOX')");
			$piolastid = $this->db->insert_id();

			$transfernoteid = $rw->transfernoteid;
			$items = $this->db->query('select * from transfernoteitem where transfernote='.$transfernoteid);
			$mlog = array();
			$pioitem = array();

			foreach($items->result() as $row)
			{
				//update transferitem
				$this->db->query("update transfernoteitem set receiveqty=transferqty where transfernote=".$transfernoteid);
				$this->db->query("update stockbalance set totalquantity=(totalquantity + ".$row->transferqty.") WHERE location=".$rw->tolocation." AND productitem=".$row->productitem);
				// tidak di update qty karena dari pos sudah berkurang qty dan yang di update hanya intransit, akan balik bila di void
				$this->db->query("update stockbalance set intransit=(intransit + ".$row->transferqty.") WHERE location=".$rw->fromlocation." AND productitem=".$row->productitem);


				$mlog[] = array('locationid'=>$rw->tolocation, 'movementdate'=>date('Y-m-d H:i:s'), 'movement_type'=>'I', 'reffno'=>$rw->transfernotenumber, 'productitem'=>$row->productitem, 'movement_qty'=>$row->transferqty, 'movement_value'=>0, 'createduser'=>$rw->receiveby, 'tofromlocation'=>$rw->tolocation, 'movement_qtybalance'=>0);
				$mlog[] = array('locationid'=>$rw->fromlocation, 'movementdate'=>date('Y-m-d H:i:s'), 'movement_type'=>'O', 'reffno'=>$rw->transfernotenumber, 'productitem'=>$row->productitem, 'movement_qty'=>$row->transferqty, 'movement_value'=>0, 'createduser'=>$rw->raiseby, 'tofromlocation'=>$rw->fromlocation, 'movement_qtybalance'=>0);

				$pioitem[] = array("prodioid"=>$piolastid, "productitem"=>$row->productitem, "qty"=>$row->transferqty);
			}
			$this->db->insert_batch('inventory_movement',$mlog);
			$this->db->insert_batch('productionumberdetail',$pioitem);
		}

		$this->db->trans_complete();
		$time_end = microtime(true);
    	$time = $time_end - $time_start;
		echo json_encode(array('status'=>1, 'csrf_name'=>$this->security->get_csrf_hash(), 'time'=>$time));
	}

}