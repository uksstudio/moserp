<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

	protected $_items_manual_trans = array();
	protected $_items_manual_trans_string = 'manual_item_trans';

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Public_model");
		$this->load->model("Inventory_model");
		$this->_items_manual_trans = $this->session->userdata($this->_items_manual_trans_string);
	}

	public function postingCosting()
	{
		$data["json_currency"] = $this->Public_model->getCurrency();
		$this->load->view("inventory/postingCosting_view", $data);
	}

	public function stockReceipt()
	{
		$this->load->view("inventory/stockIreceipt");
	}

	public function stkReport()
	{
		$this->load->view("inventory/stkreport_view");
	}

	public function stkReportStatus()
	{
		$this->load->view("inventory/stkstockstatus_view");
	}


	public function cartonTransfer()
	{
		$this->load->view("inventory/cartontransfer_view");
	}

	public function partialTransfer()
	{
		$this->load->view("inventory/partialtransfer_view");
	}


	public function stkMovement()
	{
		$this->load->view("inventory/stkmovement_view");
	}

	public function transferReceiveWh()
	{
		$this->load->view("inventory/receiveTransferwh");	
	}


	public function whStoreReport()
	{
		$this->load->view("inventory/whstorereport");
	}

	public function getMovementStock()
	{
		$productcode = $this->input->post('productcode');
		$stocksroom = $this->input->post('stocksroom');
		$page = $this->input->post('page');
		$rows = $this->input->post('rows');

		$offset = ($page-1)*$rows;

		$items = array();
		$gosql = false;
		if(!empty($productcode))
		{
			$gosql = true;
			$this->db->where('productitemcode', $productcode);
		}

		if(!empty($stocksroom))
		{
			$this->db->where('locationcode', $stocksroom);
		}

		if($gosql)
		{
			$this->db->order_by('inmoid', 'ASC');
			$rs = $this->db->get('inventory_movement');
			$this->db->limit($rows, $offset);

			foreach($rs->result() as $row)
			{
				array_push($items, $row);
			}
		}

		$data['rows'] = $items;
		$data['sql'] = $this->db->last_query();
		$data['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($data);
	}

	public function getProductStock()
	{
		$floc = $this->uri->segment(3);
		$tloc = $this->uri->segment(4);
		$itemcode = $this->uri->segment(5); // itemcode

		if($floc == "" && $tloc == "")
		{
			$floc = $this->input->get("floc");
			$tloc = $this->input->get("tloc");
			$itemcode = $this->input->get('itemcode'); // itemcode			
		}

		$sql = "select productitem.*, stockbalance.totalquantity, stockbalance.intransit from productitem left join stockbalance on productitem.productitemid=stockbalance.productitem where substr(productitem.productcode, 1, ".strlen($itemcode).")='".$itemcode."' AND stockbalance.locationcode='".$floc."'";
		$rs = $this->db->query($sql);
		$data = array();
		foreach($rs->result() as $row)
		{
			$data[] = array('productitemid'=>$row->productitemid, 'productcode'=>$row->productcode, 'size'=>$row->size, 'qty'=>$row->totalquantity, 'name'=>$row->size, 'value'=>($row->totalquantity - $row->intransit), 'qty'=>0);
		}
		echo json_encode($data);
		
	}

	public function getStockByItemcode()
	{
		$floc = $this->input->get("floc");
		$tloc = $this->input->get("tloc");
		$itemcode = $this->input->get('itemcode'); // itemcode	

		$sql = "select productitem.*, stockbalance.totalquantity, stockbalance.retailsalesprice, stockbalance.currentcost, (select productseasoncode from productseason where productseasonid=productitem.productseason) as pseason from productitem left join stockbalance on productitem.productitemid=stockbalance.productitem where productitem.productcode = '".$itemcode."' AND stockbalance.locationcode='".$floc."'";
		$rs = $this->db->query($sql);
		$data = array();
		foreach($rs->result() as $row)
		{
			$rowid = md5($row->productcode);
			$iscart = array();
			
			$iscart['rowid'] = $rowid;
			$iscart['productitemid'] = $row->productitemid;
			$iscart['currentcost'] = $row->currentcost;
			$iscart['itemcode'] = $row->productcode;
			$iscart['label'] = $row->productgroup;
			$iscart['cate'] = $row->productcategory;
			$iscart['gender'] = $row->sex;
			$iscart['color'] = $row->descline2;
			$iscart['fabric'] = $row->productitemdesc;
			$iscart['fabric_dec'] = $row->productitemdesc;
			$iscart['style_desc'] = $row->productitemdesc;
			$iscart['retailsalesprice'] = $row->retailsalesprice;
			$iscart['season'] = $row->pseason;
			$iscart['rqty'] = 0;
			$iscart['qty'] = $row->totalquantity;
			$data[] = $iscart;
			$this->_items_manual_trans[$rowid] = $iscart;
		}
		$this->session->set_userdata(array('manual_item_trans'=>$this->_items_manual_trans));
		$result['rowid'] = true;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		$result['data'] = $data;
		echo json_encode($result);
	}


	public function updateStockPartial()
	{
		$rowid = $this->input->get("rowid");
		$rqty = $this->input->get("rqty");

		$this->_items_manual_trans[$rowid]['rqty'] = $rqty;


		// masukkan data baru ke session
		$this->session->set_userdata(array('manual_item_trans'=>$this->_items_manual_trans));
		$result['rowid'] = $rowid;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}

	//
	//Inventory/getProductStockByCarton/flocation/tlocation/cartonno/csrf
	//
	public function  getProductStockByCarton()
	{
		$floc = $this->uri->segment(3);
		$tloc = $this->uri->segment(4);
		$cartonreff = $this->uri->segment(5);

		//$sql = "select productitem.*, stockbalance.totalquantity, stockbalance.retailsalesprice, stockbalance.currentcost, (select productseasoncode from productseason where productseasonid=productitem.productseason) as pseason from productitem left join stockbalance on productitem.productitemid=stockbalance.productitem where stockbalance.lastdonumber = '".$cartonreff."' AND stockbalance.locationcode='".$floc."'";
		$sql = "select prod.*, (stkbal.totalquantity - stkbal.intransit) as totalquantity, stkbal.retailsalesprice, stkbal.currentcost, stkbal.intransit, (select productseasoncode from productseason where productseasonid=prod.productseason) as pseason, proid.qty as proidqty, proid.ioid from productionumber proi LEFT JOIN productionumberdetail proid ON proi.id=proid.prodioid LEFT JOIN productitem prod ON prod.productitemid=proid.productitem LEFT JOIN stockbalance stkbal ON stkbal.productitem=proid.productitem AND stkbal.locationcode='".$floc."' WHERE proi.transfernotenumber= '".$cartonreff."' ";
		$rs = $this->db->query($sql);
		$data = array();
		$total_qty = 0;
		$total_rsp = 0;
		foreach($rs->result() as $row)
		{
			$rowid = md5($row->productcode);
			$iscart = array();
			$iscart['rowid'] = $rowid;
			$iscart['productitemid'] = $row->productitemid;
			$iscart['currentcost'] = $row->currentcost;
			$iscart['itemcode'] = $row->productcode;
			$iscart['label'] = $row->productgroup;
			$iscart['cate'] = $row->productcategory;
			$iscart['gender'] = $row->sex;
			$iscart['color'] = $row->descline2;
			$iscart['fabric'] = $row->productitemdesc;
			$iscart['fabric_dec'] = $row->productitemdesc;
			$iscart['style_desc'] = $row->productitemdesc;
			$iscart['retailsalesprice'] = $row->retailsalesprice;
			$iscart['season'] = $row->pseason;
			$iscart['qty'] = $row->totalquantity;
			$iscart['proidqty'] = $row->proidqty;
			$iscart['ioid'] = $row->ioid;
			$iscart['intransit'] = $row->intransit;
			$total_qty += $row->totalquantity;
			$total_rsp += $row->retailsalesprice;
			$data[] = $iscart;
		}
		$json['rows'] = $data;
		$json['sql'] = $this->db->last_query();
		$json['footer'] = array(
			array('proidqty'=>$total_qty, 'qty'=>$total_qty, 'retailsalesprice'=>number_format($total_rsp))
		);

		echo json_encode($json);
	}

	public function svmanualtrf2grid()
	{
		$floc = $this->input->post('floc');
		$tloc = $this->input->post('tloc');
		$itemcode = $this->input->post('productcode');
		$adata = $this->input->post('adata');
		$itemsize = json_decode($adata);

		$itemc = array();
		$pcode = array();
		for($i=0; $i < count($itemsize); $i++)
		{
			$pc = (strlen($itemcode) == 15) ? $itemcode :  $itemcode . $itemsize[$i]->size;
			$itemc[] = $pc;
			$pcode[$pc] = $itemsize[$i]->qty;
		}

		$sql = "select productitem.*, stockbalance.totalquantity, stockbalance.retailsalesprice, stockbalance.currentcost, (select productseasoncode from productseason where productseasonid=productitem.productseason) as pseason from productitem left join stockbalance on productitem.productitemid=stockbalance.productitem where productitem.productcode IN ('".implode("','", $itemc)."') AND stockbalance.locationcode='".$floc."'";
		$rs = $this->db->query($sql);
		$data = array();
		foreach($rs->result() as $row)
		{
			if($pcode[$row->productcode] > 0)
			{
				$rowid = md5($row->productcode);
				$iscart = array();
				
				$iscart['rowid'] = $rowid;
				$iscart['productitemid'] = $row->productitemid;
				$iscart['currentcost'] = $row->currentcost;
				$iscart['itemcode'] = $row->productcode;
				$iscart['label'] = $row->productgroup;
				$iscart['cate'] = $row->productcategory;
				$iscart['gender'] = $row->sex;
				$iscart['color'] = $row->descline2;
				$iscart['fabric'] = $row->productitemdesc;
				$iscart['fabric_dec'] = $row->productitemdesc;
				$iscart['style_desc'] = $row->productitemdesc;
				$iscart['retailsalesprice'] = $row->retailsalesprice;
				$iscart['season'] = $row->pseason;
				$iscart['qty'] = $pcode[$row->productcode];

				$this->_items_manual_trans[$rowid] = $iscart;
			}
		}
		$this->session->set_userdata(array('manual_item_trans'=>$this->_items_manual_trans));
		$result['rowid'] = true;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		$result['data'] = $pcode;
		$result['sql'] = $sql;
		echo json_encode($result);
	}

	public function getManualGridItems()
	{
		$item = array();
		$totalquantity = 0;
		if(count($this->_items_manual_trans) > 0)
		{
			//var_dump($this->_items_manual_trans);
			foreach ($this->_items_manual_trans as $key => $val)
			{
				//print_r($val);
				$item[] = 
					array(
						'label'=>$val["label"],
						'category'=>$val['cate'],
						'color'=>$val['color'],
						'sex'=>$val['gender'],
						'productcode'=>$val['itemcode'],
						'barcode'=>$val['itemcode'],
						'productitemdesc'=>$val['style_desc'],
						'rqty'=>$val['rqty'],
						'qty'=>$val['qty'],
						'season'=>$val['season'],
						'retailsalesprice'=>$val['retailsalesprice']
					);
				$totalquantity += $val['qty'];
			}
		}
		
		$data['rows'] = $item;
		$data['footer'] = array(
			array('qty'=>$totalquantity)
			);
		echo json_encode($data);
		
	}

	public function commitCartonTransfer()
	{
		$donumber = $this->input->post('donumber');
		$floc = $this->input->post('floc');
		$tloc = $this->input->post('tloc');
		$ifloc = $this->input->post('ifloc');
		$itloc = $this->input->post('itloc');
		$rdata = $this->input->post('rows');
		$rows = json_decode($rdata);

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();
		$founderror = true;
		$msg = "";
		if(count($rows) > 0)
		{

			$trfNo = $this->Public_model->getLastStkIssue($floc, "T");
			$s_insert_transfernote = "insert into transfernote (transfernoteid, transfernotenumber, createdatetime, fromlocation, tolocation, lasteditdatetime, transfernotepurpose, transfernotestatus, raiseby, raisedate) values 
			(nextval('transfernote_seq'::text), '".$trfNo."', '".date('Y-m-d H:i:s')."', ".$ifloc.", ".$itloc.", '".date('Y-m-d H:i:s')."', 1, 1, 1, '".date('Y-m-d')."')";
			$this->db->query($s_insert_transfernote);
			$trfId = $this->db->insert_id();
			for($i=0; $i < count($rows); $i++)
			{
				$rsitem = $this->db->query("select stockbalance.*, productitem.supplieritemcode from stockbalance left join productitem on productitem.productitemid=stockbalance.productitem where stockbalance.productitem=".$rows[$i]->productitemid." AND stockbalance.location=".$ifloc);
				$row = $rsitem->row();
				
				if(isset($row))
				{

					if(($row->totalquantity - $row->intransit) >= $rows[$i]->proidqty)
					{
						$s_itransitem = "insert into transfernoteitem(transfernoteitemid, currentcost, productitem, retailsalesprice, supplieritemcode,transfernote, transferqty, status) values (nextval('transfernoteitem_seq'::text), ".$row->currentcost.", ".$row->productitem.", ".$row->retailsalesprice.", '".$row->supplieritemcode."',".$trfId.", ".$rows[$i]->proidqty.",0)";
						$s_stkbal = "update stockbalance set intransit= intransit + ".$rows[$i]->proidqty." WHERE location=".$ifloc." AND productitem=".$row->productitem;

						//$this->db->query("update productionumberdetail set qty=qty-".$rows[$i]->proidqty." WHERE ioid=".$rows[$i]->ioid);
						$this->db->query("delete from productionumberdetail where ioid=".$rows[$i]->ioid);
						//$this->db->query("update productionumber set transfernotenumber='' WHERE transfernotenumber='".$rows[$i]->ioid."'");
						$this->db->query($s_itransitem);
						$this->db->query($s_stkbal);
						$this->db->query("insert into inventory_movement (locationcode, locationid, movementdate, movement_type, reffno, productitem, productitemcode, movement_qty, movement_value, createduser, movement_qtybalance, reason, narrative, tofromlocation, movement_oldvalue) values ('".$floc."',".$ifloc.",CURRENT_DATE, 'O', '".$trfNo."', ".$row->productitem.",'".$row->productcode."', ".-$rows[$i]->proidqty.", ".$row->retailsalesprice.", '".$this->session->userdata('username')."', ".$row->totalquantity.", '', 'INTRANSIT','".$itloc."', ".$row->retailsalesprice.")");
						$this->Inventory_model->movementPeriode($row->productcode, $floc, "dn", $rows[$i]->proidqty);
						$founderror = false;
					}
					else
					{
						$this->db->query("delete from productionumberdetail where ioid=".$rows[$i]->ioid);
					}
				}
				else
				{
					$founderror = true;
					$msg = "Items not found";
				}
			}

			$this->db->query("delete from productionumber WHERE transfernotenumber='".$donumber."'");
		}
		$this->db->trans_complete();
		$result['csrf_name'] = $this->security->get_csrf_hash();
		$result['status'] = 1;
		$result['donum'] = $trfNo;
		$result['transferid'] = $trfId;
		if ($this->db->trans_status() === FALSE || $founderror)
		{
			$this->db->trans_rollback();
			$result['status'] = 0;
			$result['msgErr'] = $msg;
		}
		else
		{
			$result['msgErr'] = 'Successfully';
			$this->db->trans_commit();
			$this->_items_manual_trans = array();
			$this->session->unset_userdata('manual_item_trans');
		}
		echo json_encode($result);
	}


	public function getStockReceipt()
	{
		$productcode = $this->input->post('productcode');
		$stocksroom = $this->input->post('stocksroom');
		$page = $this->input->post('page');
		$rows = $this->input->post('rows');

		$offset = ($page-1)*$rows;

		$items = array();
		$gosql = false;

		if(!empty($stocksroom))
		{
			$gosql = true;
			//$this->db->where('fromlocation', "(select locationid from locationinfo where locationcode='".$stocksroom."')", false);
			$this->db->where('tolocation', "(select locationid from locationinfo where locationcode='".$stocksroom."')", false);
		}

		if($gosql)
		{
			$this->db->select("transfernote.*, (select locationcode from locationinfo where locationid=transfernote.fromlocation) as flocation, (select locationcode from locationinfo where locationid=transfernote.tolocation) as tlocation");
			$rs = $this->db->get('transfernote');
			$this->db->order_by('transfernoteid', 'DESC');
	    	$this->db->limit($rows, $offset);

			foreach($rs->result() as $row)
			{
				array_push($items, $row);
			}
		}

		$data['rows'] = $items;
		$data['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($data);


	}


	/*
	// insert ke transfernote
	// insert ke transfernoteitem
	// update stockbalance field totalquantity, transit
	*/
	public function commitmanualtrf2grid()
	{
		$floc = $this->input->post('floc');
		$tloc = $this->input->post('tloc');
		$ifloc = $this->input->post('ifloc');
		$itloc = $this->input->post('itloc');

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$trfNo = $this->Public_model->getLastStkIssue($floc, "T");;//date('y').$this->Public_model->getCountTransfernotebyStr($ifloc);
		
		$this->db->set("donumber", $trfNo);
		$this->db->set("fromlocation", $floc);
		$this->db->set("tolocation", $tloc);
		$this->db->set("ifromlocation", $ifloc);
		$this->db->set("itolocation", $itloc);
		$this->db->insert("manual_allocate");
		$allocateid = $this->db->insert_id();
		$msg = "";
		$founderror = false;
		foreach ($this->_items_manual_trans as $key => $val)
		{
			$rsitem = $this->db->query("select * from stockbalance where productitem=".$val['productitemid']." AND location=".$ifloc);
			$row = $rsitem->row();
			
			if(isset($row))
			{

				if(($row->totalquantity - $row->intransit) >= $val['qty'])
				{

					$this->db->set("manual_allocate", $allocateid);
					$this->db->set("productitem", $val['productitemid']);
					$this->db->set("qty", $val['qty']);
					$this->db->set("retailsalesprice", $row->retailsalesprice);
					$this->db->insert("manual_allocate_item");
					
				}
				else
				{
					$founderror = true;
					$msg = "QTY is over";
				}
			}
			else
			{
				$founderror = true;
				$msg = "Product Item not found";
			}
		}
		$this->db->trans_complete();

		$result['csrf_name'] = $this->security->get_csrf_hash();
		$result['status'] = 1;
		$result['donum'] = $trfNo;
		if ($this->db->trans_status() === FALSE || $founderror)
		{
			$this->db->trans_rollback();
			$result['status'] = 0;
			$result['msgErr'] = $msg;
		}
		else
		{
			$result['msgErr'] = 'Successfully';
			$this->db->trans_commit();
			$this->_items_manual_trans = array();
			$this->session->unset_userdata('manual_item_trans');
		}
		echo json_encode($result);
	}


	

	public function remanualtrf2grid()
	{
		$productcode = $this->input->post('productcode');
		$rowid = md5($productcode);
		unset($this->_items_manual_trans[$rowid]);
		$this->session->set_userdata(array('manual_item_trans'=>$this->_items_manual_trans));
		$result['csrf_name'] = $this->security->get_csrf_hash();
		$result['rowid'] = $rowid;
		echo json_encode($result);
	}

	


	public function printTransferOut()
	{
		$this->load->model('Inventory_model');
		$trfno = $this->uri->segment(3);
		$data['header'] = $this->Inventory_model->_readreprint($trfno);
		$this->load->view("printing/print_stock_issue_A4", $data);
	}




	public function getInventoryGoodStock()
	{
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$rows = isset($_GET['rows']) ? intval($_GET['rows']) : 50;
		$outletcode = $this->uri->segment(3);;//$this->input->get('outletcode');

		if(empty($outletcode))
		{
			$result["rows"] = array();
			$result["total"] = 0;
			
			die(json_encode($result));
		}
		$offset = ($page-1)*$rows;
		$result = array();

	    $this->db->where('locationcode', $outletcode); // and etc...
	    $this->db->where('totalquantity != ', '0'); // and etc...
	    $this->db->from('stockbalance');
	    $total_rows = $this->db->count_all_results(); // This will get the real total rows

	    $this->db->select('stkbal.locationcode, stkbal.productcode, stkbal.lastdonumber, stkbal.intransit, stkbal.frozenqty, stkbal.retailsalesprice, stkbal.totalquantity, stkbal.currentcost, pitem.sex, pitem.productitemdesc, pitem.descline1, (select productseasoncode from productseason where productseasonid=pitem.productseason) as season, (select productgroupcode from productgroup where productgroupid=pitem.productgroup) as label, (select productcategorydesc from productcategory where productcategoryid=pitem.productcategory) as category');
	    $this->db->where('stkbal.locationcode', $outletcode); // and etc...
	    $this->db->where('totalquantity !=', '0'); // and etc...
	    $this->db->from('stockbalance stkbal');
	    $this->db->join('productitem pitem', 'stkbal.productitem=pitem.productitemid', 'LEFT');
	    $this->db->order_by('pitem.productitemid', 'DESC');
	    $this->db->limit($rows, $offset);

	    $rs = $this->db->get();
	    $items = array();
		$total_quantity = 0;

		foreach ($rs->result() as $row) {
			array_push($items, $row);
			$total_quantity += $row->totalquantity;
		}

		$result["rows"] = $items;
		$result["footer"] = array(
			array('locationcode'=>'', 'productcode'=>'', 'lastdonumber'=>'', 'intransit'=>'', 'frozenqty'=>'', 'retailsalesprice'=>'', 'totalquantity'=>$total_quantity, 'currentcost'=>'', 'sex'=>'', 'productitemdesc'=>'', 'descline1'=>'', 'season'=>'', 'label'=>'', 'category'=>'')
		);
		$result["total"] = $total_rows;
		echo json_encode($result);
	}

	public function getWhStoreReport()
	{
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$rows = isset($_GET['rows']) ? intval($_GET['rows']) : 50;
		$outletcode = $this->uri->segment(3);;//$this->input->get('outletcode');

		if(empty($outletcode))
		{
			$result["rows"] = array();
			$result["total"] = 0;
			
			die(json_encode($result));
		}
		$offset = ($page-1)*$rows;
		$result = array();

	    $this->db->where('locationcode', $outletcode); // and etc...
	    $this->db->from('inventory_move_open');
	    $total_rows = $this->db->count_all_results(); // This will get the real total rows

	    $this->db->select("imo.*, pitem.productitemdesc, stk.retailsalesprice");
	    $this->db->where('imo.locationcode', $outletcode); // and etc...
	    $this->db->from('inventory_move_open imo');
	    $this->db->join('productitem pitem', 'imo.productitem=pitem.productitemid', 'LEFT');
	    $this->db->join('stockbalance stk', 'stk.productitem=imo.productitem AND stk.location=imo.location', 'LEFT');
	    $this->db->order_by('imo.id', 'ASC');
	    $this->db->limit($rows, $offset);

	    $rs = $this->db->get();
	    //echo $this->db->last_query();
	    $items = array();
		$total_quantity = 0;
		foreach ($rs->result() as $row) {
			//array_push($items, $row);
			$eom = ($row->cn+$row->adjin+$row->bom+$row->khr+$row->cpin)-($row->dn+$row->adjout+$row->exp+$row->sal+$row->cpout);
			$items[] = array(
				'productcode'=>$row->productitemcode,
				'productitemdesc'=>$row->productitemdesc,
				'retailsalesprice'=>$row->retailsalesprice,
				'totalprice'=>($row->rsp * $eom),
				'bom'=>$row->bom,
				'khr'=>$row->khr,
				'cn'=>($row->cn+$row->adjin),
				'dn'=>($row->dn+$row->adjout),
				'sal'=>$row->sal,
				'exp'=>$row->exp,
				'adjin'=>$row->adjin,
				'adjout'=>$row->adjout,
				'cpin'=>$row->cpin,
				'cpout'=>$row->cpout,
				'eom'=>$eom,
				);
			$total_quantity += $eom;
		}

		$result["rows"] = $items;
		$result["footer"] = array(
			array(
				'productcode'=>'',
				'productitemdesc'=>'',
				'retailsalesprice'=>'',
				'totalprice'=>'',
				'bom'=>'',
				'khr'=>'',
				'cn'=>'',
				'dn'=>'',
				'sal'=>'',
				'exp'=>'',
				'adjin'=>'',
				'adjout'=>'',
				'cpin'=>'',
				'cpout'=>'',
				'eom'=>$total_quantity
				)
		);
		$result["total"] = $total_rows;
		echo json_encode($result);
	}

	public function getTransferReceive()
	{
		$outlet = $this->uri->segment(3);

		$page = $this->input->post('page');
		$rows = $this->input->post('rows');

		$offset = ($page-1)*$rows;

		//3 = WH
		$sql = "select note.transfernoteid, note.receivedate, note.transfernotestatus, note.transfernotenumber, note.stockreceiptnumber, note.fromlocation, (select locationcode from locationinfo where note.fromlocation=locationid) as fromlocationcode, location.locationcode  from transfernote note, locationinfo location WHERE note.tolocation=3 AND note.tolocation=location.locationid order by note.createdatetime DESC limit $rows OFFSET $offset";

		//$this->db->limit($rows, $offset);
		$rs = $this->db->query($sql);
		$data = array();
		foreach ($rs->result() as $row) {
			array_push($data, $row);
		}

		$item['rows'] = $data;
		$item['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($item);
	}


	public function printReceiveWh()
	{
		$this->load->model('Inventory_model');
		$trfnoid = $this->uri->segment(3);
		$action = $this->uri->segment(4);
		$data['header'] = $this->Inventory_model->_readreprint($trfnoid);
		$this->load->view("printing/print_stock_issue_A4", $data);	
	}

	/*
	WH received
	1. receive update table transfernote and transfernoteitemid 			ok
	2. insert ke table productionumber dan atau set rack management			ok
	3. insert ke movementlog												ok
	4. update qty stockbalance 												ok
	*/
	public function setReceived()
	{
		$transfernoteid = $this->input->post('transfernoteid');

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$stockreceiptnumber = $this->Public_model->getLastStkReceipt('WH', 'T');

		$tnote = $this->db->get_where('transfernote', array('transfernoteid'=>$transfernoteid))->row();
		if($tnote->transfernotestatus == 1)
		{
			$this->db->query("update transfernote set transfernotestatus=2, receivedate=CURRENT_DATE, stockreceiptnumber='".$stockreceiptnumber."', receiveby=".$this->session->userdata('userid').", transfernotepurpose=1 where transfernoteid=".$transfernoteid);
			//$this->db->query("insert into productionumber (transfernotenumber, lastupdate, fromlocation, uom) values ('".$tnote->transfernotenumber."', CURRENT_DATE, ".$tnote->fromlocation.", 'BOX')");
			//$piolastid = $this->db->insert_id();

			$rs = $this->db->get_where('transfernoteitem', array('transfernote'=>$transfernoteid));

			$mlog = array();
			$pioitem = array();
			foreach($rs->result() as $row)
			{
				//update transferitem
				$this->db->query("update transfernoteitem set receiveqty=transferqty where transfernote=".$transfernoteid);
				$this->db->query("update stockbalance set totalquantity=(totalquantity + ".$row->transferqty.") WHERE location=".$tnote->tolocation." AND productitem=".$row->productitem);
				// tidak di update qty karena dari pos sudah berkurang qty dan yang di update hanya intransit, akan balik bila di void
				$this->db->query("update stockbalance set intransit=(intransit - ".$row->transferqty.") WHERE location=".$tnote->fromlocation." AND productitem=".$row->productitem);


				//--
				// IN
				$this->db->set('movementdate',date('Y-m-d H:i:s'));
				$this->db->set('movement_value',$row->retailsalesprice);
				$this->db->set('movement_qty',$row->transferqty);
				$this->db->set('locationcode', "(select locationcode from locationinfo where locationid=".$tnote->tolocation.")", false);
				$this->db->set('movement_type', 'I');
				$this->db->set('reffno', $tnote->transfernotenumber);
				$this->db->set('locationid', $tnote->tolocation);
				$this->db->set('productitemcode', "(select productcode from productitem where productitemid=".$row->productitem.")", false);
				$this->db->set('productitem', $row->productitem);
				$this->db->set('createduser', $this->session->userdata('username'));

				$this->db->set('movement_qtybalance',0);
				$this->db->set('reason','');
				$this->db->set('narrative','INTRANSIT ADJUSTMENT');
				$this->db->set('tofromlocation',$tnote->fromlocation);
				$this->db->set('tofromlocationname','');

				$this->db->insert('inventory_movement');

				//OUT
				$this->db->set('movementdate',date('Y-m-d H:i:s'));
				$this->db->set('movement_value',$row->retailsalesprice);
				$this->db->set('movement_qty',-$row->transferqty);
				$this->db->set('locationcode', "(select locationcode from locationinfo where locationid=".$tnote->fromlocation.")", false);
				$this->db->set('movement_type', 'O');
				$this->db->set('reffno', $tnote->transfernotenumber);
				$this->db->set('locationid', $tnote->fromlocation);
				$this->db->set('productitemcode', "(select productcode from productitem where productitemid=".$row->productitem.")", false);
				$this->db->set('productitem', $row->productitem);
				$this->db->set('createduser', $this->session->userdata('username'));

				$this->db->set('movement_qtybalance',0);
				$this->db->set('reason','');
				$this->db->set('narrative','INTRANSIT ADJUSTMENT');
				$this->db->set('tofromlocation',$tnote->fromlocation);
				$this->db->set('tofromlocationname','');

				$this->db->insert('inventory_movement');
				//--

				$barcode = $this->db->query("select productcode from productitem where productitemid=".$row->productitem)->row()->productcode;
				$tolocationcode = $this->db->query("select locationcode from locationinfo where locationid=".$tnote->tolocation)->row()->locationcode;
				$locationcode = $this->db->query("select locationcode from locationinfo where locationid=".$tnote->fromlocation)->row()->locationcode;
				$this->Inventory_model->movementPeriode($barcode, $tolocationcode, "cn", $row->transferqty);
				$this->Inventory_model->movementPeriode($barcode, $locationcode, "dn", $row->transferqty);
			}
			
			//$this->db->insert_batch('productionumberdetail',$pioitem);
		
			$result['status'] = 1;
			$result['csrf_name'] = $this->security->get_csrf_hash();
			$result['stknumber'] = $stockreceiptnumber;
			$msg = "Successfully";
		}
		else
		{
			$result['status'] = 0;
			$result['csrf_name'] = $this->security->get_csrf_hash();
			$result['stknumber'] = "";
			$msg = "DO Number already received";
		}
		
		$this->db->trans_complete();

		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$result['status'] = 0;
			$result['msgErr'] = $msg;
		}
		else
		{
			$result['msgErr'] = $msg;
			$this->db->trans_commit();
		}	
		echo json_encode($result);
	}



	public function setReceivedStore()
	{
		$transfernoteid = $this->input->post('transfernoteid');
		$locationcode = $this->input->post('locationcode');
		$terminal = $this->input->post('terminal');

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$stockreceiptnumber = $this->Public_model->getLastStkReceipt($locationcode, 'T', $terminal);

		$tnote = $this->db->get_where('transfernote', array('transfernoteid'=>$transfernoteid))->row();
		if($tnote->transfernotestatus == 1)
		{
			// putaway di set 9 = jika receive toko dari WEB, artinya dari WH ke STORE
			// putaway di set 1 = jika iot dari TOKO ke WH dan di lakukan putaway
			// putaway di set 0 default
			$this->db->query("update transfernote set putaway=9, transfernotestatus=2, receivedate=CURRENT_DATE, stockreceiptnumber='".$stockreceiptnumber."', receiveby=".$this->session->userdata('userid').", transfernotepurpose=1 where transfernoteid=".$transfernoteid);
			
			$rs = $this->db->get_where('transfernoteitem', array('transfernote'=>$transfernoteid));

			$mlog = array();
			$pioitem = array();
			foreach($rs->result() as $row)
			{
				//update transferitem
				$this->db->query("update transfernoteitem set receiveqty=transferqty where transfernote=".$transfernoteid);

				$count = $this->db->query("select * from stockbalance where location=".$tnote->tolocation." AND productitem=".$row->productitem);
				$num = $count->num_rows();
				if($num > 0)
				{
					$this->db->set('totalquantity',"(totalquantity + ".$row->transferqty.")", false);
					$this->db->set('bfquantity',0);

					$this->db->where('location', $tnote->tolocation);
					$this->db->where('productitem', $row->productitem);
					$this->db->update('stockbalance');
				}
				else
				{
					$this->db->set('transactiondate',date('Y-m-d H:i:s'));
					$this->db->set('retailsalesprice',$row->retailsalesprice);
					$this->db->set('lastdonumber','');
					$this->db->set('currentcost',$row->currentcost);
					$this->db->set('totalquantity',$row->transferqty);
					$this->db->set('locationcode', $locationcode);
					$this->db->set('location', $tnote->tolocation);
					$this->db->set('productcode', "(select productcode from productitem where productitemid=".$row->productitem.")", false);
					$this->db->set('productitem', $row->productitem);
					$this->db->set('stockbalanceid', "nextval('stockbalance_seq'::text)", FALSE);
					$this->db->set('productwithqty',1);
					$this->db->set('bfvalue',0);
					$this->db->set('bfquantity',0);

					$this->db->insert('stockbalance');
				}
				// tidak di update qty karena dari pos sudah berkurang qty dan yang di update hanya intransit, akan balik bila di void
				$this->db->query("update stockbalance set intransit=(intransit - ".$row->transferqty."), totalquantity=(totalquantity - ".$row->transferqty.") WHERE location=".$tnote->fromlocation." AND productitem=".$row->productitem);



				$this->db->set('movementdate',date('Y-m-d H:i:s'));
				$this->db->set('movement_value',$row->retailsalesprice);
				$this->db->set('movement_qty',$row->transferqty);
				$this->db->set('locationcode', $locationcode);
				$this->db->set('movement_type', 'I');
				$this->db->set('reffno', $tnote->transfernotenumber);
				$this->db->set('locationid', $tnote->tolocation);
				$this->db->set('productitemcode', "(select productcode from productitem where productitemid=".$row->productitem.")", false);
				$this->db->set('productitem', $row->productitem);
				$this->db->set('createduser', $this->session->userdata('username'));

				$this->db->set('movement_qtybalance',0);
				$this->db->set('reason','');
				$this->db->set('narrative','INTRANSIT ADJUSTMENT');
				$this->db->set('tofromlocation',$tnote->fromlocation);
				$this->db->set('tofromlocationname','');

				$this->db->insert('inventory_movement');
				$barcode = $this->db->query("select productcode from productitem where productitemid=".$row->productitem)->row()->productcode;
				$this->Inventory_model->movementPeriode($barcode, $locationcode, "cn", $row->transferqty);
			}
		
			$result['status'] = 1;
			$result['csrf_name'] = $this->security->get_csrf_hash();
			$result['stknumber'] = $stockreceiptnumber;
			$msg = "Successfully";
		}
		else
		{
			$result['status'] = 0;
			$result['csrf_name'] = $this->security->get_csrf_hash();
			$result['stknumber'] = "";
			$msg = "DO Number already received";
		}
		
		$this->db->trans_complete();

		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$result['status'] = 0;
			$result['msgErr'] = $msg;
		}
		else
		{
			$result['msgErr'] = $msg;
			$this->db->trans_commit();
		}	
		echo json_encode($result);
	}

	public function cekLastReceipt()
	{
		print_r($this->session->userdata());
	}
}