<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse extends CI_Controller {

	var $_prefix_cost = 'CO';

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_logedin'))
		{
			redirect("login");
		}
		$this->load->model("Public_model");
		$this->load->model("Inventory_model");
	}

	public function putaway()
	{
		$this->load->view("warehouse/putaway");
	}

	public function manualAllocate()
	{
		$this->load->view("warehouse/manuallocate");
	}

	public function whimportStatus()
	{
		$this->load->view("warehouse/importStatus");	
	}

	public function costingfreight()
	{
		$data["costingnumber"] = $this->_prefix_cost.date('ym').$this->Public_model->getCountCosting();
		$data["json_currency"] = $this->Public_model->getCurrency();
		$this->load->view("warehouse/costingfreigt", $data);
	}

	public function getCostingnumber()
	{
		echo $this->Public_model->getCostingnumber();
	}

	public function getDonumberPutaway()
	{
		$donumber = $this->input->post("donumber");

		$this->db->select("transfernoteitem.*, productitem.productcode, productitem.productitemdesc");
		$this->db->join("transfernoteitem", "transfernoteitem.transfernote=transfernote.transfernoteid", "LEFT");
		$this->db->join("productitem", "productitem.productitemid=transfernoteitem.productitem", "LEFT");
		$this->db->from("transfernote");
		$this->db->where("transfernote.transfernotenumber", $donumber);
		$this->db->where("transfernote.tolocation", 3);
		$this->db->where("transfernote.putaway", 0);
		$rs = $this->db->get();

		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}

		$result['rows'] = $data;
		$result['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}

	public function commitPutaway()
	{
		$donumber = $this->input->post("donumber");
		$rackcode = $this->input->post("rackcode");
		$narrative = $this->input->post("narrative");

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$this->db->select("transfernoteitem.*, productitem.productcode, productitem.productitemdesc");
		$this->db->join("transfernoteitem", "transfernoteitem.transfernote=transfernote.transfernoteid", "LEFT");
		$this->db->join("productitem", "productitem.productitemid=transfernoteitem.productitem", "LEFT");
		$this->db->from("transfernote");
		$this->db->where("transfernote.transfernotenumber", $donumber);
		$this->db->where("transfernote.tolocation", 3);
		$this->db->where("transfernote.putaway", 0);
		$rs = $this->db->get();

		$this->db->query("update transfernote set putaway=1 where transfernotenumber='".$donumber."'");
		$this->db->query("insert into productionumber (transfernotenumber, lastupdate, fromlocation, fromlocationcode, uom, rackcode, remark) values ('".$donumber."', CURRENT_DATE, (select fromlocation from transfernote where transfernotenumber='".$donumber."'), (select locationcode from locationinfo where locationid=(select fromlocation from transfernote where transfernotenumber='".$donumber."')),'BOX','".$rackcode."','".$narrative."')");
		$ioid = $this->db->insert_id();
		$data = array();
		foreach($rs->result() as $row)
		{
			$this->db->query("insert into productionumberdetail(prodioid, productitem, productcode, qty) values (".$ioid.", ".$row->productitem.", '".$row->productcode."',".$row->receiveqty.")");
		}

		$this->db->trans_complete();
		$result['csrf_name'] = $this->security->get_csrf_hash();
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$result['status'] = 0;
			$result['msgErr'] = $msg;
		}
		else
		{
			$result['status'] = 1;
			$result['msgErr'] = 'Successfully';
			$this->db->trans_commit();
		}
		echo json_encode($result);
	}


	public function showManualAllocate()
	{
		$donumber = $this->input->post('donumber');
		$this->db->select("mai.*, productitem.productitemdesc, productitem.productcode");
		$this->db->join("manual_allocate_item mai", "mai.manual_allocate=ma.id","LEFT");
		$this->db->join("productitem", "productitem.productitemid=mai.productitem","LEFT");
		$this->db->from("manual_allocate ma");
		$this->db->where("ma.donumber", $donumber);
		$rs = $this->db->get();
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		//echo $this->db->last_query();
		$json['rows'] = $data;
		$json['csrf_name'] = $this->security->get_csrf_hash();
		echo json_encode($json);
	}

	public function setManualAllocate()
	{
		$donumber = $this->input->post('donumber');
		$this->db->where("donumber", $donumber);
		$row = $this->db->get("manual_allocate")->row();

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();
		$trfId = 0;
		$trfNo = "";
		if(isset($row))
		{
			$trfNo = $row->donumber;
			$s_insert_transfernote = "insert into transfernote (transfernoteid, transfernotenumber, createdatetime, fromlocation, tolocation, lasteditdatetime, transfernotepurpose, transfernotestatus, raiseby, raisedate) values 
			(nextval('transfernote_seq'::text), '".$row->donumber."', '".date('Y-m-d H:i:s')."', ".$row->ifromlocation.", ".$row->itolocation.", '".date('Y-m-d H:i:s')."', 1, 1, 1, '".date('Y-m-d')."')";
			$this->db->query($s_insert_transfernote);
			$trfId = $this->db->insert_id();

			$rs = $this->db->get_where("manual_allocate_item", array("manual_allocate"=>$row->id));
			foreach($rs->result() as $rw)
			{
				$rsitem = $this->db->query("select * from stockbalance where productitem=".$rw->productitem." AND location=".$row->ifromlocation);
				$field = $rsitem->row();
				
				if(isset($field))
				{

					if(($field->totalquantity - $field->intransit) >= $rw->qty)
					{
						$s_itransitem = "insert into transfernoteitem(transfernoteitemid, currentcost, productitem, retailsalesprice, transfernote, transferqty) values (nextval('transfernoteitem_seq'::text), ".$field->currentcost.", ".$rw->productitem.", ".$rw->retailsalesprice.", ".$trfId.", ".$rw->qty.")";
						$s_stkbal = "update stockbalance set intransit= intransit + ".$rw->qty." WHERE location=".$row->ifromlocation." AND productitem=".$rw->productitem;

						$this->db->query($s_itransitem);
						$this->db->query($s_stkbal);
						$this->db->query("insert into inventory_movement (locationcode, locationid, movementdate, movement_type, reffno, productitem, productitemcode, movement_qty, movement_value, createduser, movement_qtybalance, reason, narrative, tofromlocation, tofromlocationname, movement_oldvalue) values ('".$row->fromlocation."',".$row->ifromlocation.",CURRENT_DATE, 'O', '".$trfNo."', ".$rw->productitem.",'".$field->productcode."', ".-$rw->qty.", ".$rw->retailsalesprice.", '".$this->session->userdata('username')."', ".$rw->qty.", '', 'INTRANSIT',".$row->itolocation.",'".$row->tolocation."', ".$rw->retailsalesprice.")");
						$this->Inventory_model->movementPeriode($field->productcode, $row->fromlocation, "dn", $rw->qty);
						$founderror = false;
					}
					else
					{
						$founderror = true;
						$msg = "QTY is over";
					}
				}
				else
				{
					$founderror = true;
					$msg = "Product Item not found";
				}
			}

			$this->db->query("delete from manual_allocate where id=".$row->id);
			$this->db->query("delete from manual_allocate_item where manual_allocate=".$row->id);
		}
		else
		{

		}
		
		$this->db->trans_complete();
		$result['csrf_name'] = $this->security->get_csrf_hash();
		$result['transferid'] = $trfId;
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$result['status'] = 0;
			$result['msgErr'] = $msg;
		}
		else
		{
			$result['status'] = 1;
			$result['msgErr'] = 'Successfully';
			$this->db->trans_commit();
		}
		echo json_encode($result);
	}

	public function saveCostingItems()
	{
		
		$nopib = $this->input->post('nopib');
		$spcpdate = $this->input->post('spcpdate');
		$awbno = $this->input->post('awbno');
		$noinvoice = $this->input->post('noinvoice');
		$costingField = $this->input->post('costingField');
		
		$currency = $this->input->post('currency');
		$exchangerate = $this->input->post('exchangerate');

		$itcost = json_decode($costingField);
		
		$costing_number = $this->_prefix_cost.date('ym').$this->Public_model->getCountCosting();
		
		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$co_header = array(
			'costingdate'=>date('Y-m-d'),
			'pibnumber'=>$nopib,
			'awbnumber'=>$awbno,
			'costingnumber'=>$costing_number,
			'invoicenumber'=>$noinvoice,
			'spcpdate'=>$spcpdate,
			'costingstatus'=>'NEW',
			'currency'=>$currency,
			'exchangerate'=>$exchangerate,
			'prepareduser'=>$this->session->userdata('username')
			);
		$this->db->insert('costing_header', $co_header);
		$co_header_id = $this->db->insert_id();
		$co_detail = array();
		foreach ($itcost as $group)
		{
			$cosfieldid = $this->db->get_where("costing_field", array("subcategory"=>$group->group))->row()->id;
			$co_detail[] = array('costing_header_id'=>$co_header_id, 'groupname'=>$group->groupname, 'fee_category_name'=>$group->group, 'amount'=>$group->amount, 'cosfieldid'=>$cosfieldid);
		}

		
		$this->db->insert_batch('costing_detail',$co_detail);
		$this->db->trans_complete();
		
		echo json_encode(array('status'=>1, 'co_number'=>$costing_number, 'csrf_name'=>$this->security->get_csrf_hash()));
	}
}