<?php
class Finance_model extends CI_Model
{

	public function getCashAdvanceNumber()
	{
		//penomoran : CA/Nomor Urut/tgl/bln/th
		// contoh CA/0001/05/VI/18

		$table = "acc_cash_advance";
		$this->db->where('EXTRACT(YEAR from issuedate) = ', date('Y'), false);
		
		$total_count = $this->db->count_all_results($table);
		$actual_count = ($total_count + 1);

		$this->db->reset_query();
		$ainc = substr("0000",strlen($actual_count)) . $actual_count; 
		$_day = date("d");
		$_month = date("m");
		$_year = date("y");
		return "CA/".$ainc."/".$_day."/".$this->parseMthRomawi($_month)."/".$_year;
	}


	private function parseMthRomawi($mth)
	{
		$arm = array("01"=>"I", "02"=>"II", "03"=>"III", "04"=>"IV", "05"=>"V", "06"=>"VI", "07"=>"VII", "08"=>"VIII", "09"=>"IX", "11"=>"XI", "12"=>"XII");
		return $arm[$mth];

	}

	public function getCountKas($akun, $type="")
	{
		$table = "acc_gl_journal";
		$this->db->where('EXTRACT(YEAR from createdate) = ', date('Y'), false);
		if($akun != "") $this->db->where('coaccount', $akun);
		if($type != "") $this->db->where('type', $type);
		$this->db->where("createby != 'SYS'", "", false);
		
		$total_count = $this->db->count_all_results($table);
		$actual_count = ($total_count +1);
		$this->db->reset_query();
		return substr("0000",strlen($actual_count)) . $actual_count;
	}

	public function getCountDepreciation($activanumber)
	{
		$table = "acc_depreciation";
		$this->db->where('EXTRACT(YEAR from createdate) = ', date('Y'), false);
		if($activanumber != "") $this->db->where("substr(activanumber, 1, ".strlen($activanumber).")='".$activanumber."'", "", false);
		
		$total_count = $this->db->count_all_results($table);
		$actual_count = ($total_count +1);
		$this->db->reset_query();
		$_year = date("y");
		return $_year . substr("0000",strlen($actual_count)) . $actual_count;
	}


	public function getCoa($q="",$digit = 0)
	{

		if($digit > 0)
			$this->db->where("length(subcode)", $digit);

		if(!empty($q))
		{
			$this->db->group_start();
			$this->db->like("lower(subcode)", strtolower($q));
			$this->db->or_like("lower(name)", strtolower($q));
			$this->db->group_end();
		}
		
		$this->db->order_by("subcode", "ASC");


		$rs = $this->db->get("acc_coa");
		//echo $this->db->last_query();
		$data = array();
		foreach($rs->result() as $row)
		{
			$data[] = array("id"=>$row->id, "code"=>$row->code, "subcode"=>$row->subcode, "name"=>$row->name, "typecode"=>$row->typecode, "balance"=>$row->balance);
		}
		$json['rows'] = $data;
		$json['sql'] = $this->db->last_query();
		return json_encode($json);
	}

	public function getCoaBalance($q="",$digit = 0)
	{

		if($digit > 0)
			$this->db->where("length(subcode)", $digit);

		if(!empty($q))
		{
			$this->db->group_start();
			$this->db->like("lower(subcode)", strtolower($q));
			$this->db->or_like("lower(name)", strtolower($q));
			$this->db->group_end();
		}
		$prevy = date('Y')-1;
		$this->db->select("id, code, subcode, name, typecode, balance, saldoawal");
		$this->db->order_by("subcode", "ASC");


		$rs = $this->db->get("acc_coa");
		//echo $this->db->last_query();
		$data = array();
		foreach($rs->result() as $row)
		{
			$lastabalance = ($row->saldoawal) ? $row->saldoawal : 0;
			$data[] = array("id"=>$row->id, "code"=>$row->code, "subcode"=>$row->subcode, "name"=>$row->name, "typecode"=>$row->typecode, "balance"=>$row->saldoawal);
		}
		$json['rows'] = $data;
		$json['sql'] = $this->db->last_query();
		return json_encode($json);
	}


	public function getLocationCode()
	{
		$rs = $this->db->get("locationinfo");
		$data = array();
		foreach ($rs->result() as $row) {
			array_push($data, $row);
		}

		return $data;
	}

	public function getKasHeader($pvnumber)
	{
		$this->db->select("acc_gl_journal.*, TO_CHAR(createdate, 'DD-MM-YYYY') as parsedate");
		$rs = $this->db->get_where("acc_gl_journal", array("voucherno"=>$pvnumber));
		$data = array();
		foreach($rs->result() as $key)
		{
			array_push($data, $key);
		}
		$json['rows']=$data;
		return $json;
	}


	public function updateBalanceCoa($type, $amount, $lakun, $makun)
	{
		if($type == "PV")
		{
			$this->db->query("update acc_coa set balance = balance + ".$amount ." where subcode='".$lakun."'");
			$this->db->query("update acc_coa set balance = balance - ".$amount ." where subcode='".$makun."'");
		}
		else if($type == "RV"){
			$this->db->query("update acc_coa set balance = balance + ".$amount ." where subcode='".$makun."'");
			$this->db->query("update acc_coa set balance = balance - ".$amount ." where subcode='".$lakun."'");
		}
	}

	public function updateBalanceCoaV1($type, $amount, $akun)
	{
		if($type == "D")
		{
			$this->db->query("update acc_coa set balance = balance + ".$amount ." where subcode='".$akun."'");
		}
		else if($type == "K"){
			$this->db->query("update acc_coa set balance = balance - ".$amount ." where subcode='".$akun."'");
		}
	}

	public function getPrevBalance($akun, $period)
	{
		$pervperiod = date( "Y-m", strtotime( $period."-01 -1 month" ));
		$row = $this->db->get_where("acc_gl_balance", array("coacode"=>$akun, "period"=>$pervperiod))->row();
		//echo $this->db->last_query();
		if(isset($row))
			return $row->begin_balance_db;
		else
			return 0;
	}

	public function getCurrYearBalance($akun, $minperiod, $maxperiod)
	{
		$midate = strtotime($minperiod."-01");
		$madate = strtotime($maxperiod."-01");
		$prevy = date('Y',$midate);
		$maxy = date('Y',$madate);
		$prevm = date('m',$midate);
		$maxm = date('m',$madate);

		$row = $this->db->query("select SUM(begin_balance_db) as begin_balance from acc_gl_balance where substr(coacode, 1, 3)='".$akun."' and substring(period from 1 for 4) >= '".$prevy."' and substring(period from 6 for 7) >= '".$prevm."' and substring(period from 1 for 4) <= '".$maxy."' and substring(period from 6 for 7) <= '".$maxm."'")->row();

		if(isset($row))
			return $row->begin_balance;
		else
			return 0;
	}

	public function getCurrMthBalance($akun, $minperiod, $maxperiod)
	{
		$midate = strtotime($minperiod."-01");
		$madate = strtotime($maxperiod."-01");
		$prevy = date('Y',$midate);
		$maxy = date('Y',$madate);
		$prevm = date('m',$midate);
		$maxm = date('m',$madate);

		$this->db->select("agc.coacode, sum(agc.begin_balance_db) as begin_balance_db, sum(agc.totaldb) as totaldb, sum(agc.totalkr) as totalkr, aco.name as namaakun");
		$this->db->join("acc_coa aco", "agc.coacode=aco.subcode", "left");
		$this->db->from("acc_gl_balance agc");

		$this->db->where("substring(agc.period from 1 for 4) >= '".$prevy."'", '', false);
		$this->db->where("substring(agc.period from 6 for 7) >= '".$prevm."'", '', false);
		$this->db->where("substring(agc.period from 1 for 4) <= '".$maxy."'", '', false);
		$this->db->where("substring(agc.period from 6 for 7) <= '".$maxm."'", '', false);
		$this->db->where("substr(coacode, 1, 3) = '".$akun."'", '', false);
		$this->db->group_by("agc.coacode, namaakun");
		$this->db->order_by("agc.coacode", "ASC");
		$rs = $this->db->get();
		//echo $this->db->last_query();
		$saldo  = 0;
		$prevbalance = $this->getPrevBalance($akun, $minperiod);
		foreach($rs->result() as $row)
		{
			$saldo += ($prevbalance + $row->totaldb) - $row->totalkr;
		}
		return $saldo;
	}

	public function getPrevYearBalance($akun, $period)
	{
		
		//$row = $this->db->query("select SUM(begin_balance_db) as begin_balance from acc_gl_balance where substr(coacode, 1, 3)='".$akun."' and substring(period from 1 for 4)='".$period."'")->row();

		$row = $this->db->query("select SUM(begin_balance_db) as begin_balance from acc_gl_balance where substr(coacode, 1, 3)='".$akun."' and substring(period from 1 for 4)='".$period."'")->row();
		//echo $this->db->last_query();
		if(isset($row))
			return $row->begin_balance;
		else
			return 0;
	}

	public function updateLastBalYearofCoa($akun, $yr, $amt)
	{
		$row = $this->db->get_where("acc_balance_yr", array("coacode"=>$akun, "yearperiod"=>$yr))->row();
		$this->db->set("coacode", $akun);
		$this->db->set("yearperiod", $yr);
		$this->db->set("createdate", "NOW()", false);
		$this->db->set("lastupdate", "NOW()", false);
		$this->db->set("amount", $amt);
		if(isset($row)){
			$this->db->where("coacode", $akun);
			$this->db->where("yearperiod", $yr);
			$this->db->update("acc_balance_yr");
		}
		else
		{
			$this->db->insert("acc_balance_yr");
		}
	}

	public function updatePrevBalance($akun, $period, $lastbalance, $totaldb, $totalkr, $outletcode="HO")
	{
		
		
		$row = $this->db->get_where("acc_gl_balance", array("coacode"=>$akun, "period"=>$period))->row();

		$this->db->set("coacode", $akun);
		$this->db->set("begin_balance_db", $lastbalance);
		$this->db->set("period", $period);
		$this->db->set("createdate", "NOW()", false);
		$this->db->set("lastupdate", "NOW()", false);
		$this->db->set("locationcode", $outletcode);
		$this->db->set("totaldb", $totaldb);
		$this->db->set("totalkr", $totalkr);
		if(isset($row)){
			$this->db->where("coacode", $akun);
			$this->db->where("period", $period);
			$this->db->update("acc_gl_balance");
		}
		else
		{
			$this->db->insert("acc_gl_balance");
		}
	}


	public function getLastBalanceOfCoa($yrmin, $mthmin, $yrmax, $mthmax, $prefik, $location, $prefixreff="")
	{
		$this->db->select("to_char(aks.createdate, 'YYYY-MM-DD') as createdate, to_char(aks.createdate, 'YYYY-MM') as period, aks.voucherno, aks.coaccount, akd.description, akd.amount, akd.typedk, akd.accountno, aco.name as namaakun");
		$this->db->join("acc_gl_journal_detail akd", "akd.kasid=aks.id", "left");
		$this->db->join("acc_coa aco", "akd.accountno=aco.subcode", "left");
		$this->db->from("acc_gl_journal aks");
		
		$left = strlen($prefik);

		$this->db->where("date_part('year', createdate) >= '".$yrmin."'", '', false);
		$this->db->where("date_part('month', createdate) >= '".$mthmin."'", '', false);
		$this->db->where("date_part('year', createdate) <= '".$yrmax."'", '', false);
		$this->db->where("date_part('month', createdate) <= '".$mthmax."'", '', false);
		$this->db->where("substr(akd.accountno, 1, $left) = '".$prefik."'", '', false);
		if(!empty($prefixreff)) $this->db->where("aks.type", $prefixreff);
		$this->db->where("isposting", true);
		$this->db->order_by("aco.subcode, akd.id", "ASC");
		$rs = $this->db->get();
		//echo $this->db->last_query();
		
		$saldodebet = 0;
		$saldokredit = 0;
		foreach($rs->result() as $row)
		{
			if($row->typedk == "K")
				$saldokredit += $row->amount;
			else if($row->typedk == "D")
				$saldodebet += $row->amount;
		}
		
		$balance = ($saldodebet - $saldokredit);
		return ($balance > 0) ? $balance : ($balance * -1);
	}

	public function batch_journal_detail($id, $description, $dk, $amount, $akun)
	{
		$item = array();
		$item['kasid'] = $id;
		$item['description'] = $description;
		$item['typedk'] = $dk;
		$item['amount'] = $amount;
		$item['accountno'] = $akun;
		return $item;
	}
	

}