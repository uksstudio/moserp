<?php
class Inventory_model extends CI_Model
{

	/*
	type : DO, CN
	*/
	public function _readreprint($trfno, $type="DO")
	{
		
		$rs = $this->db->query("select trnote.*, (select locationcode from locationinfo where locationid=trnote.fromlocation) as flocation, (select locationcode from locationinfo where locationid=trnote.tolocation) as tlocation from transfernote trnote where trnote.transfernotenumber='".$trfno."'");
		
		$data = array();
		foreach ($rs->result() as $key) {
			array_push($data, $key);
		}
		$json['rows']=$data;
		return $json;
	}


	public function _readreprintitems($trfnoid)
	{
		$rs = $this->db->query("select trnote.*, from transfernoteitem trnote left join productitem pro on pro.productitemid=trnote.productitem where trnote.transfernote=".$trfnoid);
		
		$data = array();
		foreach ($rs->result() as $key) {
			array_push($data, $key);
		}
		$json['rows']=$data;
		return $json;
	}

	/*
	productitem = idnya
	
	*/
	public function movementPeriode($barcode, $locationcode, $field, $value)
	{
		$this->db->where("productitemcode", (strlen($barcode) > 15) ? $barcode : strtoupper($barcode));
		$this->db->where("locationcode", $locationcode);
		$row = $this->db->get("inventory_move_open")->row();
		if(isset($row))
		{
			$this->db->set($field, "(".$field."+".$value.")", false);
			$this->db->set("lastupdate","CURRENT_DATE", false);
			$this->db->where("productitemcode",$barcode);
			$this->db->where("locationcode",$locationcode);
			$this->db->update("inventory_move_open");
		}
		else
		{
			$this->db->set('productitem', "(select productitemid from productitem where productcode='".$barcode."')", false);
			$this->db->set("productitemcode", $barcode);
			$this->db->set("location","(select locationid from locationinfo where locationcode='".$locationcode."')", false);
			$this->db->set("locationcode",$locationcode);
			$this->db->set("cost", "(select currentcost from stockbalance where productcode='".$barcode."' AND locationcode='".$locationcode."')", false);
			$this->db->set("rsp", "(select retailsalesprice from stockbalance where productcode='".$barcode."' AND locationcode='".$locationcode."')", false);
			$this->db->set($field, $value);
			$this->db->set("raisedate","CURRENT_DATE", false);
			$this->db->insert("inventory_move_open");
		}


	}
}