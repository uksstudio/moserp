<?php
class Depreciation_model extends CI_Model
{

	/*
	array(13) {
	  ["activanumber"]=>
	  string(8) "DC180001"
	  [""]=>
	  string(4) "CKSC"
	  [""]=>
	  string(10) "PV18070002"
	  [""]=>
	  string(8) "Komputer"
	  [""]=>
	  string(1) "4"
	  ["yr_fisc"]=>
	  string(1) "4"
	  ["mth_fisc"]=>
	  string(2) "48"
	  ["mth_com"]=>
	  string(2) "48"
	  ["description"]=>
	  string(4) "KOMP"
	  ["amountvalas"]=>
	  string(6) "600000"
	  ["kurs"]=>
	  string(1) "1"
	  ["rupiah"]=>
	  string(6) "600000"
	  ["tglbeli"]=>
	  string(19) "2018-06-05 10:19:10"
	}
	*/
	public function Insert()
	{
		$activanumber = $this->input->post("activanumber");
		$storecode = $this->input->post("storecode");
		$reffrenumber = $this->input->post("reffrenumber");
		$kelompok = $this->input->post("kelompok");
		$yr_com = $this->input->post("yr_com");
		$yr_fisc = $this->input->post("yr_fisc");
		$mth_fisc = $this->input->post("mth_fisc");
		$mth_com = $this->input->post("mth_com");
		$description = $this->input->post("description");
		$amountvalas = $this->input->post("amountvalas");
		$kurs = $this->input->post("kurs");
		$rupiah = $this->input->post("rupiah");
		$tglbeli = $this->input->post("tglbeli");
		$dep_id = $this->input->post("dep_id");
		//$dstaccount = $this->input->post("dstaccount");
		$coacodedb = $this->input->post("coacodedb");
		$coacodecr = $this->input->post("coacodecr");

		$journal_detail_id = $this->input->post("journal_detail_id");
		$srcaccount = $this->input->post("srcaccount");

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$this->db->where("id", $journal_detail_id);
		$this->db->update("acc_gl_journal_detail", array("depreciation"=>1));

		$this->db->set("createdate", "CURRENT_DATE", false);
		$this->db->set("storecode", $storecode);
		$this->db->set("activanumber", $activanumber);
		$this->db->set("activaname", $description);
		$this->db->set("depreciation_group", $dep_id);
		$this->db->set("reffnumber", $reffrenumber);
		$this->db->set("amtvalas", $amountvalas);
		$this->db->set("exchangerate", $kurs);
		$this->db->set("amount", $rupiah);
		$this->db->set("startingdate", $tglbeli);
		$this->db->set("count_yr_fiscal", $yr_fisc);
		$this->db->set("count_yr_comm", $yr_com);
		$this->db->set("count_mth_fisc", $mth_fisc);
		$this->db->set("count_mth_comm", $mth_com);
		$this->db->set("sisakom", $rupiah);
		$this->db->set("sisafiscal", $rupiah);
		$this->db->set("kreditaccount", $coacodecr);
		$this->db->set("debaccount", $coacodedb);
		$this->db->set("accountgroup", $srcaccount);
		$this->db->set("issuedby", $this->session->userdata("name"));

		$this->db->insert('acc_depreciation');
		$lastid = $this->db->insert_id();
		$batch = array();
		$start = strtotime($tglbeli);
		$komesial = ($rupiah / $mth_com);
		$fiskal = ($rupiah / $mth_fisc);

		$maxcount = ($mth_fisc > $mth_fisc) ? $mth_fisc : $mth_fisc;
		for($i=0; $i < $maxcount; $i++){
			//2018-07-26 17:18:54
			//date("Y-m-d h:i:s", $start);
			if($i >= $mth_fisc)
				$fiskal = 0;
			elseif ($i >= $mth_com) 
				$komesial = 0;

			$start = strtotime("+1month", $start);
			$mytime = date("Y-m-d h:i:s", $start);
			$batch[] = array("depreciationid"=>$lastid, "amountfiscal"=>$fiskal, "amountcommercial"=>$komesial,"journaldate"=>$mytime,"setjournal"=>0);
		}
		$this->db->insert_batch("acc_depreciation_detail", $batch);

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return -1;
		}
		else
			return $this->db->insert_id();
	}


	public function read()
	{
		$curYr = date("Y");
		$curMt = date("m");

		$this->db->select("ad.*,
			(select titlename from acc_depreciation_group where id=ad.depreciation_group) as titlename,
			(select SUM(amountfiscal) from acc_depreciation_detail where to_char(journaldate, 'YYYY') <  '".$curYr."' and setjournal=1 and depreciationid=ad.id) as prevyearfiscal,
			(select SUM(amountcommercial) from acc_depreciation_detail where to_char(journaldate, 'YYYY') <  '".$curYr."' and setjournal=1 and depreciationid=ad.id) as prevyearcommercial,
			(select SUM(amountfiscal) from acc_depreciation_detail where to_char(journaldate, 'YYYY') =  '".$curYr."' and setjournal=1 and depreciationid=ad.id) as curryearfiscal,
			(select SUM(amountcommercial) from acc_depreciation_detail where to_char(journaldate, 'YYYY') =  '".$curYr."' and setjournal=1 and depreciationid=ad.id) as curryearcommercial,
			(select amountfiscal from acc_depreciation_detail where to_char(journaldate, 'YYYY') =  '".$curYr."' and to_char(journaldate, 'MM') =  '".$curMt."' and setjournal=0 and depreciationid=ad.id) as currmthfiscal,
			(select amountcommercial from acc_depreciation_detail where to_char(journaldate, 'YYYY') =  '".$curYr."' and to_char(journaldate, 'MM') =  '".$curMt."' and setjournal=0 and depreciationid=ad.id) as currmthcommercial,
			(select SUM(amountfiscal) from acc_depreciation_detail where setjournal=0 and depreciationid=ad.id) as sisafiscal,
			(select SUM(amountcommercial) from acc_depreciation_detail where setjournal=0 and depreciationid=ad.id) as sisacommercial
			");
		$this->db->order_by("ad.accountgroup", "ASC");
		$rs = $this->db->get("acc_depreciation ad");
		$data = array();
		foreach ($rs->result() as $row) {
			$dep_mth_com = ($row->amount / $row->count_mth_comm);
			$dep_mth_fisc = ($row->amount / $row->count_mth_fisc);

			
			$data[] = array(
				"id"=>$row->id,
				"createdate"=>$row->createdate,
				"storecode"=>$row->storecode,
				"companycode"=>$row->companycode,
				"activanumber"=>"<a href='#' id='showdetail' class='easyui-linkbutton' plain='true' row-id='".$row->id."'>".$row->activanumber."</a>",
				"activaname"=>$row->activaname,
				"accountgroup"=>$row->accountgroup . " - ".$row->titlename,
				"depreciation_group"=>$row->depreciation_group,
				"reffnumber"=>$row->reffnumber,
				"amtvalas"=>$row->amtvalas,
				"storecode1"=>$row->storecode1,
				"storecode2"=>$row->storecode2,
				"exchangerate"=>$row->exchangerate,
				"amount"=>$row->amount,
				"startingdate"=>date_format(date_create($row->startingdate), 'd-m-Y'),
				"startingmonth"=>$row->startingmonth,
				"adjcom"=>$row->adjcom,
				"adjfiscal"=>$row->adjfiscal,
				"count_yr_fiscal"=>$row->count_yr_fiscal,
				"count_yr_comm"=>$row->count_yr_comm,
				"count_mth_fisc"=>$row->count_mth_fisc,
				"count_mth_comm"=>$row->count_mth_comm,
				"dep_mth_comm"=>$dep_mth_com,
				"dep_mth_fisc"=>$dep_mth_fisc,
				"dep_prev_yr_fisc"=>$row->prevyearfiscal,
				"dep_prev_yr_comm"=>$row->prevyearcommercial,
				"dep_curr_yr_comm"=>$row->curryearcommercial,
				"dep_curr_yr_fisc"=>$row->curryearfiscal,
				"adj_comm"=>($row->adjcom > 0) ? $row->adjcom : 0,
				"adj_fisc"=>($row->adjfiscal > 0) ? $row->adjfiscal : 0,
				"dep_curr_mth_fisc"=>($row->adjcom > 0) ? 0 : $row->currmthfiscal,
				"dep_curr_mth_comm"=>($row->adjfiscal > 0) ? 0 : $row->currmthcommercial,
				"sisakom"=>($row->sisacommercial - $row->adjcom),
				"sisafiscal"=>($row->sisafiscal - $row->adjfiscal),
				"debaccount"=>$row->debaccount,
				"kreditaccount"=>$row->kreditaccount
			);
		}
		return $data;
	}


	public function setAdjustment($id)
	{
		$adj_comm = $this->input->post("adj_comm");
		$adj_fisc = $this->input->post("adj_fisc");
		$remarkadj = $this->input->post("remarkadj");


		$this->db->trans_strict(TRUE);
		$this->db->trans_start();

		$this->db->where("id", $id);
		$ok = $this->db->update("acc_depreciation", array("adjcom"=>$adj_comm,"adjfiscal"=>$adj_fisc,"adjustby"=>$this->session->userdata("name"),"remarkadj"=>$remarkadj));

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return -1;
		}
		else
			return $ok;
	}


	public function setJournal($depid)
	{

		$this->db->trans_strict(TRUE);
		$this->db->trans_start();
		$curYr = date("Y");
		$curMt = date("m");

		$data['type'] = "JM";
		$data['invoice'] = $data['type'].date('ym').$this->Finance_model->getCountKas("",$data['type']);

		//$sql = $this->db->query("select storecode, activanumber, activaname, kreditaccount, debaccount from acc_depreciation where IN (".$depid.")");
		//$row = $sql->row();

		$det = $this->db->query("select * from acc_depreciation_detail where depreciationid IN (".$depid.") AND to_char(journaldate, 'YYYY') =  '".$curYr."' and to_char(journaldate, 'MM') =  '".$curMt."' and setjournal=0");
		$rdet = $det->row();
		
		//$this->db->set("createdate", "CURRENT_DATE", false);
		$this->db->set("createdate", $this->input->post("tanggal"));
		$this->db->set("voucherno", $data['invoice']);
		$this->db->set("remark", "");
		$this->db->set("supplierid", 0);
		$this->db->set("ceknumber", "");
		//$this->db->set("coaccount", $row->kreditaccount);
		$this->db->set("status", 0);
		$this->db->set("paymentto", "Beban Penyusutan");
		$this->db->set("locationcode", $this->input->post("jrstorecode"));
		$this->db->set("type", $data['type']);
		$this->db->set("balance", 0, false);
		$this->db->set("companyid", $this->session->userdata("compid"));
		$this->db->set("createby", $this->session->userdata("name"));
		$this->db->insert("acc_gl_journal");
		$id = $this->db->insert_id();

		
		$in_item = array();
		$in_item[] = array("kasid"=>$id, "description"=>$this->input->post("jrname"), "typedk"=>"D", "amount"=>$this->input->post("jramount"), "accountno"=>$this->input->post("jrdbaccount"));
		$in_item[] = array("kasid"=>$id, "description"=>$this->input->post("jrname"), "typedk"=>"K", "amount"=>$this->input->post("jramount"), "accountno"=>$this->input->post("jrcraccount"));
		
		$this->db->insert_batch('acc_gl_journal_detail',$in_item);
		foreach ($det->result() as $rd) {
			$this->db->query("update acc_depreciation_detail set setjournal = 1 where id=".$rd->id);
		}
		

		$this->db->trans_complete();

	
		if ($this->db->trans_status() === FALSE) return false;
		else return true;

	}

	public function changeLocation($id)
	{
		$this->db->trans_strict(TRUE);
		$this->db->trans_start();
		
		$this->db->set("storecode", $this->input->post("storecode1"));
		$this->db->set("storecode1", $this->input->post("storecode"));
		$this->db->where("id", $id);
		$ok = $this->db->update("acc_depreciation");


		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) return false;
		else return true;
	}
}