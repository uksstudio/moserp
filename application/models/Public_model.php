<?php
class Public_model extends CI_Model
{

	public function getLastStkReceipt($outletcode, $code, $terminal='P9999')
	{

		$row = $this->db->get_where('locationinfo', array('locationcode'=>$outletcode))->row();
		$terminal = $this->db->get_where('terminalinfo', array('location'=>$row->locationid, 'terminalcode'=>$terminal))->row();

		$this->db->where("locationcode", $outletcode);
		$this->db->set("stockreceiptseq", "(stockreceiptseq  + 1)", false);
		$this->db->update("locationinfo");
		$actual_count = ($row->stockreceiptseq+1);
		$incr = substr("000000",strlen($actual_count)) . $actual_count;
		return $terminal->invoicenumberprefix.$code.$incr;
	}

	public function getLastStkIssue($outletcode, $code)
	{

		$row = $this->db->get_where('locationinfo', array('locationcode'=>$outletcode))->row();
		$terminal = $this->db->get_where('terminalinfo', array('location'=>$row->locationid, 'terminalcode'=>'P9999'))->row();

		$this->db->where("locationcode", $outletcode);
		$this->db->set("stockissueseq", "(stockissueseq  + 1)", false);
		$this->db->update("locationinfo");

		return $terminal->invoicenumberprefix.$code.($row->stockissueseq+1);
	}

	public function getCountPonum()
	{
		$total_count = $this->db->count_all_results('po_header', array('EXTRACT(YEAR from createdate)'=>date('Y')));
		$actual_count = ($total_count +1);
		$this->db->reset_query();
		return substr("0000",strlen($actual_count)) . $actual_count;
	}

	public function getCountCosting()
	{
		$total_count = $this->db->count_all_results('costing_header', array('EXTRACT(YEAR from costingdate)'=>date('Y')));
		$actual_count = ($total_count +1);
		
		$this->db->reset_query();
		return substr("0000",strlen($actual_count)) . $actual_count;
	}

	public function getChangePriceReff()
	{
		$total_count = $this->db->count_all_results('stockadjustment', array('EXTRACT(YEAR from raisedate)'=>date('Y')));
		$actual_count = ($total_count +1);
		$this->db->reset_query();
		return substr("0000",strlen($actual_count)) . $actual_count;
	}

	public function getAdjustmentReff()
	{
		$total_count = $this->db->count_all_results('stockadjustment', array('EXTRACT(YEAR from raisedate)'=>date('Y')));
		$actual_count = ($total_count +1);
		$this->db->reset_query();
		return "AJ".date('ym').substr("0000",strlen($actual_count)) . $actual_count;
	}

	public function getCountPosting()
	{
		$total_count = $this->db->count_all_results('post_invcost_header', array('EXTRACT(YEAR from issuedate)'=>date('Y')));
		$actual_count = ($total_count +1);
		$this->db->reset_query();
		return substr("0000",strlen($actual_count)) . $actual_count;
	}	

	public function getCountTransfernotebyStr($i_flocation)
	{
		$total_count = $this->db->count_all_results('transfernote', array('EXTRACT(YEAR from createdatetime)'=>date('Y'), 'fromlocation'=>$i_flocation));
		$actual_count = ($total_count +1);
		$this->db->reset_query();
		return substr("00000",strlen($actual_count)) . $actual_count;
	}	
	

	public function getCountReportPosting()
	{
		$total_count = $this->db->count_all_results('post_report_items_header', array('EXTRACT(YEAR from datetime)'=>date('Y')));
		$actual_count = ($total_count +1);
		$this->db->reset_query();
		return 'RC'.date('ym').substr("0000",strlen($actual_count)) . $actual_count;
	}

	private function getValueSize($arrJson, $key)
	{
		foreach ($arrJson as $k => $value) {
			if($value['nsize'] == $key) return $value['nqty'];
		}
		return 0;
	}

	public function distincSizeGrid($sizecode="", $selected = false, $arraySize=array())
	{
		if(($sizecode != "") && !$selected)
		{
			$rs = $this->db->get_where("size_grid", array('size_code'=>$sizecode));
			$group = array();
			foreach($rs->result() as $row)
			{
				$group[] = array('name'=>$row->size_field, 'value'=>$this->getValueSize($arraySize, $row->size_field),'group'=>$row->size_code, 'editor'=>'numberbox');
			}
			$data['sizeitem'] = $arraySize;
			$data['rows'] = $group;
			$data['total'] = count($group);
			
		}
		else
		{
			$this->db->distinct();
			$this->db->select('size_code');
			$rs = $this->db->get('size_grid');
			$data = array();
			foreach($rs->result() as $row)
			{
				$items = array();
				$items['id'] = $row->size_code;
				$items['text'] = $row->size_code;
				if($sizecode == $row->size_code) $items['selected'] = true; 
				$data[] = $items;
			}
		}
		
		return json_encode($data);
	}

	public function getCurrency()
	{
		$rs = $this->db->get("currency");
		
		$curr = array();
		foreach($rs->result() as $row)
		{
			$curr[] = array("id"=>(string)$row->currencyid, "text"=>$row->currencycode,"kurs"=>$row->exchangerate);
		}
		return json_encode($curr);
	}

	public function getCostingnumber()
	{
		$rs = $this->db->get_where("costing_header", array("isposting"=>false, "costingstatus"=>"NEW"));
		$data = array();
		foreach($rs->result() as $row)
		{
			$data[] = array("costingid"=>(string)$row->id, "costingnumber"=>$row->costingnumber,"exchangerate"=>$row->exchangerate,"invoicenumber"=>$row->invoicenumber);
		}
		$rs->free_result();
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}

	public function getExchangeRate()
	{
		$rs = $this->db->get("currency");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}

	public function getSupplierInfo()
	{
		$q = $this->input->get("q");
		if(!empty($q))
		{
			$this->db->like("suppliercode", strtoupper($q));
			$this->db->or_like("suppliername", strtoupper($q));
		}
		$rs = $this->db->get("supplierinfo");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		//$rows['sql'] = $this->db->last_query();
		return json_encode($rows);
	}

	public function getProductCategory($isjson = true)
	{
		$q = $this->input->get("q");
		if(!empty($q))
		{
			$this->db->like("productcategorycode", strtoupper($q));
			$this->db->or_like("productcategorydesc", strtoupper($q));
		}
		$rs = $this->db->get("productcategory");
		$data = array();
		$markup = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
			$markup[$row->productcategorycode] = $row->markup;
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		if($isjson)
			return json_encode($rows);
		else
			return $markup;
	}

	public function getProductSex()
	{
		$q = $this->input->get("q");
		if(!empty($q))
		{
			$this->db->like("sexcode", strtoupper($q));
			$this->db->or_like("description", strtoupper($q));
		}
		$rs = $this->db->get("gender");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}

	public function getProductColor()
	{
		$q = $this->input->get("q");
		if(!empty($q))
		{
			$this->db->like("colorcode", strtoupper($q));
			$this->db->or_like("colordescription", strtoupper($q));
		}
		$rs = $this->db->get("productcolor");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}

	public function getProductLabel()
	{
		$q = $this->input->get("q");
		if(!empty($q))
		{
			$this->db->like("productgroupcode", strtoupper($q));
			$this->db->or_like("productgroupdesc", strtoupper($q));
		}
		$rs = $this->db->get("productgroup");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}

	public function getProductSeason()
	{
		$q = $this->input->get("q");
		if(!empty($q))
		{
			$this->db->like("productseasoncode", strtoupper($q));
		}
		$rs = $this->db->get("productseason");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}

	public function getPaymentterm()
	{
		$rs = $this->db->get("paymentterm");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}

	public function getProductBrand()
	{
		$q = $this->input->get("q");
		if(!empty($q))
		{
			$this->db->like("productbrandcode", strtoupper($q));
			$this->db->or_like("productbranddesc", strtoupper($q));
			$this->db->or_like("brandcode", strtoupper($q));
		}
		$this->db->where("ishide", 0);
		$rs = $this->db->get("productbrand");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);	
	}

	public function getListNewPO()
	{
		$rs = $this->db->get_where("po_header", array('status'=>'NEW'));
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}


	public function getOutletList()
	{
		$rs = $this->db->get("locationinfo");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}

	public function getAdjustmentType()
	{
		$rs = $this->db->get("stockadjustmenttype");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}

	public function getTerminal($outletid)
	{
		$rs = $this->db->get_where("terminalinfo", array("location"=>$outletid));
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}

	public function getCompanyDivision($comp_id)
	{
		$rs = $this->db->get_where("company_division", array('company_id'=>$comp_id));

		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}


	public function getCompany()
	{
		$rs = $this->db->get("company");
		$data = array();
		foreach($rs->result() as $row)
		{
			array_push($data, $row);
		}
		$rows['rows'] = $data;
		$rows['total'] = $rs->num_rows();
		return json_encode($rows);
	}

}
?>