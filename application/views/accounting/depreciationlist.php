
<table id="dgmodul" class="easyui-datagrid"  title="Depreciasion"
    data-options="rownumbers:true,singleSelect:false, fit: true, url:'Accounting/depreciationlist/show',method:'get',toolbar:'#toolbar', showFooter: 'true'">
    <thead>
        <tr>
            <th data-options="field:'activanumber',width:100" rowspan="2">Number</th>
            <th data-options="field:'storecode',width:80, formatter:function(val, row){if(row.storecode1 != '' && row.storecode1 != null) return row.storecode1 + '-'+row.storecode; else return row.storecode;}" rowspan="2">Location Code</th>
            <th data-options="field:'activaname',width:200" rowspan="2">Name</th>
            <th data-options="field:'reffnumber',width:100" rowspan="2">Reff</th>
            <th data-options="field:'accountgroup',width:100" rowspan="2">Account Group</th>
            <th data-options="field:'startingdate',width:100" rowspan="2">Buying Date</th>
            <th colspan="2">Eco Life Time /Yr</th>
            <th colspan="2">Eco Life Time /Mt</th>
            <th colspan="3">Aquisition Cost</th>
            <th colspan="2">Depresiasi /Mth</th>
            <th colspan="2">Dep Prev Year</th>
            <th colspan="2">Dep Curr Year</th>
            <th colspan="2">Adjustment</th>
            <th colspan="2">Dep Curr Month</th>
            <th colspan="2">Amount Value</th>
        </tr>
        <tr>
            <th data-options="field:'count_yr_comm',width:60,align:'center'" formatter='formatPrice'>Kom</th>
            <th data-options="field:'count_yr_fiscal',width:60,align:'center'" formatter='formatPrice'>Fisc</th>
            <th data-options="field:'count_mth_comm',width:60,align:'center'" formatter='formatPrice'>Kom</th>
            <th data-options="field:'count_mth_fisc',width:60,align:'center'" formatter='formatPrice'>Fisc</th>
            <th data-options="field:'amtvalas',width:100,align:'right'" formatter='formatPrice'>Valas</th>
            <th data-options="field:'exchangerate',width:50,align:'center'" formatter='formatPrice'>Kurs</th>
            <th data-options="field:'amount',width:100,align:'right'" formatter='formatPrice'>Amount</th>
            <th data-options="field:'dep_mth_comm',width:100,align:'right'" formatter='formatPrice'>Kom</th>
            <th data-options="field:'dep_mth_fisc',width:100,align:'right'" formatter='formatPrice'>Fisc</th>
            <th data-options="field:'dep_prev_yr_comm',width:100,align:'right'" formatter='formatPrice'>Kom</th>
            <th data-options="field:'dep_prev_yr_fisc',width:100,align:'right'" formatter='formatPrice'>Fisc</th>
            <th data-options="field:'dep_curr_yr_comm',width:100,align:'right'" formatter='formatPrice'>Kom</th>
            <th data-options="field:'dep_curr_yr_fisc',width:100,align:'right'" formatter='formatPrice'>Fisc</th>
            <th data-options="field:'adj_comm',width:100,align:'right'" formatter='formatPrice'>Kom</th>
            <th data-options="field:'adj_fisc',width:100,align:'right'" formatter='formatPrice'>Fisc</th>
            <th data-options="field:'dep_curr_mth_comm',width:100,align:'right'" formatter='formatPrice'>Kom</th>
            <th data-options="field:'dep_curr_mth_fisc',width:100,align:'right'" formatter='formatPrice'>Fisc</th>
            <th data-options="field:'sisakom',width:100,align:'right'" formatter='formatPrice'>Kom</th>
            <th data-options="field:'sisafiscal',width:100,align:'right'" formatter='formatPrice'>Fisc</th>
        </tr>
    </thead>
</table>

 <div id="toolbar">
 	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">New</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="adjustment()">Adjustment</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="setJournal()">Set Journal Memorial</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="changeLocation()">Move Location</a>
</div>


<div id="dlg" class="easyui-dialog" style="width:700px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
    <?php echo form_open("",'novalidate style="margin:0;padding:10px 30px" id="fm"');?>
        <table >
            <tr>
                <td>
                    <div style="margin-bottom:10px">
                        <input name="activanumber" id="activanumber" class="easyui-textbox" label="Nomor:" value="Automatic" style="width:100%" readonly="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="storecode" class="easyui-combobox"  id="storecode" required="true" label="Location Code :" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="depreciation_group" class="easyui-combogrid" id="depreciation_group" required="true" label="Src Account :" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="reffnumber" id="reffnumber" class="easyui-combogrid" required="true" label="Reff Number:" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="coacodedb" class="easyui-combogrid" id="coacodedb" required="true" label="Db Account :" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="coacodecr" class="easyui-combogrid" id="coacodecr" required="true" label="Cr Account :" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="activaname" class="easyui-textbox" id="activaname" required="true" label="Description :" style="width:100%">
                    </div>
                </td>
                <td>&nbsp;</td>
                <td valign="top">
                    <div style="margin-bottom:10px">
                        <input name="descr" id="descr" class="easyui-textbox" autocomplete="false" label="Journal desc:" style="width:100%" >
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="amount" id="amount" class="easyui-numberbox" required="true" label="Amount:" style="width:100%" >
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="kurs" id="kurs" class="easyui-numberbox" required="true" label="Kurs:" style="width:100%" value="1">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="rupiah" id="rupiah" class="easyui-numberbox" required="true" label="Rupiah:" style="width:100%" readonly="true">
                    </div>
                </td>
            </tr>
        </table>
        
    </form>
</div>
<div id="dlg-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" style="width:90px">Save</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
</div>


<div id="dlgadj" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlgadj-buttons'">
    <?php echo form_open("",'novalidate style="margin:0;padding:10px 30px" id="fmadj"');?>
        <table width="100%">
            <tr>
                <td>
                    <div style="margin-bottom:10px">
                        <input name="id" id="id" class="easyui-textbox" label="ID:" style="width:100%" readonly="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="adj_comm" class="easyui-numberbox"  id="adj_comm" required="true" label="Adj Commercial :" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="adj_fisc" class="easyui-numberbox" id="adj_fisc" required="true" label="Adj Fiscal :" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="coacode2" class="easyui-combogrid" id="coacode2" required="true" label="Cr Account :" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="remarkadj" class="easyui-textbox" id="remarkadj" style="width:100%;height:60px" required="true" data-options="label:'Remark :',multiline:true">
                    </div>
                </td>
                
            </tr>
        </table>
        
    </form>
</div>
<div id="dlgadj-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="setAdjustment()" style="width:90px">Save</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgadj').dialog('close')" style="width:90px">Cancel</a>
</div>


<div id="dlgmloc" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlgmloc-buttons'">
    <?php echo form_open("",'novalidate style="margin:0;padding:10px 30px" id="fmmloc"');?>
        <table width="100%">
            <tr>
                <td>
                    <div style="margin-bottom:10px">
                        <input class="easyui-combobox" id="storecode1" name="storecode" style="width:40%" data-options="label:'Fr Loc:',required:true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input class="easyui-combobox" id="storecode2" name="storecode1" style="width:40%" data-options="label:'To Loc:',required:true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="remarkmloc" class="easyui-textbox" id="remarkmloc" style="width:100%;height:60px" required="true" data-options="label:'Remark :',multiline:true">
                    </div>
                </td>
                
            </tr>
        </table>
        
    </form>
</div>
<div id="dlgmloc-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="setChangeLocation()" style="width:90px">Save</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgmloc').dialog('close')" style="width:90px">Cancel</a>
</div>



<div id="dlgjour" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlgjour-buttons'">
    <?php echo form_open("",'novalidate style="margin:0;padding:10px 30px" id="fmjour"');?>
        <table width="100%">
            <tr>
                <td>
                    <div style="margin-bottom:10px">
                        <input name="id" id="idjournal" class="easyui-textbox" label="ID:" style="width:100%" readonly="true">
                    </div>
                    
                     <div style="margin-bottom:10px">
                        <input class="easyui-datetimebox" id="tanggal" name="tanggal" style="width:100%" data-options="label:'Date Time:', showSeconds:true, required:true" >
                    </div>
                     <div style="margin-bottom:10px">
                        <input name="jrstorecode" class="easyui-combobox"  id="jrstorecode" required="true" label="Location Code :" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="jramount" class="easyui-numberbox"  id="jramount" required="true" label="Amount :" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="jrname" class="easyui-textbox"  id="jrname" required="true" label="Desc :" style="width:100%">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="jrcraccount" id="jrcraccount" class="easyui-textbox" label="Cr Account:" style="width:100%" readonly="true">
                    </div>
                    <div style="margin-bottom:10px">
                        <input name="jrdbaccount" id="jrdbaccount" class="easyui-textbox" label="Db Account:" style="width:100%" readonly="true">
                    </div>                    
                </td>
                
            </tr>
        </table>
        
    </form>
</div>
<div id="dlgjour-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveJournal()" style="width:90px">Save</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgjour').dialog('close')" style="width:90px">Cancel</a>
</div>

<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var src_account = "";
	$(function () {
		$('#dgmodul').datagrid({
            onLoadSuccess:function(){
            $(this).datagrid('getPanel').find('.easyui-linkbutton').each(function(){
                $(this).linkbutton({
                    onClick:function(){
                        var id = $(this).attr('row-id');
                        //alert("oke id " + id);
                        // destroy the row
                    }
                })
            })
        }
        });


        $("#storecode").combobox({
            selectOnNavigation: false,
            valueField: 'locationcode',
            textField: 'locationcode',
            url:'Finance/getLocationCode',
            method: 'get',
            editable: true,
            required: true,    
            validType: 'exists["#storecode"]',
            onLoadSuccess: function () { },
            filter: function (q, row) {
                var opts = $(this).combobox('options');
                //return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
                return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) >= 0;
            },
            onSelect: function(record)
            {
                
            }
        });

        $("#jrstorecode").combobox({
            selectOnNavigation: false,
            valueField: 'locationcode',
            textField: 'locationcode',
            url:'Finance/getLocationCode',
            method: 'get',
            editable: true,
            required: true,    
            validType: 'exists["#jrstorecode"]',
            onLoadSuccess: function () { },
            filter: function (q, row) {
                var opts = $(this).combobox('options');
                //return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
                return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) >= 0;
            },
            onSelect: function(record)
            {
                
            }
        });

        getCmbCoa("#coacode");
        getCmbCoa("#coacodedb");
		getCmbCoa("#coacodecr");
        getCmbCoa("#coacode2");

        
        
        $("#storecode1").combobox({
            selectOnNavigation: false,
            valueField: 'locationcode',
            textField: 'locationcode',
            url:'Finance/getLocationCode',
            method: 'get',
            editable: true,
            required: true,    
            validType: 'exists["#storecode1"]',
            onLoadSuccess: function () { },
            filter: function (q, row) {
                var opts = $(this).combobox('options');
                //return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
                return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) >= 0;
            },
            onSelect: function(record)
            {
                
            }
        });

        $("#storecode2").combobox({
            selectOnNavigation: false,
            valueField: 'locationcode',
            textField: 'locationcode',
            url:'Finance/getLocationCode',
            method: 'get',
            editable: true,
            required: true,    
            validType: 'exists["#storecode2"]',
            onLoadSuccess: function () { },
            filter: function (q, row) {
                var opts = $(this).combobox('options');
                //return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
                return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) >= 0;
            },
            onSelect: function(record)
            {
                
            }
        });


        $('#reffnumber').combogrid({
            panelWidth:500,
            url: "finance/getJournalDetail/"+src_account,
            idField:'id',
            textField:'voucherno',
            mode:'remote',
            fitColumns:true,
            method: 'get',
            columns:[[
                {field:'voucherno',title:'Voucer No',width:80},
                {field:'description',title:'Description',width:99},
                {field:'accountno',title:'Account',width:60},
                {field:'typedk',title:'D/K',width:50},
                {field:'amount',title:'Amount',width:80}
            ]],
            onChange:function(value){
                var g = $('#reffnumber').combogrid('grid');
                g.datagrid('load',{q:value});
                console.log("find id : "+value);
            },
            onSelect: function(index,row){
                var codigo = row.amount;
                $('#amount').textbox('setText',codigo);
                $('#descr').textbox('setText',row.description);
                calculateTotal(codigo);
                
            }
        });


        $('#depreciation_group').combogrid({
            panelWidth:500,
            url: "finance/getDepreciationGroup",
            idField:'titlename',
            textField:'titlename',
            mode:'remote',
            fitColumns:true,
            method: 'get',
            columns:[[
                {field:'titlename',title:'Name',width:80},
                {field:'yr_commercial',title:'Yr Com',width:60},
                {field:'yr_fiscal',title:'Yr FIsc',width:60},
                {field:'mth_commercial',title:'Mth Com',width:60},
                {field:'mth_fiscal',title:'Mth FIsc',width:60},
                {field:'account',title:'Account',width:60}
            ]],
            onChange:function(value){
                var g = $('#depreciation_group').combogrid('grid');
                g.datagrid('load',{q:value});
            },
            onSelect: function(index,row){
                var groupcode = row.groupcode;
                src_account = row.account;
                console.log(row);
                $('#reffnumber').combogrid('grid').datagrid('reload', {q: src_account});
                $.get( "Accounting/getDepreciationNumber?akun="+groupcode, function( data ) {
                    $( "#activanumber" ).textbox('setText',groupcode.trim()+data);
                });
                
            }
        });
        $('#amount').numberbox({
            inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
                keyup:function(e){
                    calculateTotal($("#amount").numberbox('getText'));
                }
            })
        })

        $('#kurs').numberbox({
            inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
                keyup:function(e){
                    calculateTotal($("#amount").numberbox('getText'));
                }
            })
        })
        
	});


    $.fn.datetimebox.defaults.parser = function(s){
        if (!s){return new Date();}
        var dt = s.split(' ');
        var dd = dt[0].split('-');
        var date = new Date(dd[0],parseInt(dd[1])-1,dd[2]);
        if (dt.length>1){
            dd = dt[1].split(':');
            date.setHours(dd[0]);
            date.setMinutes(dd[1]);
            date.getSeconds(dd[2])
        }
        return date;
    }
    $.fn.datetimebox.defaults.formatter = function(date){
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
        return y+'-'+_ff(m)+'-'+_ff(d) + ' ' + _ff(date.getHours())+':'+_ff(date.getMinutes()) +':'+_ff(date.getSeconds());
    }

    

    function _ff(v) {
        return (v < 10 ? "0" : "") + v;
    }

    function formatPrice(val,row){
        val = (val == null) ? 0 : val;
        return number_format(val, false, ".", ",");
    }


    function calculateTotal(value)
    {
        var total = 0;
        total = parseFloat(value) || 0;
        var kurs = parseFloat($("#kurs").numberbox('getText')) || 0;
        total = (total * kurs);
        $('#rupiah').numberbox('setValue', total);
    }
	var url;
    function newUser(){
    	csrf = $("input[name*='csrf_name']").val();
        $('#dlg').dialog('open').dialog('center').dialog('setTitle','Add New');
        $('#fm').form('clear');
        $("input[name*='csrf_name']").val(csrf);
        url = 'Accounting/depreciationlist/add';
    }
    function adjustment(){
        var row = $('#dgmodul').datagrid('getSelected');
        if (row){
            if(row.adj_comm > 0){
                $.messager.alert('Failed',"Please select For journal.",'error');
            }
            else
            {
                $('#dlgadj').dialog('open').dialog('center').dialog('setTitle','Update');
                $('#fmadj').form('load',row);
                url = 'Accounting/depreciationlist/setadjustment/'+row.id;    
            }
            
        }
    }

    function changeLocation()
    {

        var row = $('#dgmodul').datagrid('getSelected');
        if (row){
            csrf = $("input[name*='csrf_name']").val();
            $('#dlgmloc').dialog('open').dialog('center').dialog('setTitle','Change Location');
            $('#fmmloc').form('load',row);
            $("input[name*='csrf_name']").val(csrf);
            url = 'Accounting/depreciationlist/changeLocation/'+row.id;
        }

        
    }


    function setJournal()
    {
        var ids = [];
        var rows = $('#dgmodul').datagrid('getSelections');
        if(rows.length <=0)
        {
            $.messager.alert('Failed',"Please select For journal.",'error');
            return;
        }

        var jramount = 0;
        var craccount = "";
        var dbaccount = "";
        var storecode = "";
        var ids = new Array();
        for(var i=0; i<rows.length; i++){
            ids.push(rows[i].id);
            if(rows[i].dep_curr_mth_comm > 0)
            {
                console.log("dep_curr_mth_fisc > " + rows[i].dep_curr_mth_comm);
                //$.get( "Accounting/depreciationlist/setjournal/"+rows[i].id, function( data ) {
                //    $('#dgmodul').datagrid('reload');
                //});
                jramount += parseFloat(rows[i].dep_curr_mth_comm);
            }
            craccount = rows[i].kreditaccount;
            dbaccount = rows[i].debaccount;
            storecode = rows[i].storecode;
        }

        if(jramount <= 0)
        {
            $.messager.alert('Failed',"Current Month Depreciasion must be greater than zero.",'error');
            return;
        }
        
        csrf = $("input[name*='csrf_name']").val();
        $('#dlgjour').dialog('open').dialog('center').dialog('setTitle','Journal');
        $('#fmjour').form('clear');
        $("input[name*='csrf_name']").val(csrf);
        $("#idjournal").textbox('setText', ids.join(","));
        $("#jramount").numberbox('setText', jramount);
        $("#jrcraccount").textbox('setText', craccount);
        $("#jrdbaccount").textbox('setText', dbaccount);
        $('#jrstorecode').combobox('setValue',storecode.trim());
        url = 'Accounting/depreciationlist/setjournal';
    }


    function saveJournal()
    {
        csrf = $("input[name*='csrf_name']").val();
        var idjournal = $("#idjournal").textbox('getText');
        var jrname = $("#jrname").textbox('getText');
        var jramount = $("#jramount").numberbox('getText');
        var jrdbaccount = $('#jrdbaccount').textbox('getText');
        var jrcraccount = $('#jrcraccount').textbox('getText');
        var jrstorecode = $('#jrstorecode').combobox('getText');
        var tanggal = $('#tanggal').datetimebox('getValue');

        if(jrname == "")
        {
            $.messager.alert('Failed',"Please input description.",'error');
            return;
        }

        if(tanggal == "")
        {
            $.messager.alert('Failed',"Please input Periode.",'error');
            return;
        }

        $.post( url, 
        {
            csrf_name:csrf,
            idjournal:idjournal,
            jramount:jramount,
            jrdbaccount:jrdbaccount,
            jrcraccount:jrcraccount,
            jrname:jrname,
            jrstorecode:jrstorecode,
            tanggal:tanggal
        },"json")
        .done(
            function(msg)
            {
                var obj = jQuery.parseJSON( msg );
                $("input[name*='csrf_name']").val(obj.csrf_name);
                $('#dlgjour').dialog('close');
                $('#dgmodul').datagrid('reload');
                if(obj.status == 0)
                {
                    $.messager.alert('Failed',obj.msg,'error'); 
                }
                console.log(obj);
            }
         )
        .fail(function(xhr, status, error) {
            // error handling
            
            console.log(xhr.status);
            console.log(error);
            console.log(status);
            $.messager.alert('Failed',xhr.status + "("+error+")",'error');
        });
    }

    function setChangeLocation()
    {
        csrf = $("input[name*='csrf_name']").val();
        var storecode = $("#storecode1").combobox('getText');
        var storecode1 = $("#storecode2").combobox('getText');
        var remarkmloc = $('#remarkmloc').textbox('getText');
        $.post( url, 
        {
            csrf_name:csrf,
            storecode:storecode,
            storecode1:storecode1,
            remarkmloc:remarkmloc
        },"json")
        .done(
            function(msg)
            {
                var obj = jQuery.parseJSON( msg );
                $("input[name*='csrf_name']").val(obj.csrf_name);
                $('#dlgmloc').dialog('close');
                $('#dgmodul').datagrid('reload');
                if(obj.status == 0)
                {
                    $.messager.alert('Failed',obj.msg,'error'); 
                }
                console.log(obj);
            }
         )
        .fail(function(xhr, status, error) {
            // error handling
            
            console.log(xhr.status);
            console.log(error);
            console.log(status);
            $.messager.alert('Failed',xhr.status + "("+error+")",'error');
        });
    }
    function setAdjustment()
    {
        csrf = $("input[name*='csrf_name']").val();
        var adj_comm = $('#adj_comm').numberbox('getText');
        var adj_fisc = $('#adj_fisc').numberbox('getText');
        var remarkadj = $('#remarkadj').textbox('getText');
        $.post( url, 
        {
            csrf_name:csrf,
            adj_comm:adj_comm,
            adj_fisc:adj_fisc,
            remarkadj:remarkadj
        },"json")
        .done(
            function(msg)
            {
                var obj = jQuery.parseJSON( msg );
                $("input[name*='csrf_name']").val(obj.csrf_name);
                $('#dlgadj').dialog('close');
                $('#dgmodul').datagrid('reload');

                if(obj.status == 0)
                {
                    $.messager.alert('Failed',obj.msg,'error'); 
                }

                
                console.log(obj);
            }
         )
        .fail(function(xhr, status, error) {
            // error handling
            
            console.log(xhr.status);
            console.log(error);
            console.log(status);
            $.messager.alert('Failed',xhr.status + "("+error+")",'error');
        });
    }

    function saveUser(){
        var reffnumbergrid = $('#reffnumber').combogrid('grid').datagrid('getSelected');
        var cocdb = $('#coacodedb').combogrid('grid').datagrid('getSelected');
        var coccr = $('#coacodecr').combogrid('grid').datagrid('getSelected');
        var depreciation_groupgrid = $('#depreciation_group').combogrid('grid').datagrid('getSelected');
        console.log(url);
        //return;
        csrf = $("input[name*='csrf_name']").val();
        var activanumber = $('#activanumber').textbox('getText');
        var storecode = $('#storecode').combobox('getText');
        var tglbeli = reffnumbergrid.createdate;
        var reffrenumber = reffnumbergrid.voucherno;
        var kelompok = depreciation_groupgrid.titlename;
        var yr_com = depreciation_groupgrid.yr_commercial;
        var yr_fisc = depreciation_groupgrid.yr_fiscal;
        var mth_com = depreciation_groupgrid.mth_commercial;
        var mth_fisc = depreciation_groupgrid.mth_fiscal;
        var dep_id = depreciation_groupgrid.id;
        var journal_detail_id = reffnumbergrid.id;
        var srcaccount = depreciation_groupgrid.account;

        var description = $('#activaname').textbox('getText');
        //var dstaccount = coacode.subcode;
        var coacodedb = cocdb.subcode;
        var coacodecr = coccr.subcode;
        var amountvalas = $('#amount').numberbox('getText');
        var kurs = $('#kurs').numberbox('getText');
        var rupiah = $('#rupiah').numberbox('getText');
        
        if(coacodedb == "")
        {
            $.messager.alert('Failed',"Please select Db Account for Credit.",'error');
            return;
        }

        if(coacodedb == null)
        {
            $.messager.alert('Failed',"Please select Db Account for Credit.",'error');
            return;
        }
        
        if(coacodecr == "")
        {
            $.messager.alert('Failed',"Please select Cr Account for Credit.",'error');
            return;
        }

        if(coacodecr == null)
        {
            $.messager.alert('Failed',"Please select Cr Account for Credit.",'error');
            return;
        }

    	$.post( url, 
        {
            csrf_name:csrf,
            activanumber:activanumber,
            storecode:storecode,
            reffrenumber:reffrenumber,
            kelompok:kelompok,
            yr_com:yr_com,
            yr_fisc:yr_fisc,
            mth_fisc:mth_fisc,
            mth_com:mth_com,
            description:description,
            amountvalas:amountvalas,
            kurs:kurs,
            rupiah:rupiah,
            tglbeli:tglbeli,
            dep_id:dep_id,
            journal_detail_id:journal_detail_id,
            //dstaccount:dstaccount,
            srcaccount:srcaccount,
            coacodedb:coacodedb,
            coacodecr:coacodecr
        },"json")
        .done(
            function(msg)
            {
                console.log(msg);
                var obj = jQuery.parseJSON( msg );
                $("input[name*='csrf_name']").val(obj.csrf_name);
                $('#dlg').dialog('close');
                $('#dgmodul').datagrid('reload');

                if(obj.status == 0)
                {
                    $.messager.alert('Failed',obj.msg,'error'); 
                }

                
                console.log(obj);
            }
         )
        .fail(function(xhr, status, error) {
            // error handling
            
            console.log(xhr.status);
            console.log(error);
            console.log(status);
            $.messager.alert('Failed',xhr.status + "("+error+")",'error');
        });
    }

    

</script>