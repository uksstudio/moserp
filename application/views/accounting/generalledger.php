<table id="dg" class="easyui-datagrid"></table>

<div id="tb" style="padding:2px 5px;">
	<?php echo form_open(); ?>
	
	<table width="100%" style="border: 0px solid gray;">
		<tr>
			<td style="width:10%;" valign="top">
				<div >
					<div style="margin-bottom:5px">
			            <input class="easyui-datetimespinner" name="startdate" id="startdate" value="<?php echo date('Y-m')?>" data-options="label:'Start From : yyyy-mm',labelPosition:'top',formatter:formatter2,parser:parser2,selections:[[0,4,[5,7]]]" style="width:100%;">
			        </div>
			        
				</div>
			</td>
			<td style="width:10%;" valign="top">
				<div style="margin-bottom:5px">
		            
		            <input class="easyui-datetimespinner" name="enddate" id="enddate" value="<?php echo date('Y-m')?>" data-options="label:'End Date : yyyy-mm',labelPosition:'top',formatter:formatter2,parser:parser2,selections:[[0,4],[5,7]]" style="width:100%;">
		        </div>
			</td>
			<td style="width:20%;" valign="bottom">
				<div style="margin-bottom:5px">
		            <a href="#" id="search" class="easyui-linkbutton c1" style="width:120px">Generate</a>
		        </div>
			</td>
			<td align="right" valign="bottom">
				<a href="javascript:void(0)" onClick="datagridPrint()" class="easyui-linkbutton" data-options="iconCls:'icon-print',iconAlign:'right',plain:false"></a>
				<a href="javascript:void(0)" onClick="exportXls()" class="easyui-linkbutton" data-options="iconCls:'icon-xls',iconAlign:'right',plain:false"></a>
				<a href="javascript:void(0)" onClick="exportPdf()" class="easyui-linkbutton" data-options="iconCls:'icon-pdf',iconAlign:'right',plain:false"></a>
			</td>
		</tr>
	</table>
	
	<?php echo form_close(); ?>
</div>


<?php echo script_tag('includes/datagrid-groupview.js');?>
<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<?php echo script_tag('includes/plugins/datagrid-export.js');?>
<?php echo script_tag('includes/plugins/pdfmake.min.js');?>
<?php echo script_tag('includes/plugins/vfs_fonts.js');?>
<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var editIndex = undefined;
	var type = "";
	
	$(function () {
		
		$('#dg').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'po_number',
			fit: true,
			title:'General Ledger',
			rownumbers:true,
			toolbar:'#tb',
			view:groupview,
			groupField:'akun',
			showFooter: true,
            groupFormatter:function(value,rows){
            	var html = '<table width="100%" ><tr>';
            		html = html +'<td width=100% colspan=2 style="border: 0px;">'+value + ' - ' + rows[0].namaakun+'</td>';
            		
            		html = html +'<td width=100 align="right" style="border: 0px;">'+rows[0].beginbalance+'</td>';
            		html = html + '</tr></table>';
                return html;//"<span >"+value + " - " + rows[0].namaakun+"</span><span style='float: right;'>SALDO</span>";
            },
            groupStyler:function(value,rows){ // deixar a linha com nome da unidade azul //#337ab7
	          return 'background-color:#efefef;color:#000000;width:100%;';
	        },
	        rowStyler: function(index, rows)
	        {
	        	if(rows.novoucher == 'PREV') return "";
	        },
			columns:[[
				{field:'tanggal',title:'Date',width:150},
				{field:'novoucher',title:'No Voucher',width:150},
				{field:'description',title:'Description',width:300},
				{field:'debet',title:'Debet',width:100, align:'right'},
				{field:'kredit',title:'Credit',width:100, align:'right'},
				{field:'saldo',title:'Balance',width:100, align:'right'}
			]],
			onLoadSuccess:function(){
			    var groups = $(this).datagrid('groups');
			    var lastIndex = 0;
			    var namaakun = "";
			    var saldokemarin = 0;
			    
			    var saldoakhir = 0;
				for(var i=0; i<groups.length; i++){
					var group = groups[i];
					var saldoakhir = 0;
					var debet = 0;
			    	var kredit = 0;
					$.map(group.rows, function(row){
						namaakun = row.namaakun;
						saldokemarin = row.beginbalance;
						if(row.novoucher != 'PREV') {
							//console.log("group "+i+" voc "+row.novoucher+" debet : " + row.debet + " kredit : " + row.kredit);
							saldoakhir = parse2float(row.saldo);
							namaakun = row.namaakun;
							
							debet += parse2float(row.debet);
							kredit += parse2float(row.kredit);
						}
					});
					
					$(this).datagrid('appendRow', {
						tanggal: 'TOTAL',
						akun:group.value,
						novoucher:'TOTAL',
						namaakun: namaakun,
						debet: number_format(debet, 0, '.', ','),
						kredit: number_format(kredit, 0, '.', ','),
						beginbalance:saldokemarin,
						saldo: number_format(saldoakhir, 0, '.', ',')
					});
					lastIndex += group.rows.length;
					$(this).datagrid('mergeCells', {
						index: lastIndex-1 ,
						field:'tanggal',
						colspan:3
					});
				}

				var index = 0;
				for(var i=0; i<groups.length; i++){
					var group = groups[i];
					//console.log(group.rows);
					if(group.rows[0].novoucher == 'PREV')$(this).datagrid('deleteRow', index);
					$.map(group.rows, function(row){
						index++;
					});
					
					
				}

			}
		});
		/*
		$('#dg').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'po_number',
			fit: true,
			title:'General Ledger',
			rownumbers:true,
			toolbar:'#tb',
			showFooter: true,
			rowStyler:function(index,row){
		        if (row.novoucher == 'PREV'){
		            return 'background-color:grey;color:blue;font-weight:bold;';
		        }
		    },
			columns:[[
				{field:'tanggal',title:'Date',width:150},
				{field:'novoucher',title:'No Voucher',width:150},
				{field:'description',title:'Description',width:300},
				{field:'debet',title:'Debet',width:100, align:'right'},
				{field:'kredit',title:'Credit',width:100, align:'right'},
				{field:'saldo',title:'Balance',width:100, align:'right'}
			]]
		});
		*/
		
	});


	function formatter1(date){
        if (!date){return '';}
        return $.fn.datebox.defaults.formatter.call(this, date);
    }
    function parser1(s){
        if (!s){return null;}
        return $.fn.datebox.defaults.parser.call(this, s);
    }
    function formatter2(date){
        if (!date){return '';}
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        return y + '-' + (m<10?('0'+m):m);
    }
    function parser2(s){
        if (!s){return null;}
        var ss = s.split('-');
        var y = parseInt(ss[0],10);
        var m = parseInt(ss[1],10);
        if (!isNaN(y) && !isNaN(m)){
            return new Date(y,m-1,1);
        } else {
            return new Date();
        }
    }

    function formatter3(date){
        if (!date){return '';}
        var y = date.getFullYear();
        return y;
    }
    function parser3(s){
        if (!s){return null;}
        if (!isNaN(s) ){
            return new Date(s,1,1);
        } else {
            return new Date();
        }
    }

    function exportXls()
    {
		$('#dg').datagrid('toExcel', {
		    filename: 'datagrid.xls',
		    worksheet: 'Worksheet'
		});
    }

    function exportPdf()
    {
		var body = $('#dg').datagrid('toArray');
		var docDefinition = {
		    content: [{
		        table: {
		            headerRows: 1,
		            widths: ['*','*','*','*','auto','*'],
		            body: body
		        },
		        style: 'table'
		    }]
		};
		pdfMake.createPdf(docDefinition).open();
    }

    function datagridPrint()
    {
		$('#dg').datagrid('print', 'DataGrid');
    }
	

    $("#search").on('click', function()
    {
    	var csrf = $("input[name*='csrf_name']").val();
    	var startdate = $('#startdate').datetimespinner('getValue');
    	var enddate = $('#enddate').datetimespinner('getValue');
    	

    	
    	if(startdate == "")
    	{
    		$.messager.alert('Failed',"Please input startdate.",'error');
			return;
    	}

    	if(enddate == "")
    	{
    		$.messager.alert('Failed',"Please input Enddate to.",'error');
			return;
    	}
    	var yrmin = startdate.substring(0, 4);
    	var mthmin = startdate.substring(5, 7);
    	var yrmax = enddate.substring(0, 4);
    	var mthmax = enddate.substring(5, 7);
		var win = $.messager.progress({title:'Please waiting',msg:'Sending data...'});
		$.post( "Accounting/getGeneralLedger", 
		{
			csrf_name:csrf,
			yrmin:yrmin,
			mthmin:mthmin,
			yrmax:yrmax,
			mthmax:mthmax,
			startdate:startdate,
			enddate:enddate
		},"json")
		.done(
		    	function(msg)
		    	{
		    		//console.log(msg);
		    		var obj = jQuery.parseJSON( msg );
		    		$.messager.progress('close');
		    		$("input[name*='csrf_name']").val(obj.csrf_name);
					if(obj.status == 0)
					{
						$.messager.alert('Failed',obj.msg,'error');	
					}
					else
					{
						var dg = $('#dg');
						dg.datagrid({data: obj.rows});
					}

					
		     	}
		     )
		    .fail(function(xhr, status, error) {
		        // error handling
		        $.messager.progress('close');
		        console.log(xhr.status);
		        console.log(error);
		        console.log(status);
		        $.messager.alert('Failed',xhr.status + "("+error+")",'error');
		    });
    });


    function doSearch(value){
        alert('You input: ' + value);
    }
</script>