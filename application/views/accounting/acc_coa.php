<table id="dg" class="easyui-datagrid"></table>

<div id="tb" style="padding:2px 5px;">
	<?php echo form_open(); ?>

	<table width="100%" style="border: 0px solid black;">
		<tr>
			<td >
				<div style="">
		            <input class="easyui-searchbox" data-options="prompt:'Search Account or Name',searcher:doSearch" style="width:30%">
		        </div>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>

	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">New</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Edit</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove</a>
    |
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-sum" plain="true" onclick="updateBalance()">Balance</a>
</div>

<div id="dlg" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
    <?php echo form_open("",'novalidate style="margin:0;padding:10px 30px" id="fm"');?>
        <div style="margin-bottom:10px">
            <input name="code" id="code" class="easyui-combobox" required="true" label="Code:" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="subcode" id="subcode" class="easyui-combobox" required="true" label="Sub Code :" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="name" id="name" class="easyui-textbox" required="true" label="Name :" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input class="easyui-combobox" id="typecode" name="typecode" style="width:60%" data-options="label:'Type : ', labelPosition:'left', required: true, panelHeight:'auto'">
        </div>
        
    </form>
</div>
<div id="dlg-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" style="width:90px">Save</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
</div>


<div id="dlgbalance" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttonsbalance'">
    <?php echo form_open("",'novalidate style="margin:0;padding:10px 30px" id="fmbalance"');?>
    	<div style="margin-bottom:10px">
            <input name="subcode" id="subcode" class="easyui-combobox" required="true" label="Sub Code :" style="width:100%">
        </div>
       	<div style="margin-bottom:10px">
            <input class="easyui-datetimebox" id="tanggal" name="tanggal" style="width:100%" data-options="label:'Date Time:', showSeconds:true, required:true" >
        </div>
        <div style="margin-bottom:10px">
            <input name="balance" id="balance" class="easyui-numberbox" required="true" label="Balance :" style="width:100%">
        </div>
        
        
    </form>
</div>
<div id="dlg-buttonsbalance">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveBalance()" style="width:90px">Save</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgbalance').dialog('close')" style="width:90px">Cancel</a>
</div>

<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var url;
	var editIndex = undefined;
	var dataBl = [
				{"typecode":"P/L"},
				{"typecode":"B/S"}
				];
	$(function () {
		$('#dg').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'po_number',
			fit: true,
			title:'Chart Of Account',
			rownumbers:true,
			toolbar:'#tb',
			url: "Accounting/showCoaBalance",
			method: 'get',
			columns:[[
				{field:'code',title:'Parent Code',width:100},
				{field:'subcode',title:'Account',width:100},
				{field:'name',title:'Name',width:300},
				{field:'typecode',title:'Type',width:50},
				{field:'balance',title:'Balance',width:100, align:'right', formatter:
					function(val,row)
					{
						//return number_format(val);
						var num = $.number( val);
						return num;
					}
				}
			]]
		});

		
		$("#code").combobox({
			valueField:'subcode',
	        textField:'subcode',
	        url:'Finance/getCoaChar/1',
	        method:'get',
	        panelHeight:'auto',
	        required:false,
	        onSelect: function(rec){
	            $("#subcode").combobox('reload','Finance/getCoaChar?code='+rec.subcode);
	        }
		});

		$("#subcode").combobox({
			valueField:'subcode',
	        textField:'subcode',
	        url:'Finance/getCoaChar/1',
	        method:'get',
	        panelHeight:'auto',
	        required:false,
	        onSelect: function(rec){
	            
	        }
		});



		$("#typecode").combobox({
			valueField:'typecode',
	        textField:'typecode',
	        data:dataBl,
	        panelHeight:'auto',
	        required:false,
	        onSelect: function(rec){
	            
	        }
		});

	});


	$.fn.datetimebox.defaults.parser = function(s){
	    if (!s){return new Date();}
	    var dt = s.split(' ');
	    var dd = dt[0].split('-');
	    var date = new Date(dd[0],parseInt(dd[1])-1,dd[2]);
	    if (dt.length>1){
	        dd = dt[1].split(':');
	        date.setHours(dd[0]);
	        date.setMinutes(dd[1]);
	        date.getSeconds(dd[2])
	    }
	    return date;
	}
	$.fn.datetimebox.defaults.formatter = function(date){
	    var y = date.getFullYear();
	    var m = date.getMonth()+1;
	    var d = date.getDate();
	    return y+'-'+_ff(m)+'-'+_ff(d) + ' ' + _ff(date.getHours())+':'+_ff(date.getMinutes()) +':'+_ff(date.getSeconds());
	}

	

	function _ff(v) {
        return (v < 10 ? "0" : "") + v;
    }


	function updateBalance()
	{
		var row = $('#dg').datagrid('getSelected');
        if (row){
            $('#dlgbalance').dialog('open').dialog('center').dialog('setTitle','Update Balance');
            $('#fmbalance').form('load',row);
            
            url = 'Accounting/addNewCoa/updateBalance/'+row.id;
        }
	}

	function saveBalance()
	{
		$("#fmbalance").submit();
	}
	
	function newUser(){
    	csrf = $("input[name*='csrf_name']").val();
        $('#dlg').dialog('open').dialog('center').dialog('setTitle','Add New');
        $('#fm').form('clear');
        $("input[name*='csrf_name']").val(csrf);
        url = 'Accounting/addNewCoa';
    }

	function editUser(){
        var row = $('#dg').datagrid('getSelected');
        if (row){
            $('#dlg').dialog('open').dialog('center').dialog('setTitle','Update');
            $('#fm').form('load',row);
            $("#subcode").combobox('reload','Finance/getCoaChar?code='+row.code);
            url = 'Accounting/addNewCoa/update/'+row.id;
        }
    }

    function destroyUser(){
        var row = $('#dg').datagrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to delete this ACCOUNT?',function(r){
                if (r){
                    $.messager.show({    // show error message
                                title: 'Error',
                                msg: "not allowed"
                            });
                }
            });
        }
    }

    $('#novoucher').textbox({
		inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
			keyup:function(e){
				if(e.keyCode == 13)
				{
					var novoc = $("#novoucher").textbox('getText');
					$.get( "Accounting/getPaymentVoucher/"+novoc, function( data ) {
						var obj = jQuery.parseJSON( data );
						if(obj.detail.length <= 0)
						{
							$.messager.alert('Failed',"Your PV not found.",'error');
							return;
						}
						$("#supplier").textbox('setText', obj.kas.supplierid);
						$("#payto").textbox('setText', obj.kas.paymentto);
						$("#nocek").textbox('setText', obj.kas.ceknumber);
						$("#remark").textbox('setText', obj.kas.remark);
						$("#akun").textbox('setText', obj.kas.coaccount);
						$('#dg').datagrid({
							data: obj.detail
						});
					});
				}
			}
		})

		
	});


	
    function saveUser(){
    	$("#fm").submit();
    }

    $('#fmbalance').submit(function(event){
		event.preventDefault();
		$.ajax({
			 type: 'POST',
			 url: url, 
			 data: $(this).serialize()
		})
		.done(function(msg){
			var obj = jQuery.parseJSON( msg );
    		$("input[name*='csrf_name']").val(obj.csrf_name);
			if (obj.staus){
                $.messager.show({
                    title: 'Error',
                    msg: obj.errorMsg
                });
            } else {
                $('#dlgbalance').dialog('close');        // close the dialog
                $('#dg').datagrid('reload');    // reload the user data
            }
			console.log(obj);
		})
		.fail(function(xhr, status, error) {
	        // error handling
	        console.log(xhr.status);
	        console.log(error);
	        console.log(status);
	        $.messager.alert('Failed',xhr.status + "("+error+")",'error');

		});
	});

    $('#fm').submit(function(event){
		event.preventDefault();
		$.ajax({
			 type: 'POST',
			 url: url, 
			 data: $(this).serialize()
		})
		.done(function(msg){
			var obj = jQuery.parseJSON( msg );
    		$("input[name*='csrf_name']").val(obj.csrf_name);
			if (obj.staus){
                $.messager.show({
                    title: 'Error',
                    msg: obj.errorMsg
                });
            } else {
                $('#dlg').dialog('close');        // close the dialog
                $('#dg').datagrid('reload');    // reload the user data
            }
			console.log(obj);
		})
		.fail(function(xhr, status, error) {
	        // error handling
	        console.log(xhr.status);
	        console.log(error);
	        console.log(status);
	        $.messager.alert('Failed',xhr.status + "("+error+")",'error');

		});
	});

	$("#btnSave").on('click', function()
    {
    	var csrf = $("input[name*='csrf_name']").val();
    	var coa1 = $("#coa1").combobox('getText');
    	var coa2 = $("#coa2").combobox('getText');
    	var coa3 = $("#coa3").combobox('getText');
    	var coa5 = $("#coa5").combobox('getText');
    	var name = $("#name").textbox('getText');
    	var typecode = $("#typecode").combobox('getText');
    	
    	var code = "";
    	var subcode = "";
    	var oke = false;
    	if(coa5 != "")
    	{
    		subcode = coa5;

    		if(coa3 != "")
    		{
    			code = coa3;
    			oke = true;
    		}
    		else
    		{
    			$.messager.alert('Failed',"Please select parent Account.",'error');
				return;
    		}
    	}


    	if(coa3 != "" && !oke)
    	{
    		subcode = coa3;

    		if(coa2 != "")
    		{
    			code = coa2;
    			oke = true;
    		}
    		else
    		{
    			$.messager.alert('Failed',"Please select parent Account.",'error');
				return;
    		}
    	}

    	if(coa2 != "" && !oke)
    	{
    		subcode = coa2;

    		if(coa1 != "")
    		{
    			code = coa1;
    			oke = true;
    		}
    		else
    		{
    			$.messager.alert('Failed',"Please select parent Account.",'error');
				return;
    		}
    	}


    	if(name == "" )
    	{
    		$.messager.alert('Failed',"Please input a name of account.",'error');
			return;
    	}

    	$.post( "Accounting/addNewCoa", 
		{
			csrf_name:csrf,
			code:code,
			subcode:subcode,
			name:name,
			typecode:typecode,
		},"json")
		.done(
	    	function(msg)
	    	{
	    		console.log(msg);
	    		var obj = jQuery.parseJSON( msg );
	    		$.messager.progress('close');
	    		$("input[name*='csrf_name']").val(obj.csrf_name);
				if(obj.status == 0)
				{
					$.messager.alert('Failed',obj.msg,'error');	
				}
				else
				{
					$("#name").textbox('setText','');
					$('#dg').datagrid({data: obj.rows});
				}

				
				console.log(obj);
	     	}
	     )
	    .fail(function(xhr, status, error) {
	        // error handling
	        $.messager.progress('close');
	        console.log(xhr.status);
	        console.log(error);
	        console.log(status);
	        $.messager.alert('Failed',xhr.status + "("+error+")",'error');
	    });

    });

    function doSearch(value){
        $('#dg').datagrid({
			queryParams: {
				q: value
			}
		});
    }
</script>