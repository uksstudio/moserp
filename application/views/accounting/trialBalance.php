<table id="dg" class="easyui-datagrid"></table>

<div id="tb" style="padding:2px 5px;">
	<?php echo form_open(); ?>
	
	<table width="100%">
		<tr>
			<td style="width:5%;">
				<div >
					<div style="margin-bottom:5px">
			            <input class="easyui-datetimespinner" name="startdate" id="startdate" value="<?php echo date('Y-m')?>" data-options="label:'Start From : yyyy-mm',labelPosition:'top',formatter:formatter2,parser:parser2,selections:[[0,4,[5,7]]]" style="width:100%;">
			        </div>
			        
				</div>
			</td>
			<td style="width:5%;" valign="top">
				<div style="margin-bottom:5px">
		            
		            <input class="easyui-datetimespinner" name="enddate" id="enddate" value="<?php echo date('Y-m')?>" data-options="label:'End Date : yyyy-mm',labelPosition:'top',formatter:formatter2,parser:parser2,selections:[[0,4],[5,7]]" style="width:100%;">
		        </div>
			</td>
			<td style="width:20%;" valign="bottom">
				<div style="margin-bottom:5px">
		            <a href="#" id="search" class="easyui-linkbutton c1" style="width:120px">Generate</a>
		        </div>
			</td>
			<td align="right" valign="bottom">
				<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-print',iconAlign:'right',plain:false"></a>
				<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-xls',iconAlign:'right',plain:false"></a>
				<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-pdf',iconAlign:'right',plain:false"></a>
			</td>
		</tr>
	</table>
	
	<?php echo form_close(); ?>
</div>


<?php echo script_tag('includes/datagrid-groupview.js');?>
<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var editIndex = undefined;
	var type = "";
	
	$(function () {

		$('#dg').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'po_number',
			fit: true,
			title:'Trial Balance',
			rownumbers:true,
			toolbar:'#tb',
			showFooter: true,
			columns:[[
				{field:'akun',title:'Account',width:80},
				{field:'description',title:'Description',width:300},
				{field:'prevbalance',title:'Prev Balance',width:100, align:'right'},
				{field:'debet',title:'Debet',width:100, align:'right'},
				{field:'kredit',title:'Credit',width:100, align:'right'},
				{field:'saldo',title:'Balance',width:100, align:'right'}
			]]
		});

		
		
	});


	function formatter1(date){
            if (!date){return '';}
            return $.fn.datebox.defaults.formatter.call(this, date);
        }
        function parser1(s){
            if (!s){return null;}
            return $.fn.datebox.defaults.parser.call(this, s);
        }
        function formatter2(date){
            if (!date){return '';}
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            return y + '-' + (m<10?('0'+m):m);
        }
        function parser2(s){
            if (!s){return null;}
            var ss = s.split('-');
            var y = parseInt(ss[0],10);
            var m = parseInt(ss[1],10);
            if (!isNaN(y) && !isNaN(m)){
                return new Date(y,m-1,1);
            } else {
                return new Date();
            }
        }

        function formatter3(date){
            if (!date){return '';}
            var y = date.getFullYear();
            return y;
        }
        function parser3(s){
            if (!s){return null;}
            if (!isNaN(s) ){
                return new Date(s,1,1);
            } else {
                return new Date();
            }
        }

	

    $("#search").on('click', function()
    {
    	var csrf = $("input[name*='csrf_name']").val();
    	var startdate = $('#startdate').datetimespinner('getValue');
    	var enddate = $('#enddate').datetimespinner('getValue');
    	

    	
    	if(startdate == "")
    	{
    		$.messager.alert('Failed',"Please input startdate.",'error');
			return;
    	}

    	if(enddate == "")
    	{
    		$.messager.alert('Failed',"Please input Enddate to.",'error');
			return;
    	}
    	var yrmin = startdate.substring(0, 4);
    	var mthmin = startdate.substring(5, 7);
    	var yrmax = enddate.substring(0, 4);
    	var mthmax = enddate.substring(5, 7);
		var win = $.messager.progress({title:'Please waiting',msg:'Sending data...'});
		$.post( "Accounting/showTrialBalance", 
		{
			csrf_name:csrf,
			yrmin:yrmin,
			mthmin:mthmin,
			yrmax:yrmax,
			mthmax:mthmax
		},"json")
		.done(
		    	function(msg)
		    	{
		    		console.log(msg);
		    		var obj = jQuery.parseJSON( msg );
		    		$.messager.progress('close');
		    		$("input[name*='csrf_name']").val(obj.csrf_name);
					if(obj.status == 0)
					{
						$.messager.alert('Failed',obj.msg,'error');	
					}
					else
					{
						var dg = $("#dg");
						dg.datagrid({data: obj.rows});
						dg.datagrid('reloadFooter', obj.footer);
					}

		     	}
		     )
		    .fail(function(xhr, status, error) {
		        // error handling
		        $.messager.progress('close');
		        console.log(xhr.status);
		        console.log(error);
		        console.log(status);
		        $.messager.alert('Failed',xhr.status + "("+error+")",'error');
		    });
    });


    function doSearch(value){
        alert('You input: ' + value);
    }
</script>