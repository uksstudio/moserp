<table class="easyui-treegrid" title="Profi And Lost" id="dg"
		data-options="
			method: 'post',
			rownumbers: true,
			showFooter: true,
			idField: 'id',
			treeField: 'namaakun',
			toolbar:'#tb',
			rowStyler: function(row){
				var val = row.type;
				if(val == 'TOTAL')
					return 'background-color:#efefef;color:#000000;font-weight:bold;';
		    }	
		">
	<thead frozen="true">
		<tr>
			<th field="namaakun" width="400">Account Name</th>
		</tr>
	</thead>
	<thead>
		<tr>
			<th field="balance" width="100" align="right">Amount</th>
		</tr>
	</thead>
</table>

<div id="tb" style="padding:2px 5px;">
	<?php echo form_open(); ?>
	
	<table width="100%">
		<tr>
			<td style="width:5%;">
				<div >
					<div style="margin-bottom:5px">
			            <input class="easyui-datetimespinner" name="startdate" id="startdate" value="<?php echo date('Y-m')?>" data-options="label:'Start From : yyyy-mm',labelPosition:'top',formatter:formatter2,parser:parser2,selections:[[0,4,[5,7]]]" style="width:100%;">
			        </div>
			        
				</div>
			</td>
			<td style="width:5%;" valign="top">
				<div style="margin-bottom:5px">
		            
		            <input class="easyui-datetimespinner" name="enddate" id="enddate" value="<?php echo date('Y-m')?>" data-options="label:'End Date : yyyy-mm',labelPosition:'top',formatter:formatter2,parser:parser2,selections:[[0,4],[5,7]]" style="width:100%;">
		        </div>
			</td>
			<td style="width:20%;" valign="bottom">
				<div style="margin-bottom:5px">
		            <a href="#" id="search" class="easyui-linkbutton c1" style="width:120px">Generate</a>
		        </div>
			</td>
			<td align="right" valign="bottom">
				<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-print',iconAlign:'right',plain:false"></a>
				<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-xls',iconAlign:'right',plain:false"></a>
				<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-pdf',iconAlign:'right',plain:false"></a>
			</td>
		</tr>
	</table>
	
	<?php echo form_close(); ?>
</div>


<?php echo script_tag('includes/datagrid-groupview.js');?>
<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var editIndex = undefined;
	var type = "";
	
	$(function () {

		

	});


	function formatter1(date){
            if (!date){return '';}
            return $.fn.datebox.defaults.formatter.call(this, date);
        }
        function parser1(s){
            if (!s){return null;}
            return $.fn.datebox.defaults.parser.call(this, s);
        }
        function formatter2(date){
            if (!date){return '';}
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            return y + '-' + (m<10?('0'+m):m);
        }
        function parser2(s){
            if (!s){return null;}
            var ss = s.split('-');
            var y = parseInt(ss[0],10);
            var m = parseInt(ss[1],10);
            if (!isNaN(y) && !isNaN(m)){
                return new Date(y,m-1,1);
            } else {
                return new Date();
            }
        }

        function formatter3(date){
            if (!date){return '';}
            var y = date.getFullYear();
            return y;
        }
        function parser3(s){
            if (!s){return null;}
            if (!isNaN(s) ){
                return new Date(s,1,1);
            } else {
                return new Date();
            }
        }

	

    $("#search").on('click', function()
    {
    	var csrf = $("input[name*='csrf_name']").val();
    	var startdate = $('#startdate').datetimespinner('getValue');
    	var enddate = $('#enddate').datetimespinner('getValue');
    	

    	
    	if(startdate == "")
    	{
    		$.messager.alert('Failed',"Please input startdate.",'error');
			return;
    	}

    	if(enddate == "")
    	{
    		$.messager.alert('Failed',"Please input Enddate to.",'error');
			return;
    	}
    	var yrmin = startdate.substring(0, 4);
    	var mthmin = startdate.substring(5, 7);
    	var yrmax = enddate.substring(0, 4);
    	var mthmax = enddate.substring(5, 7);
		///var win = $.messager.progress({title:'Please waiting',msg:'Sending data...'});
		var dg = $("#dg");
		dg.treegrid({
			url:'Accounting/showProfitandLost',
			method:'post',
			queryParams:{
				csrf_name:csrf,
				yrmin:yrmin,
				mthmin:mthmin,
				yrmax:yrmax,
				mthmax:mthmax,
				startdate:startdate,
				enddate:enddate
			},
			onLoadSuccess: function(row, data)
			{
				console.log(data.csrf_name);
				//$.messager.progress('close');
				$("input[name*='csrf_name']").val(data.csrf_name);
			},
			onLoadError: function(argv)
			{

			}
		}
		);
		
		
    });


    function doSearch(value){
        alert('You input: ' + value);
    }
</script>