<table id="dgmodul" class="easyui-datagrid"></table>

 <div id="toolbar">
 	<input class="easyui-searchbox" data-options="prompt:'Search PV/RV/CA',searcher:doSearch" style="width:30%">
</div>


<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	
	$(function () {
		$('#dgmodul').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'id',
			rownumbers:true,
			url:'Accounting/getPreviewJournal',
			method: 'get',
			title:'Print Preview Journal',
			toolbar:'#toolbar',
			columns:[[
				{field:'createdate',title:'Date',width:100},
				{field:'voucherno',title:'Voucher No',width:100},
				{field:'isposting',title:'Posting ST',width:100},
				{field:'locationcode',title:'Location Code',width:100},
				{field:'ceknumber',title:'Bilyet',width:150},
				{field:'supplier',title:'Supplier',width:200},
				{field:'action',title:'Action',width:100}
			]],
            onLoadSuccess:function(){
                $(this).datagrid('getPanel').find('.easyui-linkbutton').each(function(){
                    $(this).linkbutton({
                        onClick:function(){
                            var row = $('#dgmodul').datagrid('getSelected');
                            //alert(row.type);
                            var id = $(this).attr('row-id');
                            var po_number = $(this).attr('po-number');
                            $.messager.confirm('Transfer Out', 'Your DO Number is '+po_number+', want to print press OK!', function(r){
                                if (r){
                                    loadPrintDocument(this, "finance/printPaymentVoucher/"+po_number+"/"+row.type);
                                }
                            });
                            //alert("oke id " + id);
                            // destroy the row
                        }
                    })
                })
            }
		});


	});

	var url;
    function newUser(){
    	csrf = $("input[name*='csrf_name']").val();
        $('#dlg').dialog('open').dialog('center').dialog('setTitle','Add New');
        $('#fm').form('clear');
        $("input[name*='csrf_name']").val(csrf);
        url = 'Admin/addEdc/add';
    }
    function editUser(){
        var row = $('#dgmodul').datagrid('getSelected');
        if (row){
            $('#dlg').dialog('open').dialog('center').dialog('setTitle','Update');
            $('#fm').form('load',row);
            url = 'Admin/addEdc/update/'+row.id;
        }
    }

    $('#fm').submit(function(event){
		event.preventDefault();
		$.ajax({
			 type: 'POST',
			 url: url, 
			 data: $(this).serialize()
		})
		.done(function(msg){
			var obj = jQuery.parseJSON( msg );
    		$("input[name*='csrf_name']").val(obj.csrf_name);
			if (obj.staus){
                $.messager.show({
                    title: 'Error',
                    msg: obj.errorMsg
                });
            } else {
                $('#dlg').dialog('close');        // close the dialog
                $('#dgmodul').datagrid('reload');    // reload the user data
            }
			console.log(obj);
		})
		.fail(function(xhr, status, error) {
	        // error handling
	        console.log(xhr.status);
	        console.log(error);
	        console.log(status);
	        $.messager.alert('Failed',xhr.status + "("+error+")",'error');

		});
	});
    function saveUser(){
    	$("#fm").submit();
    }
    function destroyUser(){
        var row = $('#dgmodul').datagrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to destroy this EDC?',function(r){
                if (r){
                    $.post('Admin/addEdc/delete/'+row.id,{id:row.id},function(result){
                        if (result.status){
                            $('#dgmodul').datagrid('reload');    // reload the user data
                        } else {
                            $.messager.show({    // show error message
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        }
                    },'json');
                }
            });
        }
    }

    function doSearch(value){
        alert('You input: ' + value);
    }
</script>