<div style="padding:5px;margin-bottom:5px;text-align: right;">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onClick="doCreate()">Save</a>
	
</div>
<div class="easyui-layout" style="width:100%;height:90%;">
	
	<div id="p" data-options="region:'east'" style="width:35%;">
		<table id="dgcostingfield" class="easyui-datagrid"></table>
	</div>
	<div data-options="region:'center'" style="width:100%;padding:30px 60px;">
		<?php echo form_open()?>
		<div style="margin-bottom:20px">
            <input name="costingnumber" id="costingnumber" class="easyui-textbox" data-options="label:'Costing Number:'" style="width: 200px" readonly="true" value="<?php echo $costingnumber?>"></input>
        </div>
		<div style="margin-bottom:20px">
            <input name="currency" id="currency" class="easyui-combobox" data-options="label:'Currency:'" style="width: 200px" ></input>
        </div>
        <div style="margin-bottom:20px">
            <input name="exchangerate" id="exchangerate" class="easyui-textbox" style="width: 200px" data-options="label:'Exchange Rate:'"></input>
        </div>
        <div style="margin-bottom:20px">
            <input name="nopib" id="nopib" class="easyui-textbox" style="width: 200px" data-options="label:'No PIB:'"></input>
        </div>
        <div style="margin-bottom:20px">
            <input class="easyui-datebox" name="spcpdate" name="spcpdate" data-options="required:true,formatter:myformatter,parser:myparser,onSelect:onSelect,label:'Tgl SPCP:'" style="width:200px;">
        </div>
        <div style="margin-bottom:20px">
            <input name="awbno" id="awbno" class="easyui-textbox" style="width: 200px" data-options="label:'AWB:'"></input>
        </div>
        <div style="margin-bottom:20px">
            <input name="noinvoice" id="noinvoice" class="easyui-textbox" style="width: 200px" data-options="label:'No Invoice:'"></input>
        </div>
		<?php echo form_close()?>
    </div>

<div>

<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var newdata = <?php echo $json_currency?>;
	$(function () {
		$("#dgcostingfield").propertygrid({
			url: 'Costing/costingfield',
			method:'get',
			width:'100%',
			height:'100%',
			showGroup: true,
			scrollbarSize: 0,
			columns:[[
				{field:'name',title:'Label',width:120,resizable:true},
				{field:'value',title:'Value (IDR)',width:80,resizable:false}
			]]
		});


		


		$('#currency').combobox({
			panelHeight: 'auto',
			selectOnNavigation: false,
			valueField: 'id',
			textField: 'text',
			editable: true,
			required: true,    
			validType: 'exists["#currency"]',
			onLoadSuccess: function () { },
			filter: function (q, row) {
				return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
			},
			data: newdata,
			onSelect: function(record)
			{
				if(record)
				{
					$('#exchangerate').textbox('setText', record.kurs);
				}
			}
		});
	});


	function myformatter(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	}
	function myparser(s){
		if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
			return new Date(y,m-1,d);
		} else {
			return new Date();
		}
	}
	
	function onSelect(date){
		strTanggal = date.getFullYear()+"-"+("0" + (date.getMonth() + 1).toString()).substr(-2)+"-"+("0" + date.getDate().toString()).substr(-2);
	}
	var checkedRows = [];
	function onCheck(index,row){
		for(var i=0; i<checkedRows.length; i++){
			if (checkedRows[i].po_number == row.po_number)
			{
				row.chk = 'Y';
				return;
			}
		}
		row.chk = 'Y';
		checkedRows.push(row);
		reloadFooter();
		
	}
	function onUncheck(index,row){
		for(var i=0; i<checkedRows.length; i++){
			if (checkedRows[i].po_number == row.po_number){
				row.chk = 'N';
				checkedRows.splice(i,1);
				return;
			}
		}
		row.chk = 'N';
		reloadFooter();
		
	}


	function onCheckAll(row){
		for(var i=0; i<row.length; i++){
			row[i].chk = 'Y';
		}
		reloadFooter();
	}
	function onUncheckAll(row){
		for(var i=0; i<row.length; i++){
			row[i].chk = 'N';
		}
		reloadFooter();
	}


	

	var items_cosoting_field = new Array();
	var items_po_field = new Array();

	function doCreate()
	{
		
		var propertyData = $('#dgcostingfield').propertygrid('getData');
		var propRows = propertyData.rows;
		
		$.each(propRows, function( index, value ) {
			var grid = {};
			grid['groupname'] = value.group;
			grid['group'] = value.name;
			grid['amount'] = (value.value == null) ? 0 : parseFloat(value.value);
			items_cosoting_field[items_cosoting_field.length] = grid;
		});
		
		
		
		var win = $.messager.progress({
			title:'Please waiting',
			msg:'Sending data...'
		});

		$.post( "Warehouse/saveCostingItems", 
			{
				csrf_name:csrf,
				nopib:$('#nopib').textbox('getText'),
				exchangerate:$('#exchangerate').textbox('getText'),
				currency:$('#currency').combobox('getText'),
				spcpdate:strTanggal,
				awbno:$('#awbno').textbox('getText'),
				noinvoice:$('#noinvoice').textbox('getText'),
				costingField:JSON.stringify(items_cosoting_field)
			},
			function( data ) {
				$.messager.progress('close');
				$.messager.confirm('Success',"Costing Number " + data.co_number,function(r){
				if (r){
					openUrl("<?php echo base_url()?>index.php/Warehouse/costingfreight");
				}
			});
		}, "json");
	}
</script>