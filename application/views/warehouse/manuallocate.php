<table id="dgcp" style="width: 100%"></table>

<div id="toolbar">
    <?php echo form_open()?>
    <table style="width: 100%; border: 0px solid black;">
        <tr>
            <td style="width: 100px">
                DO Number
            </td>
            <td>
                <input name="colrow" id="colrow" class="f1 easyui-textbox" required="true" style="width: 200px"></input> Enter
            </td>
            <td style="text-align: right;" >
                <a id="btnSave" class="easyui-linkbutton"  data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'">Save</a>
            </td>
        </tr>
        
    </table>
    <?php echo form_close()?>
    
</div>

<?php echo script_tag('includes/plugins/datagrid-cellediting.js');?>
<script type="text/javascript">
    var csrf = '<?php echo $this->security->get_csrf_hash();?>';
    var editIndex = undefined;
    
    $(function(){
        var Clickc = [];
        var dg = $('#dgcp').datagrid({
            title:'MANUAL ALLOCATE',
            singleSelect:true,
            idField:'itemcode',
            fit: true,
    		remoteFilter: true,
    		toolbar:'#toolbar',
    		rownumbers: true,
            clickToEdit: true,
            dblclickToEdit: false,
            columns:[[
                {field:'productcode',title:'Item Code',width:180},
                {field:'productitemdesc',title:'Description',width:250},
                {field:'qty',title:'QTY',width:100},
                {field:'retailsalesprice',title:'Retail Price',width:150}
            ]],
            onLoadSuccess: function (data)
            {
                console.log(data);
                csrf = data.csrf_name;
                $("input[name*='csrf_name']").val(data.csrf_name);
            },
            onLoadError: function()
            {

            },
            onEndEdit:function(index,row){
                var dg = $(this);
                //console.log(index +" == "+ Clickc.field );
                
            },
            onBeforeEdit:function(index,row){
                
                editIndex = index;
                $(this).datagrid('refreshRow', index);
                console.log('onBeforeEdit');
            },
            onAfterEdit:function(index,row){
                $(this).datagrid('refreshRow', index);
                console.log('onAfterEdit ' + row.newprice);

                var ed = dg.datagrid('getEditor', {index:index,field:Clickc.field});
                var row = dg.datagrid('getRows')[index];
                if(row.newprice != row.retailprice)
                {
                    
                    var varian = ((row.retailprice - row.newprice) / row.newprice) * 100;
                    console.log("varian " + varian);
                    row.varian = number_format(varian, 2, '.', ',');
                    $(this).datagrid('refreshRow', index);
                    
                    $.post('merchandise/adjustNewpricesession',{csrf_name:csrf, rowid:row.rowid,newprice:row.newprice},function(result){
                        if (result.status){
                            csrf = result.csrf_name;
                            
                        } else {
                            $.messager.show({    // show error message
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        }
                    },'json');
                    
                }
            },
            onCancelEdit:function(index,row){
                $(this).datagrid('refreshRow', index);
                console.log('onCancelEdit');
            },
            onCellEdit: function(index,field){
                var dg = $(this);
                var input = dg.datagrid('input', {index:index,field:field});
                var ed = dg.datagrid('getEditor', {index: index,field: field});
                if (ed){
                    Clickc.index = index; // get the row that was clicked
                    Clickc.field = field; // get the field which was clicked
                    Clickc.value = $(ed.target).val();  //Get cell current value
                }
                input.bind('keydown', function(e){
                    if (e.keyCode == 38 && index>0){    // up
                        dg.datagrid('endEdit', index);
                        dg.datagrid('editCell', {
                            index: index-1,
                            field: field
                        });
                    } else if (e.keyCode == 40 && index<dg.datagrid('getRows').length-1){   // down
                        dg.datagrid('endEdit', index);
                        dg.datagrid('editCell', {
                            index: index+1,
                            field: field
                        });
                    }
                    else if (e.keyCode == 13){
                        

                        dg.datagrid('endEdit', index);
                        dg.datagrid('gotoCell', {index:0,field:'productitemid'});
                        //dg.datagrid('gotoCell', 'right');
                        //dg.datagrid('editCell',dg.datagrid('cell'));

                        return false;
                    }
                })
            }
        });

        dg.datagrid('enableCellEditing');

        $.extend($.fn.textbox.defaults.rules, {
            maxLength: { 
                validator: function(value, param){
                    //console.log(value.length);
                    return value.length <= param[0];
                },
                message: 'Maximum characters allowed only {0}'
            }
        });

    });
    
    function onClickCell(index, field){
        if (editIndex != index){
            if (endEditing()){
                $('#dgcp').datagrid('selectRow', index).datagrid('beginEdit', index);
                var ed = $('#dgcp').datagrid('getEditor', {index:index,field:field});
                if (ed){
                    ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                }
                editIndex = index;
            } else {
                setTimeout(function(){
                    $('#dgcp').datagrid('selectRow', editIndex);
                },0);
            }
        }
    }

    function endEditing(){
        if (editIndex == undefined){return true}
        if ($('#dgcp').datagrid('validateRow', editIndex)){
            $('#dgcp').datagrid('endEdit', editIndex);
            editIndex = undefined;
            return true;
        } else {
            return false;
        }
    }

    $('#colrow').textbox({
        inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
            keyup:function(e){
                if(e.keyCode == 13)
                {
                    var donumber = $('#colrow').textbox('getText');
                    if(donumber != '') 
                    {
                        $('#dgcp').datagrid({
                            url:"warehouse/showManualAllocate",
                            method: 'post',
                            queryParams:{
                                csrf_name:csrf, 
                                donumber:donumber
                            }
                        });
                        
                    }
                    else
                    {
                        $.messager.alert('Failed','Please fill all field required.','error');
                    }
                }
            }
        })

        
    });

    
    $('#btnSave').on('click', function()
    {
        var rows = $("#dgcp").datagrid('getRows');
        var donumber  = $('#colrow').textbox('getText');
        if(rows.length > 0)
        {
            $.post( "warehouse/setManualAllocate", 
            {
                csrf_name:csrf,
                donumber:donumber
            }, "json")
            .done(
                function(msg)
                {
                    console.log(msg);
                    var data = jQuery.parseJSON( msg );
                    csrf = data.csrf_name;
                    if(data.status == 0)
                    {
                        $.messager.alert('Failed',data.msgErr,'error'); 
                    }
                    else
                    {
                        console.log('do printPage');
                        $.messager.confirm('Transfer Out', 'Your DO Number is '+donumber+', want to print?', function(r){
                            if (r){
                                loadPrintDocument(this, "inventory/printTransferOut/"+data.transferid);
                            }
                        });
                    }
                }
             )
            .fail(function(xhr, status, error) {
                // error handling
                console.log(xhr.status);
                console.log(error);
                console.log(status);
                $.messager.alert('Failed',xhr.status + "("+error+")",'error');
            }); 

        }
        else
        {
            $.messager.alert('Failed',"Please enter Productcode",'error');
        }

          
    });
</script>