<?php

$ponum = strtoupper(substr($this->session->userdata('username'),0,2).date('ym')."XXXX");
?>
<div style="padding:5px;margin-bottom:5px;text-align: right;">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" onClick="refreshAndClear()">Clear</a>
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onClick="doCreate()">Save</a>
	
</div>
<div class="easyui-tabs" data_option="fit:true;" style="width:100%;height:180px">
	<div title="Purchase Order" style="padding:10px">
		<?php echo form_open("");?>
		<table style="width:100%;">
			<tr>
				<td style="text-align: right">Po Number</td>
				<td><input name="ponum" id="ponum" class="f1 easyui-textbox" value="<?php echo $ponum;?>" readonly></input></td>

				<td>Season</td>
				<td>
					<input name="season" id="season" class="easyui-combogrid" data-options="required:true"></input></td>

				<td style="text-align: right">Type</td>
				<td>
					<select class="easyui-combobox" name="potype" id="potype">
						<option value="GOODS">Goods</option>
					</select>
				</td>
			</tr>

			<tr>
				<td style="text-align: right">Supplier Name</td>
				<td><input name="suppliername" id="suppliername" class="easyui-combogrid" data-options="required:true"></input></td>

				<td>Supplier Code</td>
				<td><input name="suppliercode" id="suppliercode" class="f1 easyui-textbox"></input></td>

				<td style="text-align: right">Division</td>
				<td><input name="division" id="division" class="f1 easyui-textbox" value="<?php echo $division;?>"></input></td>
			</tr>

			<tr>
				<td style="text-align: right">Supp Reff Number</td>
				<td><input name="supp_reff_num" id="supp_reff_num" class="f1 easyui-textbox"></input></td>

				<td>Description</td>
				<td><input name="description" id="description" class="f1 easyui-textbox"></input></td>

				<td style="text-align: right">Buyer</td>
				<td><input name="buyer" id="buyer" class="f1 easyui-textbox" value="<?php echo $this->session->userdata('name')?>"></input></td>
			</tr>
			
			<tr>
				<td style="text-align: right">Currency</td>
				<td>
				<input name="currency" id="currency" class="easyui-combobox" ></input>
				</td>

				<td>Exchange Rate</td>
				<td><input name="exchangerate" id="exchangerate" class="easyui-textbox"></input></td>

				<td style="text-align: right">Shipment Reff</td>
				<td><input name="shipmentreff" id="shipmentreff" class="easyui-textbox" ></input></td>
			</tr>
		</table>
		<?php echo form_close();?>
	</div>
	<div title="Finance" style="padding:10px">
		<ul class="easyui-tree" data-options="url:'tree_data1.json',method:'get',animate:true"></ul>
	</div>
	<div title="Address" style="padding:10px">
		<ul class="easyui-tree" data-options="url:'tree_data1.json',method:'get',animate:true"></ul>
	</div>
</div>

<h4></h4>
<div class="easyui-tabs" style="width:100%;height:320px">
	<div title="Purchase Order Item" style="padding:0px">
		<table id="dg"></table>
	</div>
	<div id="toolbar">
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="openWindowItem();">Add Item</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="removeit()">Remove</a>
    </div>
	
	

</div>

<div id="dlgAddItem" 
	class="easyui-window" 
	title="Add List Item" 
	data-options="modal:true,closed:true,iconCls:'icon-save',footer:'#footerDialog'" style="width:800px;height:520px;padding:10px">
	
	<?php echo form_open("",'id="frmAddItem"')?>
	<table style="width:100%;">
		<tr>
			<td style="width:45%;">
				<table style="width:100%;">
					<tr>
						<td >
							<input id="addLabel" class="easyui-combogrid" name="addLabel" style="width:80%;" />
						</td>
						<td >
							<input id="addCate" class="easyui-combogrid" name="addCate" style="width:80%;" />
						</td>
						<td >
							<input id="addSex" class="easyui-combogrid" name="addSex" style="width:70%;" />
						</td>
						<td >
							<input id="addColorCode" class="easyui-combogrid" name="addColorCode" style="width:80%;" />
						</td>
						<td >
							<input id="addFabricCode" class="easyui-textbox" name="addFabricCode" style="width:70%;" data-options="
								label: 'Fabric:',
								labelPosition: 'top'
								" />
						</td>
					</tr>

					<tr>
						<td colspan=5>
							<input class="easyui-textbox" id="addsuppitemreffcode" name="addsuppitemreffcode" data-options="label:'Supplier Item Reff Code (Max 36 char)',labelPosition:'top'" style="width:90%;" maxlength="36">
						</td>
					</tr>

					<tr>
						<td colspan=5>
							<input class="easyui-textbox" id="addStyle" name="addStyle" data-options="label:'Style (Should 7 char)',labelPosition:'top'" style="width:30%;" maxlength="6">
						</td>
					</tr>
					<tr>
						<td colspan=5 >
							<input class="easyui-textbox" id="addsuppliercolor" name="addsuppliercolor" data-options="label:'Supplier Color',labelPosition:'top'" style="width:90%;" >
						</td>
					</tr>

					<tr>
						<td colspan=5 >
							<input class="easyui-textbox" id="addfabricdesc" name="addfabricdesc" data-options="label:'Fabric Desc (Max 36 char)',labelPosition:'top'" style="width:90%;" >
						</td>
					</tr>
					<tr>
						<td colspan=5 >
							<input class="easyui-textbox" id="addstyledesc" name="addstyledesc" data-options="label:'Style Desc (Max 36 char)',labelPosition:'top'" style="width:90%;" >
						</td>
					</tr>

					<tr>
						<td colspan=5 >
							<input class="easyui-textbox" id="addcountryorigin" name="addcountryorigin" data-options="label:'Country Origin',labelPosition:'top'" style="width:90%;" maxlength="36">
						</td>
					</tr>

					<tr>
						<td  colspan=2>
							<input class="easyui-numberbox" id="addcost" name="addcost" data-options="precision:'2',label:'FOB',labelPosition:'top'" style="width:90%;" maxlength="6">
						</td>
						<td  colspan=3>
							<input class="easyui-numberbox" id="sugestedrp" name="sugestedrp"  style="width:90%;" >
						</td>
					</tr>
				</table>
			</td>

			<td style="width:45%;" valign="top">
				<div style="margin:20px 0;"></div>
				<table class="easyui-datagrid" title="Size Grid" id="tblSizeQty"
						data-options="singleSelect:true,toolbar:'#tbaddqty'">
					<thead>
						<tr>
							<th data-options="field:'size',width:80">Size</th>
							<th data-options="field:'qty',width:100">QTY</th>
						</tr>
					</thead>
				</table>
			</td>
		</tr>
	</table>
	
	
	<?php echo form_close()?>
	
	<div id="tbaddqty" style="padding:2px 5px;">
		<input id="sizecodegroup" style="width:50px;" class="easyui-combobox" name="sizecodegroup">
	</div>
</div>

<div id="footerDialog" style="padding:5px;"><a class="easyui-linkbutton" data-options="iconCls:'icon-ok'" href="javascript:void(0)" onclick="submitForm()" style="width:80px">Ok</a></div>

<script type="text/javascript">
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var newdata = <?php echo $json_currency?>;
	var items_size_qty = new Array();
	var size_code = "";
	$(function () {
		$('#sugestedrp').numberbox({
			min:0,
			precision:2,
			label: 'Sugested Retail Price',
			labelPosition: 'top',
			value:10.00
		});

		$('#dg').datagrid({
			width:'100%',
			height:250,
			singleSelect:true,
			idField:'barcode',
			fit: true,
			toolbar:'#toolbar',
			url:'purchasing/getItems',
			method: 'get',
			view: detailview,
			columns:[[
				{field:'label',title:'Label',width:50},
				{field:'style',title:'Style',width:70},
				{field:'color',title:'Color',width:50},
				{field:'gender',title:'Sex',width:50},
				{field:'cate',title:'Cate',width:50},
				{field:'barcode',title:'Item Code',width:100},
				{field:'description',title:'Style Description',width:250},
				{field:'qty',title:'Tot Qty',width:50},
				{field:'unitcost',title:'FOB',width:100},
				{field:'unitprice',title:'Unit Price',width:100}
			]],
			detailFormatter:function(index,row){
				return '<div style="padding:2px;position:relative;"><table class="ddv"></table></div>';
			},
			onExpandRow: function(index,row){
				var ddv = $(this).datagrid('getRowDetail',index).find('table.ddv');
				ddv.datagrid({
					url:'purchasing/getSizing?rowid='+row.barcode,
					method: 'get',
					fitColumns:true,
					singleSelect:true,
					rownumbers:true,
					loadMsg:'',
					height:'auto',
					columns:[[
						{field:'nsize',title:'Size',width:200},
						{field:'nqty',title:'Quantity',width:100}
					]],
					onResize:function(){
						$('#dg').datagrid('fixDetailRowHeight',index);
					},
					onLoadSuccess:function(){
						setTimeout(function(){
							$('#dg').datagrid('fixDetailRowHeight',index);
						},0);
					}
				});
				$('#dg').datagrid('fixDetailRowHeight',index);
			}
		});


		$('#sizecodegroup').combobox({
			panelHeight: 'auto',
			selectOnNavigation: false,
			valueField: 'id',
			textField: 'text',
			required: true,
			panelMaxHeight: 200,
			validType: 'exists["#sizecodegroup"]',
			onLoadSuccess: function () { },
			filter: function (q, row) {
				return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
			},
			url: 'Purchasing/getSizeGrid',
			method: 'get',
			onSelect: function(record)
			{
				console.log(record.text);
				size_code = record.text;
				$("#tblSizeQty").propertygrid({
					url: 'Purchasing/getSizeGrid/'+size_code,
					method:'get',
					width:'95%',
					height:'90%',
					showGroup: true,
					scrollbarSize: 0,
					columns:[[
						{field:'name',title:'Size',width:100,resizable:true},
						{field:'value',title:'Qty',width:100,resizable:false}
					]]
				});
			}
		});

		$('#currency').combobox({
			panelHeight: 'auto',
			selectOnNavigation: false,
			valueField: 'id',
			textField: 'text',
			editable: true,
			required: true,    
			validType: 'exists["#currency"]',
			onLoadSuccess: function () { },
			filter: function (q, row) {
				return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
			},
			data: newdata,
			onSelect: function(record)
			{
				if(record)
				{
					$('#exchangerate').textbox('setText', record.kurs);
				}
			}
		});

		$('#suppliername').combogrid({
			panelWidth:500,
			url: "purchasing/getSupplierInfo",
			idField:'supplierid',
			textField:'suppliername',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			columns:[[
				{field:'supplierid',title:'Supplier ID',width:60},
				{field:'suppliercode',title:'Supplier Code',width:80},
				{field:'suppliername',title:'Name',width:60}
			]],
			onChange:function(value){
				var g = $('#suppliername').combogrid('grid');
				g.datagrid('load',{assay_type_id:value});
				console.log("find id : "+value);
			},
			onSelect: function(index,row){
				var codigo = row.suppliercode;
				$('#suppliercode').textbox('setText', codigo);
				console.log(codigo);
			}
		});


		$('#addCate').combogrid({
			panelWidth:400,
			url: "purchasing/getProductCategory",
			idField:'productcategorycode',
			textField:'productcategorycode',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			label: 'Cate:',
			labelPosition: 'top',
			columns:[[
				{field:'productcategoryid',title:'Cat ID',width:10},
				{field:'productcategorycode',title:'Category Code',width:20},
				{field:'productcategorydesc',title:'Description',width:60},
				{field:'markup',title:'MU',width:20}
			]],
			onChange:function(value){
				var g = $('#addCate').combogrid('grid');
				g.datagrid('load',{assay_type_id:value});
				console.log("find id : "+value);
			},
			onSelect: function(index,row){
				var codigo = row.productcategorycode;
				console.log(codigo);
			}
		});

		$('#addSex').combogrid({
			panelWidth:300,
			url: "purchasing/getProductSex",
			idField:'sexcode',
			textField:'sexcode',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			label: 'Sex:',
			labelPosition: 'top',
			columns:[[
				{field:'genderid',title:'ID',width:20},
				{field:'sexcode',title:'Sex Code',width:20},
				{field:'description',title:'Description',width:80}
			]],
			onChange:function(value){
				var g = $('#addSex').combogrid('grid');
				g.datagrid('load',{assay_type_id:value});
				console.log("find id : "+value);
			},
			onSelect: function(index,row){
				var codigo = row.sexcode;
				console.log(codigo);
			}
		});


		$('#addColorCode').combogrid({
			panelWidth:300,
			url: "purchasing/getProductColor",
			idField:'colorcode',
			textField:'colorcode',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			label: 'Color :',
			labelPosition: 'top',
			columns:[[
				{field:'colorid',title:'ID',width:20},
				{field:'colorcode',title:'Color Code',width:30},
				{field:'colordescription',title:'Description',width:80}
			]],
			onChange:function(value){
				var g = $('#addColorCode').combogrid('grid');
				g.datagrid('load',{assay_type_id:value});
				console.log("find id : "+value);
			},
			onSelect: function(index,row){
				var codigo = row.sexcode;
				console.log(codigo);
			}
		});

		$('#addLabel').combogrid({
			panelWidth:300,
			url: "purchasing/getProductLabel",
			idField:'productgroupcode',
			textField:'productgroupcode',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			label: 'Label :',
			labelPosition: 'top',
			columns:[[
				{field:'productgroupid',title:'ID',width:20},
				{field:'productgroupcode',title:'Label Code',width:30},
				{field:'productgroupdesc',title:'Description',width:80}
			]],
			onChange:function(value){
				var g = $('#addLabel').combogrid('grid');
				g.datagrid('load',{assay_type_id:value});
				console.log("find id : "+value);
			},
			onSelect: function(index,row){
				var codigo = row.productgroupcode;
				console.log(codigo);
			}
		});

		$('#season').combogrid({
			panelWidth:300,
			url: "purchasing/getProductSeason",
			idField:'productseasoncode',
			textField:'productseasoncode',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			columns:[[
				{field:'productseasonid',title:'ID',width:20},
				{field:'productseasoncode',title:'Season Code',width:30},
				{field:'productseasondesc',title:'Description',width:80}
			]],
			onChange:function(value){
				var g = $('#season').combogrid('grid');
				g.datagrid('load',{assay_type_id:value});
				console.log("find id : "+value);
			},
			onSelect: function(index,row){
				var codigo = row.productseasoncode;
				console.log(codigo);
			}
		});
		

		$('#addStyle').textbox({validType: "maxLength[7]"});
		$('#addsuppitemreffcode').textbox({validType: "maxLength[36]"});
		$('#addfabricdesc').textbox({validType: "maxLength[36]"});
		$('#addstyledesc').textbox({validType: "maxLength[36]"});
		$('#addFabricCode').textbox({validType: "maxLength[1]"});
		$('#addSize').textbox({validType: "maxLength[3]"});
		
	});
	
	

	$.extend($.fn.validatebox.defaults.rules,{
		exists:{
			validator:function(value,param){
				var cc = $(param[0]);
				var v = cc.combobox('getValue');
				var rows = cc.combobox('getData');
				for(var i=0; i<rows.length; i++){
					if (rows[i].id == v){return true}
				}
				return false;
			},
			message:'The entered value does not exists.'
		}
	});

	

	$.extend($.fn.textbox.defaults.rules, {
		maxLength: { 
			validator: function(value, param){
				//console.log(value.length);
				return value.length <= param[0];
			},
			message: 'Maximum characters allowed only {0}'
		}
	});


	function submitForm(){

		// hitung size yg di input
		calculateSize();

		var str = $( "#frmAddItem" ).serialize();
		var index = $('#dg').datagrid('getRows').length;
		index = (index == undefined) ? 0 : index;
		var label = $('#addLabel').combogrid('grid').datagrid('getSelected');
		var cate = $('#addCate').combogrid('grid').datagrid('getSelected');
		var color = $('#addColorCode').combogrid('grid').datagrid('getSelected');
		var fabric = $('#addFabricCode').textbox("getText");
		var gender = $('#addSex').combogrid('grid').datagrid('getSelected');
		var supplier_ref = $('#addsuppitemreffcode').textbox('getText');
		var supplier_color = $('#addsuppliercolor').textbox('getText');
		var fabric_dec = $('#addfabricdesc').textbox('getText');
		var style_desc = $('#addstyledesc').textbox('getText');
		var origin = $('#addcountryorigin').textbox('getText');
		
		var ucost = $('#addcost').numberbox('getValue');
		var sugestedrp = $('#sugestedrp').numberbox('getValue');
		sugestedrp = (sugestedrp == '') ? 0 : sugestedrp;
		var style = $('#addStyle').textbox('getText');
		if(style == "")
		{
			$.messager.alert('Failed',"Please input the STYLE !!",'error');
			return;
		}

		if(style.length != 7)
		{
			$.messager.alert('Failed',"Please input the STYLE !!",'error');
			return;
		}

		var season = $('#season').combogrid('grid').datagrid('getSelected');
		
		if(season == null || season.productseasoncode == "")
		{
			$.messager.alert('Failed',"Please input the SEASON!!",'error');
			return;
		}
		var kurs = $('#exchangerate').textbox('getText');
		
		if(kurs == "")
		{
			$.messager.alert('Failed',"Please select Currency",'error');
			return;
		}

		if(ucost == "")
		{
			$.messager.alert('Failed',"Please input FOB",'error');
			return;
		}
		
		if(items_size_qty.length <= 0)
		{
			$.messager.alert('Failed',"Please Input SIZE and QTY!",'error');
			return;
		}


		var uprice = (parseFloat(ucost) * parseFloat(kurs));
		var barcode = season.productseasoncode + style + color.colorcode + fabric;
		$('#dg').datagrid('insertRow',{
			index: index,
			row:{
				label:label.productgroupcode,
				style:style,
				color:color.colorcode,
				gender:gender.sexcode,
				cate:cate.productcategorycode,
				barcode:barcode,
				description:style_desc,
				qty:sumQty(),
				unitcost:ucost,
				unitprice:uprice
			}
		});
		
		$.post( "purchasing/saveDraftPoitems", 
			{csrf_name:csrf, adata:JSON.stringify(items_size_qty), 
			itemcode:barcode,
			label:label.productgroupcode,
			cate:cate.productcategorycode,
			gender:gender.sexcode,
			color:color.colorcode,
			fabric:fabric,
			supplier_reff:supplier_ref,
			style:style,
			supplier_color:supplier_color,
			fabric_dec:fabric_dec,
			style_desc:style_desc,
			origin:origin,
			cost:ucost,
			sugestedrp:sugestedrp,
			season:season.productseasoncode,
			exchangerate:kurs,
			currency: ''
			},
			function( data ) {
				items_size_qty = new Array();
				$('#tblSizeQty').propertygrid('loadData', []);
				csrf = data.csrf_name;
				
		}, "json");

		

	}

	function sumQty () {

		var propertyData = $('#tblSizeQty').propertygrid('getData');
		var propRows = propertyData.rows;
		var sum = 0;
		$.each(propRows, function( index, value ) {
			sum+=parseFloat(value.value);
		});

		return sum;
	}

	function openWindowItem()
	{
		$('#frmAddItem').form('clear');
		$('#tblSizeQty').propertygrid('loadData', []);
		$('#dlgAddItem').dialog('open');
		$('#sugestedrp').numberbox('setValue',0);
		$('#addcost').numberbox('setValue',0);
	}
	

	function calculateSize()
	{
		
		
		var propertyData = $('#tblSizeQty').propertygrid('getData');
		var propRows = propertyData.rows;
		
		$.each(propRows, function( index, value ) {
			if(value.value != null || value.value != "")
			{
				if(parseFloat(value.value) > 0)
				{
					var grid = {};
					grid['size'] = value.name;
					grid['qty'] = value.value;
					items_size_qty[items_size_qty.length] = grid;
				}
				
			}
		});
		console.log(items_size_qty);

	}

	


	function refreshAndClear()
	{
		openUrl("<?php echo base_url()?>index.php/purchasing/createpo/refreshpo");
	}

	function doCreate()
	{
		var ponum = $('#ponum').textbox("getText");
		var supplierName = $('#suppliername').combogrid('grid').datagrid('getSelected');
		var suppReffnum = $('#supp_reff_num').textbox("getText");
		var currency = $('#currency').combobox('getText');
		var season = $('#season').combogrid('grid').datagrid('getSelected');
		var supplierCode = $('#suppliercode').textbox("getText");
		var description = $('#description').textbox("getText");
		var exchangerate = $('#exchangerate').textbox('getText');
		var type = $('#potype').combobox('getText');
		var division = $('#division').textbox('getText');
		var buyer = $('#buyer').textbox('getText');
		var shipmentReff = $('#shipmentreff').textbox('getText');

		var win = $.messager.progress({
                title:'Please waiting',
                msg:'Sending data...'
            });

			$.post( "purchasing/saveCreatingPo", 
			{
				csrf_name:csrf, 
				ponum:ponum,
				supplierId:supplierName.supplierid,
				supplierName:supplierName.suppliername,
				suppReffnum:suppReffnum,
				currency:currency,
				season:season.productseasoncode,
				supplierCode:supplierCode,
				description:description,
				exchangerate:exchangerate,
				type:type,
				division:division,
				shipmentReff:shipmentReff,
				buyer:buyer
			},
			function( data ) {
				$.messager.progress('close');
				var dlg = $.messager.confirm({
				    title: 'Successfully',
				    msg: 'Successfully you PO is '+data.po_number,
				    buttons:[{
				        text: 'OK',
				        onClick: function(){
				        	dlg.dialog('destroy')
				            openUrl("<?php echo base_url()?>index.php/purchasing/createpo/refreshpo");
				        }
				    }]
				});
					
		}, "json");
	}
	
</script>