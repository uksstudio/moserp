
<div style="padding:5px;margin-bottom:5px;text-align: right;">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit'" onClick="doCreate()">Edit PO</a>
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove'" onClick="voidPO()">Cancel PO</a>
</div>
<table id="dgcostingreview" class="easyui-datagrid"></table>

<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var newdata = <?php echo $json_currency?>;
	var username = '<?php echo $this->session->userdata("username")?>';
	$(function () {
		$('#dgcostingreview').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'po_number',
			fit: true,
			rownumbers:true,
			url:'purchasing/getPurchaseOrderList',
			method: 'get',
			showFooter: true,
			view: detailview,
			columns:[[
				{field:'po_number',title:'Number',width:80},
				{field:'status',title:'Status',width:80},
				{field:'createdate',title:'Issue Date',width:65},
				{field:'season',title:'Season',width:50},
				{field:'doc_currency',title:'Currency',width:80},
				{field:'exchangerate',title:'Exchange Rate',width:90},
				{field:'po_type',title:'Type',width:80},
				{field:'supplier_name',title:'Supplier Name',width:150},
				{field:'reff',title:'Supp Reff',width:100},
				{field:'description',title:'Description',width:150},
				{field:'prepared_by',title:'Prepared By',width:100},
				{field:'action',title:'Action',width:80, formatter:formatterPrint}
			]],
			detailFormatter:function(index,row){
				return '<div style="padding:2px;position:relative;"><table class="ddv"></table></div>';
			},
			onExpandRow: function(index,row){
				var ddv = $(this).datagrid('getRowDetail',index).find('table.ddv');
				ddv.datagrid({
					url:'purchasing/getDetailPurchasing/'+row.id,
					method: 'get',
					fitColumns:true,
					singleSelect:true,
					rownumbers:true,
					loadMsg:'',
					height:'auto',
					columns:[[
						{field:'label_code',title:'Label Code',width:60},
						{field:'style_code',title:'Style Code',width:80},
						{field:'color_code',title:'Color',width:50},
						{field:'gender',title:'Gender',width:50},
						{field:'cate_code',title:'Cate Code',width:50},
						{field:'barcode',title:'Item Code',width:100},
						{field:'size_code',title:'Size',width:40},
						{field:'fabric_desc',title:'Fabric Desc',width:150},
						{field:'qty',title:'QTY',width:50},
						{field:'unitcost',title:'FOB',width:100},
						{field:'unitprice',title:'Unit Price',width:100}
					]],
					onResize:function(){
						$('#dgcostingreview').datagrid('fixDetailRowHeight',index);
					},
					onLoadSuccess:function(){
						setTimeout(function(){
							$('#dgcostingreview').datagrid('fixDetailRowHeight',index);
						},0);
					}
				});
				$('#dgcostingreview').datagrid('fixDetailRowHeight',index);
			},
			onCheck:onCheck,
			onUncheck:onUncheck,
			onCheckAll:onCheckAll,
			onUncheckAll:onUncheckAll,
		});
	});


	function formatterPrint(value,row,index)
	{
		var costingnumber = "'"+row.po_number+"'";
		var e = '<a href="javascript:void(0)" class="easyui-linkbutton" onclick="printing('+costingnumber+')">Print</a> ';
		return e;
	}

	function printing(trfno)
	{
		loadPrintDocument(this, "report/reprintdncn_A4/"+trfno);
	}

	function myformatter(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	}
	function myparser(s){
		if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
			return new Date(y,m-1,d);
		} else {
			return new Date();
		}
	}
	
	function onSelect(date){
		strTanggal = date.getFullYear()+"-"+("0" + (date.getMonth() + 1).toString()).substr(-2)+"-"+("0" + date.getDate().toString()).substr(-2);
	}
	var checkedRows = [];
	function onCheck(index,row){
		for(var i=0; i<checkedRows.length; i++){
			if (checkedRows[i].po_number == row.po_number)
			{
				row.chk = 'Y';
				return;
			}
		}
		row.chk = 'Y';
		checkedRows.push(row);
		
		console.log("onCheck:"+row.po_number);
	}
	function onUncheck(index,row){
		for(var i=0; i<checkedRows.length; i++){
			if (checkedRows[i].po_number == row.po_number){
				row.chk = 'N';
				checkedRows.splice(i,1);
				return;
			}
		}
		row.chk = 'N';
		console.log("onUncheck:"+index);
	}


	function onCheckAll(row){
		for(var i=0; i<row.length; i++){
			row[i].chk = 'Y';
		}
	}
	function onUncheckAll(row){
		for(var i=0; i<row.length; i++){
			row[i].chk = 'N';
		}
		
	}

	
	
	function parseFloat2Decimals(value) {
		return parseFloat(parseFloat(value).toFixed(2));
	}

	function roundUp(num, precision) {
	  precision = Math.pow(10, precision)
	  return Math.ceil(num * precision) / precision
	}

	function voidPO()
	{
		var row = $('#dgcostingreview').datagrid('getSelected');
        if (!row){
            $.messager.alert('Info', "Please select ROW.");
            return;
        }

        if(row.status != 'NEW')
        {
        	$.messager.alert('Info', "PO already process, can not void.");
            return;
        }

        if(row.prepared_by == "" || row.prepared_by != username)
        {
        	$.messager.alert('Info', "PO is not your own.");
            return;	
        }

        $.messager.confirm('Confirm','Are you sure you want to VOID this PO?',function(r){
			if (r){
				$.post('purchasing/voidPo',{csrf_name:csrf, po_number:row.po_number},function(result){
					if (result.status){
						csrf = result.csrf_name;
						$('#dgcostingreview').datagrid('reload');    // reload the user data
					} else {
						$.messager.show({    // show error message
							title: 'Error',
							msg: result.errorMsg
						});
					}
				},'json');
			}
		});
        

	}
	
</script>