<?php

$ponum = strtoupper(substr($this->session->userdata('username'),0,2).date('ym')."XXXX");
?>
<div id="ft" style="padding:5px;margin-bottom:5px;text-align: right;">
	<a href="#" class="easyui-linkbutton c2" data-options="iconCls:'icon-edit',plain:true" onclick="updateRows()">Update (F10)</a>
	<a href="#" class="easyui-linkbutton c1" data-options="iconCls:'icon-save'" onClick="doCreate()">Save (F8)</a>
	<a href="#" class="easyui-linkbutton c3" data-options="iconCls:'icon-reload'" onClick="refreshAndClear()">Clear</a>
</div>


<div id="cc" class="easyui-layout" style="width:100%;height:100%;">
    <div data-options="region:'south',split:true" style="height:65%"> <!-- buat datagrid -->
    	<table id="dg"></table>
    </div>
    <div data-options="region:'center'" style="height:30%;"> <!-- buat tab -->
    	<div class="easyui-tabs"  style="width:100%;height:100%">
			<div title="Purchase Order V1" style="padding:10px;height: 300px;">
				<?php echo form_open("");?>
				<table style="width:100%; ">
					<tr>
						<td style="text-align: right">Po Number</td>
						<td><input name="ponum" id="ponum" class="f1 easyui-textbox" value="<?php echo $ponum;?>" readonly></input></td>

						<td>Season</td>
						<td>
							<input name="season" id="season" class="easyui-combogrid" data-options="required:true"></input></td>

						<td style="text-align: right">Type</td>
						<td>
							<select class="easyui-combobox" name="potype" id="potype">
								<option value="GOODS">Goods</option>
							</select>
						</td>
					</tr>

					<tr>
						<td style="text-align: right">Supplier Name</td>
						<td><input name="suppliername" id="suppliername" class="easyui-combogrid" data-options="required:true"></input></td>

						<td>Supplier Code</td>
						<td><input name="suppliercode" id="suppliercode" class="f1 easyui-textbox"></input></td>

						<td style="text-align: right">Division</td>
						<td><input name="division" id="division" class="f1 easyui-textbox" value="<?php echo $division;?>"></input></td>
					</tr>

					<tr>
						<td style="text-align: right">Supp Reff Number</td>
						<td><input name="supp_reff_num" id="supp_reff_num" class="f1 easyui-textbox"></input></td>

						<td>Description</td>
						<td><input name="description" id="description" class="f1 easyui-textbox"></input></td>

						<td style="text-align: right">Buyer</td>
						<td><input name="buyer" id="buyer" class="f1 easyui-textbox" value="<?php echo $this->session->userdata('name')?>"></input></td>
					</tr>
					
					<tr>
						<td style="text-align: right">Currency</td>
						<td>
						<input name="currency" id="currency" class="easyui-combobox" ></input>
						</td>

						<td>Exchange Rate</td>
						<td><input name="exchangerate" id="exchangerate" class="easyui-textbox"></input></td>

						<td style="text-align: right">Brand Code</td>
						<td><input name="brandcode" id="brandcode" class="easyui-combogrid" data-options="required:true"></input></td>
					</tr>
				</table>
				<?php echo form_close();?>
			</div>
			<div title="Finance" style="padding:10px">
				
			</div>
			<div title="Address" style="padding:10px">
				
			</div>
			
		</div>
    </div>
</div>



<div id="dlgAddItem" 
	class="easyui-dialog" 
	title="Size Grid" 
	data-options="modal:true,closed:true,iconCls:'icon-save',footer:'#footerDialog'" style="width:300px;height:500px;">
	
	<?php echo form_open("",'id="frmAddItem"')?>
	<input type="hidden" name="isizeCode" id="isizeCode">
	<table class="easyui-propertygrid" id="tblSizeQty"></table>
	<?php echo form_close()?>
	
</div>

<div id="toolbarsize">
	<input id="sizecodegroup" style="width:50px;" class="easyui-combobox" name="sizecodegroup">
</div>


<div id="footerDialog" style="padding:5px;"><a class="easyui-linkbutton" data-options="iconCls:'icon-ok'" href="javascript:void(0)" onclick="saveSize()" style="width:80px">Ok</a></div>

<?php echo script_tag('includes/plugins/datagrid-cellediting.js');?>
<script type="text/javascript">
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var newdata = <?php echo $json_currency?>;
	var items_size_qty = new Array();
	var items_size_wid = [];
	var size_code = "";
	var Clickc = [];
	var editIndex = undefined;
	$(document).keyup(function(event) {
	    // console.log(event.keyCode);
	    // open  pressing "F8"
	    if (event.keyCode === 119) {
	        alert("PRESS F8");
	    }
	    
	    if (event.keyCode === 121) { // F10
	        alert("PRESS F10");
	    }

	    if (event.keyCode === 27) { // ESC
	        reject();
	    }
	    

	});
	$(function () {
		$('#sugestedrp').numberbox({
			min:0,
			precision:2,
			label: 'Sugested Retail Price',
			labelPosition: 'top',
			value:10.00
		});

		$('#dg').datagrid({
			singleSelect:true,
			idField:'barcode',
			fit: true,
			rownumbers:true,
			url:'purchasing/getItems',
			method: 'get',
			showFooter: true,
			footer:'#ft',
            nowrap: true,
			view: detailview,
			onClickCell: onClickCell,
            //onEndEdit: onEndEdit,
			columns:[[
				{field:'itemcode',title:'Item Code',width:150, styler: 
					function(val, row, index)
					{
						return 'background-color:#e86b97;color:#ffffff;font-style:bold;';
					}
				},
				{field:'label',title:'Label',width:80, editor:{type:'combogrid',
					options:{
						panelWidth:300,
						url: "purchasing/getProductLabel",
						idField:'productgroupcode',
						textField:'productgroupcode',
						mode:'remote',
						fitColumns:true,
						required: true,
						method: 'get',
						columns:[[
							{field:'productgroupid',title:'ID',width:20},
							{field:'productgroupcode',title:'Label Code',width:30},
							{field:'productgroupdesc',title:'Description',width:80}
						]]
					}}
				},
				{field:'supplier_reff',title:'Supp Reff Code',width:180, editor:{type:'textbox',options:{required: true}}},
				{field:'cate',title:'Cate',width:50, editor:{type:'combogrid',
					options:{
						panelWidth:300,
						url: "purchasing/getProductCategory",
						idField:'productcategorycode',
						textField:'productcategorycode',
						mode:'remote',
						fitColumns:true,
						required: true,
						method: 'get',
						columns:[[
							{field:'productcategoryid',title:'Cat ID',width:10},
							{field:'productcategorycode',title:'Category Code',width:20},
							{field:'productcategorydesc',title:'Description',width:60},
							{field:'markup',title:'MU',width:20}
						]]
					}}
				},
				{field:'gender',title:'Sex',width:50, editor:{type:'combogrid',
					options:{
						panelWidth:300,
						url: "purchasing/getProductSex",
						idField:'sexcode',
						textField:'sexcode',
						mode:'remote',
						fitColumns:true,
						required: true,
						method: 'get',
						columns:[[
							{field:'genderid',title:'ID',width:20},
							{field:'sexcode',title:'Sex Code',width:20},
							{field:'description',title:'Description',width:80}
						]]
					}}
				},
				{field:'style',title:'Style',width:80, editor:{type:'textbox',options:{validType: "maxLength[9]"}}},
				{field:'color',title:'Col Code',width:50, editor:{type:'combogrid',
					options:{
						panelWidth:300,
						url: "purchasing/getProductColor",
						idField:'colorcode',
						textField:'colorcode',
						mode:'remote',
						required: true,
						fitColumns:true,
						method: 'get',
						columns:[[
							{field:'colorid',title:'ID',width:20},
							{field:'colorcode',title:'Color Code',width:30},
							{field:'colordescription',title:'Description',width:80}
						]]
					}}
				},
				{field:'supplier_color',title:'Supp Color',width:200, editor:{type:'textbox',options:{required: true}}},
				{field:'fabric',title:'Fabric',width:50, editor:{type:'textbox',options:{required: true,validType: "maxLength[1]"}}},
				{field:'fabric_dec',title:'Fabric Desc',width:300, editor:{type:'textbox',options:{required: true}}},
				{field:'style_desc',title:'Style Desc',width:300, editor:{type:'textbox',options:{required: true}}},
				{field:'origin',title:'Origin',width:100, editor:{type:'textbox'}},
				{field:'cost',title:'Cost',width:80, editor:{type:'numberbox',options:{precision:2,required: true}}},
				{field:'sugestedrp',title:'Sug Price',width:80, editor:{type:'numberbox',options:{precision:2}}},
				{field:'sizegrid',title:'Size Grid',width:70, formatter: 
					function(val, row)
					{
						
						var chn = (row.sizegrid.length > 0) ? row.sizegrid : 'Add';
						return '<a href="#" onClick="changeSizeGrid()">'+chn+'</a>';
					}
				},
				{field:'totalqty',title:'Total Qty',width:60}
			]],
			detailFormatter:function(index,row){
				return '<div style="padding:2px;position:relative;"><table class="ddv"></table></div>';
			},
			onExpandRow: function(index,row){
				var ddv = $(this).datagrid('getRowDetail',index).find('table.ddv');
				ddv.datagrid({
					url:'purchasing/getSizing/'+row.rowid,
					method: 'get',
					fitColumns:false,
					singleSelect:true,
					rownumbers:true,
					loadMsg:'Please Wait....',
					height:'auto',
					columns:[[
						{field:'nsize',title:'Size',width:200},
						{field:'nqty',title:'Quantity',width:100}
					]],
					onResize:function(){
						$('#dg').datagrid('fixDetailRowHeight',index);
					},
					onLoadSuccess:function(){
						setTimeout(function(){
							$('#dg').datagrid('fixDetailRowHeight',index);
						},0);
					}
				});
				$('#dg').datagrid('fixDetailRowHeight',index);
			},
			onEndEdit:function(index,row){
	            var style = $(this).datagrid('getEditor', {index: index,field: 'style'});
	            var color = $(this).datagrid('getEditor', {index: index,field: 'color'});
	            var fabric = $(this).datagrid('getEditor', {index: index,field: 'fabric'});
	            var s_style = $(style.target).textbox('getText');
	            console.log(s_style.length);
	            if(s_style.length < 9)
	            {
	            	var l_style = (9 - parseFloat(s_style.length));
	            	console.log(l_style);
	            	var st_style = repeat(" ", l_style);
	            	console.log(st_style);
	            	s_style = s_style + st_style;
	            }
	            console.log(s_style.length);
	            row.itemcode = s_style.toUpperCase() + $(color.target).combogrid('getText') + $(fabric.target).textbox('getText');
	            console.log(row.itemcode.length);
	            //saveRows(index);
	            console.log('onEndEdit');
	        },
	        onBeforeEdit:function(index,row){
	            row.editing = true;
	            editIndex = index;
	            $(this).datagrid('refreshRow', index);
	            console.log('onBeforeEdit');
	        },
	        onAfterEdit:function(index,row){
	            row.editing = false;
	            $(this).datagrid('refreshRow', index);
	            console.log('onAfterEdit');
	        },
	        onCancelEdit:function(index,row){
	            row.editing = false;
	            $(this).datagrid('refreshRow', index);
	            console.log('onCancelEdit');
	        },
	        onBeginEdit: function(index,row){
	        	console.log('onBeginEdit '+index);
	        	var currRows = $(this).datagrid('getRows').length; //3
	        	if((currRows - (index + 1)) <= 1)
	        	{
	        		addRows(index, 2);	
	        	}
	        	
	        	console.log('onBeginEdit ROW '+currRows);

			    var flabel = $(this).datagrid('getEditor', {index:index,field:'label'});
				if (flabel){
    				$(flabel.target).combogrid('reload','purchasing/getProductLabel?q='+row.code);
				}

				var fcate = $(this).datagrid('getEditor', {index:index,field:'cate'});
				if (fcate){
    				$(fcate.target).combogrid('reload','purchasing/getProductCategory?q='+row.code);
				}

				var fgender = $(this).datagrid('getEditor', {index:index,field:'gender'});
				if (fgender){
    				$(fgender.target).combogrid('reload','purchasing/getProductSex?q='+row.code);
				}

				var fcolor = $(this).datagrid('getEditor', {index:index,field:'color'});
				if (fcolor){
    				$(fcolor.target).combogrid('reload','purchasing/getProductColor?q='+row.code);
				}
			}
		});

		

		
		$('#currency').combobox({
			panelHeight: 'auto',
			selectOnNavigation: false,
			valueField: 'id',
			textField: 'text',
			editable: true,
			required: true,    
			validType: 'exists["#currency"]',
			onLoadSuccess: function () { },
			filter: function (q, row) {
				return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
			},
			data: newdata,
			onSelect: function(record)
			{
				if(record)
				{
					$('#exchangerate').textbox('setText', record.kurs);
				}
			}
		});

		$('#suppliername').combogrid({
			panelWidth:500,
			url: "purchasing/getSupplierInfo",
			idField:'supplierid',
			textField:'suppliername',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			columns:[[
				{field:'supplierid',title:'Supplier ID',width:60},
				{field:'suppliercode',title:'Supplier Code',width:80},
				{field:'suppliername',title:'Name',width:60}
			]],
			onChange:function(value){
				var g = $('#suppliername').combogrid('grid');
				g.datagrid('load',{q:value});
			},
			onSelect: function(index,row){
				var codigo = row.suppliercode;
				$('#suppliercode').textbox('setText', codigo);
				
			}
		});


		$('#brandcode').combogrid({
			panelWidth:300,
			url: "purchasing/getProductBrand",
			idField:'brandcode',
			textField:'brandcode',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			columns:[[
				{field:'productbrandid',title:'ID',width:20},
				{field:'brandcode',title:'Season Code',width:30},
				{field:'productbranddesc',title:'Description',width:80}
			]],
			onChange:function(value){
				var g = $('#brandcode').combogrid('grid');
				g.datagrid('load',{q:value});
			},
			onSelect: function(index,row){
				var codigo = row.productseasoncode;
				console.log(codigo);
			}
		});

		$('#season').combogrid({
			panelWidth:300,
			url: "purchasing/getProductSeason",
			idField:'productseasoncode',
			textField:'productseasoncode',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			columns:[[
				{field:'productseasonid',title:'ID',width:20},
				{field:'productseasoncode',title:'Season Code',width:30},
				{field:'productseasondesc',title:'Description',width:80}
			]],
			onChange:function(value){
				var g = $('#season').combogrid('grid');
				g.datagrid('load',{q:value});
			},
			onSelect: function(index,row){
				var codigo = row.productseasoncode;
				console.log(codigo);
			}
		});
		

		$("#tblSizeQty").propertygrid({
			url: 'Purchasing/getSizeGrid',
			method:'get',
			showGroup: true,
			scrollbarSize: 0,
			toolbar: '#toolbarsize',
			columns:[[
				{field:'name',title:'Size',width:100,resizable:true},
				{field:'value',title:'Qty',width:100,resizable:false}
			]]
		});


		$('#addStyle').textbox({validType: "maxLength[7]"});
		$('#addsuppitemreffcode').textbox({validType: "maxLength[36]"});
		$('#addfabricdesc').textbox({validType: "maxLength[36]"});
		$('#addstyledesc').textbox({validType: "maxLength[36]"});
		$('#addFabricCode').textbox({validType: "maxLength[1]"});
		$('#addSize').textbox({validType: "maxLength[3]"});
		
	});

	var repeat = function(str, count) {
	    var array = [];
	    for(var i = 0; i < count;)
	        array[i++] = str;
	    return array.join('');
	}

	$.extend($.fn.validatebox.defaults.rules,{
		exists:{
			validator:function(value,param){
				var cc = $(param[0]);
				var v = cc.combobox('getValue');
				var rows = cc.combobox('getData');
				for(var i=0; i<rows.length; i++){
					if (rows[i].id == v){return true}
				}
				return false;
			},
			message:'The entered value does not exists.'
		}
	});

	

	$.extend($.fn.textbox.defaults.rules, {
		maxLength: { 
			validator: function(value, param){
				//console.log(value.length);
				return value.length <= param[0];
			},
			message: 'Maximum characters allowed only {0}'
		}
	});


	function submitForm(){

		// hitung size yg di input
		calculateSize();

		var str = $( "#frmAddItem" ).serialize();
		var index = $('#dg').datagrid('getRows').length;
		index = (index == undefined) ? 0 : index;
		var label = $('#addLabel').combogrid('grid').datagrid('getSelected');
		var cate = $('#addCate').combogrid('grid').datagrid('getSelected');
		var color = $('#addColorCode').combogrid('grid').datagrid('getSelected');
		var fabric = $('#addFabricCode').textbox("getText");
		var gender = $('#addSex').combogrid('grid').datagrid('getSelected');
		var supplier_ref = $('#addsuppitemreffcode').textbox('getText');
		var supplier_color = $('#addsuppliercolor').textbox('getText');
		var fabric_dec = $('#addfabricdesc').textbox('getText');
		var style_desc = $('#addstyledesc').textbox('getText');
		var origin = $('#addcountryorigin').textbox('getText');
		
		var ucost = $('#addcost').numberbox('getValue');
		var sugestedrp = $('#sugestedrp').numberbox('getValue');
		sugestedrp = (sugestedrp == '') ? 0 : sugestedrp;
		var style = $('#addStyle').textbox('getText');
		if(style == "")
		{
			$.messager.alert('Failed',"Please input the STYLE !!",'error');
			return;
		}

		if(style.length != 7)
		{
			$.messager.alert('Failed',"Please input the STYLE !!",'error');
			return;
		}

		var season = $('#season').combogrid('grid').datagrid('getSelected');
		
		if(season == null || season.productseasoncode == "")
		{
			$.messager.alert('Failed',"Please input the SEASON!!",'error');
			return;
		}
		var kurs = $('#exchangerate').textbox('getText');
		
		if(kurs == "")
		{
			$.messager.alert('Failed',"Please select Currency",'error');
			return;
		}

		if(ucost == "")
		{
			$.messager.alert('Failed',"Please input FOB",'error');
			return;
		}
		
		if(items_size_qty.length <= 0)
		{
			$.messager.alert('Failed',"Please Input SIZE and QTY!",'error');
			return;
		}


		var uprice = (parseFloat(ucost) * parseFloat(kurs));
		var barcode = season.productseasoncode + style + color.colorcode + fabric;
		$('#dg').datagrid('insertRow',{
			index: index,
			row:{
				label:label.productgroupcode,
				style:style,
				color:color.colorcode,
				gender:gender.sexcode,
				cate:cate.productcategorycode,
				barcode:barcode,
				description:style_desc,
				qty:sumQty(),
				unitcost:ucost,
				unitprice:uprice
			}
		});
		
		$.post( "purchasing/saveDraftPoitems", 
			{csrf_name:csrf, adata:JSON.stringify(items_size_qty), 
			itemcode:barcode,
			label:label.productgroupcode,
			cate:cate.productcategorycode,
			gender:gender.sexcode,
			color:color.colorcode,
			fabric:fabric,
			supplier_reff:supplier_ref,
			style:style,
			supplier_color:supplier_color,
			fabric_dec:fabric_dec,
			style_desc:style_desc,
			origin:origin,
			cost:ucost,
			sugestedrp:sugestedrp,
			season:season.productseasoncode,
			exchangerate:kurs,
			currency: ''
			},
			function( data ) {
				items_size_qty = new Array();
				$('#tblSizeQty').propertygrid('loadData', []);
				csrf = data.csrf_name;
				
		}, "json");

		

	}

	function sumQty () {

		var propertyData = $('#tblSizeQty').propertygrid('getData');
		var propRows = propertyData.rows;
		var sum = 0;
		$.each(propRows, function( index, value ) {
			sum+=parseFloat(value.value);
		});

		return sum;
	}

	

	function calculateSize()
	{
		
		
		var propertyData = $('#tblSizeQty').propertygrid('getData');
		var propRows = propertyData.rows;
		var totalqty = 0;
		$.each(propRows, function( index, value ) {
			if(value.value != null || value.value != "")
			{
				if(parseFloat(value.value) > 0)
				{
					var grid = {};
					grid['size'] = value.name;
					grid['qty'] = value.value;
					totalqty += parseFloat(value.value);
					items_size_qty[items_size_qty.length] = grid;
				}
				
			}
		});
		return totalqty;
		console.log(items_size_qty);

	}


	function calculateSizeWithid(rowid)
	{
		console.log(rowid);
		var propertyData = $('#tblSizeQty').propertygrid('getData');
		var propRows = propertyData.rows;
		var totalqty = 0;
		var tmpArray = new Array();
		$.each(propRows, function( index, value ) {
			if(value.value != null || value.value != "")
			{
				if(parseFloat(value.value) > 0)
				{
					var grid = {};
					grid['size'] = value.name;
					grid['qty'] = value.value;
					totalqty += parseFloat(value.value);
					tmpArray[tmpArray.length] = grid;
				}
			}
		});
		items_size_wid[rowid] = tmpArray;
		console.log(items_size_wid);
		return totalqty;
		
	}




	function refreshAndClear()
	{
		openUrl("<?php echo base_url()?>index.php/purchasing/createpo/refreshpo");
	}

	function doCreate()
	{
		
		$.messager.confirm('Confirm','Please make sure to Updated before save?',function(r){
            if (r){
                var ponum = $('#ponum').textbox("getText");
				var supplierName = $('#suppliername').combogrid('grid').datagrid('getSelected');
				var suppReffnum = $('#supp_reff_num').textbox("getText");
				var currency = $('#currency').combobox('getText');
				var season = $('#season').combogrid('grid').datagrid('getSelected');
				var supplierCode = $('#suppliercode').textbox("getText");
				var description = $('#description').textbox("getText");
				var exchangerate = $('#exchangerate').textbox('getText');
				var type = $('#potype').combobox('getText');
				var division = $('#division').textbox('getText');
				var buyer = $('#buyer').textbox('getText');
				var brandcode = $('#brandcode').combogrid('grid').datagrid('getSelected');

				var win = $.messager.progress({
		                title:'Please waiting',
		                msg:'Sending data...'
		            });

					$.post( "purchasing/saveCreatingPo", 
					{
						csrf_name:csrf, 
						ponum:ponum,
						supplierId:supplierName.supplierid,
						supplierName:supplierName.suppliername,
						suppReffnum:suppReffnum,
						currency:currency,
						season:season.productseasoncode,
						supplierCode:supplierCode,
						description:description,
						exchangerate:exchangerate,
						type:type,
						division:division,
						shipmentReff:"",
						brandcodeid:brandcode.productbrandid,
						brandcode:brandcode.productbrandcode,
						buyer:buyer
					},
					function( data ) {
						$.messager.progress('close');
						var dlg = $.messager.confirm({
						    title: 'Successfully',
						    msg: 'Successfully you PO is '+data.po_number,
						    buttons:[{
						        text: 'OK',
						        onClick: function(){
						        	dlg.dialog('destroy')
						            openUrl("<?php echo base_url()?>index.php/purchasing/createpo/refreshpo");
						        }
						    }]
						});
							
				}, "json");
            }
        });

        
		
	}
	


	function endEditing(){
        if (editIndex == undefined){return true}
        if ($('#dg').datagrid('validateRow', editIndex)){
            $('#dg').datagrid('endEdit', editIndex);
            editIndex = undefined;
            return true;
        } else {
            return false;
        }
    }
    function onClickCell(index, field){
        if (editIndex != index){
            if (endEditing()){
                $('#dg').datagrid('selectRow', index).datagrid('beginEdit', index);
                var ed = $('#dg').datagrid('getEditor', {index:index,field:field});
                if (ed){
                    ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                }
                editIndex = index;
            } else {
                setTimeout(function(){
                    $('#dg').datagrid('selectRow', editIndex);
                },0);
            }
        }
    }


    function changeSizeGrid()
    {
    	var index = $('#dg').datagrid("getRowIndex", $("#dg").datagrid("getSelected"));
    	var row = $('#dg').datagrid('getRows')[index];

    	var url = "";
    	if(row.sizegrid == "" || row.sizegrid == undefined)
    		url = 'Purchasing/getSizeGridGroup';
    	else
    		url = 'Purchasing/getSizeGridGroup/'+row.sizegrid+"/true";

    	$('#dlgAddItem').dialog('open');
    	$('#sizecodegroup').combobox('setValue', row.sizegrid);
    	$('#sizecodegroup').combobox({
			panelHeight: 'auto',
			selectOnNavigation: false,
			valueField: 'id',
			textField: 'text',
			required: true,
			panelMaxHeight: 200,
			validType: 'exists["#sizecodegroup"]',
			onLoadSuccess: function () { },
			filter: function (q, row) {
				return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
			},
			url: url,
			method: 'get',
			onSelect: function(record)
			{

				size_code = record.text;
				$('#isizeCode').val(size_code);
				$("#tblSizeQty").propertygrid({url: 'Purchasing/getSizeGrid/'+size_code+"/"+row.rowid});
			}
		});

    }

    
	function addRows(index, countRows)
	{
		//var countRows = $('#colrow').numberbox('getText');
		$.post( "purchasing/addDraftItem", 
			{
				csrf_name:csrf, 
				rows:countRows
			},
			function( data ) {
				for (var i = 0; i < data.rows.length; i++) {
					$('#dg').datagrid('appendRow',data.rows[i]);
				}
				
				//$('#dg').datagrid('reload');
				//$('#dg').datagrid('beginEdit', index);
				csrf = data.csrf_name;
		}, "json");
	}

	function saveRows(index)
	{
		var row = $('#dg').datagrid('getRows')[index];
		$.post( "purchasing/saveRowsItems", 
			{
				csrf_name:csrf, 
				rows:JSON.stringify(row),
				index: index,
				rowid: row.rowid
			},
			function( data ) {
				items_size_qty = new Array();
				csrf = data.csrf_name;
				
		}, "json");
	}


	function saveSize()
	{
		var index = $('#dg').datagrid("getRowIndex", $("#dg").datagrid("getSelected"));
		var row = $('#dg').datagrid('getRows')[index];
		var totalqty = calculateSize();
		
		$.ajax({
			type: 'POST',
		  	url: "purchasing/addSizetoItems",
		  	data: {csrf_name:csrf, 
				adata:JSON.stringify(items_size_qty),
				rowid: row.rowid,
				sizecode:$('#isizeCode').val()
			},
		  	success: function( data ) {
		  		csrf = data.csrf_name;
				items_size_qty = new Array();
				$('#tblSizeQty').propertygrid('loadData', []);
				$('#dlgAddItem').dialog('close');
				
				var row = $('#dg').datagrid('getRows')[index];

				row.sizegrid = $('#isizeCode').val();
				row.totalqty = totalqty;
				
				
			},
			error: function() { //XMLHttpRequest, textStatus, errorThrown
				alert("error please reload");
			},
		  	dataType: "json"
		});
		
	}


	function reject(){
        $('#dg').datagrid('rejectChanges');
        editIndex = undefined;
    }

    function updateRows()
    {
    	if (endEditing()){
            $('#dg').datagrid('acceptChanges');
            var data = $('#dg').datagrid('getData');
			
			$.ajax({
				type: 'POST',
			  	url: "purchasing/updateRows",
			  	data: {csrf_name:csrf, 
					rows:JSON.stringify(data.rows),
					sizing:JSON.stringify(items_size_wid)
				},
			  	success: function( data ) {
					csrf = data.csrf_name;
					$('#dg').datagrid('reload');
					$.messager.alert('Successfully',"Session updated",'success');
				},
				error: function() { //XMLHttpRequest, textStatus, errorThrown
					alert("error please reload");
				},
			  	dataType: "json"
			});
        }
    }
</script>