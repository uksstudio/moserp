<?php

$ponum = strtoupper("PO".date('ym')."XXXX");
?>
<div style="padding:5px;margin-bottom:5px;text-align: right;">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" onClick="refreshAndClear()">Clear</a>
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onClick="doCreate()">Save</a>
	
</div>
<div class="easyui-panel" title="Head on Top" style="width:100%;height:35%;">
	<div class="easyui-tabs"  style="width:100%;height:100%">
		<div title="Purchase Request" style="padding:10px">
			<?php echo form_open("");?>
			<table style="width:100%;">
				<tr>
					<td style="text-align: right">Po Number</td>
					<td><input name="ponum" id="ponum" class="f1 easyui-textbox" value="<?php echo $ponum;?>" readonly></input></td>

					<td>Payment Term</td>
					<td>
						<input name="paymentterm" id="paymentterm" class="easyui-combogrid" data-options="required:true"></input></td>

					<td style="text-align: right">Type</td>
					<td>
						<select class="easyui-combobox" name="potype" id="potype">
							<option value="NON GOODS">Non Goods</option>
						</select>
					</td>
				</tr>

				<tr>
					<td style="text-align: right">Supplier Name</td>
					<td><input name="suppliername" id="suppliername" class="easyui-combogrid" data-options="required:true"></input></td>

					<td>Supplier Code</td>
					<td><input name="suppliercode" id="suppliercode" class="f1 easyui-textbox"></input></td>

					<td style="text-align: right">Division</td>
					<td><input name="division" id="division" class="f1 easyui-textbox" value="<?php echo $division;?>"></input></td>
				</tr>

				<tr>
					<td style="text-align: right">Supp Reff Number</td>
					<td><input name="supp_reff_num" id="supp_reff_num" class="f1 easyui-textbox"></input></td>

					<td>Description</td>
					<td><input name="description" id="description" class="f1 easyui-textbox"></input></td>

					<td style="text-align: right">Buyer</td>
					<td><input name="buyer" id="buyer" class="f1 easyui-textbox" value="<?php echo $this->session->userdata('name')?>"></input></td>
				</tr>
				
				<tr>
					<td style="text-align: right">Currency</td>
					<td>
					<input name="currency" id="currency" class="easyui-combobox" ></input>
					</td>

					<td>Exchange Rate</td>
					<td><input name="exchangerate" id="exchangerate" class="easyui-textbox"></input></td>

					<td style="text-align: right">Status PKP</td>
					<td>
						<select class="easyui-combobox" name="taxstatus" id="taxstatus">
							<option value="PKP">PKP</option>
							<option value="NPKP">NPKP</option>
						</select>
					</td>
				</tr>
			</table>
			<?php echo form_close();?>
		</div>
		<div title="Finance" style="padding:10px">
			
		</div>
		<div title="Address" style="padding:10px">
			
		</div>
		
	</div>
</div>

<div style="margin-bottom:10px"></div>
<div class="easyui-panel" title="Head on Top" style="width:100%;height:55%;">
	<table id="dg"></table>
</div>
<div id="toolbar">
	Add <input name="colrow" id="colrow" class="f1 easyui-numberbox" style="width: 50px"></input> Row(s)
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="addRows();">Add Rows</a>
	| <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="updateRows();">Update</a>
	| <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-undo',plain:true" onclick="reject()">Reject</a>
	
</div>

<div id="dlgAddItem" 
	class="easyui-dialog" 
	title="Size Grid" 
	data-options="modal:true,closed:true,iconCls:'icon-save',footer:'#footerDialog'" style="width:300px;height:500px;">
	
	<?php echo form_open("",'id="frmAddItem"')?>
	<input type="hidden" name="isizeCode" id="isizeCode">
	<table class="easyui-propertygrid" id="tblSizeQty"></table>
	<?php echo form_close()?>
	
</div>

<div id="toolbarsize">
	<input id="sizecodegroup" style="width:50px;" class="easyui-combobox" name="sizecodegroup">
</div>


<div id="footerDialog" style="padding:5px;"><a class="easyui-linkbutton" data-options="iconCls:'icon-ok'" href="javascript:void(0)" onclick="saveSize()" style="width:80px">Ok</a></div>

<?php echo script_tag('includes/plugins/datagrid-cellediting.js');?>
<script type="text/javascript">
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var newdata = <?php echo $json_currency?>;
	var items_size_qty = new Array();
	var items_size_wid = [];
	var size_code = "";
	var Clickc = [];
	var editIndex = undefined;
	$(function () {
		
		$('#dg').datagrid({
			singleSelect:true,
			idField:'barcode',
			fit: true,
			rownumbers:true,
			toolbar:'#toolbar',
			url:'purchasing/getRequestPurchaseItems',
			method: 'get',
			showFooter: true,
            nowrap: true,
			onClickCell: onClickCell,
            //onEndEdit: onEndEdit,
			columns:[[
				{field:'itemcode',title:'Item Description',width:250, editor:{type:'textbox',options:{required: true}}},
				{field:'qty',title:'Qty',width:60, editor:{type:'numberbox',options:{required: true}}},
				{field:'uom',title:'UoM',width:60, editor:{type:'textbox'}},
				{field:'unitcost',title:'Unit Cost',width:180, editor:{type:'numberbox',options:{required: true}}},
				{field:'totalprice',title:'Total Price',width:180, editor:{type:'numberbox'}}
			]],
			onEndEdit:function(index,row){
	            //var style = $(this).datagrid('getEditor', {index: index,field: 'style'});
	            
	            console.log('onEndEdit');
	        },
	        onBeginEdit:function(rowIndex){
		        var editors = $('#dg').datagrid('getEditors', rowIndex);
		        var n1 = $(editors[0].target);
		        var n2 = $(editors[1].target);
		        var n3 = $(editors[2].target);
		        var n4 = $(editors[3].target);
		        var n5 = $(editors[4].target);
		        n2.add(n4).numberbox({
		            onChange:function(){
		                var cost = n2.numberbox('getValue')*n4.numberbox('getValue');
		                n5.numberbox('setValue',cost);
		            }
		        })
		    },
	        onBeforeEdit:function(index,row){
	            row.editing = true;
	            editIndex = index;
	            $(this).datagrid('refreshRow', index);
	            console.log('onBeforeEdit');
	        },
	        onAfterEdit:function(index,row){
	            row.editing = false;
	            $(this).datagrid('refreshRow', index);
	            console.log('onAfterEdit');
	        },
	        onCancelEdit:function(index,row){
	            $(this).datagrid('refreshRow', index);
	            console.log('onCancelEdit');
	        }
		});

		

		
		$('#currency').combobox({
			panelHeight: 'auto',
			selectOnNavigation: false,
			valueField: 'id',
			textField: 'text',
			editable: true,
			required: true,    
			validType: 'exists["#currency"]',
			onLoadSuccess: function () { },
			filter: function (q, row) {
				return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
			},
			data: newdata,
			onSelect: function(record)
			{
				if(record)
				{
					$('#exchangerate').textbox('setText', record.kurs);
				}
			}
		});

		$('#suppliername').combogrid({
			panelWidth:500,
			url: "purchasing/getSupplierInfo",
			idField:'supplierid',
			textField:'suppliername',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			columns:[[
				{field:'supplierid',title:'Supplier ID',width:60},
				{field:'suppliercode',title:'Supplier Code',width:80},
				{field:'suppliername',title:'Name',width:60}
			]],
			onChange:function(value){
				var g = $('#suppliername').combogrid('grid');
				g.datagrid('load',{assay_type_id:value});
				console.log("find id : "+value);
			},
			onSelect: function(index,row){
				var codigo = row.suppliercode;
				$('#suppliercode').textbox('setText', codigo);
				
			}
		});


		
		$('#paymentterm').combogrid({
			panelWidth:300,
			url: "purchasing/getPaymentterm",
			idField:'paymenttermcode',
			textField:'paymenttermcode',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			columns:[[
				{field:'paymenttermcode',title:'ID',width:40},
				{field:'paymenttermsdesc',title:'Description',width:80},
				{field:'indays',title:'indays',width:20}
			]],
			onChange:function(value){
				var g = $('#season').combogrid('grid');
				g.datagrid('load',{assay_type_id:value});
				console.log("find id : "+value);
			},
			onSelect: function(index,row){
				var codigo = row.productseasoncode;
				console.log(codigo);
			}
		});
		

		$("#tblSizeQty").propertygrid({
			url: 'Purchasing/getSizeGrid',
			method:'get',
			showGroup: true,
			scrollbarSize: 0,
			toolbar: '#toolbarsize',
			columns:[[
				{field:'name',title:'Size',width:100,resizable:true},
				{field:'value',title:'Qty',width:100,resizable:false}
			]]
		});


	});


	$.extend($.fn.validatebox.defaults.rules,{
		exists:{
			validator:function(value,param){
				var cc = $(param[0]);
				var v = cc.combobox('getValue');
				var rows = cc.combobox('getData');
				for(var i=0; i<rows.length; i++){
					if (rows[i].id == v){return true}
				}
				return false;
			},
			message:'The entered value does not exists.'
		}
	});

	

	$.extend($.fn.textbox.defaults.rules, {
		maxLength: { 
			validator: function(value, param){
				//console.log(value.length);
				return value.length <= param[0];
			},
			message: 'Maximum characters allowed only {0}'
		}
	});


	function submitForm(){

		// hitung size yg di input
		calculateSize();

		var str = $( "#frmAddItem" ).serialize();
		var index = $('#dg').datagrid('getRows').length;
		index = (index == undefined) ? 0 : index;
		var label = $('#addLabel').combogrid('grid').datagrid('getSelected');
		var cate = $('#addCate').combogrid('grid').datagrid('getSelected');
		var color = $('#addColorCode').combogrid('grid').datagrid('getSelected');
		var fabric = $('#addFabricCode').textbox("getText");
		var gender = $('#addSex').combogrid('grid').datagrid('getSelected');
		var supplier_ref = $('#addsuppitemreffcode').textbox('getText');
		var supplier_color = $('#addsuppliercolor').textbox('getText');
		var fabric_dec = $('#addfabricdesc').textbox('getText');
		var style_desc = $('#addstyledesc').textbox('getText');
		var origin = $('#addcountryorigin').textbox('getText');
		
		var ucost = $('#addcost').numberbox('getValue');
		var sugestedrp = $('#sugestedrp').numberbox('getValue');
		sugestedrp = (sugestedrp == '') ? 0 : sugestedrp;
		var style = $('#addStyle').textbox('getText');
		if(style == "")
		{
			$.messager.alert('Failed',"Please input the STYLE !!",'error');
			return;
		}

		if(style.length != 7)
		{
			$.messager.alert('Failed',"Please input the STYLE !!",'error');
			return;
		}

		var season = $('#season').combogrid('grid').datagrid('getSelected');
		
		if(season == null || season.productseasoncode == "")
		{
			$.messager.alert('Failed',"Please input the SEASON!!",'error');
			return;
		}
		var kurs = $('#exchangerate').textbox('getText');
		
		if(kurs == "")
		{
			$.messager.alert('Failed',"Please select Currency",'error');
			return;
		}

		if(ucost == "")
		{
			$.messager.alert('Failed',"Please input FOB",'error');
			return;
		}
		
		if(items_size_qty.length <= 0)
		{
			$.messager.alert('Failed',"Please Input SIZE and QTY!",'error');
			return;
		}


		var uprice = (parseFloat(ucost) * parseFloat(kurs));
		var barcode = season.productseasoncode + style + color.colorcode + fabric;
		$('#dg').datagrid('insertRow',{
			index: index,
			row:{
				label:label.productgroupcode,
				style:style,
				color:color.colorcode,
				gender:gender.sexcode,
				cate:cate.productcategorycode,
				barcode:barcode,
				description:style_desc,
				qty:sumQty(),
				unitcost:ucost,
				unitprice:uprice
			}
		});
		
		$.post( "purchasing/saveDraftPoitems", 
			{csrf_name:csrf, adata:JSON.stringify(items_size_qty), 
			itemcode:barcode,
			label:label.productgroupcode,
			cate:cate.productcategorycode,
			gender:gender.sexcode,
			color:color.colorcode,
			fabric:fabric,
			supplier_reff:supplier_ref,
			style:style,
			supplier_color:supplier_color,
			fabric_dec:fabric_dec,
			style_desc:style_desc,
			origin:origin,
			cost:ucost,
			sugestedrp:sugestedrp,
			season:season.productseasoncode,
			exchangerate:kurs,
			currency: ''
			},
			function( data ) {
				items_size_qty = new Array();
				$('#tblSizeQty').propertygrid('loadData', []);
				csrf = data.csrf_name;
				
		}, "json");

		

	}

	function sumQty () {

		var propertyData = $('#tblSizeQty').propertygrid('getData');
		var propRows = propertyData.rows;
		var sum = 0;
		$.each(propRows, function( index, value ) {
			sum+=parseFloat(value.value);
		});

		return sum;
	}

	

	function calculateSize()
	{
		
		
		var propertyData = $('#tblSizeQty').propertygrid('getData');
		var propRows = propertyData.rows;
		var totalqty = 0;
		$.each(propRows, function( index, value ) {
			if(value.value != null || value.value != "")
			{
				if(parseFloat(value.value) > 0)
				{
					var grid = {};
					grid['size'] = value.name;
					grid['qty'] = value.value;
					totalqty += parseFloat(value.value);
					items_size_qty[items_size_qty.length] = grid;
				}
				
			}
		});
		return totalqty;
		console.log(items_size_qty);

	}


	function calculateSizeWithid(rowid)
	{
		console.log(rowid);
		var propertyData = $('#tblSizeQty').propertygrid('getData');
		var propRows = propertyData.rows;
		var totalqty = 0;
		var tmpArray = new Array();
		$.each(propRows, function( index, value ) {
			if(value.value != null || value.value != "")
			{
				if(parseFloat(value.value) > 0)
				{
					var grid = {};
					grid['size'] = value.name;
					grid['qty'] = value.value;
					totalqty += parseFloat(value.value);
					tmpArray[tmpArray.length] = grid;
				}
			}
		});
		items_size_wid[rowid] = tmpArray;
		console.log(items_size_wid);
		return totalqty;
		
	}




	function refreshAndClear()
	{
		openUrl("<?php echo base_url()?>index.php/purchasing/purchaseRequest/refreshpo");
	}

	function doCreate()
	{
		var ponum = $('#ponum').textbox("getText");
		var supplierName = $('#suppliername').combogrid('grid').datagrid('getSelected');
		var suppReffnum = $('#supp_reff_num').textbox("getText");
		var currency = $('#currency').combobox('getText');
		var paymentterm = $('#paymentterm').combogrid('grid').datagrid('getSelected');
		var supplierCode = $('#suppliercode').textbox("getText");
		var description = $('#description').textbox("getText");
		var exchangerate = $('#exchangerate').textbox('getText');
		var type = $('#potype').combobox('getText');
		var division = $('#division').textbox('getText');
		var buyer = $('#buyer').textbox('getText');
		var taxstatus = $('#taxstatus').combobox('getText');

		var win = $.messager.progress({
                title:'Please waiting',
                msg:'Sending data...'
            });

			$.post( "purchasing/savePurchaseRequestCreatingPo", 
			{
				csrf_name:csrf, 
				ponum:ponum,
				supplierId:supplierName.supplierid,
				supplierName:supplierName.suppliername,
				suppReffnum:suppReffnum,
				currency:currency,
				paymentterm:paymentterm.paymenttermcode,
				supplierCode:supplierCode,
				description:description,
				exchangerate:exchangerate,
				type:type,
				division:division,
				shipmentReff:"",
				brandcodeid:brandcode.productbrandid,
				taxstatus:taxstatus,
				buyer:buyer
			},
			function( data ) {
				$.messager.progress('close');
				var dlg = $.messager.confirm({
				    title: 'Successfully',
				    msg: 'Successfully you PO is '+data.po_number,
				    buttons:[{
				        text: 'OK',
				        onClick: function(){
				        	dlg.dialog('destroy')
				            openUrl("<?php echo base_url()?>index.php/purchasing/createpo/refreshpo");
				        }
				    }]
				});
					
		}, "json");
	}
	


	function endEditing(){
        if (editIndex == undefined){return true}
        if ($('#dg').datagrid('validateRow', editIndex)){
            $('#dg').datagrid('endEdit', editIndex);
            editIndex = undefined;
            return true;
        } else {
            return false;
        }
    }
    function onClickCell(index, field){
        if (editIndex != index){
            if (endEditing()){
                $('#dg').datagrid('selectRow', index).datagrid('beginEdit', index);
                var ed = $('#dg').datagrid('getEditor', {index:index,field:field});
                if (ed){
                    ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                }
                editIndex = index;
            } else {
                setTimeout(function(){
                    $('#dg').datagrid('selectRow', editIndex);
                },0);
            }
        }
    }


    function changeSizeGrid()
    {
    	var index = $('#dg').datagrid("getRowIndex", $("#dg").datagrid("getSelected"));
    	var row = $('#dg').datagrid('getRows')[index];

    	var url = "";
    	if(row.sizegrid == "" || row.sizegrid == undefined)
    		url = 'Purchasing/getSizeGridGroup';
    	else
    		url = 'Purchasing/getSizeGridGroup/'+row.sizegrid+"/true";

    	$('#dlgAddItem').dialog('open');
    	$('#sizecodegroup').combobox('setValue', row.sizegrid);
    	$('#sizecodegroup').combobox({
			panelHeight: 'auto',
			selectOnNavigation: false,
			valueField: 'id',
			textField: 'text',
			required: true,
			panelMaxHeight: 200,
			validType: 'exists["#sizecodegroup"]',
			onLoadSuccess: function () { },
			filter: function (q, row) {
				return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
			},
			url: url,
			method: 'get',
			onSelect: function(record)
			{

				size_code = record.text;
				$('#isizeCode').val(size_code);
				$("#tblSizeQty").propertygrid({url: 'Purchasing/getSizeGrid/'+size_code+"/"+row.rowid});
			}
		});

    }

	function addRows()
	{
		var countRows = $('#colrow').numberbox('getText');
		$.post( "purchasing/addPurchaseRequestDraftItem", 
			{
				csrf_name:csrf, 
				rows:countRows
			},
			function( data ) {
				$('#dg').datagrid('reload');
				csrf = data.csrf_name;
		}, "json");
	}

	function saveRows(index)
	{
		var row = $('#dg').datagrid('getRows')[index];
		$.post( "purchasing/saveRowsItems", 
			{
				csrf_name:csrf, 
				rows:JSON.stringify(row),
				index: index,
				rowid: row.rowid
			},
			function( data ) {
				items_size_qty = new Array();
				csrf = data.csrf_name;
				
		}, "json");
	}


	function saveSize()
	{
		var index = $('#dg').datagrid("getRowIndex", $("#dg").datagrid("getSelected"));
		var row = $('#dg').datagrid('getRows')[index];
		var totalqty = calculateSize();
		
		$.ajax({
			type: 'POST',
		  	url: "purchasing/addSizetoItems",
		  	data: {csrf_name:csrf, 
				adata:JSON.stringify(items_size_qty),
				rowid: row.rowid,
				sizecode:$('#isizeCode').val()
			},
		  	success: function( data ) {
		  		csrf = data.csrf_name;
				items_size_qty = new Array();
				$('#tblSizeQty').propertygrid('loadData', []);
				$('#dlgAddItem').dialog('close');
				
				var row = $('#dg').datagrid('getRows')[index];

				row.sizegrid = $('#isizeCode').val();
				row.totalqty = totalqty;
				
				
			},
			error: function() { //XMLHttpRequest, textStatus, errorThrown
				alert("error please reload");
			},
		  	dataType: "json"
		});
		
	}


	function reject(){
        $('#dg').datagrid('rejectChanges');
        editIndex = undefined;
    }

    function updateRows()
    {
    	if (endEditing()){
            $('#dg').datagrid('acceptChanges');
            var data = $('#dg').datagrid('getData');
			console.log(items_size_wid);
			$.ajax({
				type: 'POST',
			  	url: "purchasing/updatePurchaseRequestRows",
			  	data: {csrf_name:csrf, 
					rows:JSON.stringify(data.rows),
					sizing:JSON.stringify(items_size_wid)
				},
			  	success: function( data ) {
					csrf = data.csrf_name;
					$('#dg').datagrid('reload');
				},
				error: function() { //XMLHttpRequest, textStatus, errorThrown
					alert("error please reload");
				},
			  	dataType: "json"
			});
        }
    }
</script>