<table id="dgmodul" class="easyui-datagrid"></table>

<div id="toolbar">
	<?php echo form_open();?>
	<table style="border: 0px solid black;">
		<tr>
			
        	<td style="">
				<input class="easyui-combobox" id="group" name="group" style="width:100%" data-options="label:'Group Menu:', labelPosition:'top'">
        	</td>
        	<td style="">
				<input class="easyui-combobox" id="parentid" name="parentid" style="width:100%" data-options="label:'Parent Menu:', labelPosition:'top'">
        	</td>
        	<td style="">
				<input class="easyui-combogrid" id="coa" name="coa" style="width:100%" data-options="label:'Account Code:', labelPosition:'top'">
        	</td>
        	<td style="">
				<input class="easyui-textbox" id="name" name="name" style="width:100%" data-options="label:'Name Of Menu:', labelPosition:'top'">
        	</td>
        	<td style="" valign="bottom">
				<a href="#" id="btnSave" class="easyui-linkbutton c2" style="width:120px">Save</a>
        	</td>
		</tr>
	</table>
	<?php echo form_close();?>
	<p>* Note : Please update on user roll access, after add finished</p>
</div>

<script type="text/javascript">
var strTanggal = "";
var csrf = '<?php echo $this->security->get_csrf_hash();?>';
var dataGroup = [{"modul":"paymentvoucher", "code":"PV"}, {"modul":"voucherReceive", "code":"RV"}];
$(function () {
	$('#dgmodul').datagrid({
		width:'100%',
		height:'100%',
		singleSelect:true,
		idField:'id',
		rownumbers:true,
		url:'admin/getKasMenu',
		method: 'get',
		title:'Add/Edit Kas Menu',
		toolbar:'#toolbar',
		columns:[[
			{field:'id',title:'ID',width:50},
			{field:'nama',title:'Name',width:100},
			{field:'parent',title:'Parent ID',width:100},
			{field:'urutan',title:'Urutan',width:50},
			{field:'akun',title:'Account',width:100}
		]]
	});


	$("#group").combobox({
		valueField:'modul',
        textField:'code',
        data:dataGroup,
        panelHeight:'auto',
        required:false,
        onSelect: function(rec){
            var url = 'admin/getParentId?code='+rec.code;
            $('#parentid').combobox('reload', url);
        }
	});

	$("#parentid").combobox({
		valueField:'id',
        textField:'nama',
        method:"get",
        panelHeight:'auto',
        required:false,
        onSelect: function(rec){
            //var url = 'admin/getParentId?code='+rec.code;
            //$('#parentid').combobox('reload', url);
        }
	});

	$("#coa").combogrid({
		panelWidth:300,
		url: "Finance/getCoa",
		idField:'subcode',
		textField:'subcode',
		mode:'remote',
		fitColumns:true,
		groupField:'code',
		method: 'get',
		columns:[[
			{field:'subcode',title:'Account',width:100},
			{field:'name',title:'Name',width:250}
		]],
		onSelect: function(index,record)
		{
			
		}
	});
});

$("#btnSave").on('click', function(){
	var csrf = $("input[name*='csrf_name']").val();

	var namemenu = $('#name').textbox('getText');

	if(namemenu == "")
	{
		$.messager.alert('Failed',"Please Input Menu Name!",'error');
		return;
	}

	var valgroup = $('#group').combobox('getValue');
	if(valgroup == "")
	{
		$.messager.alert('Failed',"Please select Group Menu!",'error');
		return;
	}

	var valparentid = $('#parentid').combobox('getValue');
	if(valparentid == "")
	{
		$.messager.alert('Failed',"Please select Parent Menu!",'error');
		return;
	}

	var coa = $('#coa').combogrid('grid').datagrid('getSelected');
	if(!coa)
	{
		$.messager.alert('Failed',"Please select COA!",'error');
		return;
	}

	var win = $.messager.progress({title:'Please waiting',msg:'Sending data...'});
	$.post( "Admin/addMenuKasModul", 
	{
		csrf_name:csrf,
		coa:coa.subcode,
		group:valgroup,
		parent:valparentid,
		menu:namemenu
	},"json")
	.done(
    	function(msg)
    	{
    		
    		var obj = jQuery.parseJSON( msg );
    		$.messager.progress('close');
    		
    		
    		$("input[name*='csrf_name']").val(obj.csrf_name);

			if(obj.status == 0)
			{
				$.messager.alert('Failed',obj.msg,'error');	
			}
			else
			{
				$.messager.alert('Successfully',obj.msg,'success');	
				$('#dgmodul').datagrid('reload');
			}

			
			console.log(obj);
     	}
     )
    .fail(function(xhr, status, error) {
        // error handling
        $.messager.progress('close');
        console.log(xhr.status);
        console.log(error);
        console.log(status);
        $.messager.alert('Failed',xhr.status + "("+error+")",'error');
    });
})

</script>
