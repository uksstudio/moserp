<ul id="tt" class="easyui-tree"></ul>


<div id="dlg" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
    <?php echo form_open("",'novalidate style="margin:0;padding:10px 30px" id="fm"');?>
        <div style="margin-bottom:10px">
            <input name="nama" class="easyui-textbox" required="true" label="Name:" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="url" class="easyui-textbox" required="true" label="Path Uri:" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="urutan" class="easyui-textbox" required="true" label="Sort:" style="width:100%">
        </div>
    </form>
</div>
<div id="dlg-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" style="width:90px">Save</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
</div>

<div id="mm" class="easyui-menu" style="width:120px;">
    <div onclick="append()" data-options="iconCls:'icon-add'">Add New</div>
    <div onclick="editModul()" data-options="iconCls:'icon-edit'">Update</div>
    
</div>
<script type="text/javascript">
var strTanggal = "";
var csrf = '<?php echo $this->security->get_csrf_hash();?>';
var url;
$(function () {
		$('#tt').tree({
		    dnd: true,
		    url: 'admin/getModul/0',
		    method: 'get',
		    animate:true,
		    onContextMenu: function(e,node){
                e.preventDefault();
                $(this).tree('select',node.target);
                $('#mm').menu('show',{
                    left: e.pageX,
                    top: e.pageY
                });
            },
		    onDrop: function (targetNode, source, point)
		    {
		    	var targetId = $(target).tree('getNode', targetNode).id;
		    	console.log(targetId);
		    }
		});
});

$('#fm').submit(function(event){
		event.preventDefault();
		$.ajax({
			 type: 'POST',
			 url: url, 
			 data: $(this).serialize()
		})
		.done(function(msg){
			var obj = jQuery.parseJSON( msg );
    		$("input[name*='csrf_name']").val(obj.csrf_name);
			if (!obj.status){
                $.messager.show({
                    title: 'Error',
                    msg: obj.msgErr
                });
            } else {
                $('#dlg').dialog('close');        // close the dialog
                openUrl("<?php echo base_url()?>index.php/admin/editMenumodul");
            }
			console.log(obj);
		})
		.fail(function(xhr, status, error) {
	        // error handling
	        console.log(xhr.status);
	        console.log(error);
	        console.log(status);
	        $.messager.alert('Failed',xhr.status + "("+error+")",'error');

		});
	});
    function saveUser(){
    	$("#fm").submit();
    }


function append(){
    var node = $('#tt').tree('getSelected');
    if(node)
	{
		csrf = $("input[name*='csrf_name']").val();
		$('#dlg').dialog('open').dialog('center').dialog('setTitle','Add New');
        $('#fm').form('clear');
        $("input[name*='csrf_name']").val(csrf);
        url = 'admin/updateModul/new/'+node.id;
	}
}

function editModul()
{
	var node = $('#tt').tree('getSelected');
	if(node)
	{
		console.log(node);
		$('#dlg').dialog('open').dialog('center').dialog('setTitle','Update');
        $('#fm').form('load',{nama:node.nama, url:node.url, urutan:node.urutan});
        url = 'admin/updateModul/update/'+node.id;
	}
	
}

</script>
