<table id="dgmodul" class="easyui-datagrid"></table>

 <div id="toolbar">
 	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">New</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Edit</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove</a>
</div>


<div id="dlg" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
    <?php echo form_open("",'novalidate style="margin:0;padding:10px 30px" id="fm"');?>
        <h3>EDC Machine (POS)</h3>
        <div style="margin-bottom:10px">
            <input name="edcname" class="easyui-textbox" required="true" label="EDC Name:" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="coacode" class="easyui-combogrid" id="coacode" required="true" label="COA :" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="commision" class="easyui-numberbox" precision="1" required="true" label="Comm:" style="width:100%">
        </div>
    </form>
</div>
<div id="dlg-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" style="width:90px">Save</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
</div>

<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	
	$(function () {
		$('#dgmodul').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'id',
			rownumbers:true,
			url:'admin/getEdcMachine',
			method: 'get',
			title:'Setup EDC',
			toolbar:'#toolbar',
			columns:[[
				{field:'id',title:'ID',width:50},
				{field:'edcname',title:'Name',width:100},
				{field:'coacode',title:'Akun',width:100},
				{field:'namaakun',title:'Chart Desc',width:300},
				{field:'commision',title:'Comm',width:50}
			]]
		});

		$("#coacode").combogrid({
			panelWidth:300,
			url: "Finance/getCoa",
			idField:'subcode',
			textField:'subcode',
			mode:'remote',
			fitColumns:true,
			groupField:'code',
			method: 'get',
			columns:[[
				{field:'subcode',title:'Account',width:100},
				{field:'name',title:'Name',width:250}
			]],
			onSelect: function(index,record)
			{
				
			}
		});
	});

	var url;
    function newUser(){
    	csrf = $("input[name*='csrf_name']").val();
        $('#dlg').dialog('open').dialog('center').dialog('setTitle','Add New');
        $('#fm').form('clear');
        $("input[name*='csrf_name']").val(csrf);
        url = 'Admin/addEdc/add';
    }
    function editUser(){
        var row = $('#dgmodul').datagrid('getSelected');
        if (row){
            $('#dlg').dialog('open').dialog('center').dialog('setTitle','Update');
            $('#fm').form('load',row);
            url = 'Admin/addEdc/update/'+row.id;
        }
    }

    $('#fm').submit(function(event){
		event.preventDefault();
		$.ajax({
			 type: 'POST',
			 url: url, 
			 data: $(this).serialize()
		})
		.done(function(msg){
			var obj = jQuery.parseJSON( msg );
    		$("input[name*='csrf_name']").val(obj.csrf_name);
			if (obj.status){
                $.messager.show({
                    title: 'Error',
                    msg: obj.errorMsg
                });
            } else {
                $('#dlg').dialog('close');        // close the dialog
                $('#dgmodul').datagrid('reload');    // reload the user data
            }
			console.log(obj);
		})
		.fail(function(xhr, status, error) {
	        // error handling
	        console.log(xhr.status);
	        console.log(error);
	        console.log(status);
	        $.messager.alert('Failed',xhr.status + "("+error+")",'error');

		});
	});
    function saveUser(){
    	$("#fm").submit();
    }
    function destroyUser(){
        var row = $('#dgmodul').datagrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to destroy this EDC?',function(r){
                if (r){
                    $.post('Admin/addEdc/delete/'+row.id,{id:row.id},function(result){
                        if (result.status){
                            $('#dgmodul').datagrid('reload');    // reload the user data
                        } else {
                            $.messager.show({    // show error message
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        }
                    },'json');
                }
            });
        }
    }

</script>