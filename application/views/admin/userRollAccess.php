<div class="easyui-layout" style="width:100%;height:100%;">
	<div data-options="region:'center',title:'Main Title'">
		<table id="dgmodul" class="easyui-datagrid"></table>
	</div>
	<div data-options="region:'east',split:true" title="Modul Access" style="width:200px;">
		<ul id="tt" class="easyui-tree" data-options="url:'admin/getModul/0',method:'get',animate:true,checkbox:true"></ul>
	</div>
</div>

<div id="toolbar">
	<table style="width: 100%; border: 0px solid black;">
		<tr>
			<td style="text-align: right;">
				<a id="btnSave" class="easyui-linkbutton"  data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'">Update</a>
        	</td>
		</tr>
	</table>
</div>

<script type="text/javascript">
var strTanggal = "";
var csrf = '<?php echo $this->security->get_csrf_hash();?>';

$(function () {
	$('#dgmodul').datagrid({
		width:'100%',
		height:'100%',
		singleSelect:true,
		idField:'id',
		rownumbers:true,
		url:'admin/getUserAkses',
		method: 'get',
		toolbar:'#toolbar',
		columns:[[
			{field:'name',title:'Name',width:100},
			{field:'username',title:'Username',width:100},
			{field:'password',title:'Password',width:100}
		]],
		onClickRow: function(index, row)
		{
			$('#tt').tree({url: 'admin/getModul/0/'+row.id, 'method':'get'});
		}
	});
});

$("#btnSave").on('click', function(){
	var row = $('#dgmodul').datagrid('getSelected');
	if(!row)
	{
		$.messager.alert('Failed',"Please select one row!",'error');
		return;
	}

	var nodes = $('#tt').tree('getChecked');
    var s = '';
    for(var i=0; i<nodes.length; i++){
        if (s != '') s += ',';
        s += nodes[i].id;
    }


	$.post( "Admin/updateOwnModul", 
		{
			csrf_name:csrf, 
			userid: row.id,
			sgridmodul: s
		},
		function( data ) {
			csrf = data.csrf_name;
			$.messager.alert('Failed',data.msgErr,'error');
			console.log(data);
	}, "json");
})

</script>
