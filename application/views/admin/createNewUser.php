<div class="easyui-panel" title="New User" style="width:100%;padding:30px 60px;">
    <?php echo form_open('admin/saveNewUser','id="ff"')?>
        <div style="margin-bottom:20px">
            <input class="easyui-textbox" name="name" style="width:40%" data-options="label:'Name:',required:true">
        </div>
        <div style="margin-bottom:20px">
            <input class="easyui-textbox" name="username" style="width:40%" data-options="label:'Username:',required:true">
        </div>
        <div style="margin-bottom:20px">
            <input class="easyui-textbox" name="password" style="width:40%" data-options="label:'Password:',required:true">
        </div>
        <div style="margin-bottom:20px">
            <input class="easyui-combogrid" name="division" id="division" style="width:40%" data-options="required:true">
        </div>
        <div style="margin-bottom:20px">
            <input class="easyui-combogrid" name="company" id="company" style="width:40%" data-options="required:true">
        </div>
    </form>
    <div style="text-align:left;padding:5px 0">
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">Submit</a>
    </div>
</div>

<script>

$(function(){
    
    $('#division').combogrid({
        panelWidth:300,
        url: "admin/getDivisionByComp",
        idField:'codid',
        textField:'divisioncode',
        mode:'remote',
        fitColumns:true,
        method: 'get',
        label: 'Division :',
        labelPosition: 'left',
        required: true,
        columns:[[
            {field:'codid',title:'ID',width:20},
            {field:'divisioncode',title:'Division Code',width:30},
            {field:'divisionname',title:'Division Name',width:100}
        ]],
        onChange:function(value){
            var g = $('#division').combogrid('grid');
            g.datagrid('load',{assay_type_id:value});
            console.log("find id : "+value);
        },
        onSelect: function(index,row){
            var codigo = row.labelcode;
            console.log(codigo);
        }
    });


    $('#company').combogrid({
        panelWidth:300,
        url: "admin/getCompany",
        idField:'compid',
        textField:'compname',
        mode:'remote',
        fitColumns:true,
        method: 'get',
        label: 'Company :',
        labelPosition: 'left',
        required: true,
        columns:[[
            {field:'compid',title:'ID',width:20},
            {field:'compname',title:'Location Code',width:30},
            {field:'compaddress',title:'Location Name',width:100}
        ]],
        onChange:function(value){
            var g = $('#company').combogrid('grid');
            g.datagrid('load',{assay_type_id:value});
            
        },
        onSelect: function(index,row){
            
        }
    });

});
function submitForm(){
    $('#ff').form('submit',{
        url:"<?php echo base_url()?>index.php/admin/saveNewUser",
        onSubmit:function(){
            return $(this).form('enableValidation').form('validate');
        },
        success:function(data){
            var json = $.parseJSON(data);
            $('input[name="csrf_name"]').val(json.csrf);
            if(json.status == 0)
            {
                $.messager.alert('Failed',json.msg,'error');
            }
            else
            {
                $.messager.alert('Success',"Successfully Created",'success');
                $('#ff').form('clear');
            }
            
        }
    }, "json");
}

</script>