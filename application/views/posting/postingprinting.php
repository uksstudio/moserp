<!DOCTYPE html>
<html>
<head>
	<?php echo link_tag('includes/printing/normalize.min.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/printing/paper.css','stylesheet','text/css');?>
	<style>
		@page { size: a4 }
		div.title_label
		{
			float: left;
			width: 350px;
			border: 0px solid gray;
			margin-bottom: 5px;
			background: #ffffff;
			font-size: 15px;
		}
		div.title_label span
		{
			width: 100px;
			display: inline-block;
			background: #efefef;
			padding: 5px
			
		}
	</style>
</head>
<body class="a4">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

  <section class="sheet padding-5mm">
  	<?php
  	$rh = $this->db->get_where('post_report_items_header', array('costing_number'=>$postingid))->row();
  	?>
    <table style="width: 100%">
    	<tr>
    		<td valign="top" style="width: 50%">
    			<p>PT KELAB 21 RETAIL</p>
    			<small>DIPO BUSINESS CENTER 9th Floor, Unit F-G<br>
				JL Gatot Subroto Kav 51-52 10260 Jakarta</small>		
    		</td>
    		<td valign="top">
    			<p>LAPORANG BARANG MASUK</p>
    			<div class="title_label"><span>Costing Reff</span> <?php echo $postingid?></div>
    			<div class="title_label"><span>Posting Date</span> <?php echo $rh->datetime?></div>
    		</td>
    	</tr>
    </table>

	<br>
	<table style="width: 100%;">
		<thead>
			<td style="background: #efefef;font-size: 15px;">No</td>
			<td style="background: #efefef;font-size: 15px;">Season</td>
			<td style="background: #efefef;font-size: 15px;">Label</td>
			<td style="background: #efefef;font-size: 15px;">Cate</td>			
			<td style="background: #efefef;font-size: 15px;">Item Code</td>
			<td style="background: #efefef;font-size: 15px;">Supplier Code</td>
			<td style="background: #efefef;font-size: 15px;">Gender</td>
			<td style="background: #efefef;font-size: 15px;">Color</td>
			<td style="background: #efefef;font-size: 15px;">Size</td>
			<td style="background: #efefef;font-size: 15px;">Qty</td>
			<td style="background: #efefef;font-size: 15px;">RSP</td>
			<td style="background: #efefef;font-size: 15px;">Total</td>
		</thead>
		<tbody>
			<?php
			$this->db->select('prid.*, pri.size, pri.sex, pri.descline2, (select productseasoncode from productseason where productseasonid=pri.productseason) as season, (select productcategorydesc from productcategory where productcategoryid=pri.productcategory) as cate , (select productgroupcode from productgroup where productgroupid=pri.productgroup) as label, pri.supplieritemcode');
			$this->db->from('post_report_items_detail prid');
			$this->db->join('productitem pri','pri.productitemid=prid.productitemid','left');
			$this->db->where('prid.po_number', $postingid);
			$rs = $this->db->get();
			$i=0;
			foreach ($rs->result() as $row) {
				$i++;
				echo '<tr>';
				echo '<td style="background: #ffffff;font-size: 15px;">'.$i.'</td>';
				echo '<td style="background: #ffffff;font-size: 15px;">'.$row->season.'</td>';
				echo '<td style="background: #ffffff;font-size: 15px;">'.$row->label.'</td>';
				echo '<td style="background: #ffffff;font-size: 15px;">'.$row->cate.'</td>';		
				echo '<td style="background: #ffffff;font-size: 15px;">'.$row->productcode.'</td>';
				echo '<td style="background: #ffffff;font-size: 15px;">'.$row->supplieritemcode.'</td>';
				echo '<td style="background: #ffffff;font-size: 15px;">'.$row->sex.'</td>';
				echo '<td style="background: #ffffff;font-size: 15px;">'.$row->descline2.'</td>';
				echo '<td style="background: #ffffff;font-size: 15px;">'.$row->size.'</td>';
				echo '<td style="background: #ffffff;font-size: 15px;">'.$row->qty.'</td>';
				echo '<td style="background: #ffffff;font-size: 15px;">'.number_format($row->retailprice).'</td>';
				echo '<td style="background: #ffffff;font-size: 15px;">'.number_format($row->retailprice * $row->qty).'</td>';
				echo '</tr>';
			}
			?>
		</tbody>
		
	</table>

  </section>

</body>
</html>

