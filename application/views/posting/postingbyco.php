<div class="easyui-panel" style="width:100%;padding:10px 10px;">
	<?php echo form_open('','id="frm"')?>
	<div style="margin-bottom:10px">
        <input class="easyui-textbox" name="costing_number" id="costing_number" style="width:20%">
    </div>
    <div style="margin-bottom:10px">
        <input class="easyui-combogrid" name="location" id="location" style="width:20%">
    </div>
	
	<input type="checkbox" id="receiveReport"  name="receiveReport" checked="true" class="easyui-checkbox"> Receive Report New Items

	<?php echo form_close()?>

	<div style="text-align:left;padding:5px 0">
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">Submit</a>

    </div>
</div>

<div style="margin:20px 0;"></div>
<div class="easyui-tabs" style="width: 100%; height: 60%">
	<div title="POSTING JOBS">
		<table id="dgpostingjob" class="easyui-datagrid"></table>
	</div>
	<div title="JOBS COMPLETES" >
		<table id="dgjobcomplete" class="easyui-datagrid"></table>
	</div>
</div>


<div id="tlbcomplete">
    <a id="btnPrint" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" onclick="printing()">Print</a>
</div>


<div id="dlgAddItem" 
	class="easyui-window" 
	title="Costing Number Grid" 
	data-options="modal:true,closed:true,footer:'#footerDialog'" style="width:400px;height:400px;">
	
	<table id="dgcostingnumber" class="easyui-datagrid"></table>
	
</div>

<div id="footerDialog" style="padding:5px;">
	<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'" href="javascript:void(0)" onclick="selectAndClose();" style="width:80px">Ok</a>
</div>

<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<script type="text/javascript">
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var base_url = "<? echo base_url()?>";

	$(document).keyup(function(event) {
	    // console.log(event.keyCode);
	    // open  pressing "F8"
	    if (event.keyCode === 115) { //F4
	        showDialog();
	    }
	    
	    console.log(event.keyCode);

	});

	$(function () {
		$('#costing_number').textbox({
			label:'Costing Number',
			labelPosition: 'top',
			buttonText:'SEARCH (F4)',
			required: true,
			onClickButton: function(value)
			{
				showDialog();
			}
		});

		$('#location').combogrid({
			panelWidth:300,
			url: "posting/getLocation",
			idField:'locationcode',
			textField:'locationcode',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			label: 'Location :',
			labelPosition: 'top',
			required: true,
			columns:[[
				{field:'locationid',title:'ID',width:20},
				{field:'locationcode',title:'Location Code',width:30},
				{field:'locationname',title:'Location Name',width:100}
			]],
			onChange:function(value){
				var g = $('#location').combogrid('grid');
				g.datagrid('load',{assay_type_id:value});
				console.log("find id : "+value);
			},
			onSelect: function(index,row){
				var codigo = row.labelcode;
				console.log(codigo);
			}
		});

		$('#dgpostingjob').datagrid({
			width:'100%',
			height:'10%',
			singleSelect:false,
			idField:'po_number',
			fit: true,
			rownumbers:true,
			url:'posting/getPostingJobs',
			method: 'get',
			columns:[[
				{field:'postingid',title:'Seq ID',width:80},
				{field:'issuedate',title:'Timestamp',width:120},
				{field:'posting_number',title:'Number',width:100},
				{field:'posting_status',title:'Status',width:70},
				{field:'remark',title:'Remark',width:200},
				{field:'costing_number',title:'Costing Number',width:100}
			]]
		});


		$('#dgjobcomplete').datagrid({
			width:'100%',
			height:'10%',
			singleSelect:true,
			idField:'po_number',
			fit: true,
			rownumbers:true,
			toolbar: '#tlbcomplete',
			url:'posting/getJobsComplete',
			method: 'get',
			columns:[[
				{field:'postingid',title:'Seq ID',width:80},
				{field:'issuedate',title:'Timestamp',width:120},
				{field:'posting_number',title:'Number',width:100},
				{field:'posting_status',title:'Status',width:70},
				{field:'remark',title:'Remark',width:200},
				{field:'costing_number',title:'Costing Number',width:100},
				{field:'timeprocess',title:'TProcess',width:80}
			]]
		});

		

	});

	function showDialog()
	{
		$('#dlgAddItem').dialog({
			closed:false,
			iconCls:'icon-list-m1-edit',
			title:'&nbsp;Detail Costing Items',
			onLoad:function(){
				$('#dgcostingnumber').datagrid('reload');
				
			}
		});

		$('#dgcostingnumber').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'costingnumber',
			fit: true,
			url:'Posting/getPostingnumber',
			fitColumns:false,
			method: 'get',
			columns:[[
				{field:'costingnumber',title:'Costing Num',width:120},
				{field:'pibnumber',title:'PIB Num',width:120},
				{field:'awbnumber',title:'AWB Number',width:120},
				{field:'currency',title:'Currendy',width:120},
				{field:'exchangerate',title:'Rate',width:120},
				{field:'prepareduser',title:'User',width:120}
			]]
		});
	}

	function printing()
	{

		var row = $('#dgjobcomplete').datagrid('getSelected');
		if(row)
		{
			$("#btnPrint").printPage({
				url:'posting/printPosting/'+row.costing_number,
				attr: 'href',
				message:"Your document is being created"
			});
		}
	}
	

	function selectAndClose()
	{
		var row = $('#dgcostingnumber').datagrid('getSelected');
		if(row)
		{
			$('#costing_number').textbox('setText', row.costingnumber);		
		}

		$('#dlgAddItem').dialog('close');
	}

	function submitForm()
	{
		var conum = $('#costing_number').textbox('getText');
		var locationCode = $('#location').combogrid('getText');
		var receiveReport = $("#receiveReport").attr("checked") ? true : false;

		if(conum == '')
		{
			$.messager.alert('Error','COsting Number not set.', 'info');
			return;
		}

		if(locationCode == '')
		{
			$.messager.alert('Error','Location not set.', 'info');
			return;
		}
		$.messager.confirm('Confirm','Are you sure you want to do this POSTING PROCESS?',function(r){
			if (r){
				$.post('Posting/doPostingByCo',{csrf_name:csrf, costingnumber:conum, locationcode:locationCode, receiveReport:receiveReport},function(result){
					if (result.status){
						csrf = result.csrf_name;
						$('#dgpostingjob').datagrid('reload');    // reload the user data
					} else {
						$.messager.show({    // show error message
							title: 'Error',
							msg: result.errorMsg
						});
					}
				},'json');
			}
		});

	}
</script>
