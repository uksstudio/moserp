
<table id="dg" class="easyui-datagrid"></table>

<div id="tb" style="padding:2px 5px;">
	<?php echo form_open(); ?>
	
	<table width="100%">
		<tr>
			<td style="width:50%;">
				<div >
					<div style="margin-bottom:10px">
			            <input class="easyui-combobox" id="locationcode" name="locationcode" style="width:40%" data-options="label:'Store Location:',required:true">
			        </div>
					<div style="margin-bottom:10px">
			            <input class="easyui-combogrid" id="supplier" name="supplier" style="width:40%" data-options="label:'Supplier:',required:true">
			        </div>
			        <div style="margin-bottom:10px">
			            <input class="easyui-textbox" id="payto" name="payto" style="width:100%" data-options="label:'<?php echo ($type=='RV') ? 'Receive From:': 'Pay To:'?>',required:true">
			        </div>
			        <div style="margin-bottom:10px">
			            <input class="easyui-textbox" id="remark" name="remark" style="width:100%" data-options="label:'Remark:'">
			        </div>
				</div>
			</td>
			<td style="text-align: right;" valign="top">
				<div style="margin-bottom:10px">
		            <input class="easyui-textbox" id="akun" name="akun" style="width:40%" value="<?php echo $noakun?>" data-options="label:'No Account:',required:true" readonly="true">
		        </div>
		        <div style="margin-bottom:10px">
		            <input class="easyui-textbox" id="novoucher" name="novoucher" value="<?php echo $invoice?>" style="width:40%" data-options="label:'No Voucher:'" readonly="true">
		        </div>
		        <div style="margin-bottom:10px">
		            <!-- <input class="easyui-textbox" id="tanggal" name="tanggal" value="<?php echo date('Y-m-d H:i:s')?>" style="width:40%" data-options="label:'Date Time:'" readonly="true"> -->
		            <input class="easyui-datetimebox" id="tanggal" name="tanggal" style="width:40%" data-options="label:'Date Time:', showSeconds:true, required:true" >
		        </div>
		        <div style="width:40%;float: right;">
					<input class="easyui-searchbox" id="srcpv" data-options="prompt:'Search PV',searcher:doSearch" style="width:100%">
				</div>
			</td>
		</tr>
	</table>
	
	<?php echo form_close(); ?>
</div>

<div id="ft" style="padding:10px 10px;">
	<div style="margin-bottom:10px">
        <input class="easyui-textbox" id="nocek" name="nocek" style="width:30%" data-options="label:'No Cek/Bilyet:'">
    </div>
    
    <a href="#" id="btnSave" class="easyui-linkbutton" data-options="iconCls:'icon-save'">Save</a>
</div>

<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var editIndex = undefined;
	var type = "<?php echo $type;?>";
	var this_url = "<?php echo $url;?>";
	var url = "";
	var coaaccount = "<?php echo $noakun?>";
	var data = [
				{"description":"", "amount":0, "storelocation":"", "akun":"", "namaakun":""},
				{"description":"", "amount":0, "storelocation":"", "akun":"", "namaakun":""},
				{"description":"", "amount":0, "storelocation":"", "akun":"", "namaakun":""},
				{"description":"", "amount":0, "storelocation":"", "akun":"", "namaakun":""},
				{"description":"", "amount":0, "storelocation":"", "akun":"", "namaakun":""},
				{"description":"", "amount":0, "storelocation":"", "akun":"", "namaakun":""},
				{"description":"", "amount":0, "storelocation":"", "akun":"", "namaakun":""},
				{"description":"", "amount":0, "storelocation":"", "akun":"", "namaakun":""},
				{"description":"", "amount":0, "storelocation":"", "akun":"", "namaakun":""},
				{"description":"", "amount":0, "storelocation":"", "akun":"", "namaakun":""}
				];

	$(function () {

		$('#dg').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'po_number',
			fit: true,
			title:'<?php echo $title;?>',
			rownumbers:true,
			toolbar:'#tb',
			footer:'#ft',
			showFooter:true,
			data: data,
			onClickCell: onClickCell,
			columns:[[
				{field:'description',title:'Description',width:450, editor:{type:'textbox'}},
				{field:'amount',title:'Amount',width:100, align:'right', editor:{type:'numberbox',options:{precision:2}}},
				{field:'storelocation',title:'Location',width:100, editor:{type:'combobox',
					options:{
						selectOnNavigation: false,
						valueField: 'locationcode',
						textField: 'locationcode',
						url:'Finance/getLocationCode',
						method: 'get',
						editable: true,
						onLoadSuccess: function () { },
						filter: function (q, row) {
							var opts = $(this).combobox('options');
							return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) >= 0;
						},
						onSelect: function(record)
						{
							
						}
					}}
				},
				{field:'akun',title:'Account No',width:100, editor:{type:'combogrid',
					options:{
						panelWidth:300,
						url: "Finance/getCoa",
						idField:'subcode',
						textField:'subcode',
						mode:'remote',
						fitColumns:true,
						groupField:'code',
						method: 'get',
						columns:[[
							{field:'subcode',title:'Account',width:100},
							{field:'name',title:'Name',width:250}
						]],
						onChange:function(value){
							var g = $(this).combogrid('grid');
							g.datagrid('load',{q:value});
							console.log("find id : "+value);
						},
						onSelect: function(index,record)
						{
							var dg = $('#dg');
							var dgindex = dg.datagrid('getRowIndex', dg.datagrid('getSelected'));
							var ed = dg.datagrid('getEditor',{index:dgindex,field:'namaakun'});
							$(ed.target).textbox('setText', record.name);
						}
					}}
				},
				{field:'namaakun',title:'Account Name',width:250, editor:{type:'textbox'}}
			]],
			onEndEdit:function(index,row){
	            var ed = $(this).datagrid('getEditor', {
	                index: index,
	                field: 'namaakun'
	            });
	            row.namaakun = $(ed.target).textbox('getText');
	            reloadFooter();
	        },
			onBeginEdit:function(index,row){
		        var dg = $(this);
		        //var ed = dg.datagrid('getEditors',index)[0];
		        var ed = dg.datagrid('getEditors',index);

		        if (!ed){return;}
		        
		        var ttext = $(ed[0].target);
		        var tnum = $(ed[1].target);
		        tnum = tnum.numberbox('textbox');
		        if (ttext.hasClass('textbox-f')){
		            ttext = ttext.textbox('textbox');
		        }

		        ttext.bind('keydown', function(e){
		            if (e.keyCode == 13){
		                dg.datagrid('endEdit', index);
		            } else if (e.keyCode == 27){
		                dg.datagrid('cancelEdit', index);
		            }
		        })

		        tnum.bind('keydown', function(e){
		            if (e.keyCode == 13){
		                dg.datagrid('endEdit', index);
		            } else if (e.keyCode == 27){
		                dg.datagrid('cancelEdit', index);
		            }
		            
		        })
		    }
		});

		reloadFooter();

		$("#locationcode").combobox({
			selectOnNavigation: false,
			valueField: 'locationcode',
			textField: 'locationcode',
			url:'Finance/getLocationCode',
			method: 'get',
			editable: true,
			required: true,    
			validType: 'exists["#currency"]',
			onLoadSuccess: function () { },
			filter: function (q, row) {
				var opts = $(this).combobox('options');
				//return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
				return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) >= 0;
			},
			onSelect: function(record)
			{
				
			}
		});

		$('#supplier').combogrid({
			panelWidth:500,
			url: "purchasing/getSupplierInfo",
			idField:'supplierid',
			textField:'suppliername',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			columns:[[
				{field:'supplierid',title:'Supplier ID',width:30},
				{field:'suppliercode',title:'Supplier Code',width:60},
				{field:'suppliername',title:'Name',width:100}
			]],
			onChange:function(value){
				var g = $('#supplier').combogrid('grid');
				g.datagrid('load',{q:value});
			},
			onSelect: function(index,row){
				var codigo = row.suppliercode;
				
			}
		});
		
	});
	

	function reloadFooter()
	{
		var dg = $('#dg'); 		
		var data = dg.datagrid('getData');  
		dg.datagrid('loadData', data);  
		var total = 0;
		var rows = dg.datagrid('getRows');  
		for(var i=0; i<rows.length; i++){  
			total+= parseFloat(rows[i].amount);
		}  
		dg.datagrid('reloadFooter', [{description:'Total',amount:total, akun:"<?php echo $noakun?>", namaakun:"<?php echo $coaname;?>"}]);
	}
	


	$.fn.datetimebox.defaults.parser = function(s){
	    if (!s){return new Date();}
	    var dt = s.split(' ');
	    var dd = dt[0].split('-');
	    var date = new Date(dd[0],parseInt(dd[1])-1,dd[2]);
	    if (dt.length>1){
	        dd = dt[1].split(':');
	        date.setHours(dd[0]);
	        date.setMinutes(dd[1]);
	        date.getSeconds(dd[2])
	    }
	    return date;
	}
	$.fn.datetimebox.defaults.formatter = function(date){
	    var y = date.getFullYear();
	    var m = date.getMonth()+1;
	    var d = date.getDate();
	    return y+'-'+_ff(m)+'-'+_ff(d) + ' ' + _ff(date.getHours())+':'+_ff(date.getMinutes()) +':'+_ff(date.getSeconds());
	}

	

	function _ff(v) {
        return (v < 10 ? "0" : "") + v;
    }

	function onClickCell(index, field){
        if (editIndex != index){
            if (endEditing()){
                $('#dg').datagrid('selectRow', index).datagrid('beginEdit', index);
                var ed = $('#dg').datagrid('getEditor', {index:index,field:field});
                if (ed){
                    ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                }
                editIndex = index;
            } else {
                setTimeout(function(){
                    $('#dg').datagrid('selectRow', editIndex);
                },0);
            }
        }
    }

    function endEditing(){
        if (editIndex == undefined){return true}
        if ($('#dg').datagrid('validateRow', editIndex)){
            $('#dg').datagrid('endEdit', editIndex);
            editIndex = undefined;
            return true;
        } else {
            return false;
        }
    }

    $("#btnSave").on('click', function()
    {
    	endEditing();
    	
    	var csrf = $("input[name*='csrf_name']").val();
    	var supplier = $('#supplier').combogrid('grid').datagrid('getSelected');
    	var bilyet = $('#nocek').textbox('getText');
    	var payto = $('#payto').textbox('getText');
    	var remark = $('#remark').textbox('getText');
    	var account = $('#akun').textbox('getText');
    	var voucher = $('#novoucher').textbox('getText');
    	var srcpvno = $('#srcpv').searchbox('getValue');
    	//var tanggal = $('#tanggal').textbox('getText');
    	var tanggal = $('#tanggal').datetimebox('getValue');
    	var storecode = $('#locationcode').combobox('getText');
    	var data = $('#dg').datagrid('getData');

    	
    	if(supplier == null)
    	{
    		$.messager.alert('Failed',"Please select the supplier.",'error');
			return;
    	}

    	if(payto == "")
    	{
    		$.messager.alert('Failed',"Please input payment to.",'error');
			return;
    	}

    	if(tanggal == "")
    	{
    		$.messager.alert('Failed',"Please input timestamp.",'error');
			return;
    	}

    	var propRows = data.rows;
    	var tmpArray = new Array();
    	$.each(propRows, function( index, value ) {
			if(value.description != null && value.description != "")
			{
				var grid = {};
				grid['description'] = value.description;
				grid['amount'] = value.amount;
				grid['akun'] = value.akun;
				grid['storelocation'] = value.storelocation;
				tmpArray[tmpArray.length] = grid;
			}
		});
		console.log(tmpArray);
		var win = $.messager.progress({title:'Please waiting',msg:'Sending data...'});
		if(url == "") url = "Finance/savePaymentVoucherMulti";

		$.post( url, 
		{
			csrf_name:csrf,
			rows:JSON.stringify(tmpArray),
			supplierid:supplier.supplierid,
			remark:remark,
			payto:payto,
			bilyet:bilyet,
			noaccount:account,
			novoucher:voucher,
			tanggal:tanggal,
			type:type,
			locationcode:storecode,
			srcpv:srcpvno
		},"json")
		.done(
		    	function(msg)
		    	{
		    		console.log(msg);
		    		var obj = jQuery.parseJSON( msg );
		    		$.messager.progress('close');
		    		$('#dg').datagrid('reload');
		    		openUrl("<?php echo base_url()?>index.php/"+this_url);
					if(obj.status == 0)
					{
						$.messager.alert('Failed',obj.msg,'error');	
					}
					else
					{
						$.messager.confirm('Transfer Out', 'Your DO Number is '+obj.po_number+', want to print press OK!', function(r){
			                if (r){
			                    loadPrintDocument(this, "finance/printPaymentVoucher/"+obj.po_number+"/"+type+"/"+account);
			                }
			            });
						
					}

					
					console.log(obj);
		     	}
		     )
		    .fail(function(xhr, status, error) {
		        // error handling
		        $.messager.progress('close');
		        console.log(xhr.status);
		        console.log(error);
		        console.log(status);
		        $.messager.alert('Failed',xhr.status + "("+error+")",'error');
		    });
    });


    function doSearch(value){
        $.get( "Accounting/getPaymentVoucher/"+ value +"/"+ type + "/" + coaaccount, function( data ) {
			var obj = jQuery.parseJSON( data );
			if(obj.detail.length <= 0)
			{
				$.messager.alert('Failed',"Your PV not found.",'error');
				return;
			}
			$("#supplier").textbox('setText', obj.kas.supplierid);
			$("#payto").textbox('setText', obj.kas.paymentto);
			$("#nocek").textbox('setText', obj.kas.ceknumber);
			$("#remark").textbox('setText', obj.kas.remark);
			$("#akun").textbox('setText', obj.kas.coaccount);
			$("#locationcode").combobox('setText', obj.kas.locationcode);
			$("#tanggal").datetimebox('setValue', obj.kas.createdate);
			
			if(obj.detail.length > 0)
			{
				url = "Finance/savePaymentVoucherMulti/update";
			}
			$('#dg').datagrid({
				data: obj.detail
			});
			reloadFooter();
		});
    }
</script>