<table id="dg" class="easyui-datagrid"></table>

<div id="tb" style="padding:2px 5px;">
	<?php echo form_open(); ?>
	<table width="100%">
		<tr>
			<td style="width:50%;" valign="top">
				<div >
					
			        <div style="margin-bottom:10px">
			            <input class="easyui-textbox" id="payto" name="payto" style="width:100%" data-options="label:'Pay To:'" readonly="true">
			        </div>
			        <div style="margin-bottom:10px">
			            <input class="easyui-textbox" id="remark" name="remark" style="width:100%" data-options="label:'Remark:'">
			        </div>
				</div>
			</td>
			<td style="text-align: right;" valign="top">
				<div style="margin-bottom:10px">
		            <input class="easyui-combobox" id="novoucher" name="novoucher" value="" style="width:40%" data-options="label:'No Voucher:', required: true">
		        </div>
				<div style="margin-bottom:10px">
		            <input class="easyui-textbox" id="junomor" name="junomor" style="width:40%" value="<?php echo $invoice?>" data-options="label:'No :',required:true" readonly="true">
		        </div>

				<div style="margin-bottom:10px">
		            <input class="easyui-textbox" id="akun" name="akun" style="width:40%" value="<?php echo KAS_RUPIAH?>" data-options="label:'No Account:',required:true" readonly="true">
		        </div>
		        
		        <div style="margin-bottom:10px">
		            
		            <input class="easyui-datetimebox" id="tanggal" name="tanggal" style="width:40%" value="<?php echo date('Y-m-d H:i:s')?>" data-options="label:'Date Time:'" style="width:40%;">
		        </div>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>
</div>

<div id="ft" style="padding:10px 10px;">
	<div style="margin-bottom:20px">
        <input class="easyui-textbox" id="nocek" name="nocek" style="width:30%" data-options="label:'No Cek/Bilyet:'" readonly="true">
    </div>
    
    <a href="#" id="btnSave" class="easyui-linkbutton" data-options="iconCls:'icon-save'">POSTING</a>
</div>

<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var this_url = '<?php echo $url;?>';
	var type = "<?php echo $type;?>";
	var novoc = "<?php echo $invoice?>";
	var editIndex = undefined;
	var data = [
				{"description":"", "type":"", "amount":0, "akun":"", "namaakun":"", "id":""},
				{"description":"", "type":"", "amount":0, "akun":"", "namaakun":"", "id":""},
				{"description":"", "type":"", "amount":0, "akun":"", "namaakun":"", "id":""},
				{"description":"", "type":"", "amount":0, "akun":"", "namaakun":"", "id":""},
				{"description":"", "type":"", "amount":0, "akun":"", "namaakun":"", "id":""},
				{"description":"", "type":"", "amount":0, "akun":"", "namaakun":"", "id":""},
				{"description":"", "type":"", "amount":0, "akun":"", "namaakun":"", "id":""},
				{"description":"", "type":"", "amount":0, "akun":"", "namaakun":"", "id":""},
				{"description":"", "type":"", "amount":0, "akun":"", "namaakun":"", "id":""},
				{"description":"", "type":"", "amount":0, "akun":"", "namaakun":"", "id":""}
				];
	var typedata = [{"type":"D"},{"type":"K"}];
	$(function () {

		$('#dg').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'po_number',
			fit: true,
			title:'<?php echo $title;?>',
			rownumbers:true,
			toolbar:'#tb',
			footer:'#ft',
			data: data,
			onClickCell: onClickCell,
			columns:[[
				{field:'description',title:'Description',width:450},
				{field:'type',title:'Type',width:80},
				{field:'amount',title:'Amount',width:100},
				{field:'akun',title:'Account No',width:100, editor:{type:'combogrid',
					options:{
						panelWidth:300,
						url: "Finance/getCoa",
						idField:'subcode',
						textField:'subcode',
						mode:'remote',
						fitColumns:true,
						groupField:'code',
						method: 'get',
						columns:[[
							{field:'subcode',title:'Account',width:100},
							{field:'name',title:'Name',width:250}
						]],
						onSelect: function(index,record)
						{
							var dg = $('#dg');
							var dgindex = dg.datagrid('getRowIndex', dg.datagrid('getSelected'));
							var ed = dg.datagrid('getEditor',{index:dgindex,field:'namaakun'});
							$(ed.target).textbox('setText', record.name);
						}
					}}
				},
				{field:'namaakun',title:'Account Name',width:250, editor:{type:'textbox'}}
			]],
			onEndEdit:function(index,row){
	            var ed = $(this).datagrid('getEditor', {
	                index: index,
	                field: 'namaakun'
	            });
	            row.namaakun = $(ed.target).textbox('getText');
	        },
			onBeginEdit:function(index,row){
		        var dg = $(this);
		        //var ed = dg.datagrid('getEditors',index)[0];
		        var ed = dg.datagrid('getEditors',index);

		        if (!ed){return;}
		        
		        var ttext = $(ed[0].target);
		        var tnum = $(ed[1].target);
		        tnum = tnum.numberbox('textbox');
		        if (ttext.hasClass('textbox-f')){
		            ttext = ttext.textbox('textbox');
		        }

		        ttext.bind('keydown', function(e){
		            if (e.keyCode == 13){
		                dg.datagrid('endEdit', index);
		            } else if (e.keyCode == 27){
		                dg.datagrid('cancelEdit', index);
		            }
		        })

		        tnum.bind('keydown', function(e){
		            if (e.keyCode == 13){
		                dg.datagrid('endEdit', index);
		            } else if (e.keyCode == 27){
		                dg.datagrid('cancelEdit', index);
		            }
		        })
		    }
		});

		
		$("#novoucher").combobox({
			valueField:'canumber',
            textField:'canumber',
            url:'Finance/getCashadvanceVoucher',
            method:'get',
            panelHeight:'auto',
            required:false
		});
		
	});

	function onClickCell(index, field){
        if (editIndex != index){
            if (endEditing()){
                $('#dg').datagrid('selectRow', index).datagrid('beginEdit', index);
                var ed = $('#dg').datagrid('getEditor', {index:index,field:field});
                if (ed){
                    ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                }
                editIndex = index;
            } else {
                setTimeout(function(){
                    $('#dg').datagrid('selectRow', editIndex);
                },0);
            }
        }
    }

    function endEditing(){
        if (editIndex == undefined){return true}
        if ($('#dg').datagrid('validateRow', editIndex)){
            $('#dg').datagrid('endEdit', editIndex);
            editIndex = undefined;
            return true;
        } else {
            return false;
        }
    }

    $('#novoucher').combobox({
		inputEvents:$.extend({},$.fn.combobox.defaults.inputEvents,{
			keyup:function(e){
				if(e.keyCode == 13)
				{
					var canom = $("#novoucher").combobox('getText');
					$.get( "Finance/getCashadvanceVoucherDetail?canumber="+canom, function( data ) {
						var obj = jQuery.parseJSON( data );
						if(obj.detail.length <= 0)
						{
							$.messager.alert('Failed',"Your PV not found.",'error');
							return;
						}
						
						$("#payto").textbox('setText', obj.kas.issuename);
						//$("#nocek").textbox('setText', obj.kas.ceknumber);
						$("#remark").textbox('setText', obj.kas.remark);
						
						$('#dg').datagrid({
							data: obj.detail
						});
					});
				}
			}
		})

		
	});

    function resetAllfield()
    {
    	$('#nocek').textbox('setText', "");
    	$('#payto').textbox('setText', "");
    	$('#remark').textbox('setText', "");
    	$('#novoucher').textbox('setText', "");
    	$('#akun').textbox('setText', "");
    	$('#tanggal').textbox('setText', "<?php echo date('Y-m-d H:i:s')?>");
    	$('#dg').datagrid('loadData', []);  
    	
    }

    $("#btnSave").on('click', function()
    {
    	endEditing();

    	var csrf = $("input[name*='csrf_name']").val();
    	var bilyet = $('#nocek').textbox('getText');
    	var account = $('#akun').textbox('getText');
    	var payto = $('#payto').textbox('getText');
    	var remark = $('#remark').textbox('getText');
    	var voucher = $('#novoucher').textbox('getText');

    	var tanggal = $('#tanggal').textbox('getText');
    	var data = $('#dg').datagrid('getData');

    	
    	if(payto == "")
    	{
    		$.messager.alert('Failed',"Please input payment to.",'error');
			return;
    	}
    	var propRows = data.rows;
    	var tmpArray = new Array();
    	$.each(propRows, function( index, value ) {
			if(value.description != null && value.description != "")
			{
				var grid = {};
				grid['description'] = value.description;
				grid['amount'] = value.amount;
				grid['akun'] = value.akun;
				grid['id'] = value.id;
				grid['type'] = value.type;
				tmpArray[tmpArray.length] = grid;
			}
		});
		console.log(tmpArray);
		var win = $.messager.progress({title:'Please waiting',msg:'Sending data...'});
		$.post( "Finance/payCashAdvance", 
		{
			csrf_name:csrf,
			rows:JSON.stringify(tmpArray),
			remark:remark,
			payto:payto,
			bilyet:bilyet,
			noaccount:account,
			canumber:voucher,
			novoucher:novoc,
			type:type,
			tanggal:tanggal
		},"json")
		.done(
		    	function(msg)
		    	{
		    		console.log(msg);
		    		var obj = jQuery.parseJSON( msg );
		    		$.messager.progress('close');
		    		$('#dg').datagrid('reload');
		    		$("input[name*='csrf_name']").val(obj.csrf_name);
					if(obj.status == 0)
					{
						$.messager.alert('Failed',obj.msg,'error');	
					}
					else
					{
						//resetAllfield();
						openUrl("<?php echo base_url()?>index.php/"+this_url);
						$.messager.alert('Success',obj.msg,'success');	
						
					}

					console.log(obj);
		     	}
		     )
		    .fail(function(xhr, status, error) {
		        // error handling
		        $.messager.progress('close');
		        console.log(xhr.status);
		        console.log(error);
		        console.log(status);
		        $.messager.alert('Failed',xhr.status + "("+error+")",'error');
		    });
    });
</script>