
<table id="dgcostingreview" class="easyui-datagrid"></table>


<div id="dlgItemCosting" 
	class="easyui-window"
	data-options="modal:true,closed:true,iconCls:'icon-save',footer:'#footerDialogCosting'" style="width:95%;height:100%;">
	
	<table id="dgdetailcostingreview" class="easyui-datagrid"></table>
	
</div>

<div id="footerDialogCosting" style="padding:5px;">
	<a class="easyui-linkbutton" data-options="iconCls:'icon-print'" href="javascript:void(0)" onclick="printing()" style="width:80px">Print</a>

	<label style="text-align: right; float: right; font-size: 10px;">READY TO POSTING</label>
</div>

<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<?php echo script_tag('includes/plugins/datagrid-cellediting.js');?>

<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var newdata = <?php echo $json_currency?>;
	var cosnumber = '';
	$(function () {
		$('#dgcostingreview').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:false,
			idField:'po_number',
			fit: true,
			rownumbers:true,
			url:'costing/getCostingReview',
			method: 'get',
			showFooter: true,
			view: detailview,
			columns:[[
				
				{field:'costingnumber',title:'Number',width:80},
				{field:'costingstatus',title:'Status',width:80},
				{field:'costingdate',title:'Costing Date',width:70},
				{field:'pibnumber',title:'PIB Number',width:70},
				{field:'awbnumber',title:'AWB',width:70},
				{field:'invoicenumber',title:'Invoice',width:70},
				{field:'spcpdate',title:'SPCS Date',width:70},
				{field:'currency',title:'Curr',width:50},
				{field:'exchangerate',title:'Rate',width:50},
				{field:'importduty',title:'IMP Tax',width:90},
				{field:'luxurytax',title:'LUX Tax',width:90},
				{field:'freight',title:'Freight',width:90},
				{field:'handling',title:'Handling',width:90},
				{field:'other',title:'Other',width:90},
				{field:'total',title:'Total',width:100},
				{field:'action',title:'Action',width:110,align:'left',
					formatter:function(value,row,index){
						var costingnumber = "'"+row.costingnumber+"'";
						if(row.costingstatus == 'COSTING')
						{
							var e = '<a href="javascript:void(0)" id="btnDetail'+row.costingnumber+'" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="showDetail('+costingnumber+')">DETAIL</a> ';
							var f = '<a href="javascript:void(0)" id="" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="showDetail('+costingnumber+')">REVISE</a> ';
							var g = '<a href="javascript:void(0)" id="" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="costingVoid('+costingnumber+')">VOID</a> ';
							if(row.isposting)
								return e ;
							else
								return e+" | "+g;
						}
						else if(row.costingstatus == 'VOID')
						{
							return '';
						}
						else
						{
							var e = '<a href="javascript:void(0)" class="easyui-linkbutton" onclick="showDetail('+costingnumber+')">Detail</a> ';
							return e;
						}
					}
				}
			]],
			rowStyler: function(index,row){
                if (row.costingstatus == 'VOID'){
                    return 'background-color:#6293BB;color:#fff;font-weight:bold;';
                }
            },
			detailFormatter:function(index,row){
				return '<div style="padding:2px;position:relative;"><table class="ddv"></table></div>';
			},
			onExpandRow: function(index,row){
				var ddv = $(this).datagrid('getRowDetail',index).find('table.ddv');
				ddv.datagrid({
					url:'costing/getPoByCostingNumber/'+row.costingnumber ,
					method: 'get',
					fitColumns:true,
					singleSelect:true,
					rownumbers:true,
					loadMsg:'',
					height:'auto',
					showFooter: true,
					columns:[[
						{field:'po_number',title:'PO Number',width:100},
						{field:'qty',title:'Tot Qty',width:100},
						{field:'cost',title:'Total U-FOB',width:100},
						{field:'price',title:'Total U-Price',width:100},
						{field:'pbtax',title:'PB Tax',width:100},
						{field:'luxtax',title:'LUX Tax',width:100},
						{field:'freight',title:'Freight',width:100},
						{field:'handling',title:'Handling',width:100},
						{field:'insurance',title:'Insurance',width:100},
						{field:'total',title:'Total',width:100}
					]],

					onResize:function(){
						$('#dgcostingreview').datagrid('fixDetailRowHeight',index);
					},
					onLoadSuccess:function(){
						setTimeout(function(){
							$('#dgcostingreview').datagrid('fixDetailRowHeight',index);
						},0);
					}
				});
				$('#dgcostingreview').datagrid('fixDetailRowHeight',index);
			},
			onCheck:onCheck,
			onUncheck:onUncheck,
			onCheckAll:onCheckAll,
			onUncheckAll:onUncheckAll,
		});

		



	});


	function myformatter(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	}
	function myparser(s){
		if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
			return new Date(y,m-1,d);
		} else {
			return new Date();
		}
	}
	
	function onSelect(date){
		strTanggal = date.getFullYear()+"-"+("0" + (date.getMonth() + 1).toString()).substr(-2)+"-"+("0" + date.getDate().toString()).substr(-2);
	}
	var checkedRows = [];
	function onCheck(index,row){
		for(var i=0; i<checkedRows.length; i++){
			if (checkedRows[i].po_number == row.po_number)
			{
				row.chk = 'Y';
				return;
			}
		}
		row.chk = 'Y';
		checkedRows.push(row);
		
		console.log("onCheck:"+row.po_number);
	}
	function onUncheck(index,row){
		for(var i=0; i<checkedRows.length; i++){
			if (checkedRows[i].po_number == row.po_number){
				row.chk = 'N';
				checkedRows.splice(i,1);
				return;
			}
		}
		row.chk = 'N';
		console.log("onUncheck:"+index);
	}


	function onCheckAll(row){
		for(var i=0; i<row.length; i++){
			row[i].chk = 'Y';
		}
	}
	function onUncheckAll(row){
		for(var i=0; i<row.length; i++){
			row[i].chk = 'N';
		}
		
	}

	
	
	function parseFloat2Decimals(value) {
		return parseFloat(parseFloat(value).toFixed(2));
	}

	function roundUp(num, precision) {
	  precision = Math.pow(10, precision)
	  return Math.ceil(num * precision) / precision
	}

	function costingVoid(costingnumber)
	{
		$.messager.confirm('Confirm','Are you sure you want to VOID this COSTING PROCESS?',function(r){
			if (r){
				$.post('Costing/voidCosting',{csrf_name:csrf, costingnumber:costingnumber},function(result){
					if (result.status){
						csrf = result.csrf_name;
						$('#dgcostingreview').datagrid('reload');    // reload the user data
					} else {
						$.messager.show({    // show error message
							title: 'Error',
							msg: result.errorMsg
						});
					}
				},'json');
			}
		});
	}

	function showDetail(costingnumber)
	{
		var Clickc = [];
		cosnumber = costingnumber;
		$('#dlgItemCosting').dialog({
			closed:false,
			iconCls:'icon-list-m1-edit',
			title:'&nbsp;Detail Costing Items',
			onLoad:function(){
				$('#dgdetailcostingreview').datagrid('reload');
				
			}
		});

		$('#dgdetailcostingreview').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'po_number',
			fit: true,
			url:'Costing/detailCostingItem/' + costingnumber,
			method: 'get',
			rownumbers:true,
			showFooter: true,
			clickToEdit: true,
			dblclickToEdit: false,
			columns:[[
				{field:'barcode',title:'Style Number',width:100},
				{field:'size',title:'Size',width:50},
				{field:'qty',title:'QTY',width:50},
				{field:'unitcost',title:'Unit Price FOB',width:100},
				{field:'unitprice',title:'Retail Price',width:100},
				{field:'biaya',title:'Freight&Duty',width:100},
				{field:'cogs',title:'Landed Cost',width:100},
				{field:'markup',title:'Markup',width:50},
				{field:'retail_price',title:'Unit Price',width:100},
				{field:'margin',title:'Margin %',width:60},
				{field:'sugested_retail_price',title:'Sug R-Price',width:100,styler: 
					function(value,row,index)
					{
						return 'color:red;';
					}
				},
				
				{field:'retailprice_adjustment',title:'New Price',width:100, editor:{type:'numberbox',options:{precision:2}}, styler: 
					function(value,row,index)
					{
						return 'background-color:#ffee00;color:red;';
					}
				},
				{field:'marginafteradjust',title:'Margin %',width:60, styler: 
					function(value,row,index)
					{
						return 'background-color:#efefef;color:red;';
					}
				},
				{field:'varafteradjust',title:'Var %',width:60, styler: 
					function(value,row,index)
					{
						return 'background-color:#efefef;color:red;';
					}
				},
				{field:'retail_price_roundup',title:'Retail Price',width:100},
				{field:'total_retail_price',title:'Total R-Price',width:100},

			]]
		});

		$('#dgdetailcostingreview')
			.datagrid('enableCellEditing')
			.datagrid({
				onCellEdit: function (index, field)
				{
					var dg = $(this);
					var ed = dg.datagrid('getEditor', {index:index,field:field});
		            if (ed){
		              Clickc.index = index; // get the row that was clicked
		      	      Clickc.field = field; // get the field which was clicked
		      	      Clickc.value = $(ed.target).val();  //Get cell current value
		            }
				},
				onEndEdit:function(index){
					var dg = $(this);
					//console.log(index +" == "+ Clickc.field );
			      	var ed = dg.datagrid('getEditor', {index:index,field:Clickc.field});
		            if (ed){
		            	var row = dg.datagrid('getRows')[index];
		            	//console.log(Clickc.value +" C=R :"+row.retailprice_adjustment);
		            	if(row.isposting)
		            	{
		            		$.messager.show({    // show error message
								title: 'Error',
								msg: "Can\'t update cause data Already POST"
							});
							row.retailprice_adjustment = Clickc.value;
		            		return;
		            	}
		            	if(row.retailprice_adjustment != Clickc.value)
		            	{
		            		// do anything with php
		            		
		            		var cogs = parse2float(row.cogs);
		            		var margin = ((row.retailprice_adjustment - cogs) / row.retailprice_adjustment) * 100;
		            		var sugestprice = parse2float(row.sugested_retail_price);
		            		var retail_price = parse2float(row.retail_price);
		            		var varsugestprice = ((row.retailprice_adjustment - retail_price) / row.retailprice_adjustment) * 100;
		            		//console.log(cogs +" rp "+row.retailprice_adjustment);
		            		row.marginafteradjust = number_format(margin, 2, '.', ',');
		            		row.varafteradjust = number_format(varsugestprice, 2, '.', ',');
		            		//calculate retail price
		            		row.retail_price_roundup = number_format(row.retailprice_adjustment, 2, '.', ',');
		            		//calculate total retailprice
		            		row.total_retail_price = number_format((row.retailprice_adjustment * row.qty), 2, '.', ',');
		            		// calculate footer total retail price
		            		reloadFooterDetail();

		            		// update to db
		            		$.post('Costing/adjustRetailPrice',{csrf_name:csrf, detail_id:row.detail_id, fromprice:retail_price, newprice:row.retailprice_adjustment},function(result){
								if (result.status){
									csrf = result.csrf_name;
									$('#dgcostingreview').datagrid('reload');    // reload the user data
								} else {
									$.messager.show({    // show error message
										title: 'Error',
										msg: result.errorMsg
									});
								}
							},'json');
							
		            	}
		              
		            }
				}
			})
			.datagrid('gotoCell', {
            	index: 0,
            	field: 'barcode'
        });
	}

	/*
	array(
				'qty'=>$sum_qty,
				'unitcost'=>number_format($sum_unitcost,2,'.',','),
				'biaya'=>number_format($sum_biaya,2,'.',','), 
				'unitprice'=>number_format($sum_unitprice,2,'.',','),
				'cogs'=>number_format($sum_unitprice,2,'.',','),
				'retail_price'=>number_format($sum_retailprice,2,'.',','),
				'margin'=>0,
				'retail_price_roundup'=>0,
				'total_retail_price'=>number_format($sum_retailpriceround,2,'.',',')
	*/
	function reloadFooterDetail()
	{
		var data = $('#dgdetailcostingreview').datagrid('getData');
		var sum = 0;
		var sum_retail_price_roundup = 0;
		for (i = 0; i < data.rows.length; i++) {
			console.log(data.rows[i].total_retail_price);
			sum_retail_price_roundup+=parse2float(data.rows[i].retail_price_roundup);
		    sum+=parse2float(data.rows[i].total_retail_price);
		}
		
		var rows = $('#dgdetailcostingreview').datagrid('getFooterRows');
		rows[0]['total_retail_price'] = number_format(sum, 2, '.', ',');
		rows[0]['retail_price_roundup'] = number_format(sum_retail_price_roundup, 2, '.', ',');
		$('#dgdetailcostingreview').datagrid('reloadFooter');

		//return sum;
	}

	function doPosting()
	{
		$.messager.confirm('Confirm','Are you sure you want to POST this COSTING to INVENTORY?',function(r){
			if (r){
				$.post('Costing/doPosting',{csrf_name:csrf, costingnumber:costingnumber},function(result){
					if (result.status){
						csrf = result.csrf_name;
						$('#dgcostingreview').datagrid('reload');    // reload the user data
					} else {
						$.messager.show({    // show error message
							title: 'Error',
							msg: result.errorMsg
						});
					}
				},'json');
			}
		});
	}

	function printing()
	{

		loadPrintDocument(this, "Costing/printngkhr/"+cosnumber);
	}
</script>