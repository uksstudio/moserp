<div style="padding:5px;margin-bottom:5px;text-align: right;">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" onClick="calculate()">Calculate</a>
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onClick="doCreate()">Save</a>
	
</div>
<div class="easyui-layout" style="width:100%;height:90%;">
	<div data-options="region:'north'" style="height:50px; padding: 10px;">
		<input name="costingnumber" id="costingnumber" class="easyui-combogrid" data-options="label:'Costing Number'" style="width: 280px" ></input> 
	</div>
	<div id="p" data-options="region:'west'" style="width:25%;">
		<table id="dgcostingfield" class="easyui-datagrid"></table>
	</div>
	<div data-options="region:'center'">
		<table id="dg" class="easyui-datagrid"></table>
    </div>

<div>

<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var newdata = <?php echo $json_currency?>;
	$(function () {
		$("#dgcostingfield").propertygrid({
			url: 'Costing/costingfield',
			method:'get',
			width:'100%',
			height:'100%',
			showGroup: true,
			scrollbarSize: 0,
			columns:[[
				{field:'name',title:'Label',width:120,resizable:true},
				{field:'value',title:'Value (IDR)',width:80,resizable:false}
			]]
		});


		$('#dg').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:false,
			idField:'po_number',
			fit: true,
			rownumbers:true,
			url:'costing/getPoNewest',
			method: 'get',
			showFooter: true,
			columns:[[
				{field:'chk',title:'Invoice',width:100,checkbox:true},
				{field:'invoice',title:'Invoice',width:100},
				{field:'donumber',title:'Do Num',width:100},
				{field:'po_number',title:'PO Num',width:100},
				{field:'qty',title:'QTY',width:100},
				{field:'cost',title:'TOTAL FOB',width:100, formatter:
					function(val, row)
					{
						return number_format(val, 2, ".",",");
					}},
				{field:'price',title:'TOTAL Amt (IDR)',width:100, formatter:
					function(val, row)
					{
						return number_format(val, 2, ".",",");
					}
				},
				{field:'pbtax',title:'IMP Tax',width:100, formatter :
					function(val, row)
					{
						return number_format(val, 2, ".",",");
					}
				},
				{field:'luxtax',title:'LUX Tax',width:100, formatter :
					function(val, row)
					{
						return number_format(val, 2, ".",",");
					}
				},
				{field:'freight',title:'Freight',width:100, formatter: 
					function(val, row)
					{
						return number_format(val, 2, ".",",");
					}
				},
				{field:'handling',title:'Handling',width:100, formatter : 
					function(val, row)
					{
						return number_format(val, 2, ".",",");
					}
				},
				{field:'insurance',title:'Insurance',width:100, formatter :
					function(val, row)
					{
						return number_format(val, 2, ".",",");
					}
				},
				{field:'total',title:'Total',width:100, formatter : 
					function(val, row)
					{
						return number_format(val, 2, ".",",");
					}
				},
				{field:'avgcost',title:'A Cost',width:80, formatter:
					function(val, row)
					{
						return (val * 100);//number_format((val * 100), 2, ".",",");
					}
				}
			]],
			onCheck:onCheck,
			onUncheck:onUncheck,
			onCheckAll:onCheckAll,
			onUncheckAll:onUncheckAll,
		});


		$('#costingnumber').combogrid({
			panelWidth:300,
			url: "Warehouse/getCostingnumber",
			idField:'costingid',
			textField:'costingnumber',
			mode:'remote',
			fitColumns:true,
			method: 'get',
			columns:[[
				{field:'costingid',title:'ID',width:40},
				{field:'costingnumber',title:'Costing Number',width:90},
				{field:'exchangerate',title:'Exchangerate',width:80},
				{field:'invoicenumber',title:'Invoicenumber',width:80}
			]],
			onChange:function(value){
				//var g = $('#costingnumber').combogrid('grid');
				//g.datagrid('load',{assay_type_id:value});
				//console.log("find id : "+value);
			},
			onSelect: function(index,row){
				var codigo = row.costingid;
				$('#dgcostingfield').propertygrid({url: 'Costing/costingfield/'+codigo});
			}
		});
	});


	function myformatter(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	}
	function myparser(s){
		if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
			return new Date(y,m-1,d);
		} else {
			return new Date();
		}
	}
	
	function onSelect(date){
		strTanggal = date.getFullYear()+"-"+("0" + (date.getMonth() + 1).toString()).substr(-2)+"-"+("0" + date.getDate().toString()).substr(-2);
	}
	var checkedRows = [];
	function onCheck(index,row){
		for(var i=0; i<checkedRows.length; i++){
			if (checkedRows[i].po_number == row.po_number)
			{
				row.chk = 'Y';
				return;
			}
		}
		row.chk = 'Y';
		checkedRows.push(row);
		reloadFooter();
		
	}
	function onUncheck(index,row){
		for(var i=0; i<checkedRows.length; i++){
			if (checkedRows[i].po_number == row.po_number){
				row.chk = 'N';
				checkedRows.splice(i,1);
				return;
			}
		}
		row.chk = 'N';
		reloadFooter();
		
	}


	function onCheckAll(row){
		for(var i=0; i<row.length; i++){
			row[i].chk = 'Y';
		}
		reloadFooter();
	}
	function onUncheckAll(row){
		for(var i=0; i<row.length; i++){
			row[i].chk = 'N';
		}
		reloadFooter();
	}


	function reloadFooter()
	{
		var data = $('#dg').datagrid('getData');
		var sum_cost = 0;
		var sum_retail_price = 0;
		for (i = 0; i < data.rows.length; i++) {
			if(data.rows[i].chk == 'Y')
			{
		    	sum_cost += parseFloat(data.rows[i].cost.replace(/[^0-9-.]/g, ''));
		    	sum_retail_price += parseFloat(data.rows[i].price.replace(/[^0-9-.]/g, ''));
			}
		}
		
		var rows = $('#dg').datagrid('getFooterRows');
		rows[0]['cost'] = number_format(sum_cost,2,'.','');
		rows[0]['price'] = number_format(sum_retail_price,2,'.','');
		$('#dg').datagrid('reloadFooter');
	}

	
	function calculate()
	{
		
		var total_imp = 0;
		var total_lux = 0;
		var total_freit = 0;
		var total_handling = 0;
		var total_other = 0;

		var propertyData = $('#dgcostingfield').propertygrid('getData');
		var propRows = propertyData.rows;
		
		$.each(propRows, function( index, value ) {
			if(value.group == 'Import Duty')
			{
				total_imp += (value.value == null) ? 0 : parseFloat(value.value);
			}
			if(value.group == 'Luxury Tax')
			{
				total_lux += (value.value == null) ? 0 : parseFloat(value.value);
			}
			if(value.group == 'Freight')
			{
				total_freit += (value.value == null) ? 0 : parseFloat(value.value);
			}
			if(value.group == 'Handling')
			{
				total_handling += (value.value == null) ? 0 : parseFloat(value.value);
			}
			if(value.group == 'Other')
			{
				total_other += (value.value == null) ? 0 : parseFloat(value.value);
			}
		});


		var data = $('#dg').datagrid('getData');
		var rows = data.rows;
		var footer = data.footer;
		var footer_cost = parseFloat(footer[0].cost.replace(/[^0-9-.]/g, ''));
		var footer_price = parseFloat(footer[0].price.replace(/[^0-9-.]/g, ''));

		var sum_imp_tax = 0;
		var sum_lux_tax = 0;
		var sum_freit_tax = 0;
		var sum_handling_tax = 0;
		var sum_insurance_tax = 0;
		var sum_total_aja = 0;

		for (i = 0; i < rows.length; i++) {
			var sum_footer_price = footer_price;
			var price = parseFloat(rows[i].price.replace(/[^0-9-.]/g, ''));
			var imp_tax = (price / sum_footer_price) * total_imp;
			var lux_tax = (price / sum_footer_price) * total_lux;
			var freit_tax = (price / sum_footer_price) * total_freit;
			var handling_tax = (price / sum_footer_price) * total_handling;
			var insurance_tax = (price / sum_footer_price) * total_other;
			var total_aja = (imp_tax + lux_tax + freit_tax + handling_tax + insurance_tax);

			var avgcost = (total_aja / price);

			sum_imp_tax += parseFloat(imp_tax);
			sum_lux_tax += parseFloat(lux_tax);
			sum_freit_tax += parseFloat(freit_tax);
			sum_handling_tax += parseFloat(handling_tax);
			sum_insurance_tax += parseFloat(insurance_tax);
			sum_total_aja += parseFloat(total_aja);

			if(rows[i].chk == 'Y')
			{
				$('#dg').datagrid('updateRow',{
					index: i,
					row: {
						pbtax: roundUp(imp_tax, 2),
						luxtax: roundUp(lux_tax, 2),
						freight: roundUp(freit_tax, 2),
						handling: roundUp(handling_tax, 2),
						insurance: roundUp(insurance_tax, 2),
						total: roundUp(total_aja, 2),
						avgcost: avgcost
					}
				});
			}
		}

		$('#dg').datagrid('reloadFooter', [
				{
				cost:roundUp(footer_cost, 2),
				price:roundUp(footer_price, 2),
				pbtax: roundUp(sum_imp_tax, 2),
				luxtax: roundUp(sum_lux_tax, 2),
				freight: roundUp(sum_freit_tax, 2),
				handling: roundUp(sum_handling_tax, 2),
				insurance: roundUp(sum_insurance_tax, 2),
				total: roundUp(sum_total_aja, 2)}
			]);
	}

	

	var items_cosoting_field = new Array();
	var items_po_field = new Array();

	function doCreate()
	{
		
		var propertyData = $('#dgcostingfield').propertygrid('getData');
		var propRows = propertyData.rows;
		
		$.each(propRows, function( index, value ) {
			var grid = {};
			grid['groupname'] = value.group;
			grid['group'] = value.name;
			grid['amount'] = (value.value == null) ? 0 : parseFloat(value.value);
			items_cosoting_field[items_cosoting_field.length] = grid;
		});
		
		var data = $('#dg').datagrid('getData');
		var rows = data.rows;
		var totalChk = 0;
		for (i = 0; i < rows.length; i++) {
			var pogrid = {};
			if(rows[i].chk == 'Y')
			{
				var row = $('#dg').datagrid('getRows')[i];

				pogrid['po_id'] = row.id;
				pogrid['po_num'] = row.po_number;
				pogrid['total_biaya'] = row.total;
				pogrid['total_price'] = row.price;
				pogrid['total_qty'] = row.qty;
				items_po_field[items_po_field.length] = pogrid;
				totalChk++;
			}
		}
		
		if(totalChk == 0)
		{
			$.messager.alert('Failed',"Please Ceklis the row and calculate",'error');
			return;
		}
		
		var win = $.messager.progress({
			title:'Please waiting',
			msg:'Sending data...'
		});

		var costingnumber = $('#costingnumber').combogrid('grid').datagrid('getSelected');
		$.post( "Costing/saveCostingItems", 
			{
				csrf_name:csrf,
				costingid:costingnumber.costingid,
				costingnumber:costingnumber.costingnumber,
				costingField:JSON.stringify(items_cosoting_field), 
				poItems:JSON.stringify(items_po_field)
			},
			function( data ) {
				$('#dg').datagrid('reload');
				$.messager.progress('close');
				$.messager.alert('Success',"Costing Number " + data.co_number,'info');
		}, "json");
	}
</script>