<table id="dg" class="easyui-datagrid"></table>

<div id="tb" style="padding:2px 5px;">
	<?php echo form_open(); ?>
	
	<table width="100%">
		<tr>
			<td style="width:50%;">
				<div >
					<div style="margin-bottom:10px">
			            <input class="easyui-textbox" id="payto" name="payto" style="width:100%" data-options="label:'Pay To',required:true" value="<?php echo $this->session->userdata('name')?>" readonly="true">
			        </div>
			        <div style="margin-bottom:10px">
			            <input class="easyui-textbox" id="remark" name="remark" style="width:100%" data-options="label:'Remark:'">
			        </div>
				</div>
			</td>
			<td style="text-align: right;" valign="top">
				
		        <div style="margin-bottom:10px">
		            <input class="easyui-textbox" id="novoucher" name="novoucher" style="width:40%" data-options="label:'No Voucher:'" readonly="true" value="<?php echo $canumber?>">
		        </div>
		        <div style="margin-bottom:10px">
		            <input class="easyui-textbox" id="tanggal" name="tanggal" value="<?php echo date('Y-m-d H:i:s')?>" style="width:40%" data-options="label:'Date Time:'" readonly="true">
		            
		        </div>
		        <div style="width:40%;float: right;">
					<input class="easyui-searchbox" id="srcpv" data-options="prompt:'Search PV',searcher:doSearch" style="width:100%">
				</div>
			</td>
		</tr>
	</table>
	
	<?php echo form_close(); ?>
</div>

<div id="ft" style="padding:10px 10px;">
	
    <a href="#" id="btnSave" class="easyui-linkbutton" data-options="iconCls:'icon-save'">Save</a>
</div>

<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var editIndex = undefined;
	var this_url = "Dashboard/cashAdvance";
	var url = "";
	var data = [
				{"description":"", "amount":0},
				{"description":"", "amount":0},
				{"description":"", "amount":0},
				{"description":"", "amount":0},
				{"description":"", "amount":0},
				{"description":"", "amount":0},
				{"description":"", "amount":0},
				{"description":"", "amount":0},
				{"description":"", "amount":0},
				{"description":"", "amount":0}
				];

	$(function () {

		$('#dg').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'po_number',
			fit: true,
			title:'Cash Advance',
			rownumbers:true,
			toolbar:'#tb',
			footer:'#ft',
			data: data,
			onClickCell: onClickCell,
			columns:[[
				{field:'description',title:'Description',width:550, editor:{type:'textbox'}},
				{field:'amount',title:'Amount',width:100, editor:{type:'numberbox',options:{precision:2}}}
			]],
			onEndEdit:function(index,row){
	            
	        },
			onBeginEdit:function(index,row){
		        var dg = $(this);
		        //var ed = dg.datagrid('getEditors',index)[0];
		        var ed = dg.datagrid('getEditors',index);

		        if (!ed){return;}
		        
		        var ttext = $(ed[0].target);
		        var tnum = $(ed[1].target);
		        tnum = tnum.numberbox('textbox');
		        if (ttext.hasClass('textbox-f')){
		            ttext = ttext.textbox('textbox');
		        }

		        ttext.bind('keydown', function(e){
		            if (e.keyCode == 13){
		                dg.datagrid('endEdit', index);
		            } else if (e.keyCode == 27){
		                dg.datagrid('cancelEdit', index);
		            }
		        })

		        tnum.bind('keydown', function(e){
		            if (e.keyCode == 13){
		                dg.datagrid('endEdit', index);
		            } else if (e.keyCode == 27){
		                dg.datagrid('cancelEdit', index);
		            }
		        })
		    }
		});
		
	});
	
	

	function onClickCell(index, field){
        if (editIndex != index){
            if (endEditing()){
                $('#dg').datagrid('selectRow', index).datagrid('beginEdit', index);
                var ed = $('#dg').datagrid('getEditor', {index:index,field:field});
                if (ed){
                    ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                }
                editIndex = index;
            } else {
                setTimeout(function(){
                    $('#dg').datagrid('selectRow', editIndex);
                },0);
            }
        }
    }

    function endEditing(){
        if (editIndex == undefined){return true}
        if ($('#dg').datagrid('validateRow', editIndex)){
            $('#dg').datagrid('endEdit', editIndex);
            editIndex = undefined;
            return true;
        } else {
            return false;
        }
    }

    $("#btnSave").on('click', function()
    {
    	endEditing();
    	
    	var csrf = $("input[name*='csrf_name']").val();
    	
    	var payto = $('#payto').textbox('getText');
    	var remark = $('#remark').textbox('getText');
    	var voucher = $('#novoucher').textbox('getText');
    	var tanggal = $('#tanggal').textbox('getText');
    	var srcpvno = $('#srcpv').searchbox('getValue');
    	var storecode = "HO";
    	var data = $('#dg').datagrid('getData');

    	
    	

    	if(payto == "")
    	{
    		$.messager.alert('Failed',"Please input payment to.",'error');
			return;
    	}

    	if(tanggal == "")
    	{
    		$.messager.alert('Failed',"Please input timestamp.",'error');
			return;
    	}

    	var propRows = data.rows;
    	var tmpArray = new Array();
    	$.each(propRows, function( index, value ) {
			if(value.description != null && value.description != "")
			{
				var grid = {};
				grid['description'] = value.description;
				grid['amount'] = value.amount;
				tmpArray[tmpArray.length] = grid;
			}
		});
		var win = $.messager.progress({title:'Please waiting',msg:'Sending data...'});

		if(url == "") url = "Dashboard/saveCashAdvance";

		$.post( url, 
		{
			csrf_name:csrf,
			rows:JSON.stringify(tmpArray),
			remark:remark,
			payto:payto,
			novoucher:voucher,
			tanggal:tanggal,
			srcpvno:srcpvno
		},"json")
		.done(
		    	function(msg)
		    	{
		    		console.log(msg);
		    		var obj = jQuery.parseJSON( msg );
		    		$.messager.progress('close');
		    		$('#dg').datagrid('reload');
		    		openUrl("<?php echo base_url()?>index.php/"+this_url);
					if(obj.status == 0)
					{
						$.messager.alert('Failed',obj.msg,'error');	
					}
					else
					{
						openUrl("<?php echo base_url()?>index.php/"+this_url);
						$.messager.confirm('Transfer Out', 'Your DO Number is '+obj.po_number+', want to print press OK!', function(r){
			                if (r){
			                    loadPrintDocument(this, "Dashboard/printCashAdvance/"+obj.lastid);
			                }
			            });
						
					}

					
					console.log(obj);
		     	}
		     )
		    .fail(function(xhr, status, error) {
		        // error handling
		        $.messager.progress('close');
		        console.log(xhr.status);
		        console.log(error);
		        console.log(status);
		        $.messager.alert('Failed',xhr.status + "("+error+")",'error');
		    });
    });


    function doSearch(value){
        $.get( "Dashboard/getCashAdvance?q="+ value, function( data ) {
			var obj = jQuery.parseJSON( data );
			if(obj.detail.length <= 0)
			{
				$.messager.alert('Failed',"Your Voucher not found.",'error');
				return;
			}
			
			if(obj.detail.length > 0)
			{
				url = "Dashboard/saveCashAdvance/update";
			}

			$("#remark").textbox("setText", obj.kas.remark);

			$('#dg').datagrid({
				data: obj.detail
			});
			reloadFooter();
		});
    }
</script>