<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo TITLE_PAGE?></title>
    <?php echo link_tag('includes/icon/icon.ico','shortcut icon','');?>
	<?php echo link_tag('includes/themes/bootstrap/easyui.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/themes/icon.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/themes/color.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/demo/demo.css','stylesheet','text/css');?>
	<?php echo script_tag('includes/jquery.min.js');?>
    <?php echo script_tag('includes/jquery.easyui.min.js');?>
	<?php echo script_tag('includes/datagrid-detailview.js');?>
	<?php echo script_tag('includes/helper.js');?>
	<?php echo script_tag('includes/plugins/jquery.number.js');?>
</head>
<body class="easyui-layout">
	<!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>


    <div data-options="region:'north',split:false,border:false" style="height:60px;background:#FFFFFF;padding:10px;font-size:25px;font-weight:bold;color:#808080">
		
		<div style="padding:5px;">
			mosERP
			<div style="float: right;">
				<a href="#" class="easyui-menubutton" data-options="menu:'#mm3',iconCls:'icon-help'"></a>
				
				<a id="btn-edit" href="#" class="easyui-menubutton" data-options="menu:'#mm2'"><?php echo $this->session->userdata('name')?></a>
			</div>
		</div>
		<div id="mm2" style="width:100px;">
			<div>Help</div>
			<div>Update</div>
			<div class="menu-sep"></div>
			<div id="mnlogout">LOGOUT</a></div>
		</div>
		<div id="mm3" class="menu-content" style="background:#f0f0f0;padding:10px;text-align:left">
	        
	    </div>
	</div>
    <div data-options="region:'west',title:'Navigation Menu',split:true" style="width:200px;">
		<?php $this->load->view("dashboard/tree_menu");?>
	</div>
    <div id="mainbox" data-options="region:'center'" style="background:#eee;"></div>
</body>
<?php

?>
<script type="text/javascript">
	var base_url = "<?php echo base_url();?>";

	$(function() {
		$(".preloader").fadeOut();
	});

	$('#tMenu').tree({	
		onClick:function(node){
			if(node.children != 'undefined')
			{
				$(this).tree('toggle', node.target);
			}

			if(node.url != "" && node.url != null)
			{
				openUrl("<?php echo base_url()?>index.php/"+node.url);
			}

		}
	})

	function openUrl(url){	
		//$('body>div.menu-top').menu('destroy');
		$('body>div.window>div.window-body').window('destroy');
		$('#mainbox').panel('refresh',url);
	}

	$( "#mnlogout" ).click(function() {
		$.messager.confirm('LOGOUT', 'Are you sure to logout?', function(r){
			if (r){
				document.location.href = base_url+"index.php/login/logout";
			}
		});
	});

</script>
</html>