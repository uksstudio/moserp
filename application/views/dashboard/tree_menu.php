<?php

function has_child($id){
	$CI =& get_instance();
	$CI->db->where("parent", $id);
	$CI->db->from('menu_modul');
	$count = $CI->db->count_all_results();
	return $count > 0 ? true : false;
}
function recursiveTree($parent)
{
	$CI =& get_instance();
	$CI->db->where('mm.parent', $parent);
	$CI->db->where('mua.userid', $CI->session->userdata('userid'));
	$CI->db->select('mm.*, mua.is_add, mua.is_edit, mua.is_del, mua.is_read');
	$CI->db->from('mos_userroll_access mua');
	$CI->db->join('menu_modul mm','mua.menu_modul_id=mm.id', 'left');
	$CI->db->order_by('urutan', 'ASC');

	$rs = $CI->db->get();
	
	$class = ($parent == 0) ? 'class="easyui-tree" id="tMenu"' : '';
	print "<ul ".$class.">"."\n";
	foreach($rs->result_array() as $row)
	{
		if(has_child($row['id']))
		{
			print "\t"."<li data-options=\"state:'closed',id:".$row['id'].",url:'".$row['url']."'\">"."\n";
			print "\t\t"."<span>".$row['nama']."</span>"."\n";
			recursiveTree($row['id']);
		}
		else
		{
			print "\t"."<li data-options=\"id:".$row['id'].",url:'".$row['url']."'\">"."\n";
			print "\t\t"."<span>".$row['nama']."</span>"."\n";
		}
		print "\t"."</li>"."\n";
	}
	print "</ul>"."\n";
}
recursiveTree(0);
?>