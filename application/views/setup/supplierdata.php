<table id="dgmodul"></table>

<div id="toolbar">
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">New</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Edit</a>
    
</div>

<div id="dlg" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
    <?php echo form_open("",'novalidate style="margin:0;padding:10px 30px" id="fm"');?>
        <h3>EDC Machine (POS)</h3>
        <div style="margin-bottom:10px">
            <input name="edcname" class="easyui-textbox" required="true" label="EDC Name:" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="coacode" class="easyui-combogrid" id="coacode" required="true" label="COA :" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="commision" class="easyui-numberbox" precision="1" required="true" label="Comm:" style="width:100%">
        </div>
    </form>
</div>
<div id="dlg-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" style="width:90px">Save</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
</div>

<script type="text/javascript">
var csrf = '<?php echo $this->security->get_csrf_hash();?>';
$(function(){
    $('#dgmodul').datagrid({
        title:'Supplier Data',
        width:660,
        height:250,
        singleSelect:true,
        idField:'productcategoryid',
        url:'Setup/getSupplierInfo',
		method: 'get',
		fit: true,
		toolbar:'#toolbar',
        columns:[[
            {field:'suppliercode',title:'Code',width:90},
            {field:'suppliername',title:'Name',width:250},
            {field:'supplierphone',title:'Phone',width:80,align:'right'},
            {field:'currency',title:'Currency',width:80,align:'center'},
            {field:'billingadd1',title:'Address',width:150},
        ]]
    });
});


var url;
    function newUser(){
        csrf = $("input[name*='csrf_name']").val();
        $('#dlg').dialog('open').dialog('center').dialog('setTitle','Add New');
        $('#fm').form('clear');
        $("input[name*='csrf_name']").val(csrf);
        url = 'Setup/getSupplierInfo/add';
    }
    function editUser(){
        var row = $('#dgmodul').datagrid('getSelected');
        if (row){
            $('#dlg').dialog('open').dialog('center').dialog('setTitle','Update');
            $('#fm').form('load',row);
            url = 'Setup/getSupplierInfo/update/'+row.id;
        }
    }

    $('#fm').submit(function(event){
        event.preventDefault();
        $.ajax({
             type: 'POST',
             url: url, 
             data: $(this).serialize()
        })
        .done(function(msg){
            var obj = jQuery.parseJSON( msg );
            $("input[name*='csrf_name']").val(obj.csrf_name);
            if (obj.staus){
                $.messager.show({
                    title: 'Error',
                    msg: obj.errorMsg
                });
            } else {
                $('#dlg').dialog('close');        // close the dialog
                $('#dgmodul').datagrid('reload');    // reload the user data
            }
            console.log(obj);
        })
        .fail(function(xhr, status, error) {
            // error handling
            console.log(xhr.status);
            console.log(error);
            console.log(status);
            $.messager.alert('Failed',xhr.status + "("+error+")",'error');

        });
    });
    function saveUser(){
        $("#fm").submit();
    }
    function destroyUser(){
        var row = $('#dgmodul').datagrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to destroy this EDC?',function(r){
                if (r){
                    $.post('Setup/getSupplierInfo/delete/'+row.id,{id:row.id},function(result){
                        if (result.status){
                            $('#dgmodul').datagrid('reload');    // reload the user data
                        } else {
                            $.messager.show({    // show error message
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        }
                    },'json');
                }
            });
        }
    }

</script>