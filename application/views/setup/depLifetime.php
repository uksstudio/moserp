<table id="dgmodul" class="easyui-datagrid"></table>

 <div id="toolbar">
 	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">New</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Edit</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove</a>
</div>


<div id="dlg" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
    <?php echo form_open("",'novalidate style="margin:0;padding:10px 30px" id="fm"');?>
        <div style="margin-bottom:10px">
            <input name="titlename" class="easyui-textbox" required="true" label="Name:" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="yr_commercial" class="easyui-numberbox" required="true" label="Yearly Commercial :" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="yr_fiscal" class="easyui-numberbox" required="true" label="Yearly Fiscal :" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="mth_commercial" class="easyui-numberbox" required="true" label="Mth Commercial :" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="mth_fiscal" class="easyui-numberbox" required="true" label="Mth Fiscal :" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="groupcode" class="easyui-textbox" required="true" label="Prefix :" style="width:100%">
        </div>
        <div style="margin-bottom:10px">
            <input name="account" id="coacode" class="easyui-combogrid" required="true" label="Account:" style="width:100%">
        </div>
    </form>
</div>
<div id="dlg-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" style="width:90px">Save</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
</div>

<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	
	$(function () {
		$('#dgmodul').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'id',
			rownumbers:true,
			url:'setup/depLifetime/read',
			method: 'get',
			title:'Depreciation Life Time',
			toolbar:'#toolbar',
			columns:[[
				{field:'id',title:'ID',width:50},
				{field:'titlename',title:'Name',width:200},
				{field:'yr_commercial',title:'Yearly Commercial',width:100},
				{field:'yr_fiscal',title:'Fiscal Yearly',width:100},
				{field:'mth_commercial',title:'Mth Commercial',width:100},
                {field:'mth_fiscal',title:'Mth Fiscal',width:100},
                {field:'account',title:'Account',width:100}
			]]
		});

		$("#coacode").combogrid({
			panelWidth:300,
			url: "Finance/getCoa",
			idField:'subcode',
			textField:'subcode',
			mode:'remote',
			fitColumns:true,
			groupField:'code',
			method: 'get',
			columns:[[
				{field:'subcode',title:'Account',width:100},
				{field:'name',title:'Name',width:250}
			]],
			onChange:function(value){
                var g = $(this).combogrid('grid');
                g.datagrid('load',{q:value});
                console.log("find id : "+value);
            },
            onSelect: function(index,record)
            {
                
            }
		});
	});

	var url;
    function newUser(){
    	csrf = $("input[name*='csrf_name']").val();
        $('#dlg').dialog('open').dialog('center').dialog('setTitle','Add New');
        $('#fm').form('clear');
        $("input[name*='csrf_name']").val(csrf);
        url = 'Admin/addEdc/add';
    }
    function editUser(){
        var row = $('#dgmodul').datagrid('getSelected');
        if (row){
            $('#dlg').dialog('open').dialog('center').dialog('setTitle','Update');
            $('#fm').form('load',row);
            url = 'Setup/depLifetime/update/'+row.id;
        }
    }

    $('#fm').submit(function(event){
		event.preventDefault();
		$.ajax({
			 type: 'POST',
			 url: url, 
			 data: $(this).serialize()
		})
		.done(function(msg){
			var obj = jQuery.parseJSON( msg );
    		$("input[name*='csrf_name']").val(obj.csrf_name);
			if (!obj.status){
                $.messager.show({
                    title: 'Error',
                    msg: obj.errorMsg
                });
            } else {
                $('#dlg').dialog('close');        // close the dialog
                $('#dgmodul').datagrid('reload');    // reload the user data
            }
			console.log(obj);
		})
		.fail(function(xhr, status, error) {
	        // error handling
	        console.log(xhr.status);
	        console.log(error);
	        console.log(status);
	        $.messager.alert('Failed',xhr.status + "("+error+")",'error');

		});
	});
    function saveUser(){
    	$("#fm").submit();
    }
    function destroyUser(){
        var row = $('#dgmodul').datagrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to destroy this EDC?',function(r){
                if (r){
                    $.post('Admin/addEdc/delete/'+row.id,{id:row.id},function(result){
                        if (result.status){
                            $('#dgmodul').datagrid('reload');    // reload the user data
                        } else {
                            $.messager.show({    // show error message
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        }
                    },'json');
                }
            });
        }
    }

</script>