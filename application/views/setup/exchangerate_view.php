<table id="tt"></table>

<div id="toolbar">
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="insert();">New</a>
</div>

<script type="text/javascript">
var csrf = '<?php echo $this->security->get_csrf_hash();?>';
$(function(){
    $('#tt').datagrid({
        title:'Exchange rate',
        iconCls:'icon-edit',
        width:660,
        height:250,
        singleSelect:true,
        idField:'currencyid',
        url:'Setup/getExchangeRate',
		method: 'get',
		fit: true,
		toolbar:'#toolbar',
        columns:[[
            {field:'currencycode',title:'Currency Code',width:90,editor:'text'},
            {field:'countrycode',title:'Country Code',width:100,editor:'text'},
            {field:'exchangerate',title:'Exchange Rate',width:100,align:'right',editor:{type:'numberbox',options:{precision:1}}},
            {field:'action',title:'Action',width:80,align:'center',
                formatter:function(value,row,index){
                    if (row.editing){
                        var s = '<a href="javascript:void(0)" onclick="saverow(this)">Save</a> ';
                        var c = '<a href="javascript:void(0)" onclick="cancelrow(this)">Cancel</a>';
                        return s+c;
                    } else {
                        var e = '<a href="javascript:void(0)" onclick="editrow(this)">Edit</a> ';
                        var d = '<a href="javascript:void(0)" onclick="deleterow(this)">Delete</a>';
                        return e+d;
                    }
                }
            }
        ]],
        onEndEdit:function(index,row){
            var ed = $(this).datagrid('getEditor', {
                index: index,
                field: 'currencycode'
            });
        },
        onBeforeEdit:function(index,row){
            row.editing = true;
            $(this).datagrid('refreshRow', index);
        },
        onAfterEdit:function(index,row){
            row.editing = false;
            $(this).datagrid('refreshRow', index);
        },
        onCancelEdit:function(index,row){
            row.editing = false;
            $(this).datagrid('refreshRow', index);
        }
    });
});


function getRowIndex(target){
	var tr = $(target).closest('tr.datagrid-row');
	return parseInt(tr.attr('datagrid-row-index'));
}
function editrow(target){
	$('#tt').datagrid('beginEdit', getRowIndex(target));
	
}
function deleterow(target){
	$.messager.confirm('Confirm','Are you sure?',function(r){
		if (r){
			$('#tt').datagrid('deleteRow', getRowIndex(target));
		}
	});
}
function saverow(target){
	

	var index = getRowIndex(target);
	var item = $('#tt').datagrid('endEdit', index);
	var items = $('#tt').datagrid('getRows')[index];

	$.post( "setup/addeditexchangerate", { 
			currcode:items.currencycode, 
			countrycode:items.countrycode,
			kurs:  items.exchangerate, 
			id:items.currencyid,
			csrf_name:csrf
		}, function( data ) {
			console.log( data); // John
			csrf = data.csrf_name;
	}, "json");
}
function cancelrow(target){
	$('#tt').datagrid('cancelEdit', getRowIndex(target));
}
function insert(){
	var row = $('#tt').datagrid('getSelected');
	if (row){
		var index = $('#tt').datagrid('getRowIndex', row);
	} else {
		index = 0;
	}
	$('#tt').datagrid('insertRow', {
		index: index,
		row:{
			status:'P'
		}
	});
	$('#tt').datagrid('selectRow',index);
	$('#tt').datagrid('beginEdit',index);
}

</script>