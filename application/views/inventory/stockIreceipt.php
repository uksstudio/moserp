<table id="tt" style="width: 100%"></table>

<div id="toolbar">
	<table style="width: 100%; border: 0px solid black;">
		<tr>
			<td style="width:150px"><input class="easyui-combogrid" name="location" id="location" style="width:150px"></td>
			<td style="width:150px"><input class="easyui-combogrid" name="terminal" id="terminal" style="width:150px"></td>
			<td style="width:130px">
				<input class="easyui-textbox" name="carton_num" id="carton_num" label="DO Number:" labelPosition="top" style="width:130px">
			</td>
			<td style="text-align: right;">
				<a id="btnSave" class="easyui-linkbutton"  data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'" onclick="doReceipt()">Receive</a>

        	</td>
		</tr>
	</table>
    
    
</div>


<div id="w" class="easyui-window" data-options="closed:true,iconCls:'icon-save'" style="padding:10px;">
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'center'" >
            <table id="propsize" style="width: 100%"></table>
        </div>
        
    </div>
</div>

<?php echo script_tag('includes/plugins/jquery.printPage.js');?>

<script type="text/javascript">
var csrf = '<?php echo $this->security->get_csrf_hash();?>';

$(function(){

	$('#location').combogrid({
		panelWidth:300,
		url: "posting/getLocation",
		idField:'locationid',
		textField:'locationcode',
		mode:'remote',
		fitColumns:true,
		method: 'get',
		label: 'Stock Room :',
		labelPosition: 'top',
		required: true,
		columns:[[
			{field:'locationid',title:'ID',width:20},
			{field:'locationcode',title:'Location Code',width:30},
			{field:'locationname',title:'Location Name',width:100}
		]],
		onChange:function(value){
			console.log("find id : "+value);
		},
		onSelect: function(index,row){
			var codigo = row.locationid;
			console.log(codigo);

			$('#terminal').combogrid({
				url:"posting/getTerminal/"+codigo,
	        	method: 'get'
			});
			
		}
	});
	

	$('#terminal').combogrid({
		panelWidth:300,
		idField:'terminalinfoid',
		textField:'terminalcode',
		fitColumns:true,
		label: 'Terminal :',
		labelPosition: 'top',
		required: true,
		columns:[[
			{field:'terminalinfoid',title:'ID',width:20},
			{field:'terminalcode',title:'Terminal Code',width:30},
			{field:'invoicenumberprefix',title:'Prefix',width:100}
		]],
		onChange:function(value){
			console.log("find id : "+value);
		},
		onSelect: function(index,row){
			var codigo = row.locationid;
			console.log(codigo);

			
		}
	});


	$('#tt').datagrid({
        title:'Stock Receipt',
        singleSelect:true,
        idField:'transfernoteid',
		fit: true,
		remoteFilter: true,
		toolbar:'#toolbar',
		showFooter: true,
		rownumbers: true,
		pageSize: 500,
        pageList: [500, 1000],
        pagination: true,
		columns:[[
            {field:'raisedate',title:'Delivery Date',width:130},
            {field:'transfernotenumber',title:'Transfer Number',width:100},
            {field:'flocation',title:'FROM',width:50},
            {field:'tlocation',title:'TO',width:50},
            {field:'transfernotestatus',title:'Status',width:100, formatter:
            	function(val,row){
            		if(val == 1)
            			return 'OPEN';
            		else if (val == 2)
            			return 'RECEIVED';
            		else if(val == 3)
            			return 'VOID';
            	}
        	},
            {field:'stockreceiptnumber',title:'Receipt Number',width:150},
            {field:'remark',title:'Remark',width:200},
            {field:'receiveremark',title:'Receive Remark',width:200}
        ]],
        onLoadSuccess: function(data)
		{
			console.log(data.csrf_name);
			csrf = data.csrf_name;
		}
    });
});


$('#carton_num').textbox({
	inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
		keyup:function(e){
			if(e.keyCode == 13)
			{
				var outletcode = $('#location').combogrid('getText');
				var donumber = $('#carton_num').textbox('getText');

				if(outletcode != '') //SEASON (2) + STYLE CODE (7)
				{
					var outletcode = $('#location').combogrid('getText');
				    $('#tt').datagrid({
				    	url:'Inventory/getStockReceipt',
			        	method: 'post',
			        	queryParams:{
			        		csrf_name: csrf,
							stocksroom: outletcode
			        	}
			        });
				}
				else
				{
					$.messager.alert('Failed','Please fill all field required.','error');
				}
			}
		}
	})
})

	function doReceipt()
    {
    	var row = $('#tt').datagrid('getSelected');
		if(!row)
		{
			$.messager.alert('Failed',"Please select one row!",'error');
			return;
		}
		 var rowIndex = $("#tt").datagrid("getRowIndex", row);
		if(row.transfernotestatus==2)
		{
			$.messager.alert('Failed',"DO Number already received.",'error');
			return;	
		}
		var outletcode = $('#location').combogrid('getText');
		var terminal = $('#terminal').combogrid('getText');
		$.post('inventory/setReceivedStore', {csrf_name: csrf, 	terminal: terminal, transfernoteid: row.transfernoteid, locationcode:outletcode}, "json")
	    .done(
	    	function(msg)
	    	{
	    		console.log(msg);
	    		var obj = jQuery.parseJSON( msg );
	    		csrf = obj.csrf_name;
	    		if(obj.status == 1)
	    		{
	    			$.messager.alert('Successfully',"Your Receipt Number is : " + obj.stknumber,'success');
	    			$('#tt').datagrid('updateRow',{
	    				index:rowIndex,
	    				row: {
							stockreceiptnumber: obj.stknumber,
							transfernotestatus: 2
						}
	    			});
	    			
	    		}
	     	}
	     )
	    .fail(function(xhr, status, error) {
	        // error handling
	        console.log(xhr.status);
	        console.log(error);
	        console.log(status);
	        $.messager.alert('Failed',xhr.status + "("+error+")",'error');
	    });
    }

    function reprintInvoice(transfernoteid, action)
    {
    	if(action == "reprint_do")
    		loadPrintDocument(this, "inventory/printReceiveWh/"+transfernoteid+"/DO");
    	else if(action == "reprint_cn")
    		loadPrintDocument(this, "inventory/printReceiveWh/"+transfernoteid+"/CN");
    }


</script>