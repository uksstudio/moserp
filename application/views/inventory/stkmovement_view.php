<table id="tt" style="width: 100%"></table>

<div id="toolbar">
    <input class="easyui-textbox" name="barcode" id="barcode" style="width:20%">
    <input class="easyui-combogrid" name="location" id="location" style="width:15%">
    <a href="#" class="easyui-linkbutton" plain="true" onclick="doSearch()">Search</a>
</div>

<script type="text/javascript">
var csrf = '<?php echo $this->security->get_csrf_hash();?>';
$(function(){
    var dg = $('#tt').datagrid({
        title:'Stock Movement',
        singleSelect:true,
        idField:'currencyid',
        url:'Inventory/getMovementStock',
		method: 'post',
		fit: true,
		pagination: true,
		remoteFilter: true,
		toolbar:'#toolbar',
		showFooter: true,
		rownumbers: true,
		pageSize: 500,
        pageList: [500, 1000],
        queryParams:{
        	csrf_name: csrf
        },
        columns:[[
            {field:'productitemcode',title:'Product Code',width:150},
            {field:'reffno',title:'Refference',width:100},
            {field:'movementdate',title:'Date',width:130},
            {field:'movement_type',title:'Move Type',width:150, formatter: 
            function(val, row)
            {
            	if(val == 'I')
            		return 'IN';
                else if(val == 'X')
                    return 'OUT CP';
                else if(val == 'Z')
                    return 'IN CP';
                else if(val == 'O')
                    return 'OUT';
                else if(val == 'W')
                    return 'NEW IN';
                else if(val == 'C')
                    return 'CUSTOMER ORDER';
            	else
            		return 'OTHER';
            }},
            {field:'locationcode',title:'Whs',width:50},
            {field:'movement_qty',title:'Qty',width:50}
        ]],
        onLoadSuccess: function (data)
        {
        	console.log(data);
        	csrf = data.csrf_name;
        },
        onLoadError: function()
        {

        }
    });

    $('#barcode').textbox({
		label: 'Product Code',
		labelPosition: 'left',
        required: true
	});

    $('#location').combogrid({
		panelWidth:300,
		url: "posting/getLocation",
		idField:'locationcode',
		textField:'locationcode',
		mode:'remote',
		fitColumns:true,
		method: 'get',
		label: 'Stockroom :',
		labelPosition: 'left',
		columns:[[
			{field:'locationid',title:'ID',width:20},
			{field:'locationcode',title:'Location Code',width:30},
			{field:'locationname',title:'Location Name',width:100}
		]],
		onChange:function(value){
			var g = $('#location').combogrid('grid');
			g.datagrid('load',{assay_type_id:value});
			console.log("find id : "+value);
		},
		onSelect: function(index,row){
			var codigo = row.labelcode;
			console.log(codigo);
		}
	});

});



function doSearch(){
    $('#tt').datagrid('load',{
    	csrf_name: csrf,
    	productcode: $('#barcode').textbox('getText'),
        stocksroom: $('#location').combogrid('getText')
    });
}

</script>