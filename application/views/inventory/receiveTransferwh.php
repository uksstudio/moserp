<table id="dgreceive" class="easyui-datagrid"></table>

<div id="toolbar" style="padding: 8px;">
	
	<table style="width: 100%; border: 0px solid black;">
		<tr>
			<td style="width:30%" valign="top">
				<input class="easyui-searchbox" data-options="prompt:'Please Input Value',menu:'#mm',searcher:doSearch" style="width:100%">
			</td>
			
			<td style="text-align: right;">
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onClick="doReceipt()">Receive</a>
        	</td>
		</tr>
	</table>
</div>

<div id="mm">
    <div data-options="name:'receivedate'">Receive Date</div>
    <div data-options="name:'donumber'">DO Number</div>
</div>

<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	
	$(function () {
		$('#dgreceive').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			idField:'po_number',
			toolbar: '#toolbar',
			fit: true,
			rownumbers:true,
			pagination: true,
			remoteFilter: true,
			showFooter: true,
			rownumbers: true,
			pageSize: 100,
	        pageList: [100, 200],
			columns:[[
				{field:'receivedate',title:'Date Time',width:200},
				{field:'transfernotenumber',title:'DO Number',width:200},
				{field:'stockreceiptnumber',title:'CN Number',width:200},
				{field:'fromlocationcode',title:'From Location',width:100},
				{field:'locationcode',title:'To Location',width:100},
				{field:'transfernotestatus',title:'Status',width:100, formatter:
					function (val, row)
					{
						return (val == 1) ? 'OPEN' : 'CLOSED';
					}
				},
				{field:'action',title:'Action',width:200, formatter:
					function (val, row)
					{
						if(row.transfernotestatus == 1)
							return ''
						else
							return '<a href="#" class="easyui-linkbutton" iconCls="icon-print" plain="true" act="reprint_do" row-id="'+row.transfernoteid+'">Reprint DO</a> | <a href="#" class="easyui-linkbutton" iconCls="icon-print" plain="true" act="reprint_cn" row-id="'+row.transfernoteid+'">Reprint CN</a>';
					}
				}
			]],
			onLoadSuccess: function(data)
			{
				csrf = data.csrf_name;
				$(this).datagrid('getPanel').find('.easyui-linkbutton').each(function(){
					$(this).linkbutton({
						onClick:function(){
							var id = $(this).attr('row-id');
                        	var act = $(this).attr('act');
							reprintInvoice(id, act);
						}
					})
				})
			}
		});
	});


	function doSearch(value,name){
        $('#dgreceive').datagrid({
        	url:'Inventory/getTransferReceive/WH',
        	method: 'post',
        	queryParams:{
        		csrf_name: csrf,
        		type: name,
        		keyword: value
        	}
        });
    }

    function doReceipt()
    {
    	var row = $('#dgreceive').datagrid('getSelected');
		if(!row)
		{
			$.messager.alert('Failed',"Please select one row!",'error');
			return;
		}
		 var rowIndex = $("#dgreceive").datagrid("getRowIndex", row);
		if(row.transfernotestatus==2)
		{
			$.messager.alert('Failed',"CN Number already received.",'error');
			return;	
		}

		$.post('inventory/setReceived', {csrf_name: csrf, 	transfernoteid: row.transfernoteid}, "json")
	    .done(
	    	function(msg)
	    	{
	    		console.log(msg);
	    		var obj = jQuery.parseJSON( msg );
	    		csrf = obj.csrf_name;
	    		if(obj.status == 1)
	    		{
	    			$.messager.alert('Successfully',"Your Receipt Number is : " + obj.stknumber,'success');
	    			$('#dgreceive').datagrid('updateRow',{
	    				index:rowIndex,
	    				row: {
							stockreceiptnumber: obj.stknumber,
							transfernotestatus: 2
						}
	    			});
	    			
	    		}
	     	}
	     )
	    .fail(function(xhr, status, error) {
	        // error handling
	        console.log(xhr.status);
	        console.log(error);
	        console.log(status);
	        $.messager.alert('Failed',xhr.status + "("+error+")",'error');
	    });
    }

    function reprintInvoice(transfernoteid, action)
    {
    	if(action == "reprint_do")
    		loadPrintDocument(this, "inventory/printReceiveWh/"+transfernoteid+"/DO");
    	else if(action == "reprint_cn")
    		loadPrintDocument(this, "inventory/printReceiveWh/"+transfernoteid+"/CN");
    }

</script>