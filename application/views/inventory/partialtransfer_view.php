<table id="tt" style="width: 100%"></table>

<div id="toolbar">
	<table style="width: 100%; border: 0px solid black;">
		<tr>
			<td style="width:150px"><input class="easyui-combogrid" name="fromlocation" id="fromlocation" style="width:150px"></td>
			<td style="width:150px"><input class="easyui-combogrid" name="tolocation" id="tolocation" style="width:150px"></td>
			
			<td style="width:130px">
				<input class="easyui-textbox" name="carton_num" id="carton_num" label="Product Code:" labelPosition="top" required="true" style="width:130px">
			</td>
			<td style="text-align: right;">
				<a id="btnSave" class="easyui-linkbutton"  data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'">Save</a>
        		<a id="btnRemove" class="easyui-linkbutton" data-options="iconCls:'icon-large-remove',size:'large',iconAlign:'top'">Remove</a>

        	</td>
		</tr>
	</table>
    
    
</div>


<div id="w" class="easyui-window" data-options="closed:true,iconCls:'icon-save'" style="padding:10px;">
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'center'" >
            <table id="propsize" style="width: 100%"></table>
        </div>
        <div data-options="region:'south',border:false" style="text-align:right;padding:5px 0 0;">
            <a class="easyui-linkbutton" data-options="iconCls:'icon-ok'" href="javascript:void(0)" onclick="addtogrid()" style="width:80px">Ok</a>
            <a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" href="javascript:void(0)" onclick="$('#w').window('close')" style="width:80px">Cancel</a>
        </div>
    </div>
</div>

<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<?php echo script_tag('includes/plugins/datagrid-cellediting.js');?>

<script type="text/javascript">
var csrf = '<?php echo $this->security->get_csrf_hash();?>';


$(function(){
    var dg = $('#tt').datagrid({
        title:'Partial Transfer (Pcs)',
        singleSelect:true,
        idField:'currencyid',
        url:'Inventory/getManualGridItems',
		method: 'get',
		fit: true,
		remoteFilter: true,
		toolbar:'#toolbar',
		showFooter: true,
		rownumbers: true,
		clickToEdit: true,
		columns:[[
            {field:'season',title:'Season',width:50},
            {field:'label',title:'Lbl',width:50},
            {field:'category',title:'Cate',width:50},
            {field:'sex',title:'Gender',width:50},
            {field:'productcode',title:'Product Code',width:150},
            {field:'productitemdesc',title:'Product Desc',width:200},
            {field:'descline1',title:'FBR Desc',width:200},
            {field:'rqty',title:'QTY',width:50, editor:{type:'numberbox',options:{precision:0}}},
            {field:'qty',title:'Curr Qty',width:50},
            {field:'retailsalesprice',title:'RSP',width:100,align:'right'}
        ]]
    });


    $('#fromlocation').combogrid({
		panelWidth:300,
		url: "posting/getLocation",
		idField:'locationid',
		textField:'locationcode',
		mode:'remote',
		fitColumns:true,
		method: 'get',
		label: 'From Stock Room :',
		labelPosition: 'top',
		required: true,
		columns:[[
			{field:'locationid',title:'ID',width:20},
			{field:'locationcode',title:'Location Code',width:30},
			{field:'locationname',title:'Location Name',width:100}
		]],
		onChange:function(value){
			var g = $('#fromlocation').combogrid('grid');
			g.datagrid('load',{assay_type_id:value});
			console.log("find id : "+value);
		},
		onSelect: function(index,row){
			var codigo = row.labelcode;
			console.log(codigo);
		}
	});


	$('#tolocation').combogrid({
		panelWidth:300,
		url: "posting/getLocation",
		idField:'locationid',
		textField:'locationcode',
		mode:'remote',
		fitColumns:true,
		method: 'get',
		label: 'To Stock Room :',
		labelPosition: 'top',
		required: true,
		columns:[[
			{field:'locationid',title:'ID',width:20},
			{field:'locationcode',title:'Location Code',width:30},
			{field:'locationname',title:'Location Name',width:100}
		]],
		onChange:function(value){
			var g = $('#tolocation').combogrid('grid');
			g.datagrid('load',{assay_type_id:value});
			
		},
		onSelect: function(index,row){
			
		}
	});

});


var Clickc = [];
$('#tt')
	.datagrid('enableCellEditing')
	.datagrid({
		onCellEdit: function (index, field)
		{
			var dg = $(this);
			var ed = dg.datagrid('getEditor', {index:index,field:field});
			var input = dg.datagrid('input', {index:index,field:field});
			input.bind('keydown', function(e){
				if (e.keyCode == 38 && index>0){	// up
					dg.datagrid('endEdit', index);
					dg.datagrid('editCell', {
						index: index-1,
						field: field
					});
				} else if (e.keyCode == 40 && index<dg.datagrid('getRows').length-1){	// down
					dg.datagrid('endEdit', index);
					dg.datagrid('editCell', {
						index: index+1,
						field: field
					});
				}
				else if(e.keyCode == 13 ) { 
                    dg.datagrid('endEdit', index);
                    dg.datagrid('gotoCell','right');
                    dg.datagrid('editCell',dg.datagrid('cell'));

                     Clickc.index = index; // get the row that was clicked
		      	      Clickc.field = field; // get the field which was clicked
		      	      Clickc.value = $(ed.target).val();  //Get cell current value
                }
			})

            if (ed){
             
            }
		},
		onEndEdit:function(index){
			var dg = $(this);
			var ed = dg.datagrid('getEditor', {index:index,field:Clickc.field});
            if (ed){
            	var row = dg.datagrid('getRows')[index];
            	dg.datagrid('refreshRow', index);
            	updateRqty(row.rowid, Clickc.value);
            	reloadFooterDetail();
            	//update session
            }
		}
	})
	.datagrid('gotoCell', {
    	index: 0,
    	field: 'itemcode'
});


function updateRqty(rowid, qty)
{
	$.get('Inventory/updateStockPartial',
		{ rowid: rowid, rqty: qty}, 
		function(data)
		{
			$('#tt').datagrid('reload');
		},
		"json")
	.done(function() {
	    //alert( "second success" );
	  })
	  .fail(function() {
	    alert( "error" );
	  })
	  .always(function() {
	    //alert( "finished" );
	});
}

function reloadFooterDetail()
{
	var data = $('#tt').datagrid('getData');
	var sum = 0;
	var ssum = 0;
	var sum_retail_price_roundup = 0;
	for (i = 0; i < data.rows.length; i++) {
		console.log(data.rows[i].qty);
	    sum += parseInt(data.rows[i].qty);
	    ssum += parseInt(data.rows[i].rqty);
	}
	
	var rows = $('#tt').datagrid('getFooterRows');
	rows[0]['qty'] = sum;
	rows[0]['rqty'] = ssum;
	$('#tt').datagrid('reloadFooter');

	//return sum;
}

$('#carton_num').textbox({
	inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
		keyup:function(e){
			if(e.keyCode == 13)
			{
				var flocation = $('#fromlocation').combogrid('getText');
				var tlocation = $('#tolocation').combogrid('getText');
				var stylecode = $('#carton_num').textbox('getText');

				if(flocation != '' && tlocation != '') //SEASON (2) + STYLE CODE (7)
				{
					//getItemCode(flocation, tlocation, stylecode);
					getStockByItemcode(flocation, tlocation, stylecode);
				}
				else
				{
					$.messager.alert('Failed','Please fill all field required.','error');
				}
			}
		}
	})

	
})



function getItems(floc, tloc, itemcode)
{
	$.post( "Inventory/getProductStock", 
		{
			csrf_name:csrf, 
			floc:floc,
			tloc:tloc,
			itemcode:itemcode
		},
		function( data ) {
			csrf = data.csrf_name;
	}, "json");
}

function getItemCode(floc, tloc, itemcode) {
    //$("#w").empty();

    var $win;
    $win = $("#w").window({
        width: 500,
        height: 400,
        modal: true,
        title: 'Choice',
        collapsible: false,
        maximizable: false,
        minimizable: false,
        onResize: function () {
            $(this).window('center');
        }
    });
    $('#propsize').propertygrid({
		width:300,
		height:'auto',
		url:'Inventory/getProductStock/'+floc+'/'+tloc+'/'+itemcode,
		showGroup:false,
		scrollbarSize:0,
		method: 'get',
		fit: true,
		columns:[[
			{field:'name',title:'Name',width:50,resizable:false,
				formatter:function(value,row){
					return row.size;
				}
			},
			{field:'value',title:'A QTY',width:50,resizable:false},
			{field:'qty',title:'Value',width:150,resizable:false, editor:{type:'numberbox'}}
		]]
	});
	$win.window('open');
        
}


function getStockByItemcode(floc, tloc, itemcode)
{
	$.get('Inventory/getStockByItemcode',
		{ floc: floc, tloc: tloc, itemcode:itemcode }, 
		function(data)
		{
			console.log(data.data);
			if(data.data.length > 0)
			{
				console.log(data.data.length);
				$('#tt').datagrid('reload');
			}
		},
		"json")
	.done(function() {
	    //alert( "second success" );
	  })
	  .fail(function() {
	    alert( "error" );
	  })
	  .always(function() {
	    //alert( "finished" );
	});
}



function addtogrid()
{
	var items_size_qty = new Array();
	var propertyData = $('#propsize').propertygrid('getData');
	var propRows = propertyData.rows;
	
	$.each(propRows, function( index, value ) {
		if(value.value != null || value.value != "")
		{
			if(parseFloat(value.value) > 0)
			{
				var grid = {};
				grid['size'] = value.size;
				grid['qty'] = value.qty;
				items_size_qty[items_size_qty.length] = grid;
			}
			
		}
	});

	var flocation = $('#fromlocation').combogrid('getText');
	var tlocation = $('#tolocation').combogrid('getText');
	var stylecode = $('#carton_num').textbox('getText');

	$.post( "Inventory/svmanualtrf2grid", 
			{
				csrf_name:csrf, 
				adata:JSON.stringify(items_size_qty),
				productcode: stylecode,
				floc: flocation,
				tloc: tlocation
			},
			function( data ) {
				csrf = data.csrf_name;
				$('#carton_num').textbox('setText','');
				$('#w').window('close');
				$('#tt').datagrid('reload');
				
				console.log(data);
		}, "json");
}

function doSearch(){
    $('#tt').datagrid('load',{
        outletcode: $('#location').combogrid('getText')
    });
}


$("#btnSave").on('click', function()
	{
		var flocation = $('#fromlocation').combogrid('getText');
		var i_flocation = $('#fromlocation').combogrid('getValue');
		var tlocation = $('#tolocation').combogrid('getText');
		var i_tlocation = $('#tolocation').combogrid('getValue');

		var data = $('#tt').datagrid('getData');
		var rows = data.rows;
		if(rows.length <= 0)
		{
			$.messager.alert('Failed',"List is empty!",'error');	
			return;
		}

		if(flocation != '' && tlocation != '') //SEASON (2) + STYLE CODE (7)
		{
			var win = $.messager.progress({
		        title:'Please waiting',
		        msg:'Sending data...'
		    });
			$.post( "Inventory/commitmanualtrf2grid", 
				{
					csrf_name:csrf, 
					floc: flocation,
					tloc: tlocation,
					ifloc: i_flocation,
					itloc: i_tlocation
				},
				function( data ) {
					$.messager.progress('close');
					csrf = data.csrf_name;
					$('#tt').datagrid('reload');
					if(data.status == 0)
					{
						$.messager.alert('Failed',data.msgErr,'error');	
					}
					else
					{
						console.log('do printPage');
						$.messager.confirm('Transfer Out', 'Your DO Number is '+data.donum+' !', function(r){
			                if (r){
			                    //loadPrintDocument(this, "inventory/printTransferOut/"+data.transferid);
			                }
			            });
						
					}

					console.log(data);
			}, "json");
		}
		else
		{
			$.messager.alert('Failed','Please fill all field required.','error');
		}
	});

$('#btnRemove').on('click', function()
	{
		var row = $('#tt').datagrid('getSelected');
		if(!row)
		{
			$.messager.alert('Failed',"Please select one row!",'error');
			return;
		}


		$.post( "Inventory/remanualtrf2grid", 
			{
				csrf_name:csrf, 
				productcode: row.productcode
			},
			function( data ) {
				csrf = data.csrf_name;

				$('#tt').datagrid('reload');
				
				console.log(data);
		}, "json");
	});


</script>