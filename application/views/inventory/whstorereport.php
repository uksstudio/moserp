<table id="tt" style="width: 100%"></table>

<div id="toolbar">
    <input class="easyui-combogrid" name="location" id="location" style="width:20%">
    <a href="#" class="easyui-linkbutton" plain="true" onclick="doSearch()">Search</a>
</div>

<script type="text/javascript">
var csrf = '<?php echo $this->security->get_csrf_hash();?>';
$(function(){
    var dg = $('#tt').datagrid({
        title:'Stock Report',
        singleSelect:true,
        idField:'currencyid',
        url:'Inventory/getWhStoreReport',
		method: 'get',
		fit: true,
		pagination: true,
		remoteFilter: true,
		toolbar:'#toolbar',
		showFooter: true,
		rownumbers: true,
		pageSize: 500,
        pageList: [500, 1000],
        columns:[[
            {field:'productcode',title:'Item Code',width:180},
            {field:'productitemdesc',title:'Description',width:250},
            {field:'bom',title:'BOM',width:100},
            {field:'khr',title:'INPUT',width:100},
            {field:'cn',title:'IN-CN',width:100},
            {field:'adjin',title:'ADJT',width:100},
            {field:'cpin',title:'CP IN',width:100},
            {field:'sal',title:'SALE',width:100},
            {field:'dn',title:'OUT',width:100},
            {field:'exp',title:'EXPO',width:100},
            {field:'adjout',title:'O-ADJ',width:100},
            {field:'cpout',title:'CP OUT',width:100},
            {field:'eom',title:'EOM',width:100},            
            {field:'retailsalesprice',title:'PRICE',width:100,align:'right', formatter:
            	function(val,row)
            	{
            		return number_format(val, '2', '.', ',') ;
            	}
        	},
            {field:'totalprice',title:'TOTAL PRICE',width:100,align:'right', formatter: 
            	function(val,row){
            		return number_format(val, '2', '.', ',') ;
            	}
        	}
        ]]
    });

    

    $('#location').combogrid({
		panelWidth:300,
		url: "posting/getLocation",
		idField:'locationcode',
		textField:'locationcode',
		mode:'remote',
		fitColumns:true,
		method: 'get',
		label: 'Outlet Code :',
		labelPosition: 'left',
		required: true,
		columns:[[
			{field:'locationid',title:'ID',width:20},
			{field:'locationcode',title:'Location Code',width:30},
			{field:'locationname',title:'Location Name',width:100}
		]],
		onChange:function(value){
			var g = $('#location').combogrid('grid');
			g.datagrid('load',{q:value});
		},
		onSelect: function(index,row){
			var codigo = row.labelcode;
			console.log(codigo);
		}
	});

});


function doSearch(){
	var outletcode = $('#location').combogrid('getText');
    $('#tt').datagrid({
    	url:'Inventory/getWhStoreReport/'+outletcode
    });
    var pager = $('#tt').datagrid().datagrid('getPager');
    pager.pagination({
        buttons:[{
            iconCls:'icon-print',
            handler:function(){
                alert('print');
            }
        }]
    }); 
}

</script>