<!DOCTYPE html>
<html>
<head>
	<?php echo link_tag('includes/printing/normalize.min.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/printing/paper.css','stylesheet','text/css');?>
	
	<style>
		@page { size: a4 }
	</style>
</head>
<body class="a4">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm">
	<?php
	
	$rs = $this->db->query("select * from transfernote where transfernoteid=".$transfernote);
	$row = $rs->row();
	?>
	<table style="width: 100%; border: 0px solid black;">
		<tr>
			<td style="width: 45%;" valign="top">
				<h2 style="text-align: left;">PT KELAB 21 RETAIL</h2>
			</td>
			<td style="">
				<h4 style="text-align: center;">Delivery Order Listing</h4>
				<table style="width: 100%;border: 0px solid black;">
					<tr>
						<td style="text-align: right; width: 50%">Raised Date</td>
						<td>:</td>
						<td><?php echo $row->createdatetime?></td>
					</tr>
					<tr>
						<td style="text-align: right; width: 50%">Do No</td>
						<td>:</td>
						<td><?php echo $row->transfernotenumber?></td>
					</tr>
					<tr>
						<td style="text-align: right; width: 50%">From</td>
						<td>:</td>
						<td><?php echo $row->fromlocation?></td>
					</tr>
					<tr>
						<td style="text-align: right; width: 50%">To</td>
						<td>:</td>
						<td><?php echo $row->tolocation?></td>
					</tr>
					<tr>
						<td style="text-align: right; width: 50%">Raised By</td>
						<td>:</td>
						<td><?php echo $row->raiseby?></td>
					</tr>
				</table>
			</td>
		</tr>
		
	</table>

	<table style="width: 100%;">
		<thead>
			<td style="background: #EFEFEF; border: 1px solid gray;">No</td>
			<td style="background: #EFEFEF; border: 1px solid gray;">Item Code / Description</td>
			<td style="background: #EFEFEF; border: 1px solid gray;">Category</td>
			<td style="background: #EFEFEF; border: 1px solid gray;">Sex</td>
			<td style="background: #EFEFEF; border: 1px solid gray;">Label</td>
			<td style="background: #EFEFEF; border: 1px solid gray;">Season</td>
			<td style="background: #EFEFEF; border: 1px solid gray;">Size</td>
			<td style="background: #EFEFEF; border: 1px solid gray;">Color</td>
			<td style="background: #EFEFEF; border: 1px solid gray;">Unit RSP</td>
			<td style="background: #EFEFEF; border: 1px solid gray;">QTY</td>
		</thead>
		<tbody>
			<?php
			$no = 0;
			$rsitem = $this->db->query('select transfernoteitem.*, pitem.descline1, pitem.productcode, pitem.productcategory, pitem.size, pitem.sex, pitem.productgroup, pitem.productseason, pitem.descline2 from transfernoteitem left join productitem pitem on pitem.productitemid=transfernoteitem.productitem where transfernoteitem.transfernote='.$transfernote);
			
			foreach($rsitem->result() as $row)
			{
				$no++;
				echo "<tr>";
				echo "	<td>".$no."</td>";
				echo "	<td>".$row->productcode." <br>".$row->descline1."</td>";
				echo "	<td>".$row->productcategory."</td>";
				echo "	<td>".$row->sex."</td>";
				echo "	<td>".$row->productgroup."</td>";
				echo "	<td>".$row->productseason."</td>";
				echo "	<td>".$row->size."</td>";
				echo "	<td>".$row->descline2."</td>";
				echo "	<td>".number_format($row->retailsalesprice)."</td>";
				echo "	<td>".$row->transferqty."</td>";
				echo "</tr>";
			}
			?>
		</tbody>
	</table>
	<hr>
	
	<article>*<?php echo $row->remark?></article>

	<table style="width: 100%;">
		<tr>
			<td style="width: 25%; text-align: center;"><h5>Received By :</h5></td>
			<td style="width: 25%; text-align: center;"><h5>Total No. Of Ctns :</h5></td>
			<td style="width: 25%; text-align: center;"><h5>Date :</h5></td>
			<td style="width: 25%; text-align: center;"><h5>Delivery By :</h5></td>
		</tr>
	</table>
  </section>

</body>
</html>