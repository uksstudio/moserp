<table id="tt" style="width: 100%"></table>

<div id="toolbar">
	<table style="width: 100%; border: 0px solid black;">
		<tr>
			<td style="width:150px"><input class="easyui-combogrid" name="fromlocation" id="fromlocation" style="width:150px"></td>
			<td style="width:150px"><input class="easyui-combogrid" name="tolocation" id="tolocation" style="width:150px"></td>
			
			<td style="width:130px">
				<input class="easyui-textbox" name="carton_num" id="carton_num" label="Carton Reff:" labelPosition="top" required="true" style="width:130px">
			</td>
			<td style="text-align: right;">
				<a id="btnSave" class="easyui-linkbutton"  data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'">Save</a>
        		<a id="btnRemove" class="easyui-linkbutton" data-options="iconCls:'icon-large-remove',size:'large',iconAlign:'top'">Remove</a>

        	</td>
		</tr>
	</table>
    
    
</div>


<div id="w" class="easyui-window" data-options="closed:true,iconCls:'icon-save'" style="padding:10px;">
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'center'" >
            <table id="propsize" style="width: 100%"></table>
        </div>
        <div data-options="region:'south',border:false" style="text-align:right;padding:5px 0 0;">
            <a class="easyui-linkbutton" data-options="iconCls:'icon-ok'" href="javascript:void(0)" onclick="addtogrid()" style="width:80px">Ok</a>
            <a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" href="javascript:void(0)" onclick="$('#w').window('close')" style="width:80px">Cancel</a>
        </div>
    </div>
</div>



<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<?php echo script_tag('includes/plugins/datagrid-cellediting.js');?>
<script type="text/javascript">
var csrf = '<?php echo $this->security->get_csrf_hash();?>';


$(function(){
    var dg = $('#tt').datagrid({
        title:'Carton Transfer',
        singleSelect:true,
        idField:'currencyid',
        url:'Inventory/getManualGridItems',
		method: 'get',
		fit: true,
		remoteFilter: true,
		toolbar:'#toolbar',
		showFooter: true,
		rownumbers: true,
		clickToEdit: true,
		columns:[[
            {field:'season',title:'Season',width:50},
            {field:'label',title:'Lbl',width:50},
            {field:'cate',title:'Cate',width:50},
            {field:'gender',title:'Gender',width:50},
            {field:'itemcode',title:'Product Code',width:150},
            {field:'style_desc',title:'Product Desc',width:200},
            {field:'descline1',title:'FBR Desc',width:200},
            {field:'proidqty',title:'CQTY',width:50, editor:{type:'numberbox',options:{precision:0}}, styler: 
				function(value,row,index)
				{
					return 'background-color:#ffee00;color:red;';
				}
			},
			{field:'intransit',title:'Intransit',width:50},
			{field:'qty',title:'QTY',width:50, styler:
				function (value, row, index)
				{
					if(row.proidqty < value)
					{
						return 'background-color:green;';		
					}
					else if (row.proidqty > value)
					{
						return 'background-color:red;';	
					}
					else{
						return 'background-color:#ffffff;color:black;';
					}
				}
			},
            {field:'retailsalesprice',title:'RSP',width:100,align:'right'}
        ]],
        rowStyler:function(index,row){
	        if (row.qty < row.proidqty){
	            return 'background-color:red;color:blue;font-weight:bold;';
	        }
	    }
    });

    

    $('#fromlocation').combogrid({
		panelWidth:300,
		url: "posting/getLocation",
		idField:'locationid',
		textField:'locationcode',
		mode:'remote',
		fitColumns:true,
		method: 'get',
		label: 'From Stock Room :',
		labelPosition: 'top',
		required: true,
		columns:[[
			{field:'locationid',title:'ID',width:20},
			{field:'locationcode',title:'Location Code',width:30},
			{field:'locationname',title:'Location Name',width:100}
		]],
		onChange:function(value){
			var g = $('#fromlocation').combogrid('grid');
			g.datagrid('load',{q:value});
		},
		onSelect: function(index,row){
			var codigo = row.locationcode;
		}
	});


	$('#tolocation').combogrid({
		panelWidth:300,
		url: "posting/getLocation",
		idField:'locationid',
		textField:'locationcode',
		mode:'remote',
		fitColumns:true,
		method: 'get',
		label: 'To Stock Room :',
		labelPosition: 'top',
		required: true,
		columns:[[
			{field:'locationid',title:'ID',width:20},
			{field:'locationcode',title:'Location Code',width:30},
			{field:'locationname',title:'Location Name',width:100}
		]],
		onChange:function(value){
			var g = $('#tolocation').combogrid('grid');
			g.datagrid('load',{q:value});
			
		},
		onSelect: function(index,row){
			
		}
	});

});


$('#carton_num').textbox({
	inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
		keyup:function(e){
			if(e.keyCode == 13)
			{
				var flocation = $('#fromlocation').combogrid('getText');
				var tlocation = $('#tolocation').combogrid('getText');
				var stylecode = $('#carton_num').textbox('getText');

				if(flocation != '' && tlocation != '') //SEASON (2) + STYLE CODE (7)
				{
					//getItems(flocation, tlocation, stylecode);
					//Inventory/getProductStockByCarton/flocation/tlocation/cartonno/csrf
					$('#tt').datagrid(
						{
							url: "Inventory/getProductStockByCarton/"+flocation+"/"+tlocation+"/"+stylecode+"/"+csrf
						}
					);
				}
				else
				{
					$.messager.alert('Failed','Please fill all field required.','error');
				}
			}
		}
	})

	
});

var Clickc = [];
$('#tt')
	.datagrid('enableCellEditing')
	.datagrid({
		onCellEdit: function (index, field)
		{
			var dg = $(this);
			var ed = dg.datagrid('getEditor', {index:index,field:field});
            if (ed){
              Clickc.index = index; // get the row that was clicked
      	      Clickc.field = field; // get the field which was clicked
      	      Clickc.value = $(ed.target).val();  //Get cell current value
            }
		},
		onEndEdit:function(index){
			var dg = $(this);
			var ed = dg.datagrid('getEditor', {index:index,field:Clickc.field});
            if (ed){
            	var row = dg.datagrid('getRows')[index];
            	dg.datagrid('refreshRow', index);
            	reloadFooterDetail();
            }
		}
	})
	.datagrid('gotoCell', {
    	index: 0,
    	field: 'itemcode'
});



	function reloadFooterDetail()
	{
		var data = $('#tt').datagrid('getData');
		var sum = 0;
		var ssum = 0;
		var sum_retail_price_roundup = 0;
		for (i = 0; i < data.rows.length; i++) {
			console.log(data.rows[i].qty);
		    sum += parseInt(data.rows[i].qty);
		    ssum += parseInt(data.rows[i].proidqty);
		}
		
		var rows = $('#tt').datagrid('getFooterRows');
		rows[0]['qty'] = sum;
		rows[0]['proidqty'] = ssum;
		$('#tt').datagrid('reloadFooter');

		//return sum;
	}

function getItems(floc, tloc, itemcode)
{
	$.post( "Inventory/getProductStockByCarton", 
		{
			csrf_name:csrf, 
			floc:floc,
			tloc:tloc,
			carontreff:itemcode
		},
		function( data ) {
			csrf = data.csrf_name;
	}, "json");
};


$("#btnSave").on('click', function()
	{
		var flocation = $('#fromlocation').combogrid('getText');
		var i_flocation = $('#fromlocation').combogrid('getValue');
		var tlocation = $('#tolocation').combogrid('getText');
		var i_tlocation = $('#tolocation').combogrid('getValue');
		var donumber = $('#carton_num').textbox('getText');

		var data = $('#tt').datagrid('getData');
		var rows = data.rows;
		if(rows.length <= 0)
		{
			$.messager.alert('Failed',"List is empty!",'error');	
			return;
		}

		if(flocation != '' && tlocation != '') 
		{
			var win = $.messager.progress({
		        title:'Please waiting',
		        msg:'Sending data...'
		    });
			$.post( "Inventory/commitCartonTransfer", 
				{
					csrf_name:csrf, 
					floc: flocation,
					tloc: tlocation,
					ifloc: i_flocation,
					itloc: i_tlocation,
					donumber: donumber,
					rows:JSON.stringify(rows)
				}, "json")
			.done(
		    	function(msg)
		    	{
		    		console.log(msg);
		    		var obj = jQuery.parseJSON( msg );
		    		csrf = obj.csrf_name;
		    		$.messager.progress('close');
					$('#tt').datagrid('reload');
					if(obj.status == 0)
					{
						$.messager.alert('Failed',obj.msgErr,'error');	
					}
					else
					{
						console.log('do printPage');
						$.messager.confirm('Transfer Out', 'Your DO Number is '+obj.donum+', want to print press OK!', function(r){
			                if (r){
			                    loadPrintDocument(this, "inventory/printTransferOut/"+obj.transferid);
			                }
			            });
						
					}

					
					console.log(obj);
		     	}
		     )
		    .fail(function(xhr, status, error) {
		        // error handling
		        $.messager.progress('close');
		        console.log(xhr.status);
		        console.log(error);
		        console.log(status);
		        $.messager.alert('Failed',xhr.status + "("+error+")",'error');
		    });
		}
		else
		{
			$.messager.alert('Failed','Please fill all field required.','error');
		}
	});

$('#btnRemove').on('click', function()
	{
		
		var row = $('#tt').datagrid('getSelected');
		if(!row)
		{
			$.messager.alert('Failed',"Please select one row!",'error');
			return;
		}


		$.post( "Inventory/remanualtrf2grid", 
			{
				csrf_name:csrf, 
				productcode: row.productcode
			},
			function( data ) {
				csrf = data.csrf_name;

				$('#tt').datagrid('reload');
				
				console.log(data);
		}, "json");
		
	}

);



</script>