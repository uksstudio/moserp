
<table id="dgcostingreview" class="easyui-datagrid"></table>


<div id="dlgItem" 
	class="easyui-window" 
	title="Add List Item" 
	data-options="modal:true,closed:true,iconCls:'icon-save',footer:'#footerDialog'" style="width:80%;height:80%;">
	
	<table id="dgdetailcostingreview" class="easyui-datagrid"></table>
	
</div>

<div id="footerDialog" style="padding:5px;"><a class="easyui-linkbutton" data-options="iconCls:'icon-ok'" href="javascript:void(0)" onclick="submitForm()" style="width:80px">Ok</a></div>
<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var newdata = <?php echo $json_currency?>;
	$(function () {
		$('#dgcostingreview').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:false,
			idField:'po_number',
			fit: true,
			rownumbers:true,
			url:'costing/getCostingReview',
			method: 'get',
			showFooter: true,
			view: detailview,
			columns:[[
				
				{field:'costingnumber',title:'Number',width:80},
				{field:'costingstatus',title:'Status',width:80},
				{field:'costingdate',title:'Costing Date',width:100},
				{field:'currency',title:'Curr',width:50},
				{field:'exchangerate',title:'Rate',width:50},
				{field:'importduty',title:'IMP Tax',width:90},
				{field:'luxurytax',title:'LUX Tax',width:90},
				{field:'freight',title:'Freight',width:90},
				{field:'handling',title:'Handling',width:90},
				{field:'other',title:'Other',width:90},
				{field:'total',title:'Total',width:100},
				{field:'action',title:'Action',width:130,align:'center',
					formatter:function(value,row,index){
						console.log(row.costingnumber);
						var costingnumber = "'"+row.costingnumber+"'";
						if(row.costingstatus == 'COSTING')
						{
							var e = '<a href="javascript:void(0)" id="btnDetail'+row.costingnumber+'" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="showDetail('+costingnumber+')">DETAIL</a> ';
							var f = '<a href="javascript:void(0)" id="" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="showDetail('+costingnumber+')">REVISE</a> ';
							var g = '<a href="javascript:void(0)" id="" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="costingVoid('+costingnumber+')">VOID</a> ';
							return e+" | "+g;
						}
						else if(row.costingstatus == 'VOID')
						{
							return '';
						}
						else
						{
							var e = '<a href="javascript:void(0)" class="easyui-linkbutton" onclick="showDetail('+costingnumber+')">Detail</a> ';
							return e;
						}
					}
				}
			]],
			rowStyler: function(index,row){
                if (row.costingstatus == 'VOID'){
                    return 'background-color:#6293BB;color:#fff;font-weight:bold;';
                }
            },
			detailFormatter:function(index,row){
				return '<div style="padding:2px;position:relative;"><table class="ddv"></table></div>';
			},
			onExpandRow: function(index,row){
				var ddv = $(this).datagrid('getRowDetail',index).find('table.ddv');
				ddv.datagrid({
					url:'costing/getPoByCostingNumber?costingnumber='+row.costingnumber + "&po_number=",
					method: 'get',
					fitColumns:true,
					singleSelect:true,
					rownumbers:true,
					loadMsg:'',
					height:'auto',
					showFooter: true,
					columns:[[
						{field:'po_number',title:'PO Number',width:100},
						{field:'qty',title:'Tot Qty',width:100},
						{field:'cost',title:'Total U-Cost',width:100},
						{field:'price',title:'Total U-Price',width:100},
						{field:'pbtax',title:'PB Tax',width:100},
						{field:'luxtax',title:'LUX Tax',width:100},
						{field:'freight',title:'Freight',width:100},
						{field:'handling',title:'Handling',width:100},
						{field:'insurance',title:'Insurance',width:100},
						{field:'total',title:'Total',width:100}
					]],

					onResize:function(){
						$('#dgcostingreview').datagrid('fixDetailRowHeight',index);
					},
					onLoadSuccess:function(){
						setTimeout(function(){
							$('#dgcostingreview').datagrid('fixDetailRowHeight',index);
						},0);
					}
				});
				$('#dgcostingreview').datagrid('fixDetailRowHeight',index);
			},
			onCheck:onCheck,
			onUncheck:onUncheck,
			onCheckAll:onCheckAll,
			onUncheckAll:onUncheckAll,
		});

		



	});


	function myformatter(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	}
	function myparser(s){
		if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
			return new Date(y,m-1,d);
		} else {
			return new Date();
		}
	}
	
	function onSelect(date){
		strTanggal = date.getFullYear()+"-"+("0" + (date.getMonth() + 1).toString()).substr(-2)+"-"+("0" + date.getDate().toString()).substr(-2);
	}
	var checkedRows = [];
	function onCheck(index,row){
		for(var i=0; i<checkedRows.length; i++){
			if (checkedRows[i].po_number == row.po_number)
			{
				row.chk = 'Y';
				return;
			}
		}
		row.chk = 'Y';
		checkedRows.push(row);
		
		console.log("onCheck:"+row.po_number);
	}
	function onUncheck(index,row){
		for(var i=0; i<checkedRows.length; i++){
			if (checkedRows[i].po_number == row.po_number){
				row.chk = 'N';
				checkedRows.splice(i,1);
				return;
			}
		}
		row.chk = 'N';
		console.log("onUncheck:"+index);
	}


	function onCheckAll(row){
		for(var i=0; i<row.length; i++){
			row[i].chk = 'Y';
		}
	}
	function onUncheckAll(row){
		for(var i=0; i<row.length; i++){
			row[i].chk = 'N';
		}
		
	}

	
	
	function parseFloat2Decimals(value) {
		return parseFloat(parseFloat(value).toFixed(2));
	}

	function roundUp(num, precision) {
	  precision = Math.pow(10, precision)
	  return Math.ceil(num * precision) / precision
	}

	function costingVoid(costingnumber)
	{
		$.messager.confirm('Confirm','Are you sure you want to VOID this COSTING PROCESS?',function(r){
			if (r){
				$.post('Costing/voidCosting',{csrf_name:csrf, costingnumber:costingnumber},function(result){
					if (result.status){
						csrf = result.csrf_name;
						$('#dgcostingreview').datagrid('reload');    // reload the user data
					} else {
						$.messager.show({    // show error message
							title: 'Error',
							msg: result.errorMsg
						});
					}
				},'json');
			}
		});
	}

	function showDetail(costingnumber)
	{
		$('#dlgItem').dialog({
			closed:false,
			iconCls:'icon-list-m1-edit',
			title:'&nbsp;Detail Costing Items',
			onLoad:function(){
				//$('#dgdetailcostingreview').datagrid('reload');
				
			}
		});

		$('#dgdetailcostingreview').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:false,
			idField:'po_number',
			fit: true,
			url:'Costing/detailCostingItem?cosnumber=' + costingnumber,
			method: 'get',
			rownumbers:true,
			columns:[[
				{field:'chk',title:'Invoice',width:100,checkbox:true},
				{field:'barcode',title:'Style Number',width:100},
				{field:'qty',title:'QTY',width:50},
				{field:'unitcost',title:'Unit Cost',width:100},
				{field:'unitprice',title:'Unit Price',width:100},
				{field:'biaya',title:'FOB',width:100},
				{field:'cogs',title:'Cogs',width:100},
				{field:'markup',title:'Markup',width:50},
				{field:'retail_price',title:'Retail U-Price',width:100},
				{field:'total_retail_price',title:'Total R-Price',width:100}
			]]
		});
	}
</script>