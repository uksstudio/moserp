<table id="tt" style="width: 100%"></table>

<div id="toolbar">
    <input class="easyui-textbox" name="barcode" id="barcode" style="width:30%">
    <input class="easyui-combogrid" name="location" id="location" style="width:20%">
    <a href="#" class="easyui-linkbutton" plain="true" onclick="doSearch()">Search</a>
</div>

<script type="text/javascript">
var csrf = '<?php echo $this->security->get_csrf_hash();?>';
$(function(){
    var dg = $('#tt').datagrid({
        title:'Inventory Stock',
        singleSelect:true,
        idField:'currencyid',
        url:'Inventory/getInventoryGoodStock',
		method: 'get',
		fit: true,
		pagination: true,
		remoteFilter: true,
		toolbar:'#toolbar',
		showFooter: true,
		rownumbers: true,
		pageSize: 500,
        pageList: [500, 1000],
        columns:[[
            {field:'locationcode',title:'STRC',width:50},
            {field:'lastdonumber',title:'TRNNO',width:100},
            {field:'season',title:'Season',width:50},
            {field:'label',title:'Lbl',width:50},
            {field:'category',title:'Cate',width:50},
            {field:'sex',title:'Gender',width:50},
            {field:'productcode',title:'Product Code',width:150},
            {field:'productitemdesc',title:'Product Desc',width:200},
            {field:'descline1',title:'FBR Desc',width:200},
            {field:'totalquantity',title:'QTY',width:50},
            {field:'intransit',title:'Transit',width:50},
            {field:'frozeqty',title:'Frozen',width:50},
            {field:'currentcost',title:'COST',width:100,align:'right'},
            {field:'retailsalesprice',title:'RSP',width:100,align:'right'}
        ]]
    });

    $('#barcode').textbox({
		label: 'Barcode',
		labelPosition: 'left',
		labelPosition: 'left'
	});

    $('#location').combogrid({
		panelWidth:300,
		url: "posting/getLocation",
		idField:'locationcode',
		textField:'locationcode',
		mode:'remote',
		fitColumns:true,
		method: 'get',
		label: 'Outlet Code :',
		required: true,
		columns:[[
			{field:'locationid',title:'ID',width:20},
			{field:'locationcode',title:'Location Code',width:30},
			{field:'locationname',title:'Location Name',width:100}
		]],
		onChange:function(value){
			var g = $('#location').combogrid('grid');
			g.datagrid('load',{assay_type_id:value});
			console.log("find id : "+value);
		},
		onSelect: function(index,row){
			var codigo = row.labelcode;
			console.log(codigo);
		}
	});

});



function doSearch(){
    $('#tt').datagrid('load',{
        outletcode: $('#location').combogrid('getText')
    });
}

</script>