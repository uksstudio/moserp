<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo TITLE_PAGE?></title>
	<?php echo link_tag('includes/themes/default/easyui.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/themes/icon.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/themes/color.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/demo/demo.css','stylesheet','text/css');?>
	<?php echo script_tag('includes/jquery.min.js');?>
    <?php echo script_tag('includes/jquery.easyui.min.js');?>
	<style>
	* { margin: 0; padding: 0; }
		
		html { 
			background: url(/mosERP/includes/themes/background/login-register.jpg) no-repeat center center fixed; 
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
		}
	</style>
</head>
<body class="login-register">
	<div class="login-box widget">

		<h2>mosERP System</h2>
		<div style="margin:20px 0;"></div>
		<?php echo form_open('login/oAuth','id="ff"');?>
		<div class="easyui-panel" style="width:400px;padding:50px 60px;">
			<div style="margin-bottom:20px">
				<input class="easyui-textbox" prompt="Username" iconWidth="28" style="width:100%;height:34px;padding:10px;" name="username" data-options="required:true">
			</div>
			<div style="margin-bottom:20px">
				<input id="pass" class="easyui-passwordbox" prompt="Password" iconWidth="28" style="width:100%;height:34px;padding:10px" name="password" data-options="required:true">
			</div>
			<div style="text-align:left;padding:5px 0">
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">Submit</a>
		    </div>
		</div>
		<?echo form_close();?>
		<div id="viewer"></div>
	</div>
 
    <script type="text/javascript">
		var base_url = "<?php echo base_url();?>";
		function submitForm(){
            $('#ff').form('submit',{
				url:base_url+"index.php/login/oAuth",
				onSubmit:function(){
					return $(this).form('enableValidation').form('validate');
				},
				success:function(data){
					var json = $.parseJSON(data);
					$('input[name="csrf_name"]').val(json.csrf);
					if(json.status == 0)
					{
						$.messager.alert('Failed',json.msg,'error');
					}
					else
					{
						document.location.href = base_url+"index.php/dashboard";
					}
					
				}
			}, "json");
        }
        $('#pass').passwordbox({
            inputEvents: $.extend({}, $.fn.passwordbox.defaults.inputEvents, {
                keypress: function(e){
					if(e.keyCode == 13)
					{
						submitForm();
					}
					else
					{
						var char = String.fromCharCode(e.which);
						$('#viewer').html(char).fadeIn(200, function(){
							$(this).fadeOut();
						});
					}
                }
            })
        })
		
    </script>
    <style>
        #viewer{
            position: relative;
            padding: 0 60px;
            top: -70px;
            font-size: 54px;
            line-height: 60px;
        }
		body {
			margin:50px 0; 
			padding:0;
			text-align:center;
		}
		body.login{background-color:#EEF;height:0;overflow:hidden;}
		.login-box{
			margin:0 auto!important;
			margin-top:-125px!important;
			margin-left:-175px!important;
			position:absolute;
			top:40%;
			left:50%;
			width:350px;
		}

		.login-register {
			background-size: cover;
			background-repeat: no-repeat;
			background-position: center center;
			height: 100%;
			width: 100%;
			padding: 0 0;
			position: fixed; }
		.login-sidebar {
  padding: 0px;
  margin-top: 0px; }
  .login-sidebar .login-box {
    right: 0px;
    position: absolute;
    height: 100%; }
    </style>
</body>
</html>