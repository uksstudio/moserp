<?php
	$_split_row = 25;
	$_repeat_header = true;
	$no = 0;
	$page = 0;
	$rows = array();
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo link_tag('includes/printing/normalize.min.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/printing/paper.css','stylesheet','text/css');?>
	<style>
		@page { size: A4}
		

		body {
		    counter-reset: chapter;      /* Create a chapter counter scope */
		}
		
		table.summary
		{
			
			font-size: 12px;
		}

		table.header
		{
			border-spacing: 0px;
			border-collapse: separate;
			height: 35mm;
		}

		table.header td
		{
			font-size: 15px;

		}

		table.header td div.title_label
		{
			float: left;
			width: 200px;
			border: 0px solid gray;
			margin-bottom: 2px;
			background: #ffffff;
			font-size: 12px;
		}
		table.header td div.title_label span
		{
			width: 100px;
			display: inline-block;
			background: #efefef;
			padding: 2px
			
		}


		table.rows
		{
			border-spacing: 0px;
			border-collapse: separate;
			page-break-inside:auto;
		}
		table.rows thead td
		{
			font-size: 12px;
			padding: 5px;
			background: #efefef;
		}

		table.rows tbody tr
		{
			
		}

		table.rows tbody td.items
		{
			font-size: 11px;
			border-bottom: 1px solid gray;
			padding: 5px;
			page-break-inside:avoid;
		}

		.payto
		{
			font-size: 11px;
			page-break-inside:avoid;
		}

		table.rows tbody td.total
		{
			font-size: 12px;
			padding: 5px;
			page-break-inside:avoid;
		}
	</style>
</head>
<body class="A4">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

	<?php
	
	$pvnumber = $this->uri->segment(3);
	
	$rs = $this->db->query("select aks.*, (select name from acc_coa where subcode=aks.accountno) as namaakun  from acc_gl_journal_detail aks left join acc_gl_journal ak on aks.kasid=ak.id where ak.voucherno='".$pvnumber."' and accountno <> '".$akun."'");
	
	$total_amount = 0;
	foreach ($rs->result() as $row) {
		$no++;
		if(($no % $_split_row) == 0) $page++;
		
		$item = "<td class='items'>".$no."</td>";
		$item .= "<td class='items'>".$row->description."</td>";
		$item .= "<td class='items'>".$row->typedk."</td>";
		$item .= "<td class='items' style='text-align: right;'>".$row->wcon."".number_format($row->amount)."</td>";
		$item .= "<td class='items'>".$row->accountno." - ".$row->namaakun."</td>";

		$rows[$page][] = $item;
		$total_amount += $row->amount;
	}


	function _setHeaderTable($type, $header, $page, $pageoff)
	{
		
		$title = ($type == "PV") ? "PAYMENT VOUCHER" : "RECEIVE VOUCHER";
		$user = $header['rows'][0]->createby;
		$headers = '<table style="width: 100%; " class="header">';
		$headers .= '<tr>';
		$headers .= '	<td valign="top" style="width: 40%">';
		$headers .= '		<p>PT KELAB 21 RETAIL</p>';
		$headers .= '		<small>DIPO BUSINESS CENTER 9th Floor, Unit F-G<br>';
		$headers .= '		JL Gatot Subroto Kav 51-52 10260 Jakarta<br>';	
		$headers .= '		NPWP : <br></small>';
		$headers .= '		<p class="payto"><span>DIBAYARKAN : '.$header['rows'][0]->paymentto.'</span><br><br>';
		$headers .= '		KETERANGAN : '.$header['rows'][0]->remark.'</p>';
		$headers .= '	</td>';
		$headers .= '	<td valign="top" >';
		$headers .= '		<p>'.$title.'</p>';
		$headers .= '		<table >';
		$headers .= '			<tr>';
		$headers .= '				<td valign="top" style="width: 40%; ">';
		$headers .= '					<div class="title_label"><span>KODE</span> '.$header['rows'][0]->coaccount.'</div>';
		$headers .= '					<div class="title_label"><span>No Voucher</span> '.$header['rows'][0]->voucherno.'</div>';
		$headers .= '					<div class="title_label"><span>Tanggal</span> '.$header['rows'][0]->parsedate.'</div>';
		$headers .= '					<div class="title_label"><span>User</span> '.$user.'</div>';
		$headers .= '				</td>';
		$headers .= '				<td valign="top" >';
		$headers .= '					<table style="border: 0px solid grey;" width="100%"><tr><td colspan=5 style="text-align: center;border: 1px solid grey;">CHECK LIST</td></tr><tr><td style="border: 1px solid grey;">PO</td><td style="border: 1px solid grey;">INV</td><td style="border: 1px solid grey;">FP</td><td style="border: 1px solid grey;">DO</td><td style="border: 1px solid grey;">OTH</td></tr><tr><td style="border: 1px solid grey;">&nbsp;</td><td style="border: 1px solid grey;">&nbsp;</td><td style="border: 1px solid grey;">&nbsp;</td><td style="border: 1px solid grey;">&nbsp;</td><td style="border: 1px solid grey;">&nbsp;</td></tr></table>';
		$headers .= '				</td>';
		$headers .= '			</tr>';
		$headers .= '		</table>';
		$headers .= '	</td>';
		
		$headers .= '</tr>';
		$headers .= '</table>';
		
		return $headers;
	}


	for($page=0; $page < count($rows); $page++)
	{
		echo "<section class='sheet padding-10mm'>";
		if($_repeat_header) echo _setHeaderTable($type, $header, ($page+1), count($rows));
		else
			if($page == 0) echo _setHeaderTable($type, $header, ($page+1), count($rows));
		

		// table rows / item nya
		// dan ini headernya, tiap page ada headernya
		echo '<table style="width: 100%;" class="rows">';
		echo '<thead>';
		echo '	<td >No</td>';
		echo '	<td width="40%">Keterangan</td>';
		echo '	<td width="5%">D/K</td>';
		echo '	<td width="15%" style="text-align: center;">Rupiah</td>';
		echo '	<td >Perkiraan</td>';	
		echo '</thead>';
		echo '<tbody>';
		foreach ($rows[$page] as $td) {
			echo '<tr>';
			echo $td;
			echo '</tr>';
		}
		

		echo '</tbody>';
		echo '</table>';

		if(($page+1) == count($rows))
		{
			echo "<p class='payto'>No Cek/Bilyet : ".$header['rows'][0]->ceknumber."</p>";
			echo "<p class='payto'>Terbilang : ".Terbilang($total_amount)."</p>";
			echo "<br>";

			echo "<table style='width: 100%' class='summary'>";
			echo "<tr>";
			echo "	<td style='width: 25%; text-align:center;'>Menyetujui</td>";
			echo "	<td style='width: 25%; text-align:center;'>Diperika</td>";
			echo "	<td style='width: 25%; text-align:center;'>Dikerjakan Oleh</td>";
			echo "</tr>";
			echo "</table>";
		}

		echo "</section>";
	}
	?>
  
</body>
</html>