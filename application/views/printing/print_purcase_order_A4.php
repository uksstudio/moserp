<?php
	$_split_row = 25;
	$_repeat_header = true;
	$no = 0;
	$page = 0;
	$rows = array();
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo link_tag('includes/printing/normalize.min.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/printing/paper.css','stylesheet','text/css');?>
	<style>
		@page { size: A4}
		

		body {
		    counter-reset: chapter;      /* Create a chapter counter scope */
		}
		
		table.summary
		{
			
			font-size: 12px;
		}

		table.header
		{
			border-spacing: 0px;
			border-collapse: separate;
			height: 35mm;
		}

		table.header td
		{
			font-size: 15px;

		}

		table.header td div.title_label
		{
			float: left;
			width: 200px;
			border: 0px solid gray;
			margin-bottom: 2px;
			background: #ffffff;
			font-size: 12px;
		}
		table.header td div.title_label span
		{
			width: 100px;
			display: inline-block;
			background: #efefef;
			padding: 2px
			
		}


		table.rows
		{
			border-spacing: 0px;
			border-collapse: separate;
			page-break-inside:auto;
		}
		table.rows thead td
		{
			font-size: 12px;
			padding: 5px;
			background: #efefef;
		}

		table.rows tbody tr
		{
			
		}

		table.rows tbody td.items
		{
			font-size: 11px;
			border-bottom: 1px solid gray;
			padding: 5px;
			page-break-inside:avoid;
		}

		table.rows tbody td.total
		{
			font-size: 12px;
			padding: 5px;
			page-break-inside:avoid;
		}
	</style>
</head>
<body class="A4">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

	<?php
	$cosnumber = $this->uri->segment(3);

	$rs = $this->db->query("select trnote.*, 
		proit.productcode, proit.productitemdesc, proit.sex, proit.size, proit.descline2 as color,
		(select productcategorycode from productcategory where productcategoryid=proit.productcategory) as cate,
		(select productgroupcode from productgroup where productgroupid=proit.productgroup) as label,
		(select productseasoncode from productseason where productseasonid=proit.productseason) as season
		from post_report_items_detail trnote left join productitem proit on proit.productitemid=trnote.productitemid left join post_report_items_header pitem ON pitem.reportid=trnote.post_report_header_id where pitem.costing_number='".$cosnumber."'");
	
	$total_qty = 0;
	foreach ($rs->result() as $row) {
		$no++;
		if(($no % $_split_row) == 0) $page++;

		$item = "<td class='items'>".$no."</td>";
		$item .= "<td class='items'>".$row->productcode."</td>";
		$item .= "<td class='items'>".$row->cate."</td>";
		$item .= "<td class='items'>".$row->sex."</td>";
		$item .= "<td class='items'>".$row->label."</td>";
		$item .= "<td class='items'>".$row->season."</td>";
		$item .= "<td class='items'>".$row->size."</td>";
		$item .= "<td class='items'>".$row->color."</td>";
		$item .= "<td class='items' style='text-align: right;'>".number_format($row->retailprice)."</td>";
		$item .= "<td class='items'>".$row->qty."</td>";

		$rows[$page][] = $item;
		$total_qty += $row->qty;
	}


	function _setHeaderTable($header, $page, $pageoff)
	{
		$headers = '<table style="width: 100%; " class="header">';
		$headers .= '<tr>';
		$headers .= '	<td valign="top" style="width: 40%">';
		$headers .= '		<p>PT KELAB 21 RETAIL</p>';
		$headers .= '		<small>DIPO BUSINESS CENTER 9th Floor, Unit F-G<br>';
		$headers .= '		JL Gatot Subroto Kav 51-52 10260 Jakarta<br>';	
		$headers .= '		NPWP : <br></small>';	
		$headers .= '	</td>';
		$headers .= '	<td valign="top" >';
		$headers .= '		<p>LAPORAN BARANG MASUK</p>';
		$headers .= '		<div class="title_label"><span>Costing Reff</span> '.$header['rows'][0]['costingnumber'].'</div>';
		$headers .= '		<div class="title_label"><span>Costing Date</span> '.$header['rows'][0]['costingdate'].'</div>';
		$headers .= '		<div class="title_label"><span>PIB</span> '.$header['rows'][0]['pibnumber'].'</div>';
		$headers .= '		<div class="title_label"><span>AWB</span> '.$header['rows'][0]['awbnumber'].'</div>';
		$headers .= '		<div class="title_label"><span>Invoice No</span> '.$header['rows'][0]['invoicenumber'].'</div>';
		$headers .= '	</td>';
		//$headers .= '	<td valign="top" >';
		//$headers .= '		<p>&nbsp;</p>';
		//$headers .= '		<div class="title_label"><span>Curr/rate</span> '.$header['rows'][0]['currency'].'/'.$header['rows'][0]['exchangerate'].'</div>';
		//$headers .= '		<div class="title_label"><span>IMP Tax</span> '.$header['rows'][0]['importduty'].'</div>';
		//$headers .= '		<div class="title_label"><span>LUX Tax</span> '.$header['rows'][0]['luxurytax'].'</div>';
		//$headers .= '		<div class="title_label"><span>Freight</span> '.$header['rows'][0]['freight'].'</div>';
		//$headers .= '		<div class="title_label"><span>Handling</span> '.$header['rows'][0]['handling'].'</div>';
		//$headers .= '		<div class="title_label"><span>Other</span> '.$header['rows'][0]['other'].'</div>';
		//$headers .= '		<div class="title_label"><span>Total</span> '.$header['rows'][0]['total'].'</div>';
		//$headers .= '	</td>';
		$headers .= '</tr>';
		$headers .= '</table>';
		return $headers;
	}


	for($page=0; $page < count($rows); $page++)
	{
		echo "<section class='sheet padding-10mm'>";
		if($_repeat_header) echo _setHeaderTable($header, ($page+1), count($rows));
		else
			if($page == 0) echo _setHeaderTable($header, ($page+1), count($rows));
		

		// table rows / item nya
		// dan ini headernya, tiap page ada headernya
		echo '<table style="width: 100%;" class="rows">';
		echo '<thead>';
		echo '	<td >No</td>';
		echo '	<td >Item Code/Description</td>';
		echo '	<td >Cate</td>';
		echo '	<td >Sex</td>';	
		echo '	<td >Label</td>';
		echo '	<td >Season</td>';
		echo '	<td >Size</td>';
		echo '	<td >Color</td>';
		echo '	<td >Unit RSP</td>';
		echo '	<td >Qty</td>';
		echo '</thead>';
		echo '<tbody>';
		foreach ($rows[$page] as $td) {
			echo '<tr>';
			echo $td;
			echo '</tr>';
		}
		//total 
		if(($page+1) == count($rows))
		{
			echo "<tr><td colspan='9' style='text-align: right;padding-right: 50px;' class='total'>Total Qty</td><td class='total'>".$total_qty."</td></tr>";
		}

		echo '</tbody>';
		echo '</table>';

		if(($page+1) == count($rows))
		{
			//echo "<p>Note : ".$header['rows'][0]->remark."</p>";
			echo "<br><br>";

			echo "<table style='width: 100%' class='summary'>";
			echo "<tr>";
			echo "	<td style='width: 25%; text-align:center;'>Received By</td>";
			echo "	<td style='width: 25%; text-align:center;'>Total of Ctns</td>";
			echo "	<td style='width: 25%; text-align:center;'>Date</td>";
			echo "	<td style='width: 25%; text-align:center;'>Delivery By</td>";
			echo "</tr>";
			echo "</table>";
		}

		echo "</section>";
	}
	?>
  
</body>
</html>