<?php
	$_split_row = 14;
	$_repeat_header = true;
	$no = 0;
	$page = 0;
	$rows = array();
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo link_tag('includes/printing/normalize.min.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/printing/paper.css','stylesheet','text/css');?>
	<style>
		@page { size: A4 landscape}
		

		body {
		    counter-reset: chapter;      /* Create a chapter counter scope */
		}
		
		table.summary
		{
			
			font-size: 12px;
		}

		table.header
		{
			border-spacing: 0px;
			border-collapse: separate;
			height: 35mm;
		}

		table.header td
		{
			font-size: 15px;

		}

		table.header td div.title_label
		{
			float: left;
			width: 200px;
			border: 0px solid gray;
			margin-bottom: 2px;
			background: #ffffff;
			font-size: 12px;
		}
		table.header td div.title_label span
		{
			width: 100px;
			display: inline-block;
			background: #efefef;
			padding: 2px
			
		}


		table.rows
		{
			border-spacing: 0px;
			border-collapse: separate;
			page-break-inside:auto;
		}
		table.rows thead td
		{
			font-size: 12px;
			padding: 5px;
			background: #efefef;
			border-right: 1px solid gray;
		}



		table.rows tbody tr
		{
			
		}

		table.rows tbody td.items
		{
			font-size: 11px;
			border-bottom: 1px solid gray;
			border-right: 1px solid gray;
			padding: 5px;
			page-break-inside:avoid;
		}

		.payto
		{
			font-size: 14px;
		}

		p.terbilang{
			font-size: 14px;	
		}
		table.rows tbody td.total
		{
			font-size: 12px;
			padding: 5px;
			page-break-inside:avoid;
		}
	</style>
</head>
<body class="A4 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

	<?php
	
	$id = $this->uri->segment(3);
	
	$rs = $this->db->query("select aks.*, ak.*  from acc_cash_advance_detail aks left join acc_cash_advance ak on aks.cashadvanceid=ak.id where ak.id=".$id);
	
	$total_amount = 0;
	$field = array();
	foreach ($rs->result() as $row) {
		$no++;
		if(($no % $_split_row) == 0) $page++;
		
		$item = "<td class='items'></td>";
		$item .= "<td class='items'>".$row->description."</td>";
		$item .= "<td class='items'>".number_format($row->amount)."</td>";
		
		$rows[$page][] = $item;
		$total_amount += $row->amount;
		array_push($field, $row);
	}

	//print_r($field);
	for($page=0; $page < count($rows); $page++)
	{
		echo "<section class='sheet padding-10mm'>";
		

		// table rows / item nya
		// dan ini headernya, tiap page ada headernya
		echo "<h4>ADVANCE PAYMENT</h4>";
		echo '<table style="width: 100%;" class="rows">';
		echo '<tr ><td width="48%" style="" valign="top">';

		echo '<table style="width: 40%;margin-bottom: 10px; text-align: right; float: right" class="payto"><tr><td width="10%" style="border: 1px solid gray; ">No.</td><td style="border: 1px solid gray; text-align: left">'.$field[0]->canumber.'</td></tr><tr><td width="10%" style="border: 1px solid gray; ">Date.</td><td style="border: 1px solid gray;text-align: left">'.$field[0]->issuedate.'</td></tr></table>';

		echo '<table style="width: 80%;margin-bottom: 10px;" class="payto"><thead><td width="10%" style="border: 1px solid gray; ">Pay To</td><td style="border: 1px solid gray; text-align: left" width="50%">'.$field[0]->issuename.'</td></thead></table>';

		echo '<table style="width: 100%;border: 1px solid gray;" class="rows">';
		echo '<thead>';
		echo '	<td width="15%">Account No</td>';
		echo '	<td >Keterangan</td>';
		echo '	<td width="20%">Rupiah</td>';
		
		echo '</thead>';
		echo '<tbody>';

		for($i = 0; $i < $_split_row; $i++)
		{
			if($i <= count($rows[$page])-1)
			{
				echo '<tr>';
				echo $rows[$page][$i];
				echo '</tr>';
			}
			else
			{
				echo '<tr>';
				echo "<td class='items'>&nbsp;</td>";
				echo "<td class='items'>&nbsp;</td>";
				echo "<td class='items'>&nbsp;</td>";
				
				echo '</tr>';	
			}
			
		}
		echo "<tr><td class='items' colspan=2 style='text-align: right;'>TOTAL</td><td class='items'>".number_format($total_amount)."</td></tr>";
		echo "<tr><td class='items' colspan=2 style='text-align: left;'>&nbsp;</td><td class='items'></td></tr>";
		echo "<tr><td class='items' colspan=2 style='text-align: left;'>&nbsp;</td><td class='items'></td></tr>";

		echo '</tbody>';
		echo '</table>';


		echo "<p class='terbilang'>Terbilang : ".Terbilang($total_amount)."</p>";

		echo "<table style='width: 100%' class='summary'>";
		echo "<tr>";
		echo "	<td style='width: 100%; text-align:left; border-top: 1px solid gray;border-bottom: 1px solid gray;' colspan=2>METHOD OF PAYMENT</td>";
		echo "</tr>";
		echo "<tr>";
		echo "	<td style='width: 50%; text-align:left;border-bottom: 1px solid gray;'>BANK</td>";
		echo "	<td style='width: 50%; text-align:left; border-bottom: 1px solid gray;'>C/BG NO.</td>";
		echo "</tr>";
		echo "</table>";

		echo "<table style='width: 100%' class='summary'>";
		echo "<tr>";
		echo "	<td style='width: 20%; text-align:center; border-top: 1px solid gray;'>Applied By</td>";
		echo "	<td style='width: 20%; text-align:center; border-top: 1px solid gray;'>Dept mgr</td>";
		echo "	<td style='width: 20%; text-align:center; border-top: 1px solid gray;'>Approved By</td>";
		echo "	<td style='width: 20%; text-align:center; border-top: 1px solid gray;'>Paid By</td>";
		echo "	<td style='width: 20%; text-align:center; border-top: 1px solid gray;'>Received By</td>";
		echo "</tr>";
		echo "</table>";


		echo '</td><td width="2%">&nbsp;</td><td width="48%" style="" valign="top">';

		echo '<table style="width: 40%;margin-bottom: 10px; text-align: right; float: right" class="payto"><tr><td width="10%" style="border: 1px solid gray; ">No.</td><td style="border: 1px solid gray; "></td></tr><tr><td width="10%" style="border: 1px solid gray; ">Date.</td><td style="border: 1px solid gray; "></td></tr></table>';

		echo '<table style="width: 80%;margin-bottom: 10px;" class="payto"><thead><td width="10%" style="border: 1px solid gray; ">Pay To</td><td style="border: 1px solid gray; " width="50%"></td></thead></table>';

		echo '<table style="width: 100%;border: 1px solid gray;" class="rows">';
		echo '<thead>';
		echo '	<td width="15%">Account No</td>';
		echo '	<td >Keterangan</td>';
		echo '	<td width="20%">Rupiah</td>';
		echo '</thead>';
		echo '<tbody>';
		for($i = 0; $i < $_split_row; $i++)
		{
			echo '<tr>';
			echo "<td class='items'>&nbsp;</td>";
			echo "<td class='items'>&nbsp;</td>";
			echo "<td class='items'>&nbsp;</td>";
			echo '</tr>';	
			
		}
		echo "<tr><td class='items' colspan=2 style='text-align: left;'>TOTAL</td><td class='items'></td></tr>";
		echo "<tr><td class='items' colspan=2 style='text-align: left;'>ADVANCE</td><td class='items'></td></tr>";
		echo "<tr><td class='items' colspan=2 style='text-align: left;'>SURPLUS / SHORTAGE ADVANCE</td><td class='items'></td></tr>";
		
		

		echo '</tbody>';
		echo '</table>';

		echo "<p class='terbilang'>Terbilang : </p>";

		echo "<table style='width: 100%' class='summary'>";
		echo "<tr>";
		echo "	<td style='width: 100%; text-align:left; border-top: 1px solid gray;border-bottom: 1px solid gray;' colspan=2>METHOD OF PAYMENT</td>";
		echo "</tr>";
		echo "<tr>";
		echo "	<td style='width: 50%; text-align:left;border-bottom: 1px solid gray;'>BANK</td>";
		echo "	<td style='width: 50%; text-align:left; border-bottom: 1px solid gray;'>C/BG NO.</td>";
		echo "</tr>";
		echo "</table>";


		echo "<table style='width: 100%' class='summary'>";
		echo "<tr>";
		echo "	<td style='width: 20%; text-align:center; border-top: 1px solid gray;'>Applied By</td>";
		echo "	<td style='width: 20%; text-align:center; border-top: 1px solid gray;'>Dept mgr</td>";
		echo "	<td style='width: 20%; text-align:center; border-top: 1px solid gray;'>Approved By</td>";
		echo "	<td style='width: 20%; text-align:center; border-top: 1px solid gray;'>Paid By</td>";
		echo "	<td style='width: 20%; text-align:center; border-top: 1px solid gray;'>Received By</td>";
		echo "</tr>";
		echo "</table>";

		echo "</td></tr>";
		echo '</table>';

		echo "</section>";
	}
	?>
  
</body>
</html>