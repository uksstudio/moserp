<!DOCTYPE html>
<html>
<head>
	<?php echo link_tag('includes/printing/normalize.min.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/printing/paper.css','stylesheet','text/css');?>
	
	<style>
		body {
		    counter-reset: chapter;      /* Create a chapter counter scope */
		}
		@page { size: A4}
		
		table.summary
		{
			
			font-size: 12px;
		}

		table.header
		{
			border-spacing: 0px;
			border-collapse: separate;
			height: 35mm;
		}

		table.header td
		{
			font-size: 15px;

		}

		table.header td div.title_label
		{
			float: left;
			width: 190px;
			border: 0px solid gray;
			margin-bottom: 2px;
			background: #ffffff;
			font-size: 12px;
		}
		table.header td div.title_label span
		{
			width: 100px;
			display: inline-block;
			background: #efefef;
			padding: 2px
			
		}


		table.rows
		{
			border-spacing: 0px;
			border-collapse: separate;
			page-break-inside:auto;
		}
		table.rows thead td
		{
			font-size: 12px;
			padding: 5px;
			background: #efefef;
		}

		table.rows tbody tr
		{
			
		}

		table.rows tbody td.items
		{
			font-size: 11px;
			border-bottom: 1px dashed gray;
			padding: 5px;
			page-break-inside:avoid;
		}

		table.rows tbody td.total
		{
			font-size: 15px;
			padding: 5px;
			page-break-inside:avoid;
		}
	</style>
</head>
<body class="A4">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

	<?php
	$_split_row = 25;
	$_repeat_header = true;
	
	$type = $header['rows'][0]->adjustmenttype;


	$this->db->select("staitem.*, pitem.productitemdesc, pitem.sex, pitem.size, pitem.descline2 as color,
		(select productcategorycode from productcategory where productcategoryid=pitem.productcategory) as cate,
		(select productgroupcode from productgroup where productgroupid=pitem.productgroup) as label,
		(select productseasoncode from productseason where productseasonid=pitem.productseason) as season");
	$this->db->join("productitem pitem", "pitem.productitemid=staitem.productitem", "LEFT");
	$this->db->from("stockadjustmentitem staitem");
	$this->db->where("staitem.stockadjustment", (int)$header['rows'][0]->stockadjustmentid);

	$rs = $this->db->get();
	$no = 0;
	$page = 0;
	$rows = array();
	$total = 0;
	foreach ($rs->result() as $row) {
		$no++;
		if(($no % $_split_row) == 0) $page++;
		if($type == 6)
		{
			$rows[$page][] = "<td class='items'>".$no."</td>
			<td class='items'>".$row->productitemcode."<br>".$row->productitemdesc."</td>
			<td class='items'>".$row->cate."</td>
			<td class='items'>".$row->sex."</td>			
			<td class='items'>".$row->label."</td>
			<td class='items'>".$row->season."</td>
			<td class='items'>".$row->size."</td>
			<td class='items'>".$row->color."</td>
			<td class='items'>".number_format($row->nfbrsp)."</td>
			<td class='items'>".number_format($row->lpprice)."</td>
			<td class='items'>".$row->adjustmentqty."</td>";
		}
		else if($type == 5)
		{
			$rows[$page][] = "<td class='items'>".$no."</td>
			<td class='items'>".$row->productitemcode."<br>".$row->productitemdesc."</td>
			<td class='items'>".$row->cate."</td>
			<td class='items'>".$row->sex."</td>			
			<td class='items'>".$row->label."</td>
			<td class='items'>".$row->season."</td>
			<td class='items'>".$row->size."</td>
			<td class='items'>".$row->color."</td>
			<td class='items'>".number_format($row->lpprice)."</td>
			<td class='items'>".$row->adjustmentqty."</td>";
		}
		$total += $row->adjustmentqty;
	}

	function _setHeaderTable($header, $page, $pageoff)
	{
		$type = $header['rows'][0]->adjustmenttype;
		$user = $header['rows'][0]->username;
		$headers = '<table style="width: 100%; " class="header">';
		$headers .= '<tr>';
		$headers .= '	<td valign="top" style="width: 40%">';
		$headers .= '		<p>PT KELAB 21 RETAIL</p>';
		$headers .= '		<small>DIPO BUSINESS CENTER 9th Floor, Unit F-G<br>';
		$headers .= '		JL Gatot Subroto Kav 51-52 10260 Jakarta<br>';	
		$headers .= '		NPWP : <br></small>';	
		$headers .= '	</td>';
		$headers .= '	<td valign="top" >';
		$headers .= '		<p>STOCK ADJUSTMENT NOTE</p>';
		$headers .= '		<table >';
		$headers .= '			<tr>';
		$headers .= '				<td valign="top" style="width: 40%; ">';
		$headers .= '					<div class="title_label"><span>Doc No</span> '.$header['rows'][0]->stockadjustmentnumber.'</div>';
		$headers .= '					<div class="title_label"><span>Date</span> '.$header['rows'][0]->raisedate.'</div>';
		$headers .= '					<div class="title_label"><span>User</span> '.$user.'</div>';
		$headers .= '					<div class="title_label"><span>TAX Number</span> </div>';
		$headers .= '				</td>';
		$headers .= '				<td valign="top" >';
		$headers .= '					<div class="title_label"><span>Page</span> '.$page.' of '.$pageoff.'</div>';
		$headers .= '					<div class="title_label"><span>Reason</span> '.$type = ($type == 5) ? 'NA/PA' : 'C PRICE'.'</div>';
		$headers .= '					<div class="title_label"><span>Remark</span> '.$header['rows'][0]->remark.'</div>';
		$headers .= '				</td>';
		$headers .= '			</tr>';
		$headers .= '		</table>';
		
		$headers .= '	</td>';
		$headers .= '</tr>';
		$headers .= '</table>';
		return $headers;
	}
	

	for($page=0; $page < count($rows); $page++)
	{
		echo "<section class='sheet padding-10mm'>";
		if($_repeat_header) echo _setHeaderTable($header, ($page+1), count($rows));
		else
			if($page == 0) echo _setHeaderTable($header, ($page+1), count($rows));
		

		// table rows / item nya
		// dan ini headernya, tiap page ada headernya
		echo '<table style="width: 100%;" class="rows">';
		echo '<thead>';
		echo '	<td >No</td>';
		echo '	<td >Item Code/Description</td>';
		echo '	<td >Cate</td>';
		echo '	<td >Sex</td>';	
		echo '	<td >Label</td>';
		echo '	<td >Season</td>';
		echo '	<td >Size</td>';
		echo '	<td >Color</td>';
		if($type == 5)
		{
			echo '	<td >Unit RSP</td>';
			echo '	<td >Qty</td>';	
		}
		else if($type == 6)
		{
			echo '	<td >Bef RSP</td>';
			echo '	<td >Aft RSP</td>';
			echo '	<td >Qty</td>';
		}
		echo '</thead>';
		echo '<tbody>';
		foreach ($rows[$page] as $td) {
			echo '<tr>';
			echo $td;
			echo '</tr>';
		}
		//total 
		if(($page+1) == count($rows))
		{
			//echo "<tr><td colspan='9' style='text-align: right;padding-right: 50px;' class='total'>Total Qty</td><td class='total'>".$total."</td></tr>";
		}

		echo '</tbody>';
		echo '</table>';

		if(($page+1) == count($rows))
		{
			echo "<p>Note : ".$header['rows'][0]->remark."</p>";
			echo "<br><br>";

			echo "<table style='width: 100%' class='summary'>";
			echo "<tr>";
			echo "	<td style='width: 25%; text-align:center;'>Received By</td>";
			echo "	<td style='width: 25%; text-align:center;'>Total of Ctns</td>";
			echo "	<td style='width: 25%; text-align:center;'>Date</td>";
			echo "	<td style='width: 25%; text-align:center;'>Delivery By</td>";
			echo "</tr>";
			echo "</table>";
		}

		echo "</section>";
	}
	
	?>


</body>
</html>