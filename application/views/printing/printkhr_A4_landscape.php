<?php
	$_split_row = 22;
	$_repeat_header = true;
	$no = 0;
	$page = 0;
	$rows = array();
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo link_tag('includes/printing/normalize.min.css','stylesheet','text/css');?>
	<?php echo link_tag('includes/printing/paper.css','stylesheet','text/css');?>
	<style>
		@page { size: A4 landscape}
		

		body {
		    counter-reset: chapter;      /* Create a chapter counter scope */
		}
		
		table.summary
		{
			
			font-size: 12px;
		}

		table.header
		{
			border-spacing: 0px;
			border-collapse: separate;
			height: 50mm;
		}

		table.header td
		{
			font-size: 15px;

		}

		table.header td div.title_label
		{
			float: left;
			width: 200px;
			border: 0px solid gray;
			margin-bottom: 2px;
			background: #ffffff;
			font-size: 12px;
		}
		table.header td div.title_label span
		{
			width: 100px;
			display: inline-block;
			background: #efefef;
			padding: 2px
			
		}


		table.rows
		{
			border-spacing: 0px;
			border-collapse: separate;
			page-break-inside:auto;
		}
		table.rows thead td
		{
			font-size: 12px;
			padding: 5px;
			background: #efefef;
		}

		table.rows tbody tr
		{
			
		}

		table.rows tbody td.items
		{
			font-size: 11px;
			border-bottom: 1px solid gray;
			padding: 5px;
			page-break-inside:avoid;
		}

		table.rows tbody td.total
		{
			font-size: 15px;
			padding: 5px;
			page-break-inside:avoid;
		}
	</style>
</head>
<body class="A4 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

	<?php
	foreach ($data['rows'] as $row) {
		$no++;
		if(($no % $_split_row) == 0) $page++;

		$item = "<td class='items'>".$row['barcode']."</td>";
		$item .= "<td class='items'>".$row['size']."</td>";
		$item .= "<td class='items'>".$row['qty']."</td>";
		$item .= "<td class='items' style='text-align: right;'>".$row['unitcost']."</td>";
		$item .= "<td class='items' style='text-align: right;'>".$row['unitprice']."</td>";
		$item .= "<td class='items' style='text-align: right;'>".$row['biaya']."</td>";
		$item .= "<td class='items' style='text-align: right;'>".$row['cogs']."</td>";
		$item .= "<td class='items'>".$row['markup']."</td>";
		$item .= "<td class='items' style='text-align: right;'>".$row['retail_price']."</td>";
		$item .= "<td class='items'>".$row['margin']."</td>";
		$item .= "<td class='items' style='text-align: right;'>".$row['sugested_retail_price']."</td>";
		$item .= "<td class='items' style='text-align: right;'>".$row['retailprice_adjustment']."</td>";
		$item .= "<td class='items'>".$row['marginafteradjust']."</td>";
		$item .= "<td class='items'>".$row['varafteradjust']."</td>";
		$item .= "<td class='items' style='text-align: right;'>".$row['retail_price_roundup']."</td>";
		$item .= "<td class='items' style='text-align: right;'>".$row['total_retail_price']."</td>";
		$rows[$page][] = $item;
	}


	function _setHeaderTable($header, $page, $pageoff)
	{
		$headers = '<table style="width: 100%; " class="header">';
		$headers .= '<tr>';
		$headers .= '	<td valign="top" style="width: 40%">';
		$headers .= '		<p>PT KELAB 21 RETAIL</p>';
		$headers .= '		<small>DIPO BUSINESS CENTER 9th Floor, Unit F-G<br>';
		$headers .= '		JL Gatot Subroto Kav 51-52 10260 Jakarta<br>';	
		$headers .= '		NPWP : <br></small>';	
		$headers .= '	</td>';
		$headers .= '	<td valign="top" >';
		$headers .= '		<p>KALKULASI HARGA RETAIL</p>';
		$headers .= '		<div class="title_label"><span>Costing Reff</span> '.$header['rows'][0]['costingnumber'].'</div>';
		$headers .= '		<div class="title_label"><span>Costing Date</span> '.$header['rows'][0]['costingdate'].'</div>';
		$headers .= '		<div class="title_label"><span>PIB</span> '.$header['rows'][0]['pibnumber'].'</div>';
		$headers .= '		<div class="title_label"><span>AWB</span> '.$header['rows'][0]['awbnumber'].'</div>';
		$headers .= '	</td>';
		$headers .= '	<td valign="top" >';
		$headers .= '		<p>&nbsp;</p>';
		$headers .= '		<div class="title_label"><span>Curr/rate</span> '.$header['rows'][0]['currency'].'/'.$header['rows'][0]['exchangerate'].'</div>';
		$headers .= '		<div class="title_label"><span>IMP Tax</span> '.$header['rows'][0]['importduty'].'</div>';
		$headers .= '		<div class="title_label"><span>LUX Tax</span> '.$header['rows'][0]['luxurytax'].'</div>';
		$headers .= '		<div class="title_label"><span>Freight</span> '.$header['rows'][0]['freight'].'</div>';
		$headers .= '		<div class="title_label"><span>Handling</span> '.$header['rows'][0]['handling'].'</div>';
		$headers .= '		<div class="title_label"><span>Other</span> '.$header['rows'][0]['other'].'</div>';
		$headers .= '		<div class="title_label"><span>Total</span> '.$header['rows'][0]['total'].'</div>';
		$headers .= '	</td>';
		$headers .= '</tr>';
		$headers .= '</table>';
		return $headers;
	}


	for($page=0; $page < count($rows); $page++)
	{
		echo "<section class='sheet padding-10mm'>";
		if($_repeat_header) echo _setHeaderTable($header, ($page+1), count($rows));
		else
			if($page == 0) echo _setHeaderTable($header, ($page+1), count($rows));
		

		// table rows / item nya
		// dan ini headernya, tiap page ada headernya
		echo '<table style="width: 100%;" class="rows">';
		echo '<thead>';
		echo '	<td >Item Code</td>';
		echo '	<td >Size</td>';
		echo '	<td >Qty</td>';
		echo '	<td >FOB</td>';	
		echo '	<td >U Price</td>';
		echo '	<td >L Cost</td>';
		echo '	<td >COGS</td>';
		echo '	<td >Markup</td>';
		echo '	<td >R Unit Price</td>';
		echo '	<td >Margin</td>';
		echo '	<td >Sug R PRice</td>';
		echo '	<td >New Price</td>';
		echo '	<td >Margin</td>';
		echo '	<td >Var</td>';
		echo '	<td >Retail RU Price</td>';
		echo '	<td >Total Retail Price</td>';
		echo '</thead>';
		echo '<tbody>';
		foreach ($rows[$page] as $td) {
			echo '<tr>';
			echo $td;
			echo '</tr>';
		}
		//total 
		if(($page+1) == count($rows))
		{
			//echo "<tr><td colspan='15' style='text-align: right;padding-right: 50px;' class='total'>Total Qty</td><td class='total'>0</td></tr>";
		}

		echo '</tbody>';
		echo '</table>';

		if(($page+1) == count($rows))
		{
			//echo "<p>Note : ".$header['rows'][0]->remark."</p>";
			echo "<br><br>";

			echo "<table style='width: 100%' class='summary'>";
			echo "<tr>";
			echo "	<td style='width: 25%; text-align:center;'>Received By</td>";
			echo "	<td style='width: 25%; text-align:center;'>Total of Ctns</td>";
			echo "	<td style='width: 25%; text-align:center;'>Date</td>";
			echo "	<td style='width: 25%; text-align:center;'>Delivery By</td>";
			echo "</tr>";
			echo "</table>";
		}

		echo "</section>";
	}
	?>
  
</body>
</html>