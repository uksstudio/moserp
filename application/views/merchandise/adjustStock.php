<table id="dgcp" style="width: 100%"></table>

<div id="toolbar">
    <?php echo form_open();?>
    <table style="width: 100%; border: 0px solid black;">
        <tr>
            <td >
                Adjust Reff
            </td>
            <td>
                <input name="reff" id="reff" class="f1 easyui-textbox" required="true" style="width: 200px" value="<?php echo $this->Public_model->getAdjustmentReff()?>" readonly></input>
            </td>
            <td >
                Location
            </td>
            <td>
                <input name="location" id="location" class="f1 easyui-combogrid" style="width: 200px"></input>
            </td>
            <td style="text-align: right;" rowspan="3">
                <a id="btnSave" class="easyui-linkbutton"  data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'">Save</a>
                <a id="btnReset" class="easyui-linkbutton"  data-options="iconCls:'icon-large-remove',size:'large',iconAlign:'top'">Reset</a>
            </td>
        </tr>
        <tr>
            <td>
                Date
            </td>
            <td>
                <input name="datetime" id="datetime" class="f1 easyui-textbox" style="width: 200px" value="<?php echo date("Y-m-d H:i:s")?>" readonly></input>
            </td>
            <td>
                
            </td>
            <td style="width:50%">
                
            </td>
        </tr>
        <tr>
            <td>
                Remark
            </td>
            <td colspan="4">
                <input name="narrative" id="narrative" class="f1 easyui-textbox" style="width: 400px"></input>    
            </td>
        </tr>
    </table>
    <?php echo form_close();?>
    <br>
    <a href="#" id="btnAddline" class="easyui-linkbutton" data-options="iconCls:'icon-add'" >Add Line</a>
    <a href="#" id="btnRemoveline" class="easyui-linkbutton" data-options="iconCls:'icon-remove'" >Remove Line</a>
</div>

<script type="text/javascript">
    
    var editIndex = undefined;
    
    $(function(){
        var csrf = '<?php echo $this->security->get_csrf_hash();?>';
        var Clickc = [];
        var dg = $('#dgcp').datagrid({
            title:'Stock Adjustment',
            singleSelect:true,
            idField:'productcode',
            fit: true,
            url:"merchandise/getStockAdjustment",
            method: 'get',
    		remoteFilter: true,
    		toolbar:'#toolbar',
    		rownumbers: true,
            dblclickToEdit: false,
            columns:[[
                {field:'itemcode',title:'Item Code',width:180, editor:{type:'textbox', options:{required: true}}, styler:
                    function (val, row)
                    {
                        return 'background-color:#e86b97;color:#ffffff;font-style:bold;';
                    }
                },
                {field:'productdesc',title:'Description',width:250},
                {field:'qty',title:'Avail QTY',width:100},
                {field:'adjqty',title:'QTY',width:100, editor:{type:'numberbox', options:{required:true}}, styler:
                    function (val, row)
                    {
                        return 'background-color:#e86b97;color:#ffffff;font-style:bold;';
                    }
                },
                {field:'type',title:'Type',width:100, styler:
                    function(val, row)
                    {
                        return 'background-color:#e86b97;color:#ffffff;font-style:bold;';
                    }
                    , editor:{type:'combogrid',
                        options:{
                            panelWidth:300,
                            url: "merchandise/getAdjustmentType",
                            idField:'adjustmenttypecode',
                            textField:'adjustmenttypedesc',
                            mode:'remote',
                            fitColumns:true,
                            required: true,
                            method: 'get',
                            columns:[[
                                {field:'adjustmenttypecode',title:'CODE',width:20},
                                {field:'adjustmenttypedesc',title:'Adjustment Code',width:200}
                            ]],
                            onChange:function(value){
                                
                            },
                            onSelect: function(index,row){
                                var codigo = row.adjustmenttypedesc;
                            }
                        }}
                },
                {field:'reason',title:'Reason',width:300, editor:{type:'textbox'}},
                {field:'action',title:'Action',width:80,align:'center',
                    formatter:function(value,row,index){
                        if (row.editing){
                            var s = '<a href="javascript:void(0)" onclick="saverow(this)">Save</a> ';
                            var c = '<a href="javascript:void(0)" onclick="cancelrow(this)">Cancel</a>';
                            return s+c;
                        } else {
                            var e = '<a href="javascript:void(0)" onclick="editrow(this)">Edit</a> ';
                            var d = '<a href="javascript:void(0)" onclick="deleterow(this)">Delete</a>';
                            return e+d;
                        }
                    }
                }
            ]],
            onLoadSuccess: function (data)
            {
                console.log(data);
                csrf = data.csrf_name;
                
            },
            onLoadError: function()
            {

            },
            onEndEdit:function(index,row){
                var dg = $(this);
                var ed = $(this).datagrid('getEditor', {
                    index: index,
                    field: 'itemcode'
                });
                //row.productname = $(ed.target).textbox('getText');
                console.log($(ed.target).textbox('getText'));
                $(this).datagrid('refreshRow', index);
            },
            onBeforeEdit:function(index,row){
                
                editIndex = index;
                row.editing = true;
                $(this).datagrid('refreshRow', index);
            },
            onAfterEdit:function(index,row){
                row.editing = false;
                $(this).datagrid('refreshRow', index);
            },
            onCancelEdit:function(index,row){
                row.editing = false;
                $(this).datagrid('refreshRow', index);
            }
        });


        $.extend($.fn.textbox.defaults.rules, {
            maxLength: { 
                validator: function(value, param){
                    //console.log(value.length);
                    return value.length <= param[0];
                },
                message: 'Maximum characters allowed only {0}'
            }
        });


        $('#location').combogrid({
            panelWidth:300,
            url: "posting/getLocation",
            idField:'locationcode',
            textField:'locationcode',
            mode:'remote',
            fitColumns:true,
            method: 'get',
            required: true,
            columns:[[
                {field:'locationid',title:'ID',width:20},
                {field:'locationcode',title:'Location Code',width:30},
                {field:'locationname',title:'Location Name',width:100}
            ]],
            onChange:function(value){
                var g = $('#location').combogrid('grid');
                g.datagrid('load',{assay_type_id:value});
                console.log("find id : "+value);
            },
            onSelect: function(index,row){
                var codigo = row.labelcode;
                console.log(codigo);
            }
        });



    });
    

    function getRowIndex(target){
        var tr = $(target).closest('tr.datagrid-row');
        return parseInt(tr.attr('datagrid-row-index'));
    }
    function editrow(target){
        $('#dgcp').datagrid('beginEdit', getRowIndex(target));
    }
    function deleterow(target){
        $.messager.confirm('Confirm','Are you sure?',function(r){
            if (r){
                $('#dgcp').datagrid('deleteRow', getRowIndex(target));
                $.get( "merchandise/removeLineStockAdjustment/"+row.rowid, {}, "json")
                .done(
                    function(msg)
                    {
                        console.log(msg);
                        var obj = jQuery.parseJSON( msg );
                        
                        if(obj.status)
                        {
                            $('#dgcp').datagrid('endEdit', getRowIndex(target));
                        }
                        else
                        {
                            $.messager.alert('Failed',"Error ("+obj.msg+")",'error');
                        }
                    }
                 )
                .fail(function(xhr, status, error) {
                    // error handling
                    console.log(xhr.status);
                    console.log(error);
                    console.log(status);
                    $.messager.alert('Failed',xhr.status + "("+error+")",'error');
                }); 
            }
        });
    }

    function saverow(target){
        
        var csrf = $("input[name*='csrf_name']").val();
        
        var index = getRowIndex(target);
        var edb = $('#dgcp').datagrid('getEditor', {index: index,field: 'itemcode'});
        var edq = $('#dgcp').datagrid('getEditor', {index: index,field: 'adjqty'});
        var edr = $('#dgcp').datagrid('getEditor', {index: index,field: 'reason'});
        var edt = $('#dgcp').datagrid('getEditor', {index: index,field: 'type'});
        var barcode = $(edb.target).textbox('getText');
        var ajqty = $(edq.target).numberbox('getText');
        var reason = $(edr.target).textbox('getText');
        var type = $(edt.target).combogrid('getText');
        var typecode = $(edt.target).combogrid('getValue');
        var row = $('#dgcp').datagrid('getRows')[index];
        console.log(row);
        
        $.post( "merchandise/saveLineStockAdjustment", {csrf_name:csrf, rowid: row.rowid, productcode: barcode, qty: ajqty, reason: reason,type:type, typecode:typecode }, "json")
        .done(
            function(msg)
            {
                console.log(msg);
                var obj = jQuery.parseJSON( msg );
                $("input[name*='csrf_name']").val(obj.csrf_name);
                if(obj.status)
                {
                    $('#dgcp').datagrid('endEdit', getRowIndex(target));
                }
                else
                {
                    $.messager.alert('Failed',"Error ("+obj.msg+")",'error');
                }
            }
         )
        .fail(function(xhr, status, error) {
            // error handling
            console.log(xhr.status);
            console.log(error);
            console.log(status);
            $.messager.alert('Failed',xhr.status + "("+error+")",'error');
        }); 
        
        //$('#dgcp').datagrid('endEdit', getRowIndex(target));
    }
    function cancelrow(target){
        $('#dgcp').datagrid('cancelEdit', getRowIndex(target));
    }

    

    
    $('#btnReset').on('click', function()
    {
        openUrl("<?php echo base_url()?>index.php/merchandise/adjustStock/refreshpo");
    });


    $('#btnAddline').on('click', function()
    {
        $.get( "merchandise/addLineStockAdjustment", {csrf_name:csrf}, "json")
        .done(
            function(msg)
            {
                console.log(msg);
                var obj = jQuery.parseJSON( msg );
                csrf = obj.csrf_name;
                $('#dgcp').datagrid('reload');
            }
         )
        .fail(function(xhr, status, error) {
            // error handling
            console.log(xhr.status);
            console.log(error);
            console.log(status);
            $.messager.alert('Failed',xhr.status + "("+error+")",'error');
        }); 
    });
    
    $('#btnSave').on('click', function()
    {
        var csrf = $("input[name*='csrf_name']").val();
        var location = $('#location').combogrid('getText');
        var narrative = $('#narrative').textbox('getText');
        var rows = $('#dgcp').datagrid('getRows');

        if(rows.length > 0)
        {
            $.post( "merchandise/commitAdjustment", 
            {
                csrf_name:csrf,
                locationcode: location,
                narrative:narrative
            }, "json")
            .done(
                function(msg)
                {
                    console.log(msg);
                    var obj = jQuery.parseJSON( msg );
                    csrf = obj.csrf_name;
                    if(obj.status == 1)
                    {
                        var dlg = $.messager.confirm({
                            title: 'Successfully',
                            msg: 'Successfully your reff is '+obj.reff,
                            buttons:[{
                                text: 'OK',
                                onClick: function(){
                                    dlg.dialog('destroy')
                                    openUrl("<?php echo base_url()?>index.php/merchandise/adjustStock/refreshpo");
                                }
                            }]
                        });
                    }
                    else{
                        $.messager.alert('Failed',obj.msg,'error');
                    }
                }
             )
            .fail(function(xhr, status, error) {
                // error handling
                console.log(xhr.status);
                console.log(error);
                console.log(status);
                $.messager.alert('Failed',xhr.status + "("+error+")",'error');
            }); 
        }
        else
        {
            $.messager.alert('Failed',"Please enter Productcode",'error');
        }

          
    });
</script>