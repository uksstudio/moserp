<table id="dgcp" style="width: 100%"></table>

<div id="toolbar">
    <table style="width: 100%; border: 0px solid black;">
        <tr>
            <td>
                Location  &nbsp;<input class="easyui-combogrid" name="location" id="location" style="width:20%">
            </td>
            <td style="text-align: right;" rowspan="3">
                <a id="btnSave" class="easyui-linkbutton"  data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'">Save</a>
                <a id="btnRemove" class="easyui-linkbutton" data-options="iconCls:'icon-large-remove',size:'large',iconAlign:'top'">Remove</a>
            </td>
        </tr>
        <tr>
            <td style="width:50%">
                Itemcode <input name="colrow" id="colrow" class="f1 easyui-textbox" required="true" style="width: 200px"></input> Enter
                | <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-undo',plain:true" onclick="reject()">Reject Item</a>    
            </td>
        </tr>
        <tr>
            <td style="width:50%">
                Narrative  <input name="narrative" id="narrative" class="f1 easyui-textbox" style="width: 400px"></input>    
            </td>
        </tr>
    </table>

    
</div>

<?php echo script_tag('includes/plugins/datagrid-cellediting.js');?>
<script type="text/javascript">
    var csrf = '<?php echo $this->security->get_csrf_hash();?>';
    var editIndex = undefined;
    
    $(function(){
        var Clickc = [];
        var dg = $('#dgcp').datagrid({
            title:'Change Price Form',
            singleSelect:true,
            idField:'itemcode',
            url:'merchandise/getItems',
    		method: 'get',
    		fit: true,
    		remoteFilter: true,
    		toolbar:'#toolbar',
    		rownumbers: true,
            clickToEdit: true,
            dblclickToEdit: false,
            columns:[[
                {field:'itemcode',title:'Item Code',width:180},
                {field:'productdesc',title:'Description',width:250},
                {field:'qty',title:'QTY',width:100},
                {field:'retailprice',title:'Old Price',width:150},
                {field:'newprice',title:'New Price',width:150, styler:
                    function(val, row, index)
                    {
                        return 'background-color:#e86b97;color:#ffffff;font-style:bold;';
                    },
                    editor:{type:'numberbox',options:{precision:2}}
                },
                {field:'varian',title:'Varian',width:150}
            ]],
            onEndEdit:function(index,row){
                var dg = $(this);
                //console.log(index +" == "+ Clickc.field );
                
            },
            onBeforeEdit:function(index,row){
                
                editIndex = index;
                $(this).datagrid('refreshRow', index);
                console.log('onBeforeEdit');
            },
            onAfterEdit:function(index,row){
                $(this).datagrid('refreshRow', index);
                console.log('onAfterEdit ' + row.newprice);

                var ed = dg.datagrid('getEditor', {index:index,field:Clickc.field});
                var row = dg.datagrid('getRows')[index];
                if(row.newprice != row.retailprice)
                {
                    
                    var varian = ((row.retailprice - row.newprice) / row.newprice) * 100;
                    console.log("varian " + varian);
                    row.varian = number_format(varian, 2, '.', ',');
                    $(this).datagrid('refreshRow', index);
                    
                    $.post('merchandise/adjustNewpricesession',{csrf_name:csrf, rowid:row.rowid,newprice:row.newprice},function(result){
                        if (result.status){
                            csrf = result.csrf_name;
                            
                        } else {
                            $.messager.show({    // show error message
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        }
                    },'json');
                    
                }
            },
            onCancelEdit:function(index,row){
                $(this).datagrid('refreshRow', index);
                console.log('onCancelEdit');
            },
            onCellEdit: function(index,field){
                var dg = $(this);
                var input = dg.datagrid('input', {index:index,field:field});
                var ed = dg.datagrid('getEditor', {index: index,field: field});
                if (ed){
                    Clickc.index = index; // get the row that was clicked
                    Clickc.field = field; // get the field which was clicked
                    Clickc.value = $(ed.target).val();  //Get cell current value
                }
                input.bind('keydown', function(e){
                    if (e.keyCode == 38 && index>0){    // up
                        dg.datagrid('endEdit', index);
                        dg.datagrid('editCell', {
                            index: index-1,
                            field: field
                        });
                    } else if (e.keyCode == 40 && index<dg.datagrid('getRows').length-1){   // down
                        dg.datagrid('endEdit', index);
                        dg.datagrid('editCell', {
                            index: index+1,
                            field: field
                        });
                    }
                    else if (e.keyCode == 13){
                        

                        dg.datagrid('endEdit', index);
                        dg.datagrid('gotoCell', {index:0,field:'productitemid'});
                        //dg.datagrid('gotoCell', 'right');
                        //dg.datagrid('editCell',dg.datagrid('cell'));

                        return false;
                    }
                })
            }
        });

        dg.datagrid('enableCellEditing');

        $.extend($.fn.textbox.defaults.rules, {
            maxLength: { 
                validator: function(value, param){
                    //console.log(value.length);
                    return value.length <= param[0];
                },
                message: 'Maximum characters allowed only {0}'
            }
        });


        $('#location').combogrid({
            panelWidth:300,
            url: "posting/getLocation",
            idField:'locationid',
            textField:'locationcode',
            mode:'remote',
            fitColumns:true,
            method: 'get',
            required: true,
            columns:[[
                {field:'locationid',title:'ID',width:20},
                {field:'locationcode',title:'Location Code',width:30},
                {field:'locationname',title:'Location Name',width:100}
            ]],
            onChange:function(value){
                //var g = $('#location').combogrid('grid');
                //g.datagrid('load',{locationcode:value});
                console.log("find id : "+value);
            },
            onSelect: function(index,row){
                var codigo = row.locationcode;
                console.log(codigo);
            }
        });


    });

    $('#location').combogrid('setValue', 'WH');

    
    function onClickCell(index, field){
        if (editIndex != index){
            if (endEditing()){
                $('#dgcp').datagrid('selectRow', index).datagrid('beginEdit', index);
                var ed = $('#dgcp').datagrid('getEditor', {index:index,field:field});
                if (ed){
                    ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                }
                editIndex = index;
            } else {
                setTimeout(function(){
                    $('#dgcp').datagrid('selectRow', editIndex);
                },0);
            }
        }
    }

    function endEditing(){
        if (editIndex == undefined){return true}
        if ($('#dgcp').datagrid('validateRow', editIndex)){
            $('#dgcp').datagrid('endEdit', editIndex);
            editIndex = undefined;
            return true;
        } else {
            return false;
        }
    }

    $('#colrow').textbox({
        inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
            keyup:function(e){
                if(e.keyCode == 13)
                {
                    var itemcode = $('#colrow').textbox('getText');
                    var locationCode = $('#location').combogrid('getText');
                    if(itemcode != '') 
                    {
                        $.post( "merchandise/addDraftItem", 
                        {
                            csrf_name:csrf, 
                            productcode:itemcode,
                            location:locationCode
                        }, "json")
                        .done(
                            function(msg)
                            {
                                console.log(msg);
                                var obj = jQuery.parseJSON( msg );
                                csrf = obj.csrf_name;
                                if(obj.status == 1)
                                {
                                    $('#dgcp').datagrid('reload');
                                }
                                else{
                                    $.messager.alert('Failed',obj.msgEr,'error');
                                }
                            }
                         )
                        .fail(function(xhr, status, error) {
                            // error handling
                            console.log(xhr.status);
                            console.log(error);
                            console.log(status);
                            $.messager.alert('Failed',xhr.status + "("+error+")",'error');
                        });
                    }
                    else
                    {
                        $.messager.alert('Failed','Please fill all field required.','error');
                    }
                }
            }
        })

        
    });

    

    $('#btnRemove').on('click', function()
    {
        openUrl("<?php echo base_url()?>index.php/merchandise/changePrice/refreshpo");
    });

    $('#btnSave').on('click', function()
    {
     
        var rows = $("#dgcp").datagrid('getRows');
        var locationCode = $('#location').combogrid('getText');
        var locationId = $('#location').combogrid('getValue');
        if(rows.length > 0)
        {
            $.post( "merchandise/commitChangePrice", 
            {
                csrf_name:csrf,
                narrative: $('#narrative').textbox('getText'),
                location:locationCode,
                locationid:locationId
            }, "json")
            .done(
                function(msg)
                {
                    console.log(msg);
                    var obj = jQuery.parseJSON( msg );
                    csrf = obj.csrf_name;
                    if(obj.status == 1)
                    {
                        var dlg = $.messager.confirm({
                            title: 'Successfully',
                            msg: 'Successfully you Reff  is '+obj.reff,
                            buttons:[{
                                text: 'OK',
                                onClick: function(){
                                    dlg.dialog('destroy')
                                    openUrl("<?php echo base_url()?>index.php/merchandise/changePrice/refreshpo");
                                }
                            }]
                        });
                    }
                    else{
                        $.messager.alert('Failed',obj.msg,'error');
                    }
                }
             )
            .fail(function(xhr, status, error) {
                // error handling
                console.log(xhr.status);
                console.log(error);
                console.log(status);
                $.messager.alert('Failed',xhr.status + "("+error+")",'error');
            }); 

        }
        else
        {
            $.messager.alert('Failed',"Please enter Productcode",'error');
        }

          
    });
</script>