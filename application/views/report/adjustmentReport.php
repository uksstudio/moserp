<table id="dgreprintdocn" class="easyui-datagrid">
	<thead>
		<tr>
			<th data-options="field:'stockadjustmentnumber',width:80">Number</th>
			<th data-options="field:'adjustmenttype',width:120,formatter:formatterType">Type</th>
			<th data-options="field:'raisedate',width:150">Date</th>
			<th data-options="field:'locationcode',width:80">Location</th>
			<th data-options="field:'remark',width:200">Remark</th>
			<th data-options="field:'action',width:80,align:'center',formatter:formatterPrint">Action</th>
		</tr>
	</thead>
</table>


<div id="toolbar">
	<table style="width: 100%; border: 0px solid black;">
		<tr>
			<td style="width:180px">
				<input class="easyui-textbox" name="carton_num" id="carton_num" label="Doc Number:" labelPosition="top" required="true" style="width:130px"> enter
				
			</td>
			
		</tr>
	</table>
    
    
</div>

<?php echo script_tag('includes/plugins/datagrid-filter.js');?>
<?php echo script_tag('includes/plugins/jquery.printPage.js');?>

<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	
	var cosnumber = '';
	$(function () {
		var dg = $('#dgreprintdocn').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			toolbar:'#toolbar',
			idField:'po_number',
			fit: true,
			rownumbers:true,
			remoteFilter: true,
			url:'report/readadjustmentreport',
			method: 'get',
			showFooter: true,
			pagination: true
		});

		dg.datagrid('enableFilter');
	});

	

	$('#carton_num').textbox({
		inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
			keyup:function(e){
				if(e.keyCode == 13)
				{
					$('#dgreprintdocn').datagrid({
						queryParams:{
							trfdono:$('#carton_num').textbox('getValue')
						}
					});
				}
			}
		})
	})

	
	function printing(trfno)
	{
		loadPrintDocument(this, "report/reprintadjustment_A4/"+trfno);
	}

	function formatterPrint(value,row,index)
	{
		var costingnumber = "'"+row.stockadjustmentnumber+"'";
		var e = '<a href="javascript:void(0)" class="easyui-linkbutton" onclick="printing('+costingnumber+')">Print</a> ';
		return e;
	}

	function formatterType(value,row,index)
	{
		if(value == 5)
			return 'ADJUST STOCK';
		else if(value == 6)
			return 'CHANGE PRICE';
		else
			return 'OTHER';
	}
</script>