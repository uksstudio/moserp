<table id="dgreprintdocn" class="easyui-datagrid"></table>


<div id="toolbar">
	<table style="width: 100%; border: 0px solid black;">
		<tr>
			<td style="width:180px">
				<input class="easyui-textbox" name="carton_num" id="carton_num" label="CO Number:" labelPosition="top" required="true" style="width:130px"> press enter
				
			</td>
			
		</tr>
	</table>
    
    
</div>

<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	
	var cosnumber = '';
	$(function () {
		$('#dgreprintdocn').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			toolbar:'#toolbar',
			idField:'po_number',
			fit: true,
			rownumbers:true,
			url:'report/getPostingList',
			method: 'get',
			showFooter: false,
			columns:[[
				{field:'costingnumber',title:'Number',width:80},
				{field:'costingstatus',title:'Status',width:80},
				{field:'costingdate',title:'Costing Date',width:70},
				{field:'pibnumber',title:'PIB Number',width:70},
				{field:'awbnumber',title:'AWB',width:70},
				{field:'invoicenumber',title:'Invoice',width:70},
				{field:'spcpdate',title:'SPCS Date',width:70},
				{field:'currency',title:'Curr',width:50},
				{field:'action',title:'Action',width:110,align:'center',
					formatter:function(value,row,index){
						var costingnumber = "'"+row.costingnumber+"'";
						if(row.costingstatus == 'COSTING')
						{
							var e = '<a href="javascript:void(0)" id="btnDetail'+row.costingnumber+'" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="printing('+costingnumber+')">PRINT</a> ';
							
							return e;
						}
						else if(row.costingstatus == 'VOID')
						{
							return '';
						}
						else
						{
							return '';
						}
					}
				}
			]],
			rowStyler: function(index,row){
                if (row.costingstatus == 'VOID'){
                    return 'background-color:#6293BB;color:#fff;font-weight:bold;';
                }
            },
			detailFormatter:function(index,row){
				return '<div style="padding:2px;position:relative;"><table class="ddv"></table></div>';
			}
		});
	});

	$('#carton_num').textbox({
		inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
			keyup:function(e){
				var number = $('#carton_num').textbox('getValue');
				if(e.keyCode == 13)
				{
					$('#dgreprintdocn').datagrid({
						url:'report/getPostingList/'+number
					});
				}
			}
		})
	})

	
	function printing(cosnumber)
	{

		loadPrintDocument(this, "Costing/printngkhranother/"+cosnumber);
	}
</script>