<table id="dgreprintdocn" class="easyui-datagrid">
	<thead>
		<tr>
			<th data-options="field:'transfernotenumber',width:80">Number</th>
			<th data-options="field:'createdatetime',width:150">Date</th>
			<th data-options="field:'flocation',width:80">From</th>
			<th data-options="field:'tlocation',width:80">TO</th>
			<th data-options="field:'raiseby',width:80">Raise By</th>
			<th data-options="field:'transfernotestatus',width:80,align:'center',formatter:formatterStatus">Status</th>
			<th data-options="field:'action',width:80,align:'center',formatter:formatterPrint">Action</th>
		</tr>
	</thead>
</table>


<div id="toolbar">
	<table style="width: 100%; border: 0px solid black;">
		<tr>
			<td style="width:180px">
				<input class="easyui-textbox" name="carton_num" id="carton_num" label="IOT Number:" labelPosition="top" required="true" style="width:130px"> enter
				
			</td>
			
		</tr>
	</table>
    
    
</div>

<?php echo script_tag('includes/plugins/datagrid-filter.js');?>
<?php echo script_tag('includes/plugins/jquery.printPage.js');?>

<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	
	var cosnumber = '';
	$(function () {
		var dg = $('#dgreprintdocn').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			toolbar:'#toolbar',
			idField:'po_number',
			fit: true,
			rownumbers:true,
			remoteFilter: true,
			url:'report/readreprintdncn',
			method: 'get',
			showFooter: true,
			pagination: true,
			rowStyler: function(index,row){
                if (row.costingstatus == 'VOID'){
                    return 'background-color:#6293BB;color:#fff;font-weight:bold;';
                }
            }
		});

		dg.datagrid('enableFilter');
	});

	

	$('#carton_num').textbox({
		inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
			keyup:function(e){
				if(e.keyCode == 13)
				{
					$('#dgreprintdocn').datagrid({
						queryParams:{
							trfdono:$('#carton_num').textbox('getValue')
						}
					});
				}
			}
		})
	})

	$('#cn_num').textbox({
		inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
			keyup:function(e){
				if(e.keyCode == 13)
				{
					$('#dgreprintdocn').datagrid({
						queryParams:{
							trfcnno:$('#cn_num').textbox('getValue')
						}
					});
				}
			}
		})
	})
	function printing(trfno)
	{
		loadPrintDocument(this, "report/reprintdncn_A4/"+trfno);
	}

	function formatterPrint(value,row,index)
	{
		var costingnumber = "'"+row.transfernotenumber+"'";
		var e = '<a href="javascript:void(0)" class="easyui-linkbutton" onclick="printing('+costingnumber+')">Print</a> ';
		return e;
	}

	function formatterStatus(value,row,index)
	{
		if(value == 2)
			return 'RECEIVED';
		else if(value == 1)
			return 'OPEN';
		else if(value == 3)
			return 'VOID';
	}
</script>