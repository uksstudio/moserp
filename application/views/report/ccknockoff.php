<table id="dg" class="easyui-datagrid"></table>

<div id="tb" style="padding:2px 5px;">
	<?php echo form_open(); ?>
	
	<table width="100%">
		<tr>
			<td style="width:5%;">
				<div >
					<div style="margin-bottom:5px">
			            <input class="easyui-combobox" id="locationcode" name="locationcode" style="width:100%" data-options="label:'Store Location:',required:true,labelPosition:'top'">
			        </div>
				</div>
			</td>
			
			<td style="width:20%;" valign="bottom">
				<div style="margin-bottom:5px">
		            <a href="#" id="search" class="easyui-linkbutton c1" style="width:120px">SUBMIT</a>
		        </div>
			</td>
			<td >
				
			</td>
		</tr>
	</table>
	
	<?php echo form_close(); ?>
</div>

<?php echo script_tag('includes/datagrid-groupview.js');?>
<?php echo script_tag('includes/plugins/jquery.printPage.js');?>
<script type="text/javascript">
	var strTanggal = "";
	var csrf = '<?php echo $this->security->get_csrf_hash();?>';
	var editIndex = undefined;
	var type = "";
	
	$(function () {
		
		$('#dg').datagrid({
			width:'100%',
			height:'100%',
			singleSelect:true,
			fit: true,
			title:'Credit Card Knock Off',
			rownumbers:true,
			toolbar:'#tb',
			showFooter: true,
            columns:[[
				{field:'transactiontype',title:'Type',width:150},
				{field:'invoicedate',title:'Doc Date',width:150},
				{field:'invoicenumber',title:'Doc Number',width:150},
				{field:'creditcardtype',title:'Card Type',width:150},
				{field:'accountnumber',title:'Account Number',width:150},
				{field:'edcmachine',title:'EDC',width:150},
				{field:'amount',title:'Amount',width:100, align:'right', formatter:function(value, row){return number_format(row.amount, 0, '.', ',')}}
			]],
			onLoadSuccess:function(){
			    

			}
		});
		

		$("#locationcode").combobox({			
			selectOnNavigation: false,
			valueField: 'locationid',
			textField: 'locationcode',
			url:'Finance/getLocationCode',
			method: 'get',
			editable: true,
			required: true,
			onLoadSuccess: function () { },
			filter: function (q, row) {
				return row.text.toLowerCase().indexOf(q.toLowerCase()) == 0;
			},
			onSelect: function(record)
			{
				
			}
		});
		
	});

	

    $("#search").on('click', function()
    {
    	var csrf = $("input[name*='csrf_name']").val();
    	var storecode = $('#locationcode').combobox('getText');
    	var locationid = $('#locationcode').combobox('getValue');
    	
    	var dg = $("#dg");
		dg.datagrid({
			url:'Report/showCcKnockOff',
			method:'post',
			queryParams:{
				csrf_name:csrf,
				outletcode:storecode,
				locationid:locationid
			},
			onLoadSuccess: function(row, data)
			{
				console.log(data.csrf_name);
				//$.messager.progress('close');
				$("input[name*='csrf_name']").val(data.csrf_name);
			},
			onLoadError: function(argv)
			{

			}
		});

    	
    });


    function doSearch(value){
        alert('You input: ' + value);
    }
</script>